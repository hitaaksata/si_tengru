<?php
define("EW_PAGE_ID", "view", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_ugd', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_ugdinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_ugd->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_ugd->Export; // Get export parameter, used in header
$sExportFile = $m_ugd->TableVar; // Get export file, used in header
?>
<?php
if (@$_GET["kode_ugd"] <> "") {
	$m_ugd->kode_ugd->setQueryStringValue($_GET["kode_ugd"]);
} else {
	Page_Terminate("m_ugdlist.php"); // Return to list page
}

// Get action
if (@$_POST["a_view"] <> "") {
	$m_ugd->CurrentAction = $_POST["a_view"];
} else {
	$m_ugd->CurrentAction = "I"; // Display form
}
switch ($m_ugd->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // Set no record message
			Page_Terminate("m_ugdlist.php"); // Return to list
		}
}

// Set return url
$m_ugd->setReturnUrl("m_ugdview.php");

// Render row
$m_ugd->RowType = EW_ROWTYPE_VIEW;
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "view"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">View TABLE: m ugd
<br><br>
<a href="m_ugdlist.php">Back to List</a>&nbsp;
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_ugdadd.php">Add</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_ugd->EditUrl() ?>">Edit</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_ugd->CopyUrl() ?>">Copy</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_ugd->DeleteUrl() ?>">Delete</a>&nbsp;
<?php } ?>
</span>
</p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<p>
<form>
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">kode ugd</td>
		<td<?php echo $m_ugd->kode_ugd->CellAttributes() ?>>
<div<?php echo $m_ugd->kode_ugd->ViewAttributes() ?>><?php echo $m_ugd->kode_ugd->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">kode group</td>
		<td<?php echo $m_ugd->kode_group->CellAttributes() ?>>
<div<?php echo $m_ugd->kode_group->ViewAttributes() ?>><?php echo $m_ugd->kode_group->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">group</td>
		<td<?php echo $m_ugd->group->CellAttributes() ?>>
<div<?php echo $m_ugd->group->ViewAttributes() ?>><?php echo $m_ugd->group->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">pemeriksaan ugd</td>
		<td<?php echo $m_ugd->pemeriksaan_ugd->CellAttributes() ?>>
<div<?php echo $m_ugd->pemeriksaan_ugd->ViewAttributes() ?>><?php echo $m_ugd->pemeriksaan_ugd->ViewValue ?></div>
</td>
	</tr>
</table>
</form>
<p>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_ugd;
	$sFilter = $m_ugd->SqlKeyFilter();
	$sFilter = str_replace("@kode_ugd@", ew_AdjustSql($m_ugd->kode_ugd->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_ugd->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_ugd->CurrentFilter = $sFilter;
	$sSql = $m_ugd->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_ugd->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_ugd;
	$m_ugd->kode_ugd->setDbValue($rs->fields('kode_ugd'));
	$m_ugd->kode_group->setDbValue($rs->fields('kode_group'));
	$m_ugd->group->setDbValue($rs->fields('group'));
	$m_ugd->pemeriksaan_ugd->setDbValue($rs->fields('pemeriksaan_ugd'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_ugd;

	// Call Row Rendering event
	$m_ugd->Row_Rendering();

	// Common render codes for all row types
	// kode_ugd

	$m_ugd->kode_ugd->CellCssStyle = "";
	$m_ugd->kode_ugd->CellCssClass = "";

	// kode_group
	$m_ugd->kode_group->CellCssStyle = "";
	$m_ugd->kode_group->CellCssClass = "";

	// group
	$m_ugd->group->CellCssStyle = "";
	$m_ugd->group->CellCssClass = "";

	// pemeriksaan_ugd
	$m_ugd->pemeriksaan_ugd->CellCssStyle = "";
	$m_ugd->pemeriksaan_ugd->CellCssClass = "";
	if ($m_ugd->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode_ugd
		$m_ugd->kode_ugd->ViewValue = $m_ugd->kode_ugd->CurrentValue;
		$m_ugd->kode_ugd->CssStyle = "";
		$m_ugd->kode_ugd->CssClass = "";
		$m_ugd->kode_ugd->ViewCustomAttributes = "";

		// kode_group
		$m_ugd->kode_group->ViewValue = $m_ugd->kode_group->CurrentValue;
		$m_ugd->kode_group->CssStyle = "";
		$m_ugd->kode_group->CssClass = "";
		$m_ugd->kode_group->ViewCustomAttributes = "";

		// group
		$m_ugd->group->ViewValue = $m_ugd->group->CurrentValue;
		$m_ugd->group->CssStyle = "";
		$m_ugd->group->CssClass = "";
		$m_ugd->group->ViewCustomAttributes = "";

		// pemeriksaan_ugd
		$m_ugd->pemeriksaan_ugd->ViewValue = $m_ugd->pemeriksaan_ugd->CurrentValue;
		$m_ugd->pemeriksaan_ugd->CssStyle = "";
		$m_ugd->pemeriksaan_ugd->CssClass = "";
		$m_ugd->pemeriksaan_ugd->ViewCustomAttributes = "";

		// kode_ugd
		$m_ugd->kode_ugd->HrefValue = "";

		// kode_group
		$m_ugd->kode_group->HrefValue = "";

		// group
		$m_ugd->group->HrefValue = "";

		// pemeriksaan_ugd
		$m_ugd->pemeriksaan_ugd->HrefValue = "";
	} elseif ($m_ugd->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_ugd->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_ugd->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_ugd->Row_Rendered();
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_ugd;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_ugd->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_ugd->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_ugd->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_ugd->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_ugd->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_ugd->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_ugd->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
