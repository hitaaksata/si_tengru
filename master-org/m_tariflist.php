<?php
define("EW_PAGE_ID", "list", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_tarif', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_tarifinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_tarif->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_tarif->Export; // Get export parameter, used in header
$sExportFile = $m_tarif->TableVar; // Get export file, used in header
?>
<?php
?>
<?php

// Paging variables
$nStartRec = 0; // Start record index
$nStopRec = 0; // Stop record index
$nTotalRecs = 0; // Total number of records
$nDisplayRecs = 20;
$nRecRange = 10;
$nRecCount = 0; // Record count

// Search filters
$sSrchAdvanced = ""; // Advanced search filter
$sSrchBasic = ""; // Basic search filter
$sSrchWhere = ""; // Search where clause
$sFilter = "";

// Master/Detail
$sDbMasterFilter = ""; // Master filter
$sDbDetailFilter = ""; // Detail filter
$sSqlMaster = ""; // Sql for master record

// Handle reset command
ResetCmd();

// Get basic search criteria
$sSrchBasic = BasicSearchWhere();

// Build search criteria
if ($sSrchAdvanced <> "") {
	if ($sSrchWhere <> "") $sSrchWhere .= " AND ";
	$sSrchWhere .= "(" . $sSrchAdvanced . ")";
}
if ($sSrchBasic <> "") {
	if ($sSrchWhere <> "") $sSrchWhere .= " AND ";
	$sSrchWhere .= "(" . $sSrchBasic . ")";
}

// Save search criteria
if ($sSrchWhere <> "") {
	if ($sSrchBasic == "") ResetBasicSearchParms();
	$m_tarif->setSearchWhere($sSrchWhere); // Save to Session
	$nStartRec = 1; // Reset start record counter
	$m_tarif->setStartRecordNumber($nStartRec);
} else {
	RestoreSearchParms();
}

// Build filter
$sFilter = "";
if ($sDbDetailFilter <> "") {
	if ($sFilter <> "") $sFilter .= " AND ";
	$sFilter .= "(" . $sDbDetailFilter . ")";
}
if ($sSrchWhere <> "") {
	if ($sFilter <> "") $sFilter .= " AND ";
	$sFilter .= "(" . $sSrchWhere . ")";
}

// Set up filter in Session
$m_tarif->setSessionWhere($sFilter);
$m_tarif->CurrentFilter = "";

// Set Up Sorting Order
SetUpSortOrder();

// Set Return Url
$m_tarif->setReturnUrl("m_tariflist.php");
?>
<?php include "header.php" ?>
<?php if ($m_tarif->Export == "") { ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "list"; // Page id

//-->
</script>
<script type="text/javascript">
<!--
var firstrowoffset = 1; // First data row start at
var lastrowoffset = 0; // Last data row end at
var EW_LIST_TABLE_NAME = 'ewlistmain'; // Table name for list page
var rowclass = 'ewTableRow'; // Row class
var rowaltclass = 'ewTableAltRow'; // Row alternate class
var rowmoverclass = 'ewTableHighlightRow'; // Row mouse over class
var rowselectedclass = 'ewTableSelectRow'; // Row selected class
var roweditclass = 'ewTableEditRow'; // Row edit class

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<?php } ?>
<?php if ($m_tarif->Export == "") { ?>
<?php } ?>
<?php

// Load recordset
$bExportAll = (defined("EW_EXPORT_ALL") && $m_tarif->Export <> "");
$bSelectLimit = ($m_tarif->Export == "" && $m_tarif->SelectLimit);
if (!$bSelectLimit) $rs = LoadRecordset();
$nTotalRecs = ($bSelectLimit) ? $m_tarif->SelectRecordCount() : $rs->RecordCount();
$nStartRec = 1;
if ($nDisplayRecs <= 0) $nDisplayRecs = $nTotalRecs; // Display all records
if (!$bExportAll) SetUpStartRec(); // Set up start record position
if ($bSelectLimit) $rs = LoadRecordset($nStartRec-1, $nDisplayRecs);
?>
<p><span class="phpmaker" style="white-space: nowrap;">TABLE: m tarif
</span></p>
<?php if ($m_tarif->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<form name="fm_tariflistsrch" id="fm_tariflistsrch" action="m_tariflist.php" >
<table class="ewBasicSearch">
	<tr>
		<td><span class="phpmaker">
			<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" size="20" value="<?php echo ew_HtmlEncode($m_tarif->getBasicSearchKeyword()) ?>">
			<input type="Submit" name="Submit" id="Submit" value="Search (*)">&nbsp;
			<a href="m_tariflist.php?cmd=reset">Show all</a>&nbsp;
		</span></td>
	</tr>
	<tr>
	<td><span class="phpmaker"><input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="" <?php if ($m_tarif->getBasicSearchType() == "") { ?>checked<?php } ?>>Exact phrase&nbsp;&nbsp;<input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="AND" <?php if ($m_tarif->getBasicSearchType() == "AND") { ?>checked<?php } ?>>All words&nbsp;&nbsp;<input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="OR" <?php if ($m_tarif->getBasicSearchType() == "OR") { ?>checked<?php } ?>>Any word</span></td>
	</tr>
</table>
</form>
<?php } ?>
<?php } ?>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form method="post" name="fm_tariflist" id="fm_tariflist">
<?php if ($m_tarif->Export == "") { ?>
<table>
	<tr><td><span class="phpmaker">
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_tarifadd.php">Add</a>&nbsp;&nbsp;
<?php } ?>
	</span></td></tr>
</table>
<?php } ?>
<?php if ($nTotalRecs > 0) { ?>
<table id="ewlistmain" class="ewTable">
<?php
	$OptionCnt = 0;
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // view
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // edit
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // copy
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // delete
}
?>
	<!-- Table header -->
	<tr class="ewTableHeader">
		<td valign="top">
<?php if ($m_tarif->Export <> "") { ?>
kode
<?php } else { ?>
	<a href="m_tariflist.php?order=<?php echo urlencode('kode') ?>&ordertype=<?php echo $m_tarif->kode->ReverseSort() ?>">kode&nbsp;(*)<?php if ($m_tarif->kode->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_tarif->kode->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_tarif->Export <> "") { ?>
group jasa
<?php } else { ?>
	<a href="m_tariflist.php?order=<?php echo urlencode('group_jasa') ?>&ordertype=<?php echo $m_tarif->group_jasa->ReverseSort() ?>">group jasa&nbsp;(*)<?php if ($m_tarif->group_jasa->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_tarif->group_jasa->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_tarif->Export <> "") { ?>
kode jasa
<?php } else { ?>
	<a href="m_tariflist.php?order=<?php echo urlencode('kode_jasa') ?>&ordertype=<?php echo $m_tarif->kode_jasa->ReverseSort() ?>">kode jasa&nbsp;(*)<?php if ($m_tarif->kode_jasa->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_tarif->kode_jasa->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_tarif->Export <> "") { ?>
tarif
<?php } else { ?>
	<a href="m_tariflist.php?order=<?php echo urlencode('tarif') ?>&ordertype=<?php echo $m_tarif->tarif->ReverseSort() ?>">tarif<?php if ($m_tarif->tarif->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_tarif->tarif->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_tarif->Export <> "") { ?>
askes
<?php } else { ?>
	<a href="m_tariflist.php?order=<?php echo urlencode('askes') ?>&ordertype=<?php echo $m_tarif->askes->ReverseSort() ?>">askes<?php if ($m_tarif->askes->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_tarif->askes->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_tarif->Export <> "") { ?>
cost sharing
<?php } else { ?>
	<a href="m_tariflist.php?order=<?php echo urlencode('cost_sharing') ?>&ordertype=<?php echo $m_tarif->cost_sharing->ReverseSort() ?>">cost sharing<?php if ($m_tarif->cost_sharing->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_tarif->cost_sharing->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_tarif->Export <> "") { ?>
jasa sarana
<?php } else { ?>
	<a href="m_tariflist.php?order=<?php echo urlencode('jasa_sarana') ?>&ordertype=<?php echo $m_tarif->jasa_sarana->ReverseSort() ?>">jasa sarana<?php if ($m_tarif->jasa_sarana->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_tarif->jasa_sarana->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_tarif->Export <> "") { ?>
jasa pelayanan
<?php } else { ?>
	<a href="m_tariflist.php?order=<?php echo urlencode('jasa_pelayanan') ?>&ordertype=<?php echo $m_tarif->jasa_pelayanan->ReverseSort() ?>">jasa pelayanan<?php if ($m_tarif->jasa_pelayanan->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_tarif->jasa_pelayanan->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_tarif->Export <> "") { ?>
jasa dokter
<?php } else { ?>
	<a href="m_tariflist.php?order=<?php echo urlencode('jasa_dokter') ?>&ordertype=<?php echo $m_tarif->jasa_dokter->ReverseSort() ?>">jasa dokter<?php if ($m_tarif->jasa_dokter->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_tarif->jasa_dokter->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_tarif->Export <> "") { ?>
tim rs
<?php } else { ?>
	<a href="m_tariflist.php?order=<?php echo urlencode('tim_rs') ?>&ordertype=<?php echo $m_tarif->tim_rs->ReverseSort() ?>">tim rs<?php if ($m_tarif->tim_rs->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_tarif->tim_rs->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
<?php if ($m_tarif->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php } ?>
	</tr>
<?php
if (defined("EW_EXPORT_ALL") && $m_tarif->Export <> "") {
	$nStopRec = $nTotalRecs;
} else {
	$nStopRec = $nStartRec + $nDisplayRecs - 1; // Set the last record to display
}
$nRecCount = $nStartRec - 1;
if (!$rs->EOF) {
	$rs->MoveFirst();
	if (!$m_tarif->SelectLimit) $rs->Move($nStartRec - 1); // Move to first record directly
}
$RowCnt = 0;
while (!$rs->EOF && $nRecCount < $nStopRec) {
	$nRecCount++;
	if (intval($nRecCount) >= intval($nStartRec)) {
		$RowCnt++;

	// Init row class and style
	$m_tarif->CssClass = "ewTableRow";
	$m_tarif->CssStyle = "";

	// Init row event
	$m_tarif->RowClientEvents = "onmouseover='ew_MouseOver(this);' onmouseout='ew_MouseOut(this);' onclick='ew_Click(this);'";

	// Display alternate color for rows
	if ($RowCnt % 2 == 0) {
		$m_tarif->CssClass = "ewTableAltRow";
	}
	LoadRowValues($rs); // Load row values
	$m_tarif->RowType = EW_ROWTYPE_VIEW; // Render view
	RenderRow();
?>
	<!-- Table body -->
	<tr<?php echo $m_tarif->DisplayAttributes() ?>>
		<!-- kode -->
		<td<?php echo $m_tarif->kode->CellAttributes() ?>>
<div<?php echo $m_tarif->kode->ViewAttributes() ?>><?php echo $m_tarif->kode->ViewValue ?></div>
</td>
		<!-- group_jasa -->
		<td<?php echo $m_tarif->group_jasa->CellAttributes() ?>>
<div<?php echo $m_tarif->group_jasa->ViewAttributes() ?>><?php echo $m_tarif->group_jasa->ViewValue ?></div>
</td>
		<!-- kode_jasa -->
		<td<?php echo $m_tarif->kode_jasa->CellAttributes() ?>>
<div<?php echo $m_tarif->kode_jasa->ViewAttributes() ?>><?php echo $m_tarif->kode_jasa->ViewValue ?></div>
</td>
		<!-- tarif -->
		<td<?php echo $m_tarif->tarif->CellAttributes() ?>>
<div<?php echo $m_tarif->tarif->ViewAttributes() ?>><?php echo $m_tarif->tarif->ViewValue ?></div>
</td>
		<!-- askes -->
		<td<?php echo $m_tarif->askes->CellAttributes() ?>>
<div<?php echo $m_tarif->askes->ViewAttributes() ?>><?php echo $m_tarif->askes->ViewValue ?></div>
</td>
		<!-- cost_sharing -->
		<td<?php echo $m_tarif->cost_sharing->CellAttributes() ?>>
<div<?php echo $m_tarif->cost_sharing->ViewAttributes() ?>><?php echo $m_tarif->cost_sharing->ViewValue ?></div>
</td>
		<!-- jasa_sarana -->
		<td<?php echo $m_tarif->jasa_sarana->CellAttributes() ?>>
<div<?php echo $m_tarif->jasa_sarana->ViewAttributes() ?>><?php echo $m_tarif->jasa_sarana->ViewValue ?></div>
</td>
		<!-- jasa_pelayanan -->
		<td<?php echo $m_tarif->jasa_pelayanan->CellAttributes() ?>>
<div<?php echo $m_tarif->jasa_pelayanan->ViewAttributes() ?>><?php echo $m_tarif->jasa_pelayanan->ViewValue ?></div>
</td>
		<!-- jasa_dokter -->
		<td<?php echo $m_tarif->jasa_dokter->CellAttributes() ?>>
<div<?php echo $m_tarif->jasa_dokter->ViewAttributes() ?>><?php echo $m_tarif->jasa_dokter->ViewValue ?></div>
</td>
		<!-- tim_rs -->
		<td<?php echo $m_tarif->tim_rs->CellAttributes() ?>>
<div<?php echo $m_tarif->tim_rs->ViewAttributes() ?>><?php echo $m_tarif->tim_rs->ViewValue ?></div>
</td>
<?php if ($m_tarif->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_tarif->ViewUrl() ?>">View</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_tarif->EditUrl() ?>">Edit</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_tarif->CopyUrl() ?>">Copy</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_tarif->DeleteUrl() ?>">Delete</a>
</span></td>
<?php } ?>
<?php } ?>
	</tr>
<?php
	}
	$rs->MoveNext();
}
?>
</table>
<?php if ($m_tarif->Export == "") { ?>
<table>
	<tr><td><span class="phpmaker">
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_tarifadd.php">Add</a>&nbsp;&nbsp;
<?php } ?>
	</span></td></tr>
</table>
<?php } ?>
<?php } ?>
</form>
<?php

// Close recordset and connection
if ($rs) $rs->Close();
?>
<?php if ($m_tarif->Export == "") { ?>
<form action="m_tariflist.php" name="ewpagerform" id="ewpagerform">
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td nowrap>
<?php if (!isset($Pager)) $Pager = new cPrevNextPager($nStartRec, $nDisplayRecs, $nTotalRecs) ?>
<?php if ($Pager->RecordCount > 0) { ?>
	<table border="0" cellspacing="0" cellpadding="0"><tr><td><span class="phpmaker">Page&nbsp;</span></td>
<!--first page button-->
	<?php if ($Pager->FirstButton->Enabled) { ?>
	<td><a href="m_tariflist.php?start=<?php echo $Pager->FirstButton->Start ?>"><img src="images/first.gif" alt="First" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/firstdisab.gif" alt="First" width="16" height="16" border="0"></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($Pager->PrevButton->Enabled) { ?>
	<td><a href="m_tariflist.php?start=<?php echo $Pager->PrevButton->Start ?>"><img src="images/prev.gif" alt="Previous" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/prevdisab.gif" alt="Previous" width="16" height="16" border="0"></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" id="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $Pager->CurrentPage ?>" size="4"></td>
<!--next page button-->
	<?php if ($Pager->NextButton->Enabled) { ?>
	<td><a href="m_tariflist.php?start=<?php echo $Pager->NextButton->Start ?>"><img src="images/next.gif" alt="Next" width="16" height="16" border="0"></a></td>	
	<?php } else { ?>
	<td><img src="images/nextdisab.gif" alt="Next" width="16" height="16" border="0"></td>
	<?php } ?>
<!--last page button-->
	<?php if ($Pager->LastButton->Enabled) { ?>
	<td><a href="m_tariflist.php?start=<?php echo $Pager->LastButton->Start ?>"><img src="images/last.gif" alt="Last" width="16" height="16" border="0"></a></td>	
	<?php } else { ?>
	<td><img src="images/lastdisab.gif" alt="Last" width="16" height="16" border="0"></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;of <?php echo $Pager->PageCount ?></span></td>
	</tr></table>
	<span class="phpmaker">Records <?php echo $Pager->FromIndex ?> to <?php echo $Pager->ToIndex ?> of <?php echo $Pager->RecordCount ?></span>
<?php } else { ?>
	<?php if ($sSrchWhere == "0=101") { ?>
	<span class="phpmaker">Please enter search criteria</span>
	<?php } else { ?>
	<span class="phpmaker">No records found</span>
	<?php } ?>
<?php } ?>
		</td>
	</tr>
</table>
</form>
<?php } ?>
<?php if ($m_tarif->Export == "") { ?>
<?php } ?>
<?php if ($m_tarif->Export == "") { ?>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php } ?>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Return Basic Search sql
function BasicSearchSQL($Keyword) {
	$sKeyword = ew_AdjustSql($Keyword);
	$sql = "";
	$sql .= "kode LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "group_jasa LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "kode_jasa LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "nama_jasa LIKE '%" . $sKeyword . "%' OR ";
	if (substr($sql, -4) == " OR ") $sql = substr($sql, 0, strlen($sql)-4);
	return $sql;
}

// Return Basic Search Where based on search keyword and type
function BasicSearchWhere() {
	global $Security, $m_tarif;
	$sSearchStr = "";
	$sSearchKeyword = ew_StripSlashes(@$_GET[EW_TABLE_BASIC_SEARCH]);
	$sSearchType = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	if ($sSearchKeyword <> "") {
		$sSearch = trim($sSearchKeyword);
		if ($sSearchType <> "") {
			while (strpos($sSearch, "  ") !== FALSE)
				$sSearch = str_replace("  ", " ", $sSearch);
			$arKeyword = explode(" ", trim($sSearch));
			foreach ($arKeyword as $sKeyword) {
				if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
				$sSearchStr .= "(" . BasicSearchSQL($sKeyword) . ")";
			}
		} else {
			$sSearchStr = BasicSearchSQL($sSearch);
		}
	}
	if ($sSearchKeyword <> "") {
		$m_tarif->setBasicSearchKeyword($sSearchKeyword);
		$m_tarif->setBasicSearchType($sSearchType);
	}
	return $sSearchStr;
}

// Clear all search parameters
function ResetSearchParms() {

	// Clear search where
	global $m_tarif;
	$sSrchWhere = "";
	$m_tarif->setSearchWhere($sSrchWhere);

	// Clear basic search parameters
	ResetBasicSearchParms();
}

// Clear all basic search parameters
function ResetBasicSearchParms() {

	// Clear basic search parameters
	global $m_tarif;
	$m_tarif->setBasicSearchKeyword("");
	$m_tarif->setBasicSearchType("");
}

// Restore all search parameters
function RestoreSearchParms() {
	global $sSrchWhere, $m_tarif;
	$sSrchWhere = $m_tarif->getSearchWhere();
}

// Set up Sort parameters based on Sort Links clicked
function SetUpSortOrder() {
	global $m_tarif;

	// Check for an Order parameter
	if (@$_GET["order"] <> "") {
		$m_tarif->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
		$m_tarif->CurrentOrderType = @$_GET["ordertype"];

		// Field kode
		$m_tarif->UpdateSort($m_tarif->kode);

		// Field group_jasa
		$m_tarif->UpdateSort($m_tarif->group_jasa);

		// Field kode_jasa
		$m_tarif->UpdateSort($m_tarif->kode_jasa);

		// Field tarif
		$m_tarif->UpdateSort($m_tarif->tarif);

		// Field askes
		$m_tarif->UpdateSort($m_tarif->askes);

		// Field cost_sharing
		$m_tarif->UpdateSort($m_tarif->cost_sharing);

		// Field jasa_sarana
		$m_tarif->UpdateSort($m_tarif->jasa_sarana);

		// Field jasa_pelayanan
		$m_tarif->UpdateSort($m_tarif->jasa_pelayanan);

		// Field jasa_dokter
		$m_tarif->UpdateSort($m_tarif->jasa_dokter);

		// Field tim_rs
		$m_tarif->UpdateSort($m_tarif->tim_rs);
		$m_tarif->setStartRecordNumber(1); // Reset start position
	}
	$sOrderBy = $m_tarif->getSessionOrderBy(); // Get order by from Session
	if ($sOrderBy == "") {
		if ($m_tarif->SqlOrderBy() <> "") {
			$sOrderBy = $m_tarif->SqlOrderBy();
			$m_tarif->setSessionOrderBy($sOrderBy);
		}
	}
}

// Reset command based on querystring parameter cmd=
// - RESET: reset search parameters
// - RESETALL: reset search & master/detail parameters
// - RESETSORT: reset sort parameters
function ResetCmd() {
	global $sDbMasterFilter, $sDbDetailFilter, $nStartRec, $sOrderBy;
	global $m_tarif;

	// Get reset cmd
	if (@$_GET["cmd"] <> "") {
		$sCmd = $_GET["cmd"];

		// Reset search criteria
		if (strtolower($sCmd) == "reset" || strtolower($sCmd) == "resetall") {
			ResetSearchParms();
		}

		// Reset Sort Criteria
		if (strtolower($sCmd) == "resetsort") {
			$sOrderBy = "";
			$m_tarif->setSessionOrderBy($sOrderBy);
			$m_tarif->kode->setSort("");
			$m_tarif->group_jasa->setSort("");
			$m_tarif->kode_jasa->setSort("");
			$m_tarif->tarif->setSort("");
			$m_tarif->askes->setSort("");
			$m_tarif->cost_sharing->setSort("");
			$m_tarif->jasa_sarana->setSort("");
			$m_tarif->jasa_pelayanan->setSort("");
			$m_tarif->jasa_dokter->setSort("");
			$m_tarif->tim_rs->setSort("");
		}

		// Reset start position
		$nStartRec = 1;
		$m_tarif->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_tarif;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_tarif->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_tarif->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_tarif->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_tarif->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_tarif->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_tarif->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_tarif->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_tarif;

	// Call Recordset Selecting event
	$m_tarif->Recordset_Selecting($m_tarif->CurrentFilter);

	// Load list page sql
	$sSql = $m_tarif->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_tarif->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_tarif;
	$sFilter = $m_tarif->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($m_tarif->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_tarif->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_tarif->CurrentFilter = $sFilter;
	$sSql = $m_tarif->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_tarif->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_tarif;
	$m_tarif->kode->setDbValue($rs->fields('kode'));
	$m_tarif->group_jasa->setDbValue($rs->fields('group_jasa'));
	$m_tarif->kode_jasa->setDbValue($rs->fields('kode_jasa'));
	$m_tarif->nama_jasa->setDbValue($rs->fields('nama_jasa'));
	$m_tarif->tarif->setDbValue($rs->fields('tarif'));
	$m_tarif->askes->setDbValue($rs->fields('askes'));
	$m_tarif->cost_sharing->setDbValue($rs->fields('cost_sharing'));
	$m_tarif->jasa_sarana->setDbValue($rs->fields('jasa_sarana'));
	$m_tarif->jasa_pelayanan->setDbValue($rs->fields('jasa_pelayanan'));
	$m_tarif->jasa_dokter->setDbValue($rs->fields('jasa_dokter'));
	$m_tarif->tim_rs->setDbValue($rs->fields('tim_rs'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_tarif;

	// Call Row Rendering event
	$m_tarif->Row_Rendering();

	// Common render codes for all row types
	// kode

	$m_tarif->kode->CellCssStyle = "";
	$m_tarif->kode->CellCssClass = "";

	// group_jasa
	$m_tarif->group_jasa->CellCssStyle = "";
	$m_tarif->group_jasa->CellCssClass = "";

	// kode_jasa
	$m_tarif->kode_jasa->CellCssStyle = "";
	$m_tarif->kode_jasa->CellCssClass = "";

	// tarif
	$m_tarif->tarif->CellCssStyle = "";
	$m_tarif->tarif->CellCssClass = "";

	// askes
	$m_tarif->askes->CellCssStyle = "";
	$m_tarif->askes->CellCssClass = "";

	// cost_sharing
	$m_tarif->cost_sharing->CellCssStyle = "";
	$m_tarif->cost_sharing->CellCssClass = "";

	// jasa_sarana
	$m_tarif->jasa_sarana->CellCssStyle = "";
	$m_tarif->jasa_sarana->CellCssClass = "";

	// jasa_pelayanan
	$m_tarif->jasa_pelayanan->CellCssStyle = "";
	$m_tarif->jasa_pelayanan->CellCssClass = "";

	// jasa_dokter
	$m_tarif->jasa_dokter->CellCssStyle = "";
	$m_tarif->jasa_dokter->CellCssClass = "";

	// tim_rs
	$m_tarif->tim_rs->CellCssStyle = "";
	$m_tarif->tim_rs->CellCssClass = "";
	if ($m_tarif->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode
		$m_tarif->kode->ViewValue = $m_tarif->kode->CurrentValue;
		$m_tarif->kode->CssStyle = "";
		$m_tarif->kode->CssClass = "";
		$m_tarif->kode->ViewCustomAttributes = "";

		// group_jasa
		$m_tarif->group_jasa->ViewValue = $m_tarif->group_jasa->CurrentValue;
		$m_tarif->group_jasa->CssStyle = "";
		$m_tarif->group_jasa->CssClass = "";
		$m_tarif->group_jasa->ViewCustomAttributes = "";

		// kode_jasa
		$m_tarif->kode_jasa->ViewValue = $m_tarif->kode_jasa->CurrentValue;
		$m_tarif->kode_jasa->CssStyle = "";
		$m_tarif->kode_jasa->CssClass = "";
		$m_tarif->kode_jasa->ViewCustomAttributes = "";

		// tarif
		$m_tarif->tarif->ViewValue = $m_tarif->tarif->CurrentValue;
		$m_tarif->tarif->CssStyle = "";
		$m_tarif->tarif->CssClass = "";
		$m_tarif->tarif->ViewCustomAttributes = "";

		// askes
		$m_tarif->askes->ViewValue = $m_tarif->askes->CurrentValue;
		$m_tarif->askes->CssStyle = "";
		$m_tarif->askes->CssClass = "";
		$m_tarif->askes->ViewCustomAttributes = "";

		// cost_sharing
		$m_tarif->cost_sharing->ViewValue = $m_tarif->cost_sharing->CurrentValue;
		$m_tarif->cost_sharing->CssStyle = "";
		$m_tarif->cost_sharing->CssClass = "";
		$m_tarif->cost_sharing->ViewCustomAttributes = "";

		// jasa_sarana
		$m_tarif->jasa_sarana->ViewValue = $m_tarif->jasa_sarana->CurrentValue;
		$m_tarif->jasa_sarana->CssStyle = "";
		$m_tarif->jasa_sarana->CssClass = "";
		$m_tarif->jasa_sarana->ViewCustomAttributes = "";

		// jasa_pelayanan
		$m_tarif->jasa_pelayanan->ViewValue = $m_tarif->jasa_pelayanan->CurrentValue;
		$m_tarif->jasa_pelayanan->CssStyle = "";
		$m_tarif->jasa_pelayanan->CssClass = "";
		$m_tarif->jasa_pelayanan->ViewCustomAttributes = "";

		// jasa_dokter
		$m_tarif->jasa_dokter->ViewValue = $m_tarif->jasa_dokter->CurrentValue;
		$m_tarif->jasa_dokter->CssStyle = "";
		$m_tarif->jasa_dokter->CssClass = "";
		$m_tarif->jasa_dokter->ViewCustomAttributes = "";

		// tim_rs
		$m_tarif->tim_rs->ViewValue = $m_tarif->tim_rs->CurrentValue;
		$m_tarif->tim_rs->CssStyle = "";
		$m_tarif->tim_rs->CssClass = "";
		$m_tarif->tim_rs->ViewCustomAttributes = "";

		// kode
		$m_tarif->kode->HrefValue = "";

		// group_jasa
		$m_tarif->group_jasa->HrefValue = "";

		// kode_jasa
		$m_tarif->kode_jasa->HrefValue = "";

		// tarif
		$m_tarif->tarif->HrefValue = "";

		// askes
		$m_tarif->askes->HrefValue = "";

		// cost_sharing
		$m_tarif->cost_sharing->HrefValue = "";

		// jasa_sarana
		$m_tarif->jasa_sarana->HrefValue = "";

		// jasa_pelayanan
		$m_tarif->jasa_pelayanan->HrefValue = "";

		// jasa_dokter
		$m_tarif->jasa_dokter->HrefValue = "";

		// tim_rs
		$m_tarif->tim_rs->HrefValue = "";
	} elseif ($m_tarif->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_tarif->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_tarif->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_tarif->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
