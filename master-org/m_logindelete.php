<?php
define("EW_PAGE_ID", "delete", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_login', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_logininfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_login->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_login->Export; // Get export parameter, used in header
$sExportFile = $m_login->TableVar; // Get export file, used in header
?>
<?php

// Load Key Parameters
$sKey = "";
$bSingleDelete = TRUE; // Initialize as single delete
$arRecKeys = array();
$nKeySelected = 0; // Initialize selected key count
$sFilter = "";
if (@$_GET["NIP"] <> "") {
	$m_login->NIP->setQueryStringValue($_GET["NIP"]);
	$sKey .= $m_login->NIP->QueryStringValue;
} else {
	$bSingleDelete = FALSE;
}
if ($bSingleDelete) {
	$nKeySelected = 1; // Set up key selected count
	$arRecKeys[0] = $sKey;
} else {
	if (isset($_POST["key_m"])) { // Key in form
		$nKeySelected = count($_POST["key_m"]); // Set up key selected count
		$arRecKeys = ew_StripSlashes($_POST["key_m"]);
	}
}
if ($nKeySelected <= 0) Page_Terminate($m_login->getReturnUrl()); // No key specified, exit

// Build filter
foreach ($arRecKeys as $sKey) {
	$sFilter .= "(";

	// Set up key field
	$sKeyFld = $sKey;
	$sFilter .= "NIP='" . ew_AdjustSql($sKeyFld) . "' AND ";
	if (substr($sFilter, -5) == " AND ") $sFilter = substr($sFilter, 0, strlen($sFilter)-5) . ") OR ";
}
if (substr($sFilter, -4) == " OR ") $sFilter = substr($sFilter, 0, strlen($sFilter)-4);

// Set up filter (Sql Where Clause) and get Return Sql
// Sql constructor in m_login class, m_logininfo.php

$m_login->CurrentFilter = $sFilter;

// Get action
if (@$_POST["a_delete"] <> "") {
	$m_login->CurrentAction = $_POST["a_delete"];
} else {
	$m_login->CurrentAction = "I"; // Display record
}
switch ($m_login->CurrentAction) {
	case "D": // Delete
		$m_login->SendEmail = TRUE; // Send email on delete success
		if (DeleteRows()) { // delete rows
			$_SESSION[EW_SESSION_MESSAGE] = "Delete Successful"; // Set up success message
			Page_Terminate($m_login->getReturnUrl()); // Return to caller
		}
}

// Load records for display
$rs = LoadRecordset();
$nTotalRecs = $rs->RecordCount(); // Get record count
if ($nTotalRecs <= 0) { // No record found, exit
	$rs->Close();
	Page_Terminate($m_login->getReturnUrl()); // Return to caller
}
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "delete"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Delete from TABLE: m login<br><br><a href="<?php echo $m_login->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form action="m_logindelete.php" method="post">
<p>
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($arRecKeys as $sKey) { ?>
<input type="hidden" name="key_m[]" id="key_m[]" value="<?php echo ew_HtmlEncode($sKey) ?>">
<?php } ?>
<table class="ewTable">
	<tr class="ewTableHeader">
		<td valign="top">NIP</td>
		<td valign="top">PWD</td>
		<td valign="top">SES REG</td>
		<td valign="top">ROLES</td>
		<td valign="top">KDUNIT</td>
		<td valign="top">DEPARTEMEN</td>
	</tr>
<?php
$nRecCount = 0;
$i = 0;
while (!$rs->EOF) {
	$nRecCount++;

	// Set row class and style
	$m_login->CssClass = "ewTableRow";
	$m_login->CssStyle = "";

	// Display alternate color for rows
	if ($nRecCount % 2 <> 1) {
		$m_login->CssClass = "ewTableAltRow";
	}

	// Get the field contents
	LoadRowValues($rs);

	// Render row value
	$m_login->RowType = EW_ROWTYPE_VIEW; // view
	RenderRow();
?>
	<tr<?php echo $m_login->DisplayAttributes() ?>>
		<td<?php echo $m_login->NIP->CellAttributes() ?>>
<div<?php echo $m_login->NIP->ViewAttributes() ?>><?php echo $m_login->NIP->ViewValue ?></div>
</td>
		<td<?php echo $m_login->PWD->CellAttributes() ?>>
<div<?php echo $m_login->PWD->ViewAttributes() ?>><?php echo $m_login->PWD->ViewValue ?></div>
</td>
		<td<?php echo $m_login->SES_REG->CellAttributes() ?>>
<div<?php echo $m_login->SES_REG->ViewAttributes() ?>><?php echo $m_login->SES_REG->ViewValue ?></div>
</td>
		<td<?php echo $m_login->ROLES->CellAttributes() ?>>
<div<?php echo $m_login->ROLES->ViewAttributes() ?>><?php echo $m_login->ROLES->ViewValue ?></div>
</td>
		<td<?php echo $m_login->KDUNIT->CellAttributes() ?>>
<div<?php echo $m_login->KDUNIT->ViewAttributes() ?>><?php echo $m_login->KDUNIT->ViewValue ?></div>
</td>
		<td<?php echo $m_login->DEPARTEMEN->CellAttributes() ?>>
<div<?php echo $m_login->DEPARTEMEN->ViewAttributes() ?>><?php echo $m_login->DEPARTEMEN->ViewValue ?></div>
</td>
	</tr>
<?php
	$rs->MoveNext();
}
$rs->Close();
?>
</table>
<p>
<input type="submit" name="Action" id="Action" value="Confirm Delete">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// ------------------------------------------------
//  Function DeleteRows
//  - Delete Records based on current filter
function DeleteRows() {
	global $conn, $Security, $m_login;
	$DeleteRows = TRUE;
	$sWrkFilter = $m_login->CurrentFilter;

	// Set up filter (Sql Where Clause) and get Return Sql
	// Sql constructor in m_login class, m_logininfo.php

	$m_login->CurrentFilter = $sWrkFilter;
	$sSql = $m_login->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE) {
		return FALSE;
	} elseif ($rs->EOF) {
		$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
		$rs->Close();
		return FALSE;
	}
	$conn->BeginTrans();

	// Clone old rows
	$rsold = ($rs) ? $rs->GetRows() : array();
	if ($rs) $rs->Close();

	// Call row deleting event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$DeleteRows = $m_login->Row_Deleting($row);
			if (!$DeleteRows) break;
		}
	}
	if ($DeleteRows) {
		$sKey = "";
		foreach ($rsold as $row) {
			$sThisKey = "";
			if ($sThisKey <> "") $sThisKey .= EW_COMPOSITE_KEY_SEPARATOR;
			$sThisKey .= $row['NIP'];
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$DeleteRows = $conn->Execute($m_login->DeleteSQL($row)); // Delete
			$conn->raiseErrorFn = '';
			if ($DeleteRows === FALSE)
				break;
			if ($sKey <> "") $sKey .= ", ";
			$sKey .= $sThisKey;
		}
	} else {

		// Set up error message
		if ($m_login->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_login->CancelMessage;
			$m_login->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Delete cancelled";
		}
	}
	if ($DeleteRows) {
		$conn->CommitTrans(); // Commit the changes
	} else {
		$conn->RollbackTrans(); // Rollback changes
	}

	// Call recordset deleted event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$m_login->Row_Deleted($row);
		}	
	}
	return $DeleteRows;
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_login;

	// Call Recordset Selecting event
	$m_login->Recordset_Selecting($m_login->CurrentFilter);

	// Load list page sql
	$sSql = $m_login->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_login->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_login;
	$sFilter = $m_login->SqlKeyFilter();
	$sFilter = str_replace("@NIP@", ew_AdjustSql($m_login->NIP->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_login->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_login->CurrentFilter = $sFilter;
	$sSql = $m_login->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_login->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_login;
	$m_login->NIP->setDbValue($rs->fields('NIP'));
	$m_login->PWD->setDbValue($rs->fields('PWD'));
	$m_login->SES_REG->setDbValue($rs->fields('SES_REG'));
	$m_login->ROLES->setDbValue($rs->fields('ROLES'));
	$m_login->KDUNIT->setDbValue($rs->fields('KDUNIT'));
	$m_login->DEPARTEMEN->setDbValue($rs->fields('DEPARTEMEN'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_login;

	// Call Row Rendering event
	$m_login->Row_Rendering();

	// Common render codes for all row types
	// NIP

	$m_login->NIP->CellCssStyle = "";
	$m_login->NIP->CellCssClass = "";

	// PWD
	$m_login->PWD->CellCssStyle = "";
	$m_login->PWD->CellCssClass = "";

	// SES_REG
	$m_login->SES_REG->CellCssStyle = "";
	$m_login->SES_REG->CellCssClass = "";

	// ROLES
	$m_login->ROLES->CellCssStyle = "";
	$m_login->ROLES->CellCssClass = "";

	// KDUNIT
	$m_login->KDUNIT->CellCssStyle = "";
	$m_login->KDUNIT->CellCssClass = "";

	// DEPARTEMEN
	$m_login->DEPARTEMEN->CellCssStyle = "";
	$m_login->DEPARTEMEN->CellCssClass = "";
	if ($m_login->RowType == EW_ROWTYPE_VIEW) { // View row

		// NIP
		$m_login->NIP->ViewValue = $m_login->NIP->CurrentValue;
		$m_login->NIP->CssStyle = "";
		$m_login->NIP->CssClass = "";
		$m_login->NIP->ViewCustomAttributes = "";

		// PWD
		$m_login->PWD->ViewValue = $m_login->PWD->CurrentValue;
		$m_login->PWD->CssStyle = "";
		$m_login->PWD->CssClass = "";
		$m_login->PWD->ViewCustomAttributes = "";

		// SES_REG
		$m_login->SES_REG->ViewValue = $m_login->SES_REG->CurrentValue;
		$m_login->SES_REG->CssStyle = "";
		$m_login->SES_REG->CssClass = "";
		$m_login->SES_REG->ViewCustomAttributes = "";

		// ROLES
		$m_login->ROLES->ViewValue = $m_login->ROLES->CurrentValue;
		$m_login->ROLES->CssStyle = "";
		$m_login->ROLES->CssClass = "";
		$m_login->ROLES->ViewCustomAttributes = "";

		// KDUNIT
		$m_login->KDUNIT->ViewValue = $m_login->KDUNIT->CurrentValue;
		$m_login->KDUNIT->CssStyle = "";
		$m_login->KDUNIT->CssClass = "";
		$m_login->KDUNIT->ViewCustomAttributes = "";

		// DEPARTEMEN
		$m_login->DEPARTEMEN->ViewValue = $m_login->DEPARTEMEN->CurrentValue;
		$m_login->DEPARTEMEN->CssStyle = "";
		$m_login->DEPARTEMEN->CssClass = "";
		$m_login->DEPARTEMEN->ViewCustomAttributes = "";

		// NIP
		$m_login->NIP->HrefValue = "";

		// PWD
		$m_login->PWD->HrefValue = "";

		// SES_REG
		$m_login->SES_REG->HrefValue = "";

		// ROLES
		$m_login->ROLES->HrefValue = "";

		// KDUNIT
		$m_login->KDUNIT->HrefValue = "";

		// DEPARTEMEN
		$m_login->DEPARTEMEN->HrefValue = "";
	} elseif ($m_login->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_login->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_login->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_login->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
