<?php
define("EW_PAGE_ID", "list", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_ruang', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_ruanginfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_ruang->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_ruang->Export; // Get export parameter, used in header
$sExportFile = $m_ruang->TableVar; // Get export file, used in header
?>
<?php
?>
<?php

// Paging variables
$nStartRec = 0; // Start record index
$nStopRec = 0; // Stop record index
$nTotalRecs = 0; // Total number of records
$nDisplayRecs = 20;
$nRecRange = 10;
$nRecCount = 0; // Record count

// Search filters
$sSrchAdvanced = ""; // Advanced search filter
$sSrchBasic = ""; // Basic search filter
$sSrchWhere = ""; // Search where clause
$sFilter = "";

// Master/Detail
$sDbMasterFilter = ""; // Master filter
$sDbDetailFilter = ""; // Detail filter
$sSqlMaster = ""; // Sql for master record

// Handle reset command
ResetCmd();

// Get basic search criteria
$sSrchBasic = BasicSearchWhere();

// Build search criteria
if ($sSrchAdvanced <> "") {
	if ($sSrchWhere <> "") $sSrchWhere .= " AND ";
	$sSrchWhere .= "(" . $sSrchAdvanced . ")";
}
if ($sSrchBasic <> "") {
	if ($sSrchWhere <> "") $sSrchWhere .= " AND ";
	$sSrchWhere .= "(" . $sSrchBasic . ")";
}

// Save search criteria
if ($sSrchWhere <> "") {
	if ($sSrchBasic == "") ResetBasicSearchParms();
	$m_ruang->setSearchWhere($sSrchWhere); // Save to Session
	$nStartRec = 1; // Reset start record counter
	$m_ruang->setStartRecordNumber($nStartRec);
} else {
	RestoreSearchParms();
}

// Build filter
$sFilter = "";
if ($sDbDetailFilter <> "") {
	if ($sFilter <> "") $sFilter .= " AND ";
	$sFilter .= "(" . $sDbDetailFilter . ")";
}
if ($sSrchWhere <> "") {
	if ($sFilter <> "") $sFilter .= " AND ";
	$sFilter .= "(" . $sSrchWhere . ")";
}

// Set up filter in Session
$m_ruang->setSessionWhere($sFilter);
$m_ruang->CurrentFilter = "";

// Set Up Sorting Order
SetUpSortOrder();

// Set Return Url
$m_ruang->setReturnUrl("m_ruanglist.php");
?>
<?php include "header.php" ?>
<?php if ($m_ruang->Export == "") { ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "list"; // Page id

//-->
</script>
<script type="text/javascript">
<!--
var firstrowoffset = 1; // First data row start at
var lastrowoffset = 0; // Last data row end at
var EW_LIST_TABLE_NAME = 'ewlistmain'; // Table name for list page
var rowclass = 'ewTableRow'; // Row class
var rowaltclass = 'ewTableAltRow'; // Row alternate class
var rowmoverclass = 'ewTableHighlightRow'; // Row mouse over class
var rowselectedclass = 'ewTableSelectRow'; // Row selected class
var roweditclass = 'ewTableEditRow'; // Row edit class

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<?php } ?>
<?php if ($m_ruang->Export == "") { ?>
<?php } ?>
<?php

// Load recordset
$bExportAll = (defined("EW_EXPORT_ALL") && $m_ruang->Export <> "");
$bSelectLimit = ($m_ruang->Export == "" && $m_ruang->SelectLimit);
if (!$bSelectLimit) $rs = LoadRecordset();
$nTotalRecs = ($bSelectLimit) ? $m_ruang->SelectRecordCount() : $rs->RecordCount();
$nStartRec = 1;
if ($nDisplayRecs <= 0) $nDisplayRecs = $nTotalRecs; // Display all records
if (!$bExportAll) SetUpStartRec(); // Set up start record position
if ($bSelectLimit) $rs = LoadRecordset($nStartRec-1, $nDisplayRecs);
?>
<p><span class="phpmaker" style="white-space: nowrap;">TABLE: m ruang
</span></p>
<?php if ($m_ruang->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<form name="fm_ruanglistsrch" id="fm_ruanglistsrch" action="m_ruanglist.php" >
<table class="ewBasicSearch">
	<tr>
		<td><span class="phpmaker">
			<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" size="20" value="<?php echo ew_HtmlEncode($m_ruang->getBasicSearchKeyword()) ?>">
			<input type="Submit" name="Submit" id="Submit" value="Search (*)">&nbsp;
			<a href="m_ruanglist.php?cmd=reset">Show all</a>&nbsp;
		</span></td>
	</tr>
	<tr>
	<td><span class="phpmaker"><input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="" <?php if ($m_ruang->getBasicSearchType() == "") { ?>checked<?php } ?>>Exact phrase&nbsp;&nbsp;<input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="AND" <?php if ($m_ruang->getBasicSearchType() == "AND") { ?>checked<?php } ?>>All words&nbsp;&nbsp;<input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="OR" <?php if ($m_ruang->getBasicSearchType() == "OR") { ?>checked<?php } ?>>Any word</span></td>
	</tr>
</table>
</form>
<?php } ?>
<?php } ?>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form method="post" name="fm_ruanglist" id="fm_ruanglist">
<?php if ($m_ruang->Export == "") { ?>
<table>
	<tr><td><span class="phpmaker">
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_ruangadd.php">Add</a>&nbsp;&nbsp;
<?php } ?>
	</span></td></tr>
</table>
<?php } ?>
<?php if ($nTotalRecs > 0) { ?>
<table id="ewlistmain" class="ewTable">
<?php
	$OptionCnt = 0;
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // view
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // edit
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // copy
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // delete
}
?>
	<!-- Table header -->
	<tr class="ewTableHeader">
		<td valign="top">
<?php if ($m_ruang->Export <> "") { ?>
no
<?php } else { ?>
	<a href="m_ruanglist.php?order=<?php echo urlencode('no') ?>&ordertype=<?php echo $m_ruang->no->ReverseSort() ?>">no<?php if ($m_ruang->no->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_ruang->no->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_ruang->Export <> "") { ?>
nama
<?php } else { ?>
	<a href="m_ruanglist.php?order=<?php echo urlencode('nama') ?>&ordertype=<?php echo $m_ruang->nama->ReverseSort() ?>">nama&nbsp;(*)<?php if ($m_ruang->nama->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_ruang->nama->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_ruang->Export <> "") { ?>
kelas
<?php } else { ?>
	<a href="m_ruanglist.php?order=<?php echo urlencode('kelas') ?>&ordertype=<?php echo $m_ruang->kelas->ReverseSort() ?>">kelas&nbsp;(*)<?php if ($m_ruang->kelas->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_ruang->kelas->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_ruang->Export <> "") { ?>
ruang
<?php } else { ?>
	<a href="m_ruanglist.php?order=<?php echo urlencode('ruang') ?>&ordertype=<?php echo $m_ruang->ruang->ReverseSort() ?>">ruang&nbsp;(*)<?php if ($m_ruang->ruang->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_ruang->ruang->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_ruang->Export <> "") { ?>
jumlah tt
<?php } else { ?>
	<a href="m_ruanglist.php?order=<?php echo urlencode('jumlah_tt') ?>&ordertype=<?php echo $m_ruang->jumlah_tt->ReverseSort() ?>">jumlah tt<?php if ($m_ruang->jumlah_tt->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_ruang->jumlah_tt->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_ruang->Export <> "") { ?>
ket ruang
<?php } else { ?>
	<a href="m_ruanglist.php?order=<?php echo urlencode('ket_ruang') ?>&ordertype=<?php echo $m_ruang->ket_ruang->ReverseSort() ?>">ket ruang&nbsp;(*)<?php if ($m_ruang->ket_ruang->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_ruang->ket_ruang->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
<?php if ($m_ruang->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php } ?>
	</tr>
<?php
if (defined("EW_EXPORT_ALL") && $m_ruang->Export <> "") {
	$nStopRec = $nTotalRecs;
} else {
	$nStopRec = $nStartRec + $nDisplayRecs - 1; // Set the last record to display
}
$nRecCount = $nStartRec - 1;
if (!$rs->EOF) {
	$rs->MoveFirst();
	if (!$m_ruang->SelectLimit) $rs->Move($nStartRec - 1); // Move to first record directly
}
$RowCnt = 0;
while (!$rs->EOF && $nRecCount < $nStopRec) {
	$nRecCount++;
	if (intval($nRecCount) >= intval($nStartRec)) {
		$RowCnt++;

	// Init row class and style
	$m_ruang->CssClass = "ewTableRow";
	$m_ruang->CssStyle = "";

	// Init row event
	$m_ruang->RowClientEvents = "onmouseover='ew_MouseOver(this);' onmouseout='ew_MouseOut(this);' onclick='ew_Click(this);'";

	// Display alternate color for rows
	if ($RowCnt % 2 == 0) {
		$m_ruang->CssClass = "ewTableAltRow";
	}
	LoadRowValues($rs); // Load row values
	$m_ruang->RowType = EW_ROWTYPE_VIEW; // Render view
	RenderRow();
?>
	<!-- Table body -->
	<tr<?php echo $m_ruang->DisplayAttributes() ?>>
		<!-- no -->
		<td<?php echo $m_ruang->no->CellAttributes() ?>>
<div<?php echo $m_ruang->no->ViewAttributes() ?>><?php echo $m_ruang->no->ViewValue ?></div>
</td>
		<!-- nama -->
		<td<?php echo $m_ruang->nama->CellAttributes() ?>>
<div<?php echo $m_ruang->nama->ViewAttributes() ?>><?php echo $m_ruang->nama->ViewValue ?></div>
</td>
		<!-- kelas -->
		<td<?php echo $m_ruang->kelas->CellAttributes() ?>>
<div<?php echo $m_ruang->kelas->ViewAttributes() ?>><?php echo $m_ruang->kelas->ViewValue ?></div>
</td>
		<!-- ruang -->
		<td<?php echo $m_ruang->ruang->CellAttributes() ?>>
<div<?php echo $m_ruang->ruang->ViewAttributes() ?>><?php echo $m_ruang->ruang->ViewValue ?></div>
</td>
		<!-- jumlah_tt -->
		<td<?php echo $m_ruang->jumlah_tt->CellAttributes() ?>>
<div<?php echo $m_ruang->jumlah_tt->ViewAttributes() ?>><?php echo $m_ruang->jumlah_tt->ViewValue ?></div>
</td>
		<!-- ket_ruang -->
		<td<?php echo $m_ruang->ket_ruang->CellAttributes() ?>>
<div<?php echo $m_ruang->ket_ruang->ViewAttributes() ?>><?php echo $m_ruang->ket_ruang->ViewValue ?></div>
</td>
<?php if ($m_ruang->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_ruang->ViewUrl() ?>">View</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_ruang->EditUrl() ?>">Edit</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_ruang->CopyUrl() ?>">Copy</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_ruang->DeleteUrl() ?>">Delete</a>
</span></td>
<?php } ?>
<?php } ?>
	</tr>
<?php
	}
	$rs->MoveNext();
}
?>
</table>
<?php if ($m_ruang->Export == "") { ?>
<table>
	<tr><td><span class="phpmaker">
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_ruangadd.php">Add</a>&nbsp;&nbsp;
<?php } ?>
	</span></td></tr>
</table>
<?php } ?>
<?php } ?>
</form>
<?php

// Close recordset and connection
if ($rs) $rs->Close();
?>
<?php if ($m_ruang->Export == "") { ?>
<form action="m_ruanglist.php" name="ewpagerform" id="ewpagerform">
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td nowrap>
<?php if (!isset($Pager)) $Pager = new cPrevNextPager($nStartRec, $nDisplayRecs, $nTotalRecs) ?>
<?php if ($Pager->RecordCount > 0) { ?>
	<table border="0" cellspacing="0" cellpadding="0"><tr><td><span class="phpmaker">Page&nbsp;</span></td>
<!--first page button-->
	<?php if ($Pager->FirstButton->Enabled) { ?>
	<td><a href="m_ruanglist.php?start=<?php echo $Pager->FirstButton->Start ?>"><img src="images/first.gif" alt="First" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/firstdisab.gif" alt="First" width="16" height="16" border="0"></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($Pager->PrevButton->Enabled) { ?>
	<td><a href="m_ruanglist.php?start=<?php echo $Pager->PrevButton->Start ?>"><img src="images/prev.gif" alt="Previous" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/prevdisab.gif" alt="Previous" width="16" height="16" border="0"></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" id="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $Pager->CurrentPage ?>" size="4"></td>
<!--next page button-->
	<?php if ($Pager->NextButton->Enabled) { ?>
	<td><a href="m_ruanglist.php?start=<?php echo $Pager->NextButton->Start ?>"><img src="images/next.gif" alt="Next" width="16" height="16" border="0"></a></td>	
	<?php } else { ?>
	<td><img src="images/nextdisab.gif" alt="Next" width="16" height="16" border="0"></td>
	<?php } ?>
<!--last page button-->
	<?php if ($Pager->LastButton->Enabled) { ?>
	<td><a href="m_ruanglist.php?start=<?php echo $Pager->LastButton->Start ?>"><img src="images/last.gif" alt="Last" width="16" height="16" border="0"></a></td>	
	<?php } else { ?>
	<td><img src="images/lastdisab.gif" alt="Last" width="16" height="16" border="0"></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;of <?php echo $Pager->PageCount ?></span></td>
	</tr></table>
	<span class="phpmaker">Records <?php echo $Pager->FromIndex ?> to <?php echo $Pager->ToIndex ?> of <?php echo $Pager->RecordCount ?></span>
<?php } else { ?>
	<?php if ($sSrchWhere == "0=101") { ?>
	<span class="phpmaker">Please enter search criteria</span>
	<?php } else { ?>
	<span class="phpmaker">No records found</span>
	<?php } ?>
<?php } ?>
		</td>
	</tr>
</table>
</form>
<?php } ?>
<?php if ($m_ruang->Export == "") { ?>
<?php } ?>
<?php if ($m_ruang->Export == "") { ?>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php } ?>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Return Basic Search sql
function BasicSearchSQL($Keyword) {
	$sKeyword = ew_AdjustSql($Keyword);
	$sql = "";
	$sql .= "nama LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "kelas LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "ruang LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "ket_ruang LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "fasilitas LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "keterangan LIKE '%" . $sKeyword . "%' OR ";
	if (substr($sql, -4) == " OR ") $sql = substr($sql, 0, strlen($sql)-4);
	return $sql;
}

// Return Basic Search Where based on search keyword and type
function BasicSearchWhere() {
	global $Security, $m_ruang;
	$sSearchStr = "";
	$sSearchKeyword = ew_StripSlashes(@$_GET[EW_TABLE_BASIC_SEARCH]);
	$sSearchType = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	if ($sSearchKeyword <> "") {
		$sSearch = trim($sSearchKeyword);
		if ($sSearchType <> "") {
			while (strpos($sSearch, "  ") !== FALSE)
				$sSearch = str_replace("  ", " ", $sSearch);
			$arKeyword = explode(" ", trim($sSearch));
			foreach ($arKeyword as $sKeyword) {
				if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
				$sSearchStr .= "(" . BasicSearchSQL($sKeyword) . ")";
			}
		} else {
			$sSearchStr = BasicSearchSQL($sSearch);
		}
	}
	if ($sSearchKeyword <> "") {
		$m_ruang->setBasicSearchKeyword($sSearchKeyword);
		$m_ruang->setBasicSearchType($sSearchType);
	}
	return $sSearchStr;
}

// Clear all search parameters
function ResetSearchParms() {

	// Clear search where
	global $m_ruang;
	$sSrchWhere = "";
	$m_ruang->setSearchWhere($sSrchWhere);

	// Clear basic search parameters
	ResetBasicSearchParms();
}

// Clear all basic search parameters
function ResetBasicSearchParms() {

	// Clear basic search parameters
	global $m_ruang;
	$m_ruang->setBasicSearchKeyword("");
	$m_ruang->setBasicSearchType("");
}

// Restore all search parameters
function RestoreSearchParms() {
	global $sSrchWhere, $m_ruang;
	$sSrchWhere = $m_ruang->getSearchWhere();
}

// Set up Sort parameters based on Sort Links clicked
function SetUpSortOrder() {
	global $m_ruang;

	// Check for an Order parameter
	if (@$_GET["order"] <> "") {
		$m_ruang->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
		$m_ruang->CurrentOrderType = @$_GET["ordertype"];

		// Field no
		$m_ruang->UpdateSort($m_ruang->no);

		// Field nama
		$m_ruang->UpdateSort($m_ruang->nama);

		// Field kelas
		$m_ruang->UpdateSort($m_ruang->kelas);

		// Field ruang
		$m_ruang->UpdateSort($m_ruang->ruang);

		// Field jumlah_tt
		$m_ruang->UpdateSort($m_ruang->jumlah_tt);

		// Field ket_ruang
		$m_ruang->UpdateSort($m_ruang->ket_ruang);
		$m_ruang->setStartRecordNumber(1); // Reset start position
	}
	$sOrderBy = $m_ruang->getSessionOrderBy(); // Get order by from Session
	if ($sOrderBy == "") {
		if ($m_ruang->SqlOrderBy() <> "") {
			$sOrderBy = $m_ruang->SqlOrderBy();
			$m_ruang->setSessionOrderBy($sOrderBy);
		}
	}
}

// Reset command based on querystring parameter cmd=
// - RESET: reset search parameters
// - RESETALL: reset search & master/detail parameters
// - RESETSORT: reset sort parameters
function ResetCmd() {
	global $sDbMasterFilter, $sDbDetailFilter, $nStartRec, $sOrderBy;
	global $m_ruang;

	// Get reset cmd
	if (@$_GET["cmd"] <> "") {
		$sCmd = $_GET["cmd"];

		// Reset search criteria
		if (strtolower($sCmd) == "reset" || strtolower($sCmd) == "resetall") {
			ResetSearchParms();
		}

		// Reset Sort Criteria
		if (strtolower($sCmd) == "resetsort") {
			$sOrderBy = "";
			$m_ruang->setSessionOrderBy($sOrderBy);
			$m_ruang->no->setSort("");
			$m_ruang->nama->setSort("");
			$m_ruang->kelas->setSort("");
			$m_ruang->ruang->setSort("");
			$m_ruang->jumlah_tt->setSort("");
			$m_ruang->ket_ruang->setSort("");
		}

		// Reset start position
		$nStartRec = 1;
		$m_ruang->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_ruang;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_ruang->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_ruang->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_ruang->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_ruang->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_ruang->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_ruang->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_ruang->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_ruang;

	// Call Recordset Selecting event
	$m_ruang->Recordset_Selecting($m_ruang->CurrentFilter);

	// Load list page sql
	$sSql = $m_ruang->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_ruang->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_ruang;
	$sFilter = $m_ruang->SqlKeyFilter();
	if (!is_numeric($m_ruang->no->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@no@", ew_AdjustSql($m_ruang->no->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_ruang->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_ruang->CurrentFilter = $sFilter;
	$sSql = $m_ruang->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_ruang->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_ruang;
	$m_ruang->no->setDbValue($rs->fields('no'));
	$m_ruang->nama->setDbValue($rs->fields('nama'));
	$m_ruang->kelas->setDbValue($rs->fields('kelas'));
	$m_ruang->ruang->setDbValue($rs->fields('ruang'));
	$m_ruang->jumlah_tt->setDbValue($rs->fields('jumlah_tt'));
	$m_ruang->ket_ruang->setDbValue($rs->fields('ket_ruang'));
	$m_ruang->fasilitas->setDbValue($rs->fields('fasilitas'));
	$m_ruang->keterangan->setDbValue($rs->fields('keterangan'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_ruang;

	// Call Row Rendering event
	$m_ruang->Row_Rendering();

	// Common render codes for all row types
	// no

	$m_ruang->no->CellCssStyle = "";
	$m_ruang->no->CellCssClass = "";

	// nama
	$m_ruang->nama->CellCssStyle = "";
	$m_ruang->nama->CellCssClass = "";

	// kelas
	$m_ruang->kelas->CellCssStyle = "";
	$m_ruang->kelas->CellCssClass = "";

	// ruang
	$m_ruang->ruang->CellCssStyle = "";
	$m_ruang->ruang->CellCssClass = "";

	// jumlah_tt
	$m_ruang->jumlah_tt->CellCssStyle = "";
	$m_ruang->jumlah_tt->CellCssClass = "";

	// ket_ruang
	$m_ruang->ket_ruang->CellCssStyle = "";
	$m_ruang->ket_ruang->CellCssClass = "";
	if ($m_ruang->RowType == EW_ROWTYPE_VIEW) { // View row

		// no
		$m_ruang->no->ViewValue = $m_ruang->no->CurrentValue;
		$m_ruang->no->CssStyle = "";
		$m_ruang->no->CssClass = "";
		$m_ruang->no->ViewCustomAttributes = "";

		// nama
		$m_ruang->nama->ViewValue = $m_ruang->nama->CurrentValue;
		$m_ruang->nama->CssStyle = "";
		$m_ruang->nama->CssClass = "";
		$m_ruang->nama->ViewCustomAttributes = "";

		// kelas
		$m_ruang->kelas->ViewValue = $m_ruang->kelas->CurrentValue;
		$m_ruang->kelas->CssStyle = "";
		$m_ruang->kelas->CssClass = "";
		$m_ruang->kelas->ViewCustomAttributes = "";

		// ruang
		$m_ruang->ruang->ViewValue = $m_ruang->ruang->CurrentValue;
		$m_ruang->ruang->CssStyle = "";
		$m_ruang->ruang->CssClass = "";
		$m_ruang->ruang->ViewCustomAttributes = "";

		// jumlah_tt
		$m_ruang->jumlah_tt->ViewValue = $m_ruang->jumlah_tt->CurrentValue;
		$m_ruang->jumlah_tt->CssStyle = "";
		$m_ruang->jumlah_tt->CssClass = "";
		$m_ruang->jumlah_tt->ViewCustomAttributes = "";

		// ket_ruang
		$m_ruang->ket_ruang->ViewValue = $m_ruang->ket_ruang->CurrentValue;
		$m_ruang->ket_ruang->CssStyle = "";
		$m_ruang->ket_ruang->CssClass = "";
		$m_ruang->ket_ruang->ViewCustomAttributes = "";

		// no
		$m_ruang->no->HrefValue = "";

		// nama
		$m_ruang->nama->HrefValue = "";

		// kelas
		$m_ruang->kelas->HrefValue = "";

		// ruang
		$m_ruang->ruang->HrefValue = "";

		// jumlah_tt
		$m_ruang->jumlah_tt->HrefValue = "";

		// ket_ruang
		$m_ruang->ket_ruang->HrefValue = "";
	} elseif ($m_ruang->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_ruang->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_ruang->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_ruang->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
