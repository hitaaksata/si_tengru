<?php
define("EW_PAGE_ID", "delete", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_dokter', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_dokterinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_dokter->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_dokter->Export; // Get export parameter, used in header
$sExportFile = $m_dokter->TableVar; // Get export file, used in header
?>
<?php

// Load Key Parameters
$sKey = "";
$bSingleDelete = TRUE; // Initialize as single delete
$arRecKeys = array();
$nKeySelected = 0; // Initialize selected key count
$sFilter = "";
if (@$_GET["KDDOKTER"] <> "") {
	$m_dokter->KDDOKTER->setQueryStringValue($_GET["KDDOKTER"]);
	if (!is_numeric($m_dokter->KDDOKTER->QueryStringValue)) {
		Page_Terminate($m_dokter->getReturnUrl()); // Prevent sql injection, exit
	}
	$sKey .= $m_dokter->KDDOKTER->QueryStringValue;
} else {
	$bSingleDelete = FALSE;
}
if ($bSingleDelete) {
	$nKeySelected = 1; // Set up key selected count
	$arRecKeys[0] = $sKey;
} else {
	if (isset($_POST["key_m"])) { // Key in form
		$nKeySelected = count($_POST["key_m"]); // Set up key selected count
		$arRecKeys = ew_StripSlashes($_POST["key_m"]);
	}
}
if ($nKeySelected <= 0) Page_Terminate($m_dokter->getReturnUrl()); // No key specified, exit

// Build filter
foreach ($arRecKeys as $sKey) {
	$sFilter .= "(";

	// Set up key field
	$sKeyFld = $sKey;
	if (!is_numeric($sKeyFld)) {
		Page_Terminate($m_dokter->getReturnUrl()); // Prevent sql injection, exit
	}
	$sFilter .= "KDDOKTER=" . ew_AdjustSql($sKeyFld) . " AND ";
	if (substr($sFilter, -5) == " AND ") $sFilter = substr($sFilter, 0, strlen($sFilter)-5) . ") OR ";
}
if (substr($sFilter, -4) == " OR ") $sFilter = substr($sFilter, 0, strlen($sFilter)-4);

// Set up filter (Sql Where Clause) and get Return Sql
// Sql constructor in m_dokter class, m_dokterinfo.php

$m_dokter->CurrentFilter = $sFilter;

// Get action
if (@$_POST["a_delete"] <> "") {
	$m_dokter->CurrentAction = $_POST["a_delete"];
} else {
	$m_dokter->CurrentAction = "I"; // Display record
}
switch ($m_dokter->CurrentAction) {
	case "D": // Delete
		$m_dokter->SendEmail = TRUE; // Send email on delete success
		if (DeleteRows()) { // delete rows
			$_SESSION[EW_SESSION_MESSAGE] = "Delete Successful"; // Set up success message
			Page_Terminate($m_dokter->getReturnUrl()); // Return to caller
		}
}

// Load records for display
$rs = LoadRecordset();
$nTotalRecs = $rs->RecordCount(); // Get record count
if ($nTotalRecs <= 0) { // No record found, exit
	$rs->Close();
	Page_Terminate($m_dokter->getReturnUrl()); // Return to caller
}
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "delete"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Delete from TABLE: m dokter<br><br><a href="<?php echo $m_dokter->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form action="m_dokterdelete.php" method="post">
<p>
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($arRecKeys as $sKey) { ?>
<input type="hidden" name="key_m[]" id="key_m[]" value="<?php echo ew_HtmlEncode($sKey) ?>">
<?php } ?>
<table class="ewTable">
	<tr class="ewTableHeader">
		<td valign="top">KDDOKTER</td>
		<td valign="top">KDPOLY</td>
		<td valign="top">NAMADOKTER</td>
	</tr>
<?php
$nRecCount = 0;
$i = 0;
while (!$rs->EOF) {
	$nRecCount++;

	// Set row class and style
	$m_dokter->CssClass = "ewTableRow";
	$m_dokter->CssStyle = "";

	// Display alternate color for rows
	if ($nRecCount % 2 <> 1) {
		$m_dokter->CssClass = "ewTableAltRow";
	}

	// Get the field contents
	LoadRowValues($rs);

	// Render row value
	$m_dokter->RowType = EW_ROWTYPE_VIEW; // view
	RenderRow();
?>
	<tr<?php echo $m_dokter->DisplayAttributes() ?>>
		<td<?php echo $m_dokter->KDDOKTER->CellAttributes() ?>>
<div<?php echo $m_dokter->KDDOKTER->ViewAttributes() ?>><?php echo $m_dokter->KDDOKTER->ViewValue ?></div>
</td>
		<td<?php echo $m_dokter->KDPOLY->CellAttributes() ?>>
<div<?php echo $m_dokter->KDPOLY->ViewAttributes() ?>><?php echo $m_dokter->KDPOLY->ViewValue ?></div>
</td>
		<td<?php echo $m_dokter->NAMADOKTER->CellAttributes() ?>>
<div<?php echo $m_dokter->NAMADOKTER->ViewAttributes() ?>><?php echo $m_dokter->NAMADOKTER->ViewValue ?></div>
</td>
	</tr>
<?php
	$rs->MoveNext();
}
$rs->Close();
?>
</table>
<p>
<input type="submit" name="Action" id="Action" value="Confirm Delete">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// ------------------------------------------------
//  Function DeleteRows
//  - Delete Records based on current filter
function DeleteRows() {
	global $conn, $Security, $m_dokter;
	$DeleteRows = TRUE;
	$sWrkFilter = $m_dokter->CurrentFilter;

	// Set up filter (Sql Where Clause) and get Return Sql
	// Sql constructor in m_dokter class, m_dokterinfo.php

	$m_dokter->CurrentFilter = $sWrkFilter;
	$sSql = $m_dokter->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE) {
		return FALSE;
	} elseif ($rs->EOF) {
		$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
		$rs->Close();
		return FALSE;
	}
	$conn->BeginTrans();

	// Clone old rows
	$rsold = ($rs) ? $rs->GetRows() : array();
	if ($rs) $rs->Close();

	// Call row deleting event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$DeleteRows = $m_dokter->Row_Deleting($row);
			if (!$DeleteRows) break;
		}
	}
	if ($DeleteRows) {
		$sKey = "";
		foreach ($rsold as $row) {
			$sThisKey = "";
			if ($sThisKey <> "") $sThisKey .= EW_COMPOSITE_KEY_SEPARATOR;
			$sThisKey .= $row['KDDOKTER'];
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$DeleteRows = $conn->Execute($m_dokter->DeleteSQL($row)); // Delete
			$conn->raiseErrorFn = '';
			if ($DeleteRows === FALSE)
				break;
			if ($sKey <> "") $sKey .= ", ";
			$sKey .= $sThisKey;
		}
	} else {

		// Set up error message
		if ($m_dokter->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_dokter->CancelMessage;
			$m_dokter->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Delete cancelled";
		}
	}
	if ($DeleteRows) {
		$conn->CommitTrans(); // Commit the changes
	} else {
		$conn->RollbackTrans(); // Rollback changes
	}

	// Call recordset deleted event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$m_dokter->Row_Deleted($row);
		}	
	}
	return $DeleteRows;
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_dokter;

	// Call Recordset Selecting event
	$m_dokter->Recordset_Selecting($m_dokter->CurrentFilter);

	// Load list page sql
	$sSql = $m_dokter->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_dokter->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_dokter;
	$sFilter = $m_dokter->SqlKeyFilter();
	if (!is_numeric($m_dokter->KDDOKTER->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@KDDOKTER@", ew_AdjustSql($m_dokter->KDDOKTER->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_dokter->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_dokter->CurrentFilter = $sFilter;
	$sSql = $m_dokter->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_dokter->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_dokter;
	$m_dokter->KDDOKTER->setDbValue($rs->fields('KDDOKTER'));
	$m_dokter->KDPOLY->setDbValue($rs->fields('KDPOLY'));
	$m_dokter->NAMADOKTER->setDbValue($rs->fields('NAMADOKTER'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_dokter;

	// Call Row Rendering event
	$m_dokter->Row_Rendering();

	// Common render codes for all row types
	// KDDOKTER

	$m_dokter->KDDOKTER->CellCssStyle = "";
	$m_dokter->KDDOKTER->CellCssClass = "";

	// KDPOLY
	$m_dokter->KDPOLY->CellCssStyle = "";
	$m_dokter->KDPOLY->CellCssClass = "";

	// NAMADOKTER
	$m_dokter->NAMADOKTER->CellCssStyle = "";
	$m_dokter->NAMADOKTER->CellCssClass = "";
	if ($m_dokter->RowType == EW_ROWTYPE_VIEW) { // View row

		// KDDOKTER
		$m_dokter->KDDOKTER->ViewValue = $m_dokter->KDDOKTER->CurrentValue;
		$m_dokter->KDDOKTER->CssStyle = "";
		$m_dokter->KDDOKTER->CssClass = "";
		$m_dokter->KDDOKTER->ViewCustomAttributes = "";

		// KDPOLY
		$m_dokter->KDPOLY->ViewValue = $m_dokter->KDPOLY->CurrentValue;
		$m_dokter->KDPOLY->CssStyle = "";
		$m_dokter->KDPOLY->CssClass = "";
		$m_dokter->KDPOLY->ViewCustomAttributes = "";

		// NAMADOKTER
		$m_dokter->NAMADOKTER->ViewValue = $m_dokter->NAMADOKTER->CurrentValue;
		$m_dokter->NAMADOKTER->CssStyle = "";
		$m_dokter->NAMADOKTER->CssClass = "";
		$m_dokter->NAMADOKTER->ViewCustomAttributes = "";

		// KDDOKTER
		$m_dokter->KDDOKTER->HrefValue = "";

		// KDPOLY
		$m_dokter->KDPOLY->HrefValue = "";

		// NAMADOKTER
		$m_dokter->NAMADOKTER->HrefValue = "";
	} elseif ($m_dokter->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_dokter->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_dokter->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_dokter->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
