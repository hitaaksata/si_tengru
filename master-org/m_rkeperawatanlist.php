<?php
define("EW_PAGE_ID", "list", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_rkeperawatan', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_rkeperawataninfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_rkeperawatan->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_rkeperawatan->Export; // Get export parameter, used in header
$sExportFile = $m_rkeperawatan->TableVar; // Get export file, used in header
?>
<?php
?>
<?php

// Paging variables
$nStartRec = 0; // Start record index
$nStopRec = 0; // Stop record index
$nTotalRecs = 0; // Total number of records
$nDisplayRecs = 20;
$nRecRange = 10;
$nRecCount = 0; // Record count

// Search filters
$sSrchAdvanced = ""; // Advanced search filter
$sSrchBasic = ""; // Basic search filter
$sSrchWhere = ""; // Search where clause
$sFilter = "";

// Master/Detail
$sDbMasterFilter = ""; // Master filter
$sDbDetailFilter = ""; // Detail filter
$sSqlMaster = ""; // Sql for master record

// Handle reset command
ResetCmd();

// Build filter
$sFilter = "";
if ($sDbDetailFilter <> "") {
	if ($sFilter <> "") $sFilter .= " AND ";
	$sFilter .= "(" . $sDbDetailFilter . ")";
}
if ($sSrchWhere <> "") {
	if ($sFilter <> "") $sFilter .= " AND ";
	$sFilter .= "(" . $sSrchWhere . ")";
}

// Set up filter in Session
$m_rkeperawatan->setSessionWhere($sFilter);
$m_rkeperawatan->CurrentFilter = "";

// Set Up Sorting Order
SetUpSortOrder();

// Set Return Url
$m_rkeperawatan->setReturnUrl("m_rkeperawatanlist.php");
?>
<?php include "header.php" ?>
<?php if ($m_rkeperawatan->Export == "") { ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "list"; // Page id

//-->
</script>
<script type="text/javascript">
<!--
var firstrowoffset = 1; // First data row start at
var lastrowoffset = 0; // Last data row end at
var EW_LIST_TABLE_NAME = 'ewlistmain'; // Table name for list page
var rowclass = 'ewTableRow'; // Row class
var rowaltclass = 'ewTableAltRow'; // Row alternate class
var rowmoverclass = 'ewTableHighlightRow'; // Row mouse over class
var rowselectedclass = 'ewTableSelectRow'; // Row selected class
var roweditclass = 'ewTableEditRow'; // Row edit class

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<?php } ?>
<?php if ($m_rkeperawatan->Export == "") { ?>
<?php } ?>
<?php

// Load recordset
$bExportAll = (defined("EW_EXPORT_ALL") && $m_rkeperawatan->Export <> "");
$bSelectLimit = ($m_rkeperawatan->Export == "" && $m_rkeperawatan->SelectLimit);
if (!$bSelectLimit) $rs = LoadRecordset();
$nTotalRecs = ($bSelectLimit) ? $m_rkeperawatan->SelectRecordCount() : $rs->RecordCount();
$nStartRec = 1;
if ($nDisplayRecs <= 0) $nDisplayRecs = $nTotalRecs; // Display all records
if (!$bExportAll) SetUpStartRec(); // Set up start record position
if ($bSelectLimit) $rs = LoadRecordset($nStartRec-1, $nDisplayRecs);
?>
<p><span class="phpmaker" style="white-space: nowrap;">TABLE: m rkeperawatan
</span></p>
<?php if ($m_rkeperawatan->Export == "") { ?>
<?php } ?>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form method="post" name="fm_rkeperawatanlist" id="fm_rkeperawatanlist">
<?php if ($m_rkeperawatan->Export == "") { ?>
<table>
	<tr><td><span class="phpmaker">
	</span></td></tr>
</table>
<?php } ?>
<?php if ($nTotalRecs > 0) { ?>
<table id="ewlistmain" class="ewTable">
<?php
	$OptionCnt = 0;
?>
	<!-- Table header -->
	<tr class="ewTableHeader">
		<td valign="top">
<?php if ($m_rkeperawatan->Export <> "") { ?>
idx
<?php } else { ?>
	<a href="m_rkeperawatanlist.php?order=<?php echo urlencode('idx') ?>&ordertype=<?php echo $m_rkeperawatan->idx->ReverseSort() ?>">idx<?php if ($m_rkeperawatan->idx->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_rkeperawatan->idx->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_rkeperawatan->Export <> "") { ?>
DK
<?php } else { ?>
	<a href="m_rkeperawatanlist.php?order=<?php echo urlencode('DK') ?>&ordertype=<?php echo $m_rkeperawatan->DK->ReverseSort() ?>">DK<?php if ($m_rkeperawatan->DK->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_rkeperawatan->DK->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_rkeperawatan->Export <> "") { ?>
KE
<?php } else { ?>
	<a href="m_rkeperawatanlist.php?order=<?php echo urlencode('KE') ?>&ordertype=<?php echo $m_rkeperawatan->KE->ReverseSort() ?>">KE<?php if ($m_rkeperawatan->KE->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_rkeperawatan->KE->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_rkeperawatan->Export <> "") { ?>
RK
<?php } else { ?>
	<a href="m_rkeperawatanlist.php?order=<?php echo urlencode('RK') ?>&ordertype=<?php echo $m_rkeperawatan->RK->ReverseSort() ?>">RK<?php if ($m_rkeperawatan->RK->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_rkeperawatan->RK->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
<?php if ($m_rkeperawatan->Export == "") { ?>
<?php } ?>
	</tr>
<?php
if (defined("EW_EXPORT_ALL") && $m_rkeperawatan->Export <> "") {
	$nStopRec = $nTotalRecs;
} else {
	$nStopRec = $nStartRec + $nDisplayRecs - 1; // Set the last record to display
}
$nRecCount = $nStartRec - 1;
if (!$rs->EOF) {
	$rs->MoveFirst();
	if (!$m_rkeperawatan->SelectLimit) $rs->Move($nStartRec - 1); // Move to first record directly
}
$RowCnt = 0;
while (!$rs->EOF && $nRecCount < $nStopRec) {
	$nRecCount++;
	if (intval($nRecCount) >= intval($nStartRec)) {
		$RowCnt++;

	// Init row class and style
	$m_rkeperawatan->CssClass = "ewTableRow";
	$m_rkeperawatan->CssStyle = "";

	// Init row event
	$m_rkeperawatan->RowClientEvents = "onmouseover='ew_MouseOver(this);' onmouseout='ew_MouseOut(this);' onclick='ew_Click(this);'";

	// Display alternate color for rows
	if ($RowCnt % 2 == 0) {
		$m_rkeperawatan->CssClass = "ewTableAltRow";
	}
	LoadRowValues($rs); // Load row values
	$m_rkeperawatan->RowType = EW_ROWTYPE_VIEW; // Render view
	RenderRow();
?>
	<!-- Table body -->
	<tr<?php echo $m_rkeperawatan->DisplayAttributes() ?>>
		<!-- idx -->
		<td<?php echo $m_rkeperawatan->idx->CellAttributes() ?>>
<div<?php echo $m_rkeperawatan->idx->ViewAttributes() ?>><?php echo $m_rkeperawatan->idx->ViewValue ?></div>
</td>
		<!-- DK -->
		<td<?php echo $m_rkeperawatan->DK->CellAttributes() ?>>
<div<?php echo $m_rkeperawatan->DK->ViewAttributes() ?>><?php echo $m_rkeperawatan->DK->ViewValue ?></div>
</td>
		<!-- KE -->
		<td<?php echo $m_rkeperawatan->KE->CellAttributes() ?>>
<div<?php echo $m_rkeperawatan->KE->ViewAttributes() ?>><?php echo $m_rkeperawatan->KE->ViewValue ?></div>
</td>
		<!-- RK -->
		<td<?php echo $m_rkeperawatan->RK->CellAttributes() ?>>
<div<?php echo $m_rkeperawatan->RK->ViewAttributes() ?>><?php echo $m_rkeperawatan->RK->ViewValue ?></div>
</td>
<?php if ($m_rkeperawatan->Export == "") { ?>
<?php } ?>
	</tr>
<?php
	}
	$rs->MoveNext();
}
?>
</table>
<?php if ($m_rkeperawatan->Export == "") { ?>
<table>
	<tr><td><span class="phpmaker">
	</span></td></tr>
</table>
<?php } ?>
<?php } ?>
</form>
<?php

// Close recordset and connection
if ($rs) $rs->Close();
?>
<?php if ($m_rkeperawatan->Export == "") { ?>
<form action="m_rkeperawatanlist.php" name="ewpagerform" id="ewpagerform">
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td nowrap>
<?php if (!isset($Pager)) $Pager = new cPrevNextPager($nStartRec, $nDisplayRecs, $nTotalRecs) ?>
<?php if ($Pager->RecordCount > 0) { ?>
	<table border="0" cellspacing="0" cellpadding="0"><tr><td><span class="phpmaker">Page&nbsp;</span></td>
<!--first page button-->
	<?php if ($Pager->FirstButton->Enabled) { ?>
	<td><a href="m_rkeperawatanlist.php?start=<?php echo $Pager->FirstButton->Start ?>"><img src="images/first.gif" alt="First" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/firstdisab.gif" alt="First" width="16" height="16" border="0"></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($Pager->PrevButton->Enabled) { ?>
	<td><a href="m_rkeperawatanlist.php?start=<?php echo $Pager->PrevButton->Start ?>"><img src="images/prev.gif" alt="Previous" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/prevdisab.gif" alt="Previous" width="16" height="16" border="0"></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" id="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $Pager->CurrentPage ?>" size="4"></td>
<!--next page button-->
	<?php if ($Pager->NextButton->Enabled) { ?>
	<td><a href="m_rkeperawatanlist.php?start=<?php echo $Pager->NextButton->Start ?>"><img src="images/next.gif" alt="Next" width="16" height="16" border="0"></a></td>	
	<?php } else { ?>
	<td><img src="images/nextdisab.gif" alt="Next" width="16" height="16" border="0"></td>
	<?php } ?>
<!--last page button-->
	<?php if ($Pager->LastButton->Enabled) { ?>
	<td><a href="m_rkeperawatanlist.php?start=<?php echo $Pager->LastButton->Start ?>"><img src="images/last.gif" alt="Last" width="16" height="16" border="0"></a></td>	
	<?php } else { ?>
	<td><img src="images/lastdisab.gif" alt="Last" width="16" height="16" border="0"></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;of <?php echo $Pager->PageCount ?></span></td>
	</tr></table>
	<span class="phpmaker">Records <?php echo $Pager->FromIndex ?> to <?php echo $Pager->ToIndex ?> of <?php echo $Pager->RecordCount ?></span>
<?php } else { ?>
	<?php if ($sSrchWhere == "0=101") { ?>
	<span class="phpmaker">Please enter search criteria</span>
	<?php } else { ?>
	<span class="phpmaker">No records found</span>
	<?php } ?>
<?php } ?>
		</td>
	</tr>
</table>
</form>
<?php } ?>
<?php if ($m_rkeperawatan->Export == "") { ?>
<?php } ?>
<?php if ($m_rkeperawatan->Export == "") { ?>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php } ?>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Set up Sort parameters based on Sort Links clicked
function SetUpSortOrder() {
	global $m_rkeperawatan;

	// Check for an Order parameter
	if (@$_GET["order"] <> "") {
		$m_rkeperawatan->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
		$m_rkeperawatan->CurrentOrderType = @$_GET["ordertype"];

		// Field idx
		$m_rkeperawatan->UpdateSort($m_rkeperawatan->idx);

		// Field DK
		$m_rkeperawatan->UpdateSort($m_rkeperawatan->DK);

		// Field KE
		$m_rkeperawatan->UpdateSort($m_rkeperawatan->KE);

		// Field RK
		$m_rkeperawatan->UpdateSort($m_rkeperawatan->RK);
		$m_rkeperawatan->setStartRecordNumber(1); // Reset start position
	}
	$sOrderBy = $m_rkeperawatan->getSessionOrderBy(); // Get order by from Session
	if ($sOrderBy == "") {
		if ($m_rkeperawatan->SqlOrderBy() <> "") {
			$sOrderBy = $m_rkeperawatan->SqlOrderBy();
			$m_rkeperawatan->setSessionOrderBy($sOrderBy);
		}
	}
}

// Reset command based on querystring parameter cmd=
// - RESET: reset search parameters
// - RESETALL: reset search & master/detail parameters
// - RESETSORT: reset sort parameters
function ResetCmd() {
	global $sDbMasterFilter, $sDbDetailFilter, $nStartRec, $sOrderBy;
	global $m_rkeperawatan;

	// Get reset cmd
	if (@$_GET["cmd"] <> "") {
		$sCmd = $_GET["cmd"];

		// Reset Sort Criteria
		if (strtolower($sCmd) == "resetsort") {
			$sOrderBy = "";
			$m_rkeperawatan->setSessionOrderBy($sOrderBy);
			$m_rkeperawatan->idx->setSort("");
			$m_rkeperawatan->DK->setSort("");
			$m_rkeperawatan->KE->setSort("");
			$m_rkeperawatan->RK->setSort("");
		}

		// Reset start position
		$nStartRec = 1;
		$m_rkeperawatan->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_rkeperawatan;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_rkeperawatan->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_rkeperawatan->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_rkeperawatan->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_rkeperawatan->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_rkeperawatan->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_rkeperawatan->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_rkeperawatan->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_rkeperawatan;

	// Call Recordset Selecting event
	$m_rkeperawatan->Recordset_Selecting($m_rkeperawatan->CurrentFilter);

	// Load list page sql
	$sSql = $m_rkeperawatan->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_rkeperawatan->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_rkeperawatan;
	$sFilter = $m_rkeperawatan->SqlKeyFilter();

	// Call Row Selecting event
	$m_rkeperawatan->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_rkeperawatan->CurrentFilter = $sFilter;
	$sSql = $m_rkeperawatan->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_rkeperawatan->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_rkeperawatan;
	$m_rkeperawatan->idx->setDbValue($rs->fields('idx'));
	$m_rkeperawatan->DK->setDbValue($rs->fields('DK'));
	$m_rkeperawatan->KE->setDbValue($rs->fields('KE'));
	$m_rkeperawatan->RK->setDbValue($rs->fields('RK'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_rkeperawatan;

	// Call Row Rendering event
	$m_rkeperawatan->Row_Rendering();

	// Common render codes for all row types
	// idx

	$m_rkeperawatan->idx->CellCssStyle = "";
	$m_rkeperawatan->idx->CellCssClass = "";

	// DK
	$m_rkeperawatan->DK->CellCssStyle = "";
	$m_rkeperawatan->DK->CellCssClass = "";

	// KE
	$m_rkeperawatan->KE->CellCssStyle = "";
	$m_rkeperawatan->KE->CellCssClass = "";

	// RK
	$m_rkeperawatan->RK->CellCssStyle = "";
	$m_rkeperawatan->RK->CellCssClass = "";
	if ($m_rkeperawatan->RowType == EW_ROWTYPE_VIEW) { // View row

		// idx
		$m_rkeperawatan->idx->ViewValue = $m_rkeperawatan->idx->CurrentValue;
		$m_rkeperawatan->idx->CssStyle = "";
		$m_rkeperawatan->idx->CssClass = "";
		$m_rkeperawatan->idx->ViewCustomAttributes = "";

		// DK
		$m_rkeperawatan->DK->ViewValue = $m_rkeperawatan->DK->CurrentValue;
		$m_rkeperawatan->DK->CssStyle = "";
		$m_rkeperawatan->DK->CssClass = "";
		$m_rkeperawatan->DK->ViewCustomAttributes = "";

		// KE
		$m_rkeperawatan->KE->ViewValue = $m_rkeperawatan->KE->CurrentValue;
		$m_rkeperawatan->KE->CssStyle = "";
		$m_rkeperawatan->KE->CssClass = "";
		$m_rkeperawatan->KE->ViewCustomAttributes = "";

		// RK
		$m_rkeperawatan->RK->ViewValue = $m_rkeperawatan->RK->CurrentValue;
		$m_rkeperawatan->RK->CssStyle = "";
		$m_rkeperawatan->RK->CssClass = "";
		$m_rkeperawatan->RK->ViewCustomAttributes = "";

		// idx
		$m_rkeperawatan->idx->HrefValue = "";

		// DK
		$m_rkeperawatan->DK->HrefValue = "";

		// KE
		$m_rkeperawatan->KE->HrefValue = "";

		// RK
		$m_rkeperawatan->RK->HrefValue = "";
	} elseif ($m_rkeperawatan->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_rkeperawatan->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_rkeperawatan->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_rkeperawatan->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
