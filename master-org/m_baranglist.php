<?php
define("EW_PAGE_ID", "list", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_barang', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_baranginfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_barang->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_barang->Export; // Get export parameter, used in header
$sExportFile = $m_barang->TableVar; // Get export file, used in header
?>
<?php
?>
<?php

// Paging variables
$nStartRec = 0; // Start record index
$nStopRec = 0; // Stop record index
$nTotalRecs = 0; // Total number of records
$nDisplayRecs = 20;
$nRecRange = 10;
$nRecCount = 0; // Record count

// Search filters
$sSrchAdvanced = ""; // Advanced search filter
$sSrchBasic = ""; // Basic search filter
$sSrchWhere = ""; // Search where clause
$sFilter = "";

// Master/Detail
$sDbMasterFilter = ""; // Master filter
$sDbDetailFilter = ""; // Detail filter
$sSqlMaster = ""; // Sql for master record

// Handle reset command
ResetCmd();

// Get basic search criteria
$sSrchBasic = BasicSearchWhere();

// Build search criteria
if ($sSrchAdvanced <> "") {
	if ($sSrchWhere <> "") $sSrchWhere .= " AND ";
	$sSrchWhere .= "(" . $sSrchAdvanced . ")";
}
if ($sSrchBasic <> "") {
	if ($sSrchWhere <> "") $sSrchWhere .= " AND ";
	$sSrchWhere .= "(" . $sSrchBasic . ")";
}

// Save search criteria
if ($sSrchWhere <> "") {
	if ($sSrchBasic == "") ResetBasicSearchParms();
	$m_barang->setSearchWhere($sSrchWhere); // Save to Session
	$nStartRec = 1; // Reset start record counter
	$m_barang->setStartRecordNumber($nStartRec);
} else {
	RestoreSearchParms();
}

// Build filter
$sFilter = "";
if ($sDbDetailFilter <> "") {
	if ($sFilter <> "") $sFilter .= " AND ";
	$sFilter .= "(" . $sDbDetailFilter . ")";
}
if ($sSrchWhere <> "") {
	if ($sFilter <> "") $sFilter .= " AND ";
	$sFilter .= "(" . $sSrchWhere . ")";
}

// Set up filter in Session
$m_barang->setSessionWhere($sFilter);
$m_barang->CurrentFilter = "";

// Set Up Sorting Order
SetUpSortOrder();

// Set Return Url
$m_barang->setReturnUrl("m_baranglist.php");
?>
<?php include "header.php" ?>
<?php if ($m_barang->Export == "") { ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "list"; // Page id

//-->
</script>
<script type="text/javascript">
<!--
var firstrowoffset = 1; // First data row start at
var lastrowoffset = 0; // Last data row end at
var EW_LIST_TABLE_NAME = 'ewlistmain'; // Table name for list page
var rowclass = 'ewTableRow'; // Row class
var rowaltclass = 'ewTableAltRow'; // Row alternate class
var rowmoverclass = 'ewTableHighlightRow'; // Row mouse over class
var rowselectedclass = 'ewTableSelectRow'; // Row selected class
var roweditclass = 'ewTableEditRow'; // Row edit class

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<?php } ?>
<?php if ($m_barang->Export == "") { ?>
<?php } ?>
<?php

// Load recordset
$bExportAll = (defined("EW_EXPORT_ALL") && $m_barang->Export <> "");
$bSelectLimit = ($m_barang->Export == "" && $m_barang->SelectLimit);
if (!$bSelectLimit) $rs = LoadRecordset();
$nTotalRecs = ($bSelectLimit) ? $m_barang->SelectRecordCount() : $rs->RecordCount();
$nStartRec = 1;
if ($nDisplayRecs <= 0) $nDisplayRecs = $nTotalRecs; // Display all records
if (!$bExportAll) SetUpStartRec(); // Set up start record position
if ($bSelectLimit) $rs = LoadRecordset($nStartRec-1, $nDisplayRecs);
?>
<p><span class="phpmaker" style="white-space: nowrap;">TABLE: m barang
</span></p>
<?php if ($m_barang->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<form name="fm_baranglistsrch" id="fm_baranglistsrch" action="m_baranglist.php" >
<table class="ewBasicSearch">
	<tr>
		<td><span class="phpmaker">
			<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" size="20" value="<?php echo ew_HtmlEncode($m_barang->getBasicSearchKeyword()) ?>">
			<input type="Submit" name="Submit" id="Submit" value="Search (*)">&nbsp;
			<a href="m_baranglist.php?cmd=reset">Show all</a>&nbsp;
		</span></td>
	</tr>
	<tr>
	<td><span class="phpmaker"><input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="" <?php if ($m_barang->getBasicSearchType() == "") { ?>checked<?php } ?>>Exact phrase&nbsp;&nbsp;<input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="AND" <?php if ($m_barang->getBasicSearchType() == "AND") { ?>checked<?php } ?>>All words&nbsp;&nbsp;<input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="OR" <?php if ($m_barang->getBasicSearchType() == "OR") { ?>checked<?php } ?>>Any word</span></td>
	</tr>
</table>
</form>
<?php } ?>
<?php } ?>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form method="post" name="fm_baranglist" id="fm_baranglist">
<?php if ($m_barang->Export == "") { ?>
<table>
	<tr><td><span class="phpmaker">
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_barangadd.php">Add</a>&nbsp;&nbsp;
<?php } ?>
	</span></td></tr>
</table>
<?php } ?>
<?php if ($nTotalRecs > 0) { ?>
<table id="ewlistmain" class="ewTable">
<?php
	$OptionCnt = 0;
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // view
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // edit
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // copy
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // delete
}
?>
	<!-- Table header -->
	<tr class="ewTableHeader">
		<td valign="top">
<?php if ($m_barang->Export <> "") { ?>
kode barang
<?php } else { ?>
	<a href="m_baranglist.php?order=<?php echo urlencode('kode_barang') ?>&ordertype=<?php echo $m_barang->kode_barang->ReverseSort() ?>">kode barang<?php if ($m_barang->kode_barang->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_barang->kode_barang->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_barang->Export <> "") { ?>
no batch
<?php } else { ?>
	<a href="m_baranglist.php?order=<?php echo urlencode('no_batch') ?>&ordertype=<?php echo $m_barang->no_batch->ReverseSort() ?>">no batch&nbsp;(*)<?php if ($m_barang->no_batch->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_barang->no_batch->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_barang->Export <> "") { ?>
expiry
<?php } else { ?>
	<a href="m_baranglist.php?order=<?php echo urlencode('expiry') ?>&ordertype=<?php echo $m_barang->expiry->ReverseSort() ?>">expiry<?php if ($m_barang->expiry->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_barang->expiry->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_barang->Export <> "") { ?>
group barang
<?php } else { ?>
	<a href="m_baranglist.php?order=<?php echo urlencode('group_barang') ?>&ordertype=<?php echo $m_barang->group_barang->ReverseSort() ?>">group barang<?php if ($m_barang->group_barang->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_barang->group_barang->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_barang->Export <> "") { ?>
farmasi
<?php } else { ?>
	<a href="m_baranglist.php?order=<?php echo urlencode('farmasi') ?>&ordertype=<?php echo $m_barang->farmasi->ReverseSort() ?>">farmasi&nbsp;(*)<?php if ($m_barang->farmasi->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_barang->farmasi->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_barang->Export <> "") { ?>
satuan
<?php } else { ?>
	<a href="m_baranglist.php?order=<?php echo urlencode('satuan') ?>&ordertype=<?php echo $m_barang->satuan->ReverseSort() ?>">satuan&nbsp;(*)<?php if ($m_barang->satuan->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_barang->satuan->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_barang->Export <> "") { ?>
harga
<?php } else { ?>
	<a href="m_baranglist.php?order=<?php echo urlencode('harga') ?>&ordertype=<?php echo $m_barang->harga->ReverseSort() ?>">harga<?php if ($m_barang->harga->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_barang->harga->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_barang->Export <> "") { ?>
hide when print
<?php } else { ?>
	<a href="m_baranglist.php?order=<?php echo urlencode('hide_when_print') ?>&ordertype=<?php echo $m_barang->hide_when_print->ReverseSort() ?>">hide when print&nbsp;(*)<?php if ($m_barang->hide_when_print->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_barang->hide_when_print->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
<?php if ($m_barang->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php } ?>
	</tr>
<?php
if (defined("EW_EXPORT_ALL") && $m_barang->Export <> "") {
	$nStopRec = $nTotalRecs;
} else {
	$nStopRec = $nStartRec + $nDisplayRecs - 1; // Set the last record to display
}
$nRecCount = $nStartRec - 1;
if (!$rs->EOF) {
	$rs->MoveFirst();
	if (!$m_barang->SelectLimit) $rs->Move($nStartRec - 1); // Move to first record directly
}
$RowCnt = 0;
while (!$rs->EOF && $nRecCount < $nStopRec) {
	$nRecCount++;
	if (intval($nRecCount) >= intval($nStartRec)) {
		$RowCnt++;

	// Init row class and style
	$m_barang->CssClass = "ewTableRow";
	$m_barang->CssStyle = "";

	// Init row event
	$m_barang->RowClientEvents = "onmouseover='ew_MouseOver(this);' onmouseout='ew_MouseOut(this);' onclick='ew_Click(this);'";

	// Display alternate color for rows
	if ($RowCnt % 2 == 0) {
		$m_barang->CssClass = "ewTableAltRow";
	}
	LoadRowValues($rs); // Load row values
	$m_barang->RowType = EW_ROWTYPE_VIEW; // Render view
	RenderRow();
?>
	<!-- Table body -->
	<tr<?php echo $m_barang->DisplayAttributes() ?>>
		<!-- kode_barang -->
		<td<?php echo $m_barang->kode_barang->CellAttributes() ?>>
<div<?php echo $m_barang->kode_barang->ViewAttributes() ?>><?php echo $m_barang->kode_barang->ViewValue ?></div>
</td>
		<!-- no_batch -->
		<td<?php echo $m_barang->no_batch->CellAttributes() ?>>
<div<?php echo $m_barang->no_batch->ViewAttributes() ?>><?php echo $m_barang->no_batch->ViewValue ?></div>
</td>
		<!-- expiry -->
		<td<?php echo $m_barang->expiry->CellAttributes() ?>>
<div<?php echo $m_barang->expiry->ViewAttributes() ?>><?php echo $m_barang->expiry->ViewValue ?></div>
</td>
		<!-- group_barang -->
		<td<?php echo $m_barang->group_barang->CellAttributes() ?>>
<div<?php echo $m_barang->group_barang->ViewAttributes() ?>><?php echo $m_barang->group_barang->ViewValue ?></div>
</td>
		<!-- farmasi -->
		<td<?php echo $m_barang->farmasi->CellAttributes() ?>>
<div<?php echo $m_barang->farmasi->ViewAttributes() ?>><?php echo $m_barang->farmasi->ViewValue ?></div>
</td>
		<!-- satuan -->
		<td<?php echo $m_barang->satuan->CellAttributes() ?>>
<div<?php echo $m_barang->satuan->ViewAttributes() ?>><?php echo $m_barang->satuan->ViewValue ?></div>
</td>
		<!-- harga -->
		<td<?php echo $m_barang->harga->CellAttributes() ?>>
<div<?php echo $m_barang->harga->ViewAttributes() ?>><?php echo $m_barang->harga->ViewValue ?></div>
</td>
		<!-- hide_when_print -->
		<td<?php echo $m_barang->hide_when_print->CellAttributes() ?>>
<div<?php echo $m_barang->hide_when_print->ViewAttributes() ?>><?php echo $m_barang->hide_when_print->ViewValue ?></div>
</td>
<?php if ($m_barang->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_barang->ViewUrl() ?>">View</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_barang->EditUrl() ?>">Edit</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_barang->CopyUrl() ?>">Copy</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_barang->DeleteUrl() ?>">Delete</a>
</span></td>
<?php } ?>
<?php } ?>
	</tr>
<?php
	}
	$rs->MoveNext();
}
?>
</table>
<?php if ($m_barang->Export == "") { ?>
<table>
	<tr><td><span class="phpmaker">
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_barangadd.php">Add</a>&nbsp;&nbsp;
<?php } ?>
	</span></td></tr>
</table>
<?php } ?>
<?php } ?>
</form>
<?php

// Close recordset and connection
if ($rs) $rs->Close();
?>
<?php if ($m_barang->Export == "") { ?>
<form action="m_baranglist.php" name="ewpagerform" id="ewpagerform">
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td nowrap>
<?php if (!isset($Pager)) $Pager = new cPrevNextPager($nStartRec, $nDisplayRecs, $nTotalRecs) ?>
<?php if ($Pager->RecordCount > 0) { ?>
	<table border="0" cellspacing="0" cellpadding="0"><tr><td><span class="phpmaker">Page&nbsp;</span></td>
<!--first page button-->
	<?php if ($Pager->FirstButton->Enabled) { ?>
	<td><a href="m_baranglist.php?start=<?php echo $Pager->FirstButton->Start ?>"><img src="images/first.gif" alt="First" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/firstdisab.gif" alt="First" width="16" height="16" border="0"></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($Pager->PrevButton->Enabled) { ?>
	<td><a href="m_baranglist.php?start=<?php echo $Pager->PrevButton->Start ?>"><img src="images/prev.gif" alt="Previous" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/prevdisab.gif" alt="Previous" width="16" height="16" border="0"></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" id="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $Pager->CurrentPage ?>" size="4"></td>
<!--next page button-->
	<?php if ($Pager->NextButton->Enabled) { ?>
	<td><a href="m_baranglist.php?start=<?php echo $Pager->NextButton->Start ?>"><img src="images/next.gif" alt="Next" width="16" height="16" border="0"></a></td>	
	<?php } else { ?>
	<td><img src="images/nextdisab.gif" alt="Next" width="16" height="16" border="0"></td>
	<?php } ?>
<!--last page button-->
	<?php if ($Pager->LastButton->Enabled) { ?>
	<td><a href="m_baranglist.php?start=<?php echo $Pager->LastButton->Start ?>"><img src="images/last.gif" alt="Last" width="16" height="16" border="0"></a></td>	
	<?php } else { ?>
	<td><img src="images/lastdisab.gif" alt="Last" width="16" height="16" border="0"></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;of <?php echo $Pager->PageCount ?></span></td>
	</tr></table>
	<span class="phpmaker">Records <?php echo $Pager->FromIndex ?> to <?php echo $Pager->ToIndex ?> of <?php echo $Pager->RecordCount ?></span>
<?php } else { ?>
	<?php if ($sSrchWhere == "0=101") { ?>
	<span class="phpmaker">Please enter search criteria</span>
	<?php } else { ?>
	<span class="phpmaker">No records found</span>
	<?php } ?>
<?php } ?>
		</td>
	</tr>
</table>
</form>
<?php } ?>
<?php if ($m_barang->Export == "") { ?>
<?php } ?>
<?php if ($m_barang->Export == "") { ?>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php } ?>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Return Basic Search sql
function BasicSearchSQL($Keyword) {
	$sKeyword = ew_AdjustSql($Keyword);
	$sql = "";
	$sql .= "no_batch LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "nama_barang LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "group_barang LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "farmasi LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "satuan LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "hide_when_print LIKE '%" . $sKeyword . "%' OR ";
	if (substr($sql, -4) == " OR ") $sql = substr($sql, 0, strlen($sql)-4);
	return $sql;
}

// Return Basic Search Where based on search keyword and type
function BasicSearchWhere() {
	global $Security, $m_barang;
	$sSearchStr = "";
	$sSearchKeyword = ew_StripSlashes(@$_GET[EW_TABLE_BASIC_SEARCH]);
	$sSearchType = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	if ($sSearchKeyword <> "") {
		$sSearch = trim($sSearchKeyword);
		if ($sSearchType <> "") {
			while (strpos($sSearch, "  ") !== FALSE)
				$sSearch = str_replace("  ", " ", $sSearch);
			$arKeyword = explode(" ", trim($sSearch));
			foreach ($arKeyword as $sKeyword) {
				if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
				$sSearchStr .= "(" . BasicSearchSQL($sKeyword) . ")";
			}
		} else {
			$sSearchStr = BasicSearchSQL($sSearch);
		}
	}
	if ($sSearchKeyword <> "") {
		$m_barang->setBasicSearchKeyword($sSearchKeyword);
		$m_barang->setBasicSearchType($sSearchType);
	}
	return $sSearchStr;
}

// Clear all search parameters
function ResetSearchParms() {

	// Clear search where
	global $m_barang;
	$sSrchWhere = "";
	$m_barang->setSearchWhere($sSrchWhere);

	// Clear basic search parameters
	ResetBasicSearchParms();
}

// Clear all basic search parameters
function ResetBasicSearchParms() {

	// Clear basic search parameters
	global $m_barang;
	$m_barang->setBasicSearchKeyword("");
	$m_barang->setBasicSearchType("");
}

// Restore all search parameters
function RestoreSearchParms() {
	global $sSrchWhere, $m_barang;
	$sSrchWhere = $m_barang->getSearchWhere();
}

// Set up Sort parameters based on Sort Links clicked
function SetUpSortOrder() {
	global $m_barang;

	// Check for an Order parameter
	if (@$_GET["order"] <> "") {
		$m_barang->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
		$m_barang->CurrentOrderType = @$_GET["ordertype"];

		// Field kode_barang
		$m_barang->UpdateSort($m_barang->kode_barang);

		// Field no_batch
		$m_barang->UpdateSort($m_barang->no_batch);

		// Field expiry
		$m_barang->UpdateSort($m_barang->expiry);

		// Field group_barang
		$m_barang->UpdateSort($m_barang->group_barang);

		// Field farmasi
		$m_barang->UpdateSort($m_barang->farmasi);

		// Field satuan
		$m_barang->UpdateSort($m_barang->satuan);

		// Field harga
		$m_barang->UpdateSort($m_barang->harga);

		// Field hide_when_print
		$m_barang->UpdateSort($m_barang->hide_when_print);
		$m_barang->setStartRecordNumber(1); // Reset start position
	}
	$sOrderBy = $m_barang->getSessionOrderBy(); // Get order by from Session
	if ($sOrderBy == "") {
		if ($m_barang->SqlOrderBy() <> "") {
			$sOrderBy = $m_barang->SqlOrderBy();
			$m_barang->setSessionOrderBy($sOrderBy);
		}
	}
}

// Reset command based on querystring parameter cmd=
// - RESET: reset search parameters
// - RESETALL: reset search & master/detail parameters
// - RESETSORT: reset sort parameters
function ResetCmd() {
	global $sDbMasterFilter, $sDbDetailFilter, $nStartRec, $sOrderBy;
	global $m_barang;

	// Get reset cmd
	if (@$_GET["cmd"] <> "") {
		$sCmd = $_GET["cmd"];

		// Reset search criteria
		if (strtolower($sCmd) == "reset" || strtolower($sCmd) == "resetall") {
			ResetSearchParms();
		}

		// Reset Sort Criteria
		if (strtolower($sCmd) == "resetsort") {
			$sOrderBy = "";
			$m_barang->setSessionOrderBy($sOrderBy);
			$m_barang->kode_barang->setSort("");
			$m_barang->no_batch->setSort("");
			$m_barang->expiry->setSort("");
			$m_barang->group_barang->setSort("");
			$m_barang->farmasi->setSort("");
			$m_barang->satuan->setSort("");
			$m_barang->harga->setSort("");
			$m_barang->hide_when_print->setSort("");
		}

		// Reset start position
		$nStartRec = 1;
		$m_barang->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_barang;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_barang->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_barang->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_barang->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_barang->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_barang->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_barang->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_barang->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_barang;

	// Call Recordset Selecting event
	$m_barang->Recordset_Selecting($m_barang->CurrentFilter);

	// Load list page sql
	$sSql = $m_barang->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_barang->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_barang;
	$sFilter = $m_barang->SqlKeyFilter();
	if (!is_numeric($m_barang->kode_barang->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@kode_barang@", ew_AdjustSql($m_barang->kode_barang->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_barang->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_barang->CurrentFilter = $sFilter;
	$sSql = $m_barang->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_barang->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_barang;
	$m_barang->kode_barang->setDbValue($rs->fields('kode_barang'));
	$m_barang->no_batch->setDbValue($rs->fields('no_batch'));
	$m_barang->expiry->setDbValue($rs->fields('expiry'));
	$m_barang->nama_barang->setDbValue($rs->fields('nama_barang'));
	$m_barang->group_barang->setDbValue($rs->fields('group_barang'));
	$m_barang->farmasi->setDbValue($rs->fields('farmasi'));
	$m_barang->satuan->setDbValue($rs->fields('satuan'));
	$m_barang->harga->setDbValue($rs->fields('harga'));
	$m_barang->hide_when_print->setDbValue($rs->fields('hide_when_print'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_barang;

	// Call Row Rendering event
	$m_barang->Row_Rendering();

	// Common render codes for all row types
	// kode_barang

	$m_barang->kode_barang->CellCssStyle = "";
	$m_barang->kode_barang->CellCssClass = "";

	// no_batch
	$m_barang->no_batch->CellCssStyle = "";
	$m_barang->no_batch->CellCssClass = "";

	// expiry
	$m_barang->expiry->CellCssStyle = "";
	$m_barang->expiry->CellCssClass = "";

	// group_barang
	$m_barang->group_barang->CellCssStyle = "";
	$m_barang->group_barang->CellCssClass = "";

	// farmasi
	$m_barang->farmasi->CellCssStyle = "";
	$m_barang->farmasi->CellCssClass = "";

	// satuan
	$m_barang->satuan->CellCssStyle = "";
	$m_barang->satuan->CellCssClass = "";

	// harga
	$m_barang->harga->CellCssStyle = "";
	$m_barang->harga->CellCssClass = "";

	// hide_when_print
	$m_barang->hide_when_print->CellCssStyle = "";
	$m_barang->hide_when_print->CellCssClass = "";
	if ($m_barang->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode_barang
		$m_barang->kode_barang->ViewValue = $m_barang->kode_barang->CurrentValue;
		$m_barang->kode_barang->CssStyle = "";
		$m_barang->kode_barang->CssClass = "";
		$m_barang->kode_barang->ViewCustomAttributes = "";

		// no_batch
		$m_barang->no_batch->ViewValue = $m_barang->no_batch->CurrentValue;
		$m_barang->no_batch->CssStyle = "";
		$m_barang->no_batch->CssClass = "";
		$m_barang->no_batch->ViewCustomAttributes = "";

		// expiry
		$m_barang->expiry->ViewValue = $m_barang->expiry->CurrentValue;
		$m_barang->expiry->ViewValue = ew_FormatDateTime($m_barang->expiry->ViewValue, 5);
		$m_barang->expiry->CssStyle = "";
		$m_barang->expiry->CssClass = "";
		$m_barang->expiry->ViewCustomAttributes = "";

		// group_barang
		$m_barang->group_barang->CssStyle = "";
		$m_barang->group_barang->CssClass = "";
		$m_barang->group_barang->ViewCustomAttributes = "";

		// farmasi
		$m_barang->farmasi->ViewValue = $m_barang->farmasi->CurrentValue;
		$m_barang->farmasi->CssStyle = "";
		$m_barang->farmasi->CssClass = "";
		$m_barang->farmasi->ViewCustomAttributes = "";

		// satuan
		$m_barang->satuan->ViewValue = $m_barang->satuan->CurrentValue;
		$m_barang->satuan->CssStyle = "";
		$m_barang->satuan->CssClass = "";
		$m_barang->satuan->ViewCustomAttributes = "";

		// harga
		$m_barang->harga->ViewValue = $m_barang->harga->CurrentValue;
		$m_barang->harga->CssStyle = "";
		$m_barang->harga->CssClass = "";
		$m_barang->harga->ViewCustomAttributes = "";

		// hide_when_print
		$m_barang->hide_when_print->ViewValue = $m_barang->hide_when_print->CurrentValue;
		$m_barang->hide_when_print->CssStyle = "";
		$m_barang->hide_when_print->CssClass = "";
		$m_barang->hide_when_print->ViewCustomAttributes = "";

		// kode_barang
		$m_barang->kode_barang->HrefValue = "";

		// no_batch
		$m_barang->no_batch->HrefValue = "";

		// expiry
		$m_barang->expiry->HrefValue = "";

		// group_barang
		$m_barang->group_barang->HrefValue = "";

		// farmasi
		$m_barang->farmasi->HrefValue = "";

		// satuan
		$m_barang->satuan->HrefValue = "";

		// harga
		$m_barang->harga->HrefValue = "";

		// hide_when_print
		$m_barang->hide_when_print->HrefValue = "";
	} elseif ($m_barang->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_barang->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_barang->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_barang->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
