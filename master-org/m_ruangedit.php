<?php
define("EW_PAGE_ID", "edit", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_ruang', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_ruanginfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_ruang->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_ruang->Export; // Get export parameter, used in header
$sExportFile = $m_ruang->TableVar; // Get export file, used in header
?>
<?php

// Load key from QueryString
if (@$_GET["no"] <> "") {
	$m_ruang->no->setQueryStringValue($_GET["no"]);
}

// Create form object
$objForm = new cFormObj();
if (@$_POST["a_edit"] <> "") {
	$m_ruang->CurrentAction = $_POST["a_edit"]; // Get action code
	LoadFormValues(); // Get form values
} else {
	$m_ruang->CurrentAction = "I"; // Default action is display
}

// Check if valid key
if ($m_ruang->no->CurrentValue == "") Page_Terminate($m_ruang->getReturnUrl()); // Invalid key, exit
switch ($m_ruang->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load Record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
			Page_Terminate($m_ruang->getReturnUrl()); // Return to caller
		}
		break;
	Case "U": // Update
		$m_ruang->SendEmail = TRUE; // Send email on update success
		if (EditRow()) { // Update Record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "Update successful"; // Update success
			Page_Terminate($m_ruang->getReturnUrl()); // Return to caller
		} else {
			RestoreFormValues(); // Restore form values if update failed
		}
}

// Render the record
$m_ruang->RowType = EW_ROWTYPE_EDIT; // Render as edit
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "edit"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_nama"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - nama"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_kelas"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - kelas"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_ruang"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - ruang"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_jumlah_tt"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - jumlah tt"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_jumlah_tt"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - jumlah tt"))
				return false; 
		}
		elm = fobj.elements["x" + infix + "_ket_ruang"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - ket ruang"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_fasilitas"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - fasilitas"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_keterangan"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - keterangan"))
				return false;
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Edit TABLE: m ruang<br><br><a href="<?php echo $m_ruang->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form name="fm_ruangedit" id="fm_ruangedit" action="m_ruangedit.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_edit" id="a_edit" value="U">
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">no</td>
		<td<?php echo $m_ruang->no->CellAttributes() ?>><span id="cb_x_no">
<div<?php echo $m_ruang->no->ViewAttributes() ?>><?php echo $m_ruang->no->EditValue ?></div>
<input type="hidden" name="x_no" id="x_no" value="<?php echo ew_HtmlEncode($m_ruang->no->CurrentValue) ?>">
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">nama<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_ruang->nama->CellAttributes() ?>><span id="cb_x_nama">
<input type="text" name="x_nama" id="x_nama"  size="30" maxlength="20" value="<?php echo $m_ruang->nama->EditValue ?>"<?php echo $m_ruang->nama->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">kelas<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_ruang->kelas->CellAttributes() ?>><span id="cb_x_kelas">
<input type="text" name="x_kelas" id="x_kelas"  size="30" maxlength="10" value="<?php echo $m_ruang->kelas->EditValue ?>"<?php echo $m_ruang->kelas->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">ruang<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_ruang->ruang->CellAttributes() ?>><span id="cb_x_ruang">
<input type="text" name="x_ruang" id="x_ruang"  size="30" maxlength="25" value="<?php echo $m_ruang->ruang->EditValue ?>"<?php echo $m_ruang->ruang->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">jumlah tt<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_ruang->jumlah_tt->CellAttributes() ?>><span id="cb_x_jumlah_tt">
<input type="text" name="x_jumlah_tt" id="x_jumlah_tt"  size="30" value="<?php echo $m_ruang->jumlah_tt->EditValue ?>"<?php echo $m_ruang->jumlah_tt->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">ket ruang<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_ruang->ket_ruang->CellAttributes() ?>><span id="cb_x_ket_ruang">
<input type="text" name="x_ket_ruang" id="x_ket_ruang"  size="30" maxlength="25" value="<?php echo $m_ruang->ket_ruang->EditValue ?>"<?php echo $m_ruang->ket_ruang->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">fasilitas<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_ruang->fasilitas->CellAttributes() ?>><span id="cb_x_fasilitas">
<textarea name="x_fasilitas" id="x_fasilitas" cols="35" rows="4"<?php echo $m_ruang->fasilitas->EditAttributes() ?>><?php echo $m_ruang->fasilitas->EditValue ?></textarea>
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">keterangan<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_ruang->keterangan->CellAttributes() ?>><span id="cb_x_keterangan">
<textarea name="x_keterangan" id="x_keterangan" cols="35" rows="4"<?php echo $m_ruang->keterangan->EditAttributes() ?>><?php echo $m_ruang->keterangan->EditValue ?></textarea>
</span></td>
	</tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="   Edit   ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $m_ruang;
	$m_ruang->no->setFormValue($objForm->GetValue("x_no"));
	$m_ruang->nama->setFormValue($objForm->GetValue("x_nama"));
	$m_ruang->kelas->setFormValue($objForm->GetValue("x_kelas"));
	$m_ruang->ruang->setFormValue($objForm->GetValue("x_ruang"));
	$m_ruang->jumlah_tt->setFormValue($objForm->GetValue("x_jumlah_tt"));
	$m_ruang->ket_ruang->setFormValue($objForm->GetValue("x_ket_ruang"));
	$m_ruang->fasilitas->setFormValue($objForm->GetValue("x_fasilitas"));
	$m_ruang->keterangan->setFormValue($objForm->GetValue("x_keterangan"));
}

// Restore form values
function RestoreFormValues() {
	global $m_ruang;
	$m_ruang->no->CurrentValue = $m_ruang->no->FormValue;
	$m_ruang->nama->CurrentValue = $m_ruang->nama->FormValue;
	$m_ruang->kelas->CurrentValue = $m_ruang->kelas->FormValue;
	$m_ruang->ruang->CurrentValue = $m_ruang->ruang->FormValue;
	$m_ruang->jumlah_tt->CurrentValue = $m_ruang->jumlah_tt->FormValue;
	$m_ruang->ket_ruang->CurrentValue = $m_ruang->ket_ruang->FormValue;
	$m_ruang->fasilitas->CurrentValue = $m_ruang->fasilitas->FormValue;
	$m_ruang->keterangan->CurrentValue = $m_ruang->keterangan->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_ruang;
	$sFilter = $m_ruang->SqlKeyFilter();
	if (!is_numeric($m_ruang->no->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@no@", ew_AdjustSql($m_ruang->no->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_ruang->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_ruang->CurrentFilter = $sFilter;
	$sSql = $m_ruang->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_ruang->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_ruang;
	$m_ruang->no->setDbValue($rs->fields('no'));
	$m_ruang->nama->setDbValue($rs->fields('nama'));
	$m_ruang->kelas->setDbValue($rs->fields('kelas'));
	$m_ruang->ruang->setDbValue($rs->fields('ruang'));
	$m_ruang->jumlah_tt->setDbValue($rs->fields('jumlah_tt'));
	$m_ruang->ket_ruang->setDbValue($rs->fields('ket_ruang'));
	$m_ruang->fasilitas->setDbValue($rs->fields('fasilitas'));
	$m_ruang->keterangan->setDbValue($rs->fields('keterangan'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_ruang;

	// Call Row Rendering event
	$m_ruang->Row_Rendering();

	// Common render codes for all row types
	// no

	$m_ruang->no->CellCssStyle = "";
	$m_ruang->no->CellCssClass = "";

	// nama
	$m_ruang->nama->CellCssStyle = "";
	$m_ruang->nama->CellCssClass = "";

	// kelas
	$m_ruang->kelas->CellCssStyle = "";
	$m_ruang->kelas->CellCssClass = "";

	// ruang
	$m_ruang->ruang->CellCssStyle = "";
	$m_ruang->ruang->CellCssClass = "";

	// jumlah_tt
	$m_ruang->jumlah_tt->CellCssStyle = "";
	$m_ruang->jumlah_tt->CellCssClass = "";

	// ket_ruang
	$m_ruang->ket_ruang->CellCssStyle = "";
	$m_ruang->ket_ruang->CellCssClass = "";

	// fasilitas
	$m_ruang->fasilitas->CellCssStyle = "";
	$m_ruang->fasilitas->CellCssClass = "";

	// keterangan
	$m_ruang->keterangan->CellCssStyle = "";
	$m_ruang->keterangan->CellCssClass = "";
	if ($m_ruang->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($m_ruang->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_ruang->RowType == EW_ROWTYPE_EDIT) { // Edit row

		// no
		$m_ruang->no->EditCustomAttributes = "";
		$m_ruang->no->EditValue = $m_ruang->no->CurrentValue;
		$m_ruang->no->CssStyle = "";
		$m_ruang->no->CssClass = "";
		$m_ruang->no->ViewCustomAttributes = "";

		// nama
		$m_ruang->nama->EditCustomAttributes = "";
		$m_ruang->nama->EditValue = ew_HtmlEncode($m_ruang->nama->CurrentValue);

		// kelas
		$m_ruang->kelas->EditCustomAttributes = "";
		$m_ruang->kelas->EditValue = ew_HtmlEncode($m_ruang->kelas->CurrentValue);

		// ruang
		$m_ruang->ruang->EditCustomAttributes = "";
		$m_ruang->ruang->EditValue = ew_HtmlEncode($m_ruang->ruang->CurrentValue);

		// jumlah_tt
		$m_ruang->jumlah_tt->EditCustomAttributes = "";
		$m_ruang->jumlah_tt->EditValue = ew_HtmlEncode($m_ruang->jumlah_tt->CurrentValue);

		// ket_ruang
		$m_ruang->ket_ruang->EditCustomAttributes = "";
		$m_ruang->ket_ruang->EditValue = ew_HtmlEncode($m_ruang->ket_ruang->CurrentValue);

		// fasilitas
		$m_ruang->fasilitas->EditCustomAttributes = "";
		$m_ruang->fasilitas->EditValue = ew_HtmlEncode($m_ruang->fasilitas->CurrentValue);

		// keterangan
		$m_ruang->keterangan->EditCustomAttributes = "";
		$m_ruang->keterangan->EditValue = ew_HtmlEncode($m_ruang->keterangan->CurrentValue);
	} elseif ($m_ruang->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_ruang->Row_Rendered();
}
?>
<?php

// Update record based on key values
function EditRow() {
	global $conn, $Security, $m_ruang;
	$sFilter = $m_ruang->SqlKeyFilter();
	if (!is_numeric($m_ruang->no->CurrentValue)) {
		return FALSE;
	}
	$sFilter = str_replace("@no@", ew_AdjustSql($m_ruang->no->CurrentValue), $sFilter); // Replace key value
	$m_ruang->CurrentFilter = $sFilter;
	$sSql = $m_ruang->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE)
		return FALSE;
	if ($rs->EOF) {
		$EditRow = FALSE; // Update Failed
	} else {

		// Save old values
		$rsold =& $rs->fields;
		$rsnew = array();

		// Field no
		// Field nama

		$m_ruang->nama->SetDbValueDef($m_ruang->nama->CurrentValue, "");
		$rsnew['nama'] =& $m_ruang->nama->DbValue;

		// Field kelas
		$m_ruang->kelas->SetDbValueDef($m_ruang->kelas->CurrentValue, "");
		$rsnew['kelas'] =& $m_ruang->kelas->DbValue;

		// Field ruang
		$m_ruang->ruang->SetDbValueDef($m_ruang->ruang->CurrentValue, "");
		$rsnew['ruang'] =& $m_ruang->ruang->DbValue;

		// Field jumlah_tt
		$m_ruang->jumlah_tt->SetDbValueDef($m_ruang->jumlah_tt->CurrentValue, 0);
		$rsnew['jumlah_tt'] =& $m_ruang->jumlah_tt->DbValue;

		// Field ket_ruang
		$m_ruang->ket_ruang->SetDbValueDef($m_ruang->ket_ruang->CurrentValue, "");
		$rsnew['ket_ruang'] =& $m_ruang->ket_ruang->DbValue;

		// Field fasilitas
		$m_ruang->fasilitas->SetDbValueDef($m_ruang->fasilitas->CurrentValue, "");
		$rsnew['fasilitas'] =& $m_ruang->fasilitas->DbValue;

		// Field keterangan
		$m_ruang->keterangan->SetDbValueDef($m_ruang->keterangan->CurrentValue, "");
		$rsnew['keterangan'] =& $m_ruang->keterangan->DbValue;

		// Call Row Updating event
		$bUpdateRow = $m_ruang->Row_Updating($rsold, $rsnew);
		if ($bUpdateRow) {
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$EditRow = $conn->Execute($m_ruang->UpdateSQL($rsnew));
			$conn->raiseErrorFn = '';
		} else {
			if ($m_ruang->CancelMessage <> "") {
				$_SESSION[EW_SESSION_MESSAGE] = $m_ruang->CancelMessage;
				$m_ruang->CancelMessage = "";
			} else {
				$_SESSION[EW_SESSION_MESSAGE] = "Update cancelled";
			}
			$EditRow = FALSE;
		}
	}

	// Call Row Updated event
	if ($EditRow) {
		$m_ruang->Row_Updated($rsold, $rsnew);
	}
	$rs->Close();
	return $EditRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
