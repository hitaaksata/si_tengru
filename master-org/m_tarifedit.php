<?php
define("EW_PAGE_ID", "edit", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_tarif', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_tarifinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_tarif->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_tarif->Export; // Get export parameter, used in header
$sExportFile = $m_tarif->TableVar; // Get export file, used in header
?>
<?php

// Load key from QueryString
if (@$_GET["kode"] <> "") {
	$m_tarif->kode->setQueryStringValue($_GET["kode"]);
}

// Create form object
$objForm = new cFormObj();
if (@$_POST["a_edit"] <> "") {
	$m_tarif->CurrentAction = $_POST["a_edit"]; // Get action code
	LoadFormValues(); // Get form values
} else {
	$m_tarif->CurrentAction = "I"; // Default action is display
}

// Check if valid key
if ($m_tarif->kode->CurrentValue == "") Page_Terminate($m_tarif->getReturnUrl()); // Invalid key, exit
switch ($m_tarif->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load Record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
			Page_Terminate($m_tarif->getReturnUrl()); // Return to caller
		}
		break;
	Case "U": // Update
		$m_tarif->SendEmail = TRUE; // Send email on update success
		if (EditRow()) { // Update Record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "Update successful"; // Update success
			Page_Terminate($m_tarif->getReturnUrl()); // Return to caller
		} else {
			RestoreFormValues(); // Restore form values if update failed
		}
}

// Render the record
$m_tarif->RowType = EW_ROWTYPE_EDIT; // Render as edit
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "edit"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_kode"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - kode"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_group_jasa"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - group jasa"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_kode_jasa"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - kode jasa"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_nama_jasa"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - nama jasa"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_tarif"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - tarif"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_tarif"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - tarif"))
				return false; 
		}
		elm = fobj.elements["x" + infix + "_askes"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - askes"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_askes"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - askes"))
				return false; 
		}
		elm = fobj.elements["x" + infix + "_cost_sharing"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - cost sharing"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_cost_sharing"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - cost sharing"))
				return false; 
		}
		elm = fobj.elements["x" + infix + "_jasa_sarana"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - jasa sarana"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_jasa_sarana"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - jasa sarana"))
				return false; 
		}
		elm = fobj.elements["x" + infix + "_jasa_pelayanan"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - jasa pelayanan"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_jasa_pelayanan"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - jasa pelayanan"))
				return false; 
		}
		elm = fobj.elements["x" + infix + "_jasa_dokter"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - jasa dokter"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_jasa_dokter"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - jasa dokter"))
				return false; 
		}
		elm = fobj.elements["x" + infix + "_tim_rs"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - tim rs"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_tim_rs"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - tim rs"))
				return false; 
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Edit TABLE: m tarif<br><br><a href="<?php echo $m_tarif->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form name="fm_tarifedit" id="fm_tarifedit" action="m_tarifedit.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_edit" id="a_edit" value="U">
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">kode<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_tarif->kode->CellAttributes() ?>><span id="cb_x_kode">
<div<?php echo $m_tarif->kode->ViewAttributes() ?>><?php echo $m_tarif->kode->EditValue ?></div>
<input type="hidden" name="x_kode" id="x_kode" value="<?php echo ew_HtmlEncode($m_tarif->kode->CurrentValue) ?>">
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">group jasa<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_tarif->group_jasa->CellAttributes() ?>><span id="cb_x_group_jasa">
<input type="text" name="x_group_jasa" id="x_group_jasa"  size="30" maxlength="8" value="<?php echo $m_tarif->group_jasa->EditValue ?>"<?php echo $m_tarif->group_jasa->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">kode jasa<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_tarif->kode_jasa->CellAttributes() ?>><span id="cb_x_kode_jasa">
<input type="text" name="x_kode_jasa" id="x_kode_jasa"  size="30" maxlength="8" value="<?php echo $m_tarif->kode_jasa->EditValue ?>"<?php echo $m_tarif->kode_jasa->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">nama jasa<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_tarif->nama_jasa->CellAttributes() ?>><span id="cb_x_nama_jasa">
<textarea name="x_nama_jasa" id="x_nama_jasa" cols="35" rows="4"<?php echo $m_tarif->nama_jasa->EditAttributes() ?>><?php echo $m_tarif->nama_jasa->EditValue ?></textarea>
</span></td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">tarif<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_tarif->tarif->CellAttributes() ?>><span id="cb_x_tarif">
<input type="text" name="x_tarif" id="x_tarif"  size="30" value="<?php echo $m_tarif->tarif->EditValue ?>"<?php echo $m_tarif->tarif->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">askes<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_tarif->askes->CellAttributes() ?>><span id="cb_x_askes">
<input type="text" name="x_askes" id="x_askes"  size="30" value="<?php echo $m_tarif->askes->EditValue ?>"<?php echo $m_tarif->askes->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">cost sharing<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_tarif->cost_sharing->CellAttributes() ?>><span id="cb_x_cost_sharing">
<input type="text" name="x_cost_sharing" id="x_cost_sharing"  size="30" value="<?php echo $m_tarif->cost_sharing->EditValue ?>"<?php echo $m_tarif->cost_sharing->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">jasa sarana<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_tarif->jasa_sarana->CellAttributes() ?>><span id="cb_x_jasa_sarana">
<input type="text" name="x_jasa_sarana" id="x_jasa_sarana"  size="30" value="<?php echo $m_tarif->jasa_sarana->EditValue ?>"<?php echo $m_tarif->jasa_sarana->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">jasa pelayanan<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_tarif->jasa_pelayanan->CellAttributes() ?>><span id="cb_x_jasa_pelayanan">
<input type="text" name="x_jasa_pelayanan" id="x_jasa_pelayanan"  size="30" value="<?php echo $m_tarif->jasa_pelayanan->EditValue ?>"<?php echo $m_tarif->jasa_pelayanan->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">jasa dokter<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_tarif->jasa_dokter->CellAttributes() ?>><span id="cb_x_jasa_dokter">
<input type="text" name="x_jasa_dokter" id="x_jasa_dokter"  size="30" value="<?php echo $m_tarif->jasa_dokter->EditValue ?>"<?php echo $m_tarif->jasa_dokter->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">tim rs<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_tarif->tim_rs->CellAttributes() ?>><span id="cb_x_tim_rs">
<input type="text" name="x_tim_rs" id="x_tim_rs"  size="30" value="<?php echo $m_tarif->tim_rs->EditValue ?>"<?php echo $m_tarif->tim_rs->EditAttributes() ?>>
</span></td>
	</tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="   Edit   ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $m_tarif;
	$m_tarif->kode->setFormValue($objForm->GetValue("x_kode"));
	$m_tarif->group_jasa->setFormValue($objForm->GetValue("x_group_jasa"));
	$m_tarif->kode_jasa->setFormValue($objForm->GetValue("x_kode_jasa"));
	$m_tarif->nama_jasa->setFormValue($objForm->GetValue("x_nama_jasa"));
	$m_tarif->tarif->setFormValue($objForm->GetValue("x_tarif"));
	$m_tarif->askes->setFormValue($objForm->GetValue("x_askes"));
	$m_tarif->cost_sharing->setFormValue($objForm->GetValue("x_cost_sharing"));
	$m_tarif->jasa_sarana->setFormValue($objForm->GetValue("x_jasa_sarana"));
	$m_tarif->jasa_pelayanan->setFormValue($objForm->GetValue("x_jasa_pelayanan"));
	$m_tarif->jasa_dokter->setFormValue($objForm->GetValue("x_jasa_dokter"));
	$m_tarif->tim_rs->setFormValue($objForm->GetValue("x_tim_rs"));
}

// Restore form values
function RestoreFormValues() {
	global $m_tarif;
	$m_tarif->kode->CurrentValue = $m_tarif->kode->FormValue;
	$m_tarif->group_jasa->CurrentValue = $m_tarif->group_jasa->FormValue;
	$m_tarif->kode_jasa->CurrentValue = $m_tarif->kode_jasa->FormValue;
	$m_tarif->nama_jasa->CurrentValue = $m_tarif->nama_jasa->FormValue;
	$m_tarif->tarif->CurrentValue = $m_tarif->tarif->FormValue;
	$m_tarif->askes->CurrentValue = $m_tarif->askes->FormValue;
	$m_tarif->cost_sharing->CurrentValue = $m_tarif->cost_sharing->FormValue;
	$m_tarif->jasa_sarana->CurrentValue = $m_tarif->jasa_sarana->FormValue;
	$m_tarif->jasa_pelayanan->CurrentValue = $m_tarif->jasa_pelayanan->FormValue;
	$m_tarif->jasa_dokter->CurrentValue = $m_tarif->jasa_dokter->FormValue;
	$m_tarif->tim_rs->CurrentValue = $m_tarif->tim_rs->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_tarif;
	$sFilter = $m_tarif->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($m_tarif->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_tarif->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_tarif->CurrentFilter = $sFilter;
	$sSql = $m_tarif->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_tarif->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_tarif;
	$m_tarif->kode->setDbValue($rs->fields('kode'));
	$m_tarif->group_jasa->setDbValue($rs->fields('group_jasa'));
	$m_tarif->kode_jasa->setDbValue($rs->fields('kode_jasa'));
	$m_tarif->nama_jasa->setDbValue($rs->fields('nama_jasa'));
	$m_tarif->tarif->setDbValue($rs->fields('tarif'));
	$m_tarif->askes->setDbValue($rs->fields('askes'));
	$m_tarif->cost_sharing->setDbValue($rs->fields('cost_sharing'));
	$m_tarif->jasa_sarana->setDbValue($rs->fields('jasa_sarana'));
	$m_tarif->jasa_pelayanan->setDbValue($rs->fields('jasa_pelayanan'));
	$m_tarif->jasa_dokter->setDbValue($rs->fields('jasa_dokter'));
	$m_tarif->tim_rs->setDbValue($rs->fields('tim_rs'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_tarif;

	// Call Row Rendering event
	$m_tarif->Row_Rendering();

	// Common render codes for all row types
	// kode

	$m_tarif->kode->CellCssStyle = "";
	$m_tarif->kode->CellCssClass = "";

	// group_jasa
	$m_tarif->group_jasa->CellCssStyle = "";
	$m_tarif->group_jasa->CellCssClass = "";

	// kode_jasa
	$m_tarif->kode_jasa->CellCssStyle = "";
	$m_tarif->kode_jasa->CellCssClass = "";

	// nama_jasa
	$m_tarif->nama_jasa->CellCssStyle = "";
	$m_tarif->nama_jasa->CellCssClass = "";

	// tarif
	$m_tarif->tarif->CellCssStyle = "";
	$m_tarif->tarif->CellCssClass = "";

	// askes
	$m_tarif->askes->CellCssStyle = "";
	$m_tarif->askes->CellCssClass = "";

	// cost_sharing
	$m_tarif->cost_sharing->CellCssStyle = "";
	$m_tarif->cost_sharing->CellCssClass = "";

	// jasa_sarana
	$m_tarif->jasa_sarana->CellCssStyle = "";
	$m_tarif->jasa_sarana->CellCssClass = "";

	// jasa_pelayanan
	$m_tarif->jasa_pelayanan->CellCssStyle = "";
	$m_tarif->jasa_pelayanan->CellCssClass = "";

	// jasa_dokter
	$m_tarif->jasa_dokter->CellCssStyle = "";
	$m_tarif->jasa_dokter->CellCssClass = "";

	// tim_rs
	$m_tarif->tim_rs->CellCssStyle = "";
	$m_tarif->tim_rs->CellCssClass = "";
	if ($m_tarif->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($m_tarif->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_tarif->RowType == EW_ROWTYPE_EDIT) { // Edit row

		// kode
		$m_tarif->kode->EditCustomAttributes = "";
		$m_tarif->kode->EditValue = $m_tarif->kode->CurrentValue;
		$m_tarif->kode->CssStyle = "";
		$m_tarif->kode->CssClass = "";
		$m_tarif->kode->ViewCustomAttributes = "";

		// group_jasa
		$m_tarif->group_jasa->EditCustomAttributes = "";
		$m_tarif->group_jasa->EditValue = ew_HtmlEncode($m_tarif->group_jasa->CurrentValue);

		// kode_jasa
		$m_tarif->kode_jasa->EditCustomAttributes = "";
		$m_tarif->kode_jasa->EditValue = ew_HtmlEncode($m_tarif->kode_jasa->CurrentValue);

		// nama_jasa
		$m_tarif->nama_jasa->EditCustomAttributes = "";
		$m_tarif->nama_jasa->EditValue = ew_HtmlEncode($m_tarif->nama_jasa->CurrentValue);

		// tarif
		$m_tarif->tarif->EditCustomAttributes = "";
		$m_tarif->tarif->EditValue = ew_HtmlEncode($m_tarif->tarif->CurrentValue);

		// askes
		$m_tarif->askes->EditCustomAttributes = "";
		$m_tarif->askes->EditValue = ew_HtmlEncode($m_tarif->askes->CurrentValue);

		// cost_sharing
		$m_tarif->cost_sharing->EditCustomAttributes = "";
		$m_tarif->cost_sharing->EditValue = ew_HtmlEncode($m_tarif->cost_sharing->CurrentValue);

		// jasa_sarana
		$m_tarif->jasa_sarana->EditCustomAttributes = "";
		$m_tarif->jasa_sarana->EditValue = ew_HtmlEncode($m_tarif->jasa_sarana->CurrentValue);

		// jasa_pelayanan
		$m_tarif->jasa_pelayanan->EditCustomAttributes = "";
		$m_tarif->jasa_pelayanan->EditValue = ew_HtmlEncode($m_tarif->jasa_pelayanan->CurrentValue);

		// jasa_dokter
		$m_tarif->jasa_dokter->EditCustomAttributes = "";
		$m_tarif->jasa_dokter->EditValue = ew_HtmlEncode($m_tarif->jasa_dokter->CurrentValue);

		// tim_rs
		$m_tarif->tim_rs->EditCustomAttributes = "";
		$m_tarif->tim_rs->EditValue = ew_HtmlEncode($m_tarif->tim_rs->CurrentValue);
	} elseif ($m_tarif->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_tarif->Row_Rendered();
}
?>
<?php

// Update record based on key values
function EditRow() {
	global $conn, $Security, $m_tarif;
	$sFilter = $m_tarif->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($m_tarif->kode->CurrentValue), $sFilter); // Replace key value
	$m_tarif->CurrentFilter = $sFilter;
	$sSql = $m_tarif->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE)
		return FALSE;
	if ($rs->EOF) {
		$EditRow = FALSE; // Update Failed
	} else {

		// Save old values
		$rsold =& $rs->fields;
		$rsnew = array();

		// Field kode
		// Field group_jasa

		$m_tarif->group_jasa->SetDbValueDef($m_tarif->group_jasa->CurrentValue, "");
		$rsnew['group_jasa'] =& $m_tarif->group_jasa->DbValue;

		// Field kode_jasa
		$m_tarif->kode_jasa->SetDbValueDef($m_tarif->kode_jasa->CurrentValue, "");
		$rsnew['kode_jasa'] =& $m_tarif->kode_jasa->DbValue;

		// Field nama_jasa
		$m_tarif->nama_jasa->SetDbValueDef($m_tarif->nama_jasa->CurrentValue, "");
		$rsnew['nama_jasa'] =& $m_tarif->nama_jasa->DbValue;

		// Field tarif
		$m_tarif->tarif->SetDbValueDef($m_tarif->tarif->CurrentValue, 0);
		$rsnew['tarif'] =& $m_tarif->tarif->DbValue;

		// Field askes
		$m_tarif->askes->SetDbValueDef($m_tarif->askes->CurrentValue, 0);
		$rsnew['askes'] =& $m_tarif->askes->DbValue;

		// Field cost_sharing
		$m_tarif->cost_sharing->SetDbValueDef($m_tarif->cost_sharing->CurrentValue, 0);
		$rsnew['cost_sharing'] =& $m_tarif->cost_sharing->DbValue;

		// Field jasa_sarana
		$m_tarif->jasa_sarana->SetDbValueDef($m_tarif->jasa_sarana->CurrentValue, 0);
		$rsnew['jasa_sarana'] =& $m_tarif->jasa_sarana->DbValue;

		// Field jasa_pelayanan
		$m_tarif->jasa_pelayanan->SetDbValueDef($m_tarif->jasa_pelayanan->CurrentValue, 0);
		$rsnew['jasa_pelayanan'] =& $m_tarif->jasa_pelayanan->DbValue;

		// Field jasa_dokter
		$m_tarif->jasa_dokter->SetDbValueDef($m_tarif->jasa_dokter->CurrentValue, 0);
		$rsnew['jasa_dokter'] =& $m_tarif->jasa_dokter->DbValue;

		// Field tim_rs
		$m_tarif->tim_rs->SetDbValueDef($m_tarif->tim_rs->CurrentValue, 0);
		$rsnew['tim_rs'] =& $m_tarif->tim_rs->DbValue;

		// Call Row Updating event
		$bUpdateRow = $m_tarif->Row_Updating($rsold, $rsnew);
		if ($bUpdateRow) {
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$EditRow = $conn->Execute($m_tarif->UpdateSQL($rsnew));
			$conn->raiseErrorFn = '';
		} else {
			if ($m_tarif->CancelMessage <> "") {
				$_SESSION[EW_SESSION_MESSAGE] = $m_tarif->CancelMessage;
				$m_tarif->CancelMessage = "";
			} else {
				$_SESSION[EW_SESSION_MESSAGE] = "Update cancelled";
			}
			$EditRow = FALSE;
		}
	}

	// Call Row Updated event
	if ($EditRow) {
		$m_tarif->Row_Updated($rsold, $rsnew);
	}
	$rs->Close();
	return $EditRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
