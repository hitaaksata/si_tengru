<?php
define("EW_PAGE_ID", "view", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_dpmp', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_dpmpinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_dpmp->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_dpmp->Export; // Get export parameter, used in header
$sExportFile = $m_dpmp->TableVar; // Get export file, used in header
?>
<?php
if (@$_GET["KDMAKANAN"] <> "") {
	$m_dpmp->KDMAKANAN->setQueryStringValue($_GET["KDMAKANAN"]);
} else {
	Page_Terminate("m_dpmplist.php"); // Return to list page
}

// Get action
if (@$_POST["a_view"] <> "") {
	$m_dpmp->CurrentAction = $_POST["a_view"];
} else {
	$m_dpmp->CurrentAction = "I"; // Display form
}
switch ($m_dpmp->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // Set no record message
			Page_Terminate("m_dpmplist.php"); // Return to list
		}
}

// Set return url
$m_dpmp->setReturnUrl("m_dpmpview.php");

// Render row
$m_dpmp->RowType = EW_ROWTYPE_VIEW;
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "view"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">View TABLE: m dpmp
<br><br>
<a href="m_dpmplist.php">Back to List</a>&nbsp;
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_dpmpadd.php">Add</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_dpmp->EditUrl() ?>">Edit</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_dpmp->CopyUrl() ?>">Copy</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_dpmp->DeleteUrl() ?>">Delete</a>&nbsp;
<?php } ?>
</span>
</p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<p>
<form>
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">KDMAKANAN</td>
		<td<?php echo $m_dpmp->KDMAKANAN->CellAttributes() ?>>
<div<?php echo $m_dpmp->KDMAKANAN->ViewAttributes() ?>><?php echo $m_dpmp->KDMAKANAN->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">GROUPMAKANAN</td>
		<td<?php echo $m_dpmp->GROUPMAKANAN->CellAttributes() ?>>
<div<?php echo $m_dpmp->GROUPMAKANAN->ViewAttributes() ?>><?php echo $m_dpmp->GROUPMAKANAN->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">NAMAMAKANAN</td>
		<td<?php echo $m_dpmp->NAMAMAKANAN->CellAttributes() ?>>
<div<?php echo $m_dpmp->NAMAMAKANAN->ViewAttributes() ?>><?php echo $m_dpmp->NAMAMAKANAN->ViewValue ?></div>
</td>
	</tr>
</table>
</form>
<p>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_dpmp;
	$sFilter = $m_dpmp->SqlKeyFilter();
	if (!is_numeric($m_dpmp->KDMAKANAN->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@KDMAKANAN@", ew_AdjustSql($m_dpmp->KDMAKANAN->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_dpmp->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_dpmp->CurrentFilter = $sFilter;
	$sSql = $m_dpmp->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_dpmp->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_dpmp;
	$m_dpmp->KDMAKANAN->setDbValue($rs->fields('KDMAKANAN'));
	$m_dpmp->GROUPMAKANAN->setDbValue($rs->fields('GROUPMAKANAN'));
	$m_dpmp->NAMAMAKANAN->setDbValue($rs->fields('NAMAMAKANAN'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_dpmp;

	// Call Row Rendering event
	$m_dpmp->Row_Rendering();

	// Common render codes for all row types
	// KDMAKANAN

	$m_dpmp->KDMAKANAN->CellCssStyle = "";
	$m_dpmp->KDMAKANAN->CellCssClass = "";

	// GROUPMAKANAN
	$m_dpmp->GROUPMAKANAN->CellCssStyle = "";
	$m_dpmp->GROUPMAKANAN->CellCssClass = "";

	// NAMAMAKANAN
	$m_dpmp->NAMAMAKANAN->CellCssStyle = "";
	$m_dpmp->NAMAMAKANAN->CellCssClass = "";
	if ($m_dpmp->RowType == EW_ROWTYPE_VIEW) { // View row

		// KDMAKANAN
		$m_dpmp->KDMAKANAN->ViewValue = $m_dpmp->KDMAKANAN->CurrentValue;
		$m_dpmp->KDMAKANAN->CssStyle = "";
		$m_dpmp->KDMAKANAN->CssClass = "";
		$m_dpmp->KDMAKANAN->ViewCustomAttributes = "";

		// GROUPMAKANAN
		$m_dpmp->GROUPMAKANAN->ViewValue = $m_dpmp->GROUPMAKANAN->CurrentValue;
		$m_dpmp->GROUPMAKANAN->CssStyle = "";
		$m_dpmp->GROUPMAKANAN->CssClass = "";
		$m_dpmp->GROUPMAKANAN->ViewCustomAttributes = "";

		// NAMAMAKANAN
		$m_dpmp->NAMAMAKANAN->ViewValue = $m_dpmp->NAMAMAKANAN->CurrentValue;
		$m_dpmp->NAMAMAKANAN->CssStyle = "";
		$m_dpmp->NAMAMAKANAN->CssClass = "";
		$m_dpmp->NAMAMAKANAN->ViewCustomAttributes = "";

		// KDMAKANAN
		$m_dpmp->KDMAKANAN->HrefValue = "";

		// GROUPMAKANAN
		$m_dpmp->GROUPMAKANAN->HrefValue = "";

		// NAMAMAKANAN
		$m_dpmp->NAMAMAKANAN->HrefValue = "";
	} elseif ($m_dpmp->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_dpmp->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_dpmp->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_dpmp->Row_Rendered();
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_dpmp;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_dpmp->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_dpmp->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_dpmp->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_dpmp->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_dpmp->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_dpmp->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_dpmp->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
