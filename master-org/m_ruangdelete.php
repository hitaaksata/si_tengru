<?php
define("EW_PAGE_ID", "delete", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_ruang', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_ruanginfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_ruang->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_ruang->Export; // Get export parameter, used in header
$sExportFile = $m_ruang->TableVar; // Get export file, used in header
?>
<?php

// Load Key Parameters
$sKey = "";
$bSingleDelete = TRUE; // Initialize as single delete
$arRecKeys = array();
$nKeySelected = 0; // Initialize selected key count
$sFilter = "";
if (@$_GET["no"] <> "") {
	$m_ruang->no->setQueryStringValue($_GET["no"]);
	if (!is_numeric($m_ruang->no->QueryStringValue)) {
		Page_Terminate($m_ruang->getReturnUrl()); // Prevent sql injection, exit
	}
	$sKey .= $m_ruang->no->QueryStringValue;
} else {
	$bSingleDelete = FALSE;
}
if ($bSingleDelete) {
	$nKeySelected = 1; // Set up key selected count
	$arRecKeys[0] = $sKey;
} else {
	if (isset($_POST["key_m"])) { // Key in form
		$nKeySelected = count($_POST["key_m"]); // Set up key selected count
		$arRecKeys = ew_StripSlashes($_POST["key_m"]);
	}
}
if ($nKeySelected <= 0) Page_Terminate($m_ruang->getReturnUrl()); // No key specified, exit

// Build filter
foreach ($arRecKeys as $sKey) {
	$sFilter .= "(";

	// Set up key field
	$sKeyFld = $sKey;
	if (!is_numeric($sKeyFld)) {
		Page_Terminate($m_ruang->getReturnUrl()); // Prevent sql injection, exit
	}
	$sFilter .= "no=" . ew_AdjustSql($sKeyFld) . " AND ";
	if (substr($sFilter, -5) == " AND ") $sFilter = substr($sFilter, 0, strlen($sFilter)-5) . ") OR ";
}
if (substr($sFilter, -4) == " OR ") $sFilter = substr($sFilter, 0, strlen($sFilter)-4);

// Set up filter (Sql Where Clause) and get Return Sql
// Sql constructor in m_ruang class, m_ruanginfo.php

$m_ruang->CurrentFilter = $sFilter;

// Get action
if (@$_POST["a_delete"] <> "") {
	$m_ruang->CurrentAction = $_POST["a_delete"];
} else {
	$m_ruang->CurrentAction = "I"; // Display record
}
switch ($m_ruang->CurrentAction) {
	case "D": // Delete
		$m_ruang->SendEmail = TRUE; // Send email on delete success
		if (DeleteRows()) { // delete rows
			$_SESSION[EW_SESSION_MESSAGE] = "Delete Successful"; // Set up success message
			Page_Terminate($m_ruang->getReturnUrl()); // Return to caller
		}
}

// Load records for display
$rs = LoadRecordset();
$nTotalRecs = $rs->RecordCount(); // Get record count
if ($nTotalRecs <= 0) { // No record found, exit
	$rs->Close();
	Page_Terminate($m_ruang->getReturnUrl()); // Return to caller
}
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "delete"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Delete from TABLE: m ruang<br><br><a href="<?php echo $m_ruang->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form action="m_ruangdelete.php" method="post">
<p>
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($arRecKeys as $sKey) { ?>
<input type="hidden" name="key_m[]" id="key_m[]" value="<?php echo ew_HtmlEncode($sKey) ?>">
<?php } ?>
<table class="ewTable">
	<tr class="ewTableHeader">
		<td valign="top">no</td>
		<td valign="top">nama</td>
		<td valign="top">kelas</td>
		<td valign="top">ruang</td>
		<td valign="top">jumlah tt</td>
		<td valign="top">ket ruang</td>
	</tr>
<?php
$nRecCount = 0;
$i = 0;
while (!$rs->EOF) {
	$nRecCount++;

	// Set row class and style
	$m_ruang->CssClass = "ewTableRow";
	$m_ruang->CssStyle = "";

	// Display alternate color for rows
	if ($nRecCount % 2 <> 1) {
		$m_ruang->CssClass = "ewTableAltRow";
	}

	// Get the field contents
	LoadRowValues($rs);

	// Render row value
	$m_ruang->RowType = EW_ROWTYPE_VIEW; // view
	RenderRow();
?>
	<tr<?php echo $m_ruang->DisplayAttributes() ?>>
		<td<?php echo $m_ruang->no->CellAttributes() ?>>
<div<?php echo $m_ruang->no->ViewAttributes() ?>><?php echo $m_ruang->no->ViewValue ?></div>
</td>
		<td<?php echo $m_ruang->nama->CellAttributes() ?>>
<div<?php echo $m_ruang->nama->ViewAttributes() ?>><?php echo $m_ruang->nama->ViewValue ?></div>
</td>
		<td<?php echo $m_ruang->kelas->CellAttributes() ?>>
<div<?php echo $m_ruang->kelas->ViewAttributes() ?>><?php echo $m_ruang->kelas->ViewValue ?></div>
</td>
		<td<?php echo $m_ruang->ruang->CellAttributes() ?>>
<div<?php echo $m_ruang->ruang->ViewAttributes() ?>><?php echo $m_ruang->ruang->ViewValue ?></div>
</td>
		<td<?php echo $m_ruang->jumlah_tt->CellAttributes() ?>>
<div<?php echo $m_ruang->jumlah_tt->ViewAttributes() ?>><?php echo $m_ruang->jumlah_tt->ViewValue ?></div>
</td>
		<td<?php echo $m_ruang->ket_ruang->CellAttributes() ?>>
<div<?php echo $m_ruang->ket_ruang->ViewAttributes() ?>><?php echo $m_ruang->ket_ruang->ViewValue ?></div>
</td>
	</tr>
<?php
	$rs->MoveNext();
}
$rs->Close();
?>
</table>
<p>
<input type="submit" name="Action" id="Action" value="Confirm Delete">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// ------------------------------------------------
//  Function DeleteRows
//  - Delete Records based on current filter
function DeleteRows() {
	global $conn, $Security, $m_ruang;
	$DeleteRows = TRUE;
	$sWrkFilter = $m_ruang->CurrentFilter;

	// Set up filter (Sql Where Clause) and get Return Sql
	// Sql constructor in m_ruang class, m_ruanginfo.php

	$m_ruang->CurrentFilter = $sWrkFilter;
	$sSql = $m_ruang->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE) {
		return FALSE;
	} elseif ($rs->EOF) {
		$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
		$rs->Close();
		return FALSE;
	}
	$conn->BeginTrans();

	// Clone old rows
	$rsold = ($rs) ? $rs->GetRows() : array();
	if ($rs) $rs->Close();

	// Call row deleting event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$DeleteRows = $m_ruang->Row_Deleting($row);
			if (!$DeleteRows) break;
		}
	}
	if ($DeleteRows) {
		$sKey = "";
		foreach ($rsold as $row) {
			$sThisKey = "";
			if ($sThisKey <> "") $sThisKey .= EW_COMPOSITE_KEY_SEPARATOR;
			$sThisKey .= $row['no'];
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$DeleteRows = $conn->Execute($m_ruang->DeleteSQL($row)); // Delete
			$conn->raiseErrorFn = '';
			if ($DeleteRows === FALSE)
				break;
			if ($sKey <> "") $sKey .= ", ";
			$sKey .= $sThisKey;
		}
	} else {

		// Set up error message
		if ($m_ruang->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_ruang->CancelMessage;
			$m_ruang->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Delete cancelled";
		}
	}
	if ($DeleteRows) {
		$conn->CommitTrans(); // Commit the changes
	} else {
		$conn->RollbackTrans(); // Rollback changes
	}

	// Call recordset deleted event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$m_ruang->Row_Deleted($row);
		}	
	}
	return $DeleteRows;
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_ruang;

	// Call Recordset Selecting event
	$m_ruang->Recordset_Selecting($m_ruang->CurrentFilter);

	// Load list page sql
	$sSql = $m_ruang->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_ruang->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_ruang;
	$sFilter = $m_ruang->SqlKeyFilter();
	if (!is_numeric($m_ruang->no->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@no@", ew_AdjustSql($m_ruang->no->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_ruang->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_ruang->CurrentFilter = $sFilter;
	$sSql = $m_ruang->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_ruang->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_ruang;
	$m_ruang->no->setDbValue($rs->fields('no'));
	$m_ruang->nama->setDbValue($rs->fields('nama'));
	$m_ruang->kelas->setDbValue($rs->fields('kelas'));
	$m_ruang->ruang->setDbValue($rs->fields('ruang'));
	$m_ruang->jumlah_tt->setDbValue($rs->fields('jumlah_tt'));
	$m_ruang->ket_ruang->setDbValue($rs->fields('ket_ruang'));
	$m_ruang->fasilitas->setDbValue($rs->fields('fasilitas'));
	$m_ruang->keterangan->setDbValue($rs->fields('keterangan'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_ruang;

	// Call Row Rendering event
	$m_ruang->Row_Rendering();

	// Common render codes for all row types
	// no

	$m_ruang->no->CellCssStyle = "";
	$m_ruang->no->CellCssClass = "";

	// nama
	$m_ruang->nama->CellCssStyle = "";
	$m_ruang->nama->CellCssClass = "";

	// kelas
	$m_ruang->kelas->CellCssStyle = "";
	$m_ruang->kelas->CellCssClass = "";

	// ruang
	$m_ruang->ruang->CellCssStyle = "";
	$m_ruang->ruang->CellCssClass = "";

	// jumlah_tt
	$m_ruang->jumlah_tt->CellCssStyle = "";
	$m_ruang->jumlah_tt->CellCssClass = "";

	// ket_ruang
	$m_ruang->ket_ruang->CellCssStyle = "";
	$m_ruang->ket_ruang->CellCssClass = "";
	if ($m_ruang->RowType == EW_ROWTYPE_VIEW) { // View row

		// no
		$m_ruang->no->ViewValue = $m_ruang->no->CurrentValue;
		$m_ruang->no->CssStyle = "";
		$m_ruang->no->CssClass = "";
		$m_ruang->no->ViewCustomAttributes = "";

		// nama
		$m_ruang->nama->ViewValue = $m_ruang->nama->CurrentValue;
		$m_ruang->nama->CssStyle = "";
		$m_ruang->nama->CssClass = "";
		$m_ruang->nama->ViewCustomAttributes = "";

		// kelas
		$m_ruang->kelas->ViewValue = $m_ruang->kelas->CurrentValue;
		$m_ruang->kelas->CssStyle = "";
		$m_ruang->kelas->CssClass = "";
		$m_ruang->kelas->ViewCustomAttributes = "";

		// ruang
		$m_ruang->ruang->ViewValue = $m_ruang->ruang->CurrentValue;
		$m_ruang->ruang->CssStyle = "";
		$m_ruang->ruang->CssClass = "";
		$m_ruang->ruang->ViewCustomAttributes = "";

		// jumlah_tt
		$m_ruang->jumlah_tt->ViewValue = $m_ruang->jumlah_tt->CurrentValue;
		$m_ruang->jumlah_tt->CssStyle = "";
		$m_ruang->jumlah_tt->CssClass = "";
		$m_ruang->jumlah_tt->ViewCustomAttributes = "";

		// ket_ruang
		$m_ruang->ket_ruang->ViewValue = $m_ruang->ket_ruang->CurrentValue;
		$m_ruang->ket_ruang->CssStyle = "";
		$m_ruang->ket_ruang->CssClass = "";
		$m_ruang->ket_ruang->ViewCustomAttributes = "";

		// no
		$m_ruang->no->HrefValue = "";

		// nama
		$m_ruang->nama->HrefValue = "";

		// kelas
		$m_ruang->kelas->HrefValue = "";

		// ruang
		$m_ruang->ruang->HrefValue = "";

		// jumlah_tt
		$m_ruang->jumlah_tt->HrefValue = "";

		// ket_ruang
		$m_ruang->ket_ruang->HrefValue = "";
	} elseif ($m_ruang->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_ruang->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_ruang->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_ruang->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
