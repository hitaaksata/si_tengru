<?php
define("EW_PAGE_ID", "view", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_obat', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_obatinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_obat->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_obat->Export; // Get export parameter, used in header
$sExportFile = $m_obat->TableVar; // Get export file, used in header
?>
<?php
if (@$_GET["kode_obat"] <> "") {
	$m_obat->kode_obat->setQueryStringValue($_GET["kode_obat"]);
} else {
	Page_Terminate("m_obatlist.php"); // Return to list page
}

// Get action
if (@$_POST["a_view"] <> "") {
	$m_obat->CurrentAction = $_POST["a_view"];
} else {
	$m_obat->CurrentAction = "I"; // Display form
}
switch ($m_obat->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // Set no record message
			Page_Terminate("m_obatlist.php"); // Return to list
		}
}

// Set return url
$m_obat->setReturnUrl("m_obatview.php");

// Render row
$m_obat->RowType = EW_ROWTYPE_VIEW;
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "view"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">View TABLE: m obat
<br><br>
<a href="m_obatlist.php">Back to List</a>&nbsp;
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_obatadd.php">Add</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_obat->EditUrl() ?>">Edit</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_obat->CopyUrl() ?>">Copy</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_obat->DeleteUrl() ?>">Delete</a>&nbsp;
<?php } ?>
</span>
</p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<p>
<form>
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">nama obat</td>
		<td<?php echo $m_obat->nama_obat->CellAttributes() ?>>
<div<?php echo $m_obat->nama_obat->ViewAttributes() ?>><?php echo $m_obat->nama_obat->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">kode obat</td>
		<td<?php echo $m_obat->kode_obat->CellAttributes() ?>>
<div<?php echo $m_obat->kode_obat->ViewAttributes() ?>><?php echo $m_obat->kode_obat->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">group obat</td>
		<td<?php echo $m_obat->group_obat->CellAttributes() ?>>
<div<?php echo $m_obat->group_obat->ViewAttributes() ?>><?php echo $m_obat->group_obat->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">satuan</td>
		<td<?php echo $m_obat->satuan->CellAttributes() ?>>
<div<?php echo $m_obat->satuan->ViewAttributes() ?>><?php echo $m_obat->satuan->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">harga</td>
		<td<?php echo $m_obat->harga->CellAttributes() ?>>
<div<?php echo $m_obat->harga->ViewAttributes() ?>><?php echo $m_obat->harga->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">hide when print</td>
		<td<?php echo $m_obat->hide_when_print->CellAttributes() ?>>
<div<?php echo $m_obat->hide_when_print->ViewAttributes() ?>><?php echo $m_obat->hide_when_print->ViewValue ?></div>
</td>
	</tr>
</table>
</form>
<p>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_obat;
	$sFilter = $m_obat->SqlKeyFilter();
	if (!is_numeric($m_obat->kode_obat->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@kode_obat@", ew_AdjustSql($m_obat->kode_obat->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_obat->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_obat->CurrentFilter = $sFilter;
	$sSql = $m_obat->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_obat->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_obat;
	$m_obat->nama_obat->setDbValue($rs->fields('nama_obat'));
	$m_obat->kode_obat->setDbValue($rs->fields('kode_obat'));
	$m_obat->group_obat->setDbValue($rs->fields('group_obat'));
	$m_obat->satuan->setDbValue($rs->fields('satuan'));
	$m_obat->harga->setDbValue($rs->fields('harga'));
	$m_obat->hide_when_print->setDbValue($rs->fields('hide_when_print'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_obat;

	// Call Row Rendering event
	$m_obat->Row_Rendering();

	// Common render codes for all row types
	// nama_obat

	$m_obat->nama_obat->CellCssStyle = "";
	$m_obat->nama_obat->CellCssClass = "";

	// kode_obat
	$m_obat->kode_obat->CellCssStyle = "";
	$m_obat->kode_obat->CellCssClass = "";

	// group_obat
	$m_obat->group_obat->CellCssStyle = "";
	$m_obat->group_obat->CellCssClass = "";

	// satuan
	$m_obat->satuan->CellCssStyle = "";
	$m_obat->satuan->CellCssClass = "";

	// harga
	$m_obat->harga->CellCssStyle = "";
	$m_obat->harga->CellCssClass = "";

	// hide_when_print
	$m_obat->hide_when_print->CellCssStyle = "";
	$m_obat->hide_when_print->CellCssClass = "";
	if ($m_obat->RowType == EW_ROWTYPE_VIEW) { // View row

		// nama_obat
		$m_obat->nama_obat->ViewValue = $m_obat->nama_obat->CurrentValue;
		if (!is_null($m_obat->nama_obat->ViewValue)) $m_obat->nama_obat->ViewValue = str_replace("\n", "<br>", $m_obat->nama_obat->ViewValue); 
		$m_obat->nama_obat->CssStyle = "";
		$m_obat->nama_obat->CssClass = "";
		$m_obat->nama_obat->ViewCustomAttributes = "";

		// kode_obat
		$m_obat->kode_obat->ViewValue = $m_obat->kode_obat->CurrentValue;
		$m_obat->kode_obat->CssStyle = "";
		$m_obat->kode_obat->CssClass = "";
		$m_obat->kode_obat->ViewCustomAttributes = "";

		// group_obat
		$m_obat->group_obat->ViewValue = $m_obat->group_obat->CurrentValue;
		$m_obat->group_obat->CssStyle = "";
		$m_obat->group_obat->CssClass = "";
		$m_obat->group_obat->ViewCustomAttributes = "";

		// satuan
		$m_obat->satuan->ViewValue = $m_obat->satuan->CurrentValue;
		$m_obat->satuan->CssStyle = "";
		$m_obat->satuan->CssClass = "";
		$m_obat->satuan->ViewCustomAttributes = "";

		// harga
		$m_obat->harga->ViewValue = $m_obat->harga->CurrentValue;
		$m_obat->harga->CssStyle = "";
		$m_obat->harga->CssClass = "";
		$m_obat->harga->ViewCustomAttributes = "";

		// hide_when_print
		$m_obat->hide_when_print->ViewValue = $m_obat->hide_when_print->CurrentValue;
		$m_obat->hide_when_print->CssStyle = "";
		$m_obat->hide_when_print->CssClass = "";
		$m_obat->hide_when_print->ViewCustomAttributes = "";

		// nama_obat
		$m_obat->nama_obat->HrefValue = "";

		// kode_obat
		$m_obat->kode_obat->HrefValue = "";

		// group_obat
		$m_obat->group_obat->HrefValue = "";

		// satuan
		$m_obat->satuan->HrefValue = "";

		// harga
		$m_obat->harga->HrefValue = "";

		// hide_when_print
		$m_obat->hide_when_print->HrefValue = "";
	} elseif ($m_obat->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_obat->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_obat->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_obat->Row_Rendered();
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_obat;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_obat->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_obat->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_obat->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_obat->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_obat->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_obat->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_obat->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
