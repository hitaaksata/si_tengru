<?php
define("EW_PAGE_ID", "add", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_ambulance', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_ambulanceinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_ambulance->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_ambulance->Export; // Get export parameter, used in header
$sExportFile = $m_ambulance->TableVar; // Get export file, used in header
?>
<?php

// Load key values from QueryString
$bCopy = TRUE;
if (@$_GET["kode"] != "") {
  $m_ambulance->kode->setQueryStringValue($_GET["kode"]);
} else {
  $bCopy = FALSE;
}

// Create form object
$objForm = new cFormObj();

// Process form if post back
if (@$_POST["a_add"] <> "") {
  $m_ambulance->CurrentAction = $_POST["a_add"]; // Get form action
  LoadFormValues(); // Load form values
} else { // Not post back
  if ($bCopy) {
    $m_ambulance->CurrentAction = "C"; // Copy Record
  } else {
    $m_ambulance->CurrentAction = "I"; // Display Blank Record
    LoadDefaultValues(); // Load default values
  }
}

// Perform action based on action code
switch ($m_ambulance->CurrentAction) {
  case "I": // Blank record, no action required
		break;
  case "C": // Copy an existing record
   if (!LoadRow()) { // Load record based on key
      $_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
      Page_Terminate($m_ambulance->getReturnUrl()); // Clean up and return
    }
		break;
  case "A": // ' Add new record
		$m_ambulance->SendEmail = TRUE; // Send email on add success
    if (AddRow()) { // Add successful
      $_SESSION[EW_SESSION_MESSAGE] = "Add New Record Successful"; // Set up success message
      Page_Terminate($m_ambulance->KeyUrl($m_ambulance->getReturnUrl())); // Clean up and return
    } else {
      RestoreFormValues(); // Add failed, restore form values
    }
}

// Render row based on row type
$m_ambulance->RowType = EW_ROWTYPE_ADD;  // Render add type
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "add"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_kode"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - kode"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_group_jasa"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - group jasa"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_kode_jasa"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - kode jasa"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_nama_jasa"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - nama jasa"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_jarak_tempuh"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - jarak tempuh"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_biaya"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - biaya"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_tarif"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - tarif"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_tarif"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - tarif"))
				return false; 
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Add to TABLE: m ambulance<br><br><a href="<?php echo $m_ambulance->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") { // Mesasge in Session, display
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
  $_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
}
?>
<form name="fm_ambulanceadd" id="fm_ambulanceadd" action="m_ambulanceadd.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_add" id="a_add" value="A">
<table class="ewTable">
  <tr class="ewTableRow">
    <td class="ewTableHeader">kode<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_ambulance->kode->CellAttributes() ?>><span id="cb_x_kode">
<input type="text" name="x_kode" id="x_kode"  size="30" maxlength="8" value="<?php echo $m_ambulance->kode->EditValue ?>"<?php echo $m_ambulance->kode->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">group jasa<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_ambulance->group_jasa->CellAttributes() ?>><span id="cb_x_group_jasa">
<input type="text" name="x_group_jasa" id="x_group_jasa"  size="30" maxlength="8" value="<?php echo $m_ambulance->group_jasa->EditValue ?>"<?php echo $m_ambulance->group_jasa->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableRow">
    <td class="ewTableHeader">kode jasa<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_ambulance->kode_jasa->CellAttributes() ?>><span id="cb_x_kode_jasa">
<input type="text" name="x_kode_jasa" id="x_kode_jasa"  size="30" maxlength="8" value="<?php echo $m_ambulance->kode_jasa->EditValue ?>"<?php echo $m_ambulance->kode_jasa->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">nama jasa<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_ambulance->nama_jasa->CellAttributes() ?>><span id="cb_x_nama_jasa">
<textarea name="x_nama_jasa" id="x_nama_jasa" cols="35" rows="4"<?php echo $m_ambulance->nama_jasa->EditAttributes() ?>><?php echo $m_ambulance->nama_jasa->EditValue ?></textarea>
</span></td>
  </tr>
  <tr class="ewTableRow">
    <td class="ewTableHeader">jarak tempuh<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_ambulance->jarak_tempuh->CellAttributes() ?>><span id="cb_x_jarak_tempuh">
<textarea name="x_jarak_tempuh" id="x_jarak_tempuh" cols="35" rows="4"<?php echo $m_ambulance->jarak_tempuh->EditAttributes() ?>><?php echo $m_ambulance->jarak_tempuh->EditValue ?></textarea>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">biaya<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_ambulance->biaya->CellAttributes() ?>><span id="cb_x_biaya">
<textarea name="x_biaya" id="x_biaya" cols="35" rows="4"<?php echo $m_ambulance->biaya->EditAttributes() ?>><?php echo $m_ambulance->biaya->EditValue ?></textarea>
</span></td>
  </tr>
  <tr class="ewTableRow">
    <td class="ewTableHeader">tarif<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_ambulance->tarif->CellAttributes() ?>><span id="cb_x_tarif">
<input type="text" name="x_tarif" id="x_tarif"  size="30" value="<?php echo $m_ambulance->tarif->EditValue ?>"<?php echo $m_ambulance->tarif->EditAttributes() ?>>
</span></td>
  </tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="    Add    ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load default values
function LoadDefaultValues() {
	global $m_ambulance;
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $m_ambulance;
	$m_ambulance->kode->setFormValue($objForm->GetValue("x_kode"));
	$m_ambulance->group_jasa->setFormValue($objForm->GetValue("x_group_jasa"));
	$m_ambulance->kode_jasa->setFormValue($objForm->GetValue("x_kode_jasa"));
	$m_ambulance->nama_jasa->setFormValue($objForm->GetValue("x_nama_jasa"));
	$m_ambulance->jarak_tempuh->setFormValue($objForm->GetValue("x_jarak_tempuh"));
	$m_ambulance->biaya->setFormValue($objForm->GetValue("x_biaya"));
	$m_ambulance->tarif->setFormValue($objForm->GetValue("x_tarif"));
}

// Restore form values
function RestoreFormValues() {
	global $m_ambulance;
	$m_ambulance->kode->CurrentValue = $m_ambulance->kode->FormValue;
	$m_ambulance->group_jasa->CurrentValue = $m_ambulance->group_jasa->FormValue;
	$m_ambulance->kode_jasa->CurrentValue = $m_ambulance->kode_jasa->FormValue;
	$m_ambulance->nama_jasa->CurrentValue = $m_ambulance->nama_jasa->FormValue;
	$m_ambulance->jarak_tempuh->CurrentValue = $m_ambulance->jarak_tempuh->FormValue;
	$m_ambulance->biaya->CurrentValue = $m_ambulance->biaya->FormValue;
	$m_ambulance->tarif->CurrentValue = $m_ambulance->tarif->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_ambulance;
	$sFilter = $m_ambulance->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($m_ambulance->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_ambulance->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_ambulance->CurrentFilter = $sFilter;
	$sSql = $m_ambulance->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_ambulance->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_ambulance;
	$m_ambulance->kode->setDbValue($rs->fields('kode'));
	$m_ambulance->group_jasa->setDbValue($rs->fields('group_jasa'));
	$m_ambulance->kode_jasa->setDbValue($rs->fields('kode_jasa'));
	$m_ambulance->nama_jasa->setDbValue($rs->fields('nama_jasa'));
	$m_ambulance->jarak_tempuh->setDbValue($rs->fields('jarak_tempuh'));
	$m_ambulance->biaya->setDbValue($rs->fields('biaya'));
	$m_ambulance->tarif->setDbValue($rs->fields('tarif'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_ambulance;

	// Call Row Rendering event
	$m_ambulance->Row_Rendering();

	// Common render codes for all row types
	// kode

	$m_ambulance->kode->CellCssStyle = "";
	$m_ambulance->kode->CellCssClass = "";

	// group_jasa
	$m_ambulance->group_jasa->CellCssStyle = "";
	$m_ambulance->group_jasa->CellCssClass = "";

	// kode_jasa
	$m_ambulance->kode_jasa->CellCssStyle = "";
	$m_ambulance->kode_jasa->CellCssClass = "";

	// nama_jasa
	$m_ambulance->nama_jasa->CellCssStyle = "";
	$m_ambulance->nama_jasa->CellCssClass = "";

	// jarak_tempuh
	$m_ambulance->jarak_tempuh->CellCssStyle = "";
	$m_ambulance->jarak_tempuh->CellCssClass = "";

	// biaya
	$m_ambulance->biaya->CellCssStyle = "";
	$m_ambulance->biaya->CellCssClass = "";

	// tarif
	$m_ambulance->tarif->CellCssStyle = "";
	$m_ambulance->tarif->CellCssClass = "";
	if ($m_ambulance->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($m_ambulance->RowType == EW_ROWTYPE_ADD) { // Add row

		// kode
		$m_ambulance->kode->EditCustomAttributes = "";
		$m_ambulance->kode->EditValue = ew_HtmlEncode($m_ambulance->kode->CurrentValue);

		// group_jasa
		$m_ambulance->group_jasa->EditCustomAttributes = "";
		$m_ambulance->group_jasa->EditValue = ew_HtmlEncode($m_ambulance->group_jasa->CurrentValue);

		// kode_jasa
		$m_ambulance->kode_jasa->EditCustomAttributes = "";
		$m_ambulance->kode_jasa->EditValue = ew_HtmlEncode($m_ambulance->kode_jasa->CurrentValue);

		// nama_jasa
		$m_ambulance->nama_jasa->EditCustomAttributes = "";
		$m_ambulance->nama_jasa->EditValue = ew_HtmlEncode($m_ambulance->nama_jasa->CurrentValue);

		// jarak_tempuh
		$m_ambulance->jarak_tempuh->EditCustomAttributes = "";
		$m_ambulance->jarak_tempuh->EditValue = ew_HtmlEncode($m_ambulance->jarak_tempuh->CurrentValue);

		// biaya
		$m_ambulance->biaya->EditCustomAttributes = "";
		$m_ambulance->biaya->EditValue = ew_HtmlEncode($m_ambulance->biaya->CurrentValue);

		// tarif
		$m_ambulance->tarif->EditCustomAttributes = "";
		$m_ambulance->tarif->EditValue = ew_HtmlEncode($m_ambulance->tarif->CurrentValue);
	} elseif ($m_ambulance->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_ambulance->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_ambulance->Row_Rendered();
}
?>
<?php

// Add record
function AddRow() {
	global $conn, $Security, $m_ambulance;

	// Check for duplicate key
	$bCheckKey = TRUE;
	$sFilter = $m_ambulance->SqlKeyFilter();
	if (trim(strval($m_ambulance->kode->CurrentValue)) == "") {
		$bCheckKey = FALSE;
	} else {
		$sFilter = str_replace("@kode@", ew_AdjustSql($m_ambulance->kode->CurrentValue), $sFilter); // Replace key value
	}
	if ($bCheckKey) {
		$rsChk = $m_ambulance->LoadRs($sFilter);
		if ($rsChk && !$rsChk->EOF) {
			$_SESSION[EW_SESSION_MESSAGE] = "Duplicate value for primary key";
			$rsChk->Close();
			return FALSE;
		}
	}
	$rsnew = array();

	// Field kode
	$m_ambulance->kode->SetDbValueDef($m_ambulance->kode->CurrentValue, "");
	$rsnew['kode'] =& $m_ambulance->kode->DbValue;

	// Field group_jasa
	$m_ambulance->group_jasa->SetDbValueDef($m_ambulance->group_jasa->CurrentValue, "");
	$rsnew['group_jasa'] =& $m_ambulance->group_jasa->DbValue;

	// Field kode_jasa
	$m_ambulance->kode_jasa->SetDbValueDef($m_ambulance->kode_jasa->CurrentValue, "");
	$rsnew['kode_jasa'] =& $m_ambulance->kode_jasa->DbValue;

	// Field nama_jasa
	$m_ambulance->nama_jasa->SetDbValueDef($m_ambulance->nama_jasa->CurrentValue, "");
	$rsnew['nama_jasa'] =& $m_ambulance->nama_jasa->DbValue;

	// Field jarak_tempuh
	$m_ambulance->jarak_tempuh->SetDbValueDef($m_ambulance->jarak_tempuh->CurrentValue, "");
	$rsnew['jarak_tempuh'] =& $m_ambulance->jarak_tempuh->DbValue;

	// Field biaya
	$m_ambulance->biaya->SetDbValueDef($m_ambulance->biaya->CurrentValue, "");
	$rsnew['biaya'] =& $m_ambulance->biaya->DbValue;

	// Field tarif
	$m_ambulance->tarif->SetDbValueDef($m_ambulance->tarif->CurrentValue, 0);
	$rsnew['tarif'] =& $m_ambulance->tarif->DbValue;

	// Call Row Inserting event
	$bInsertRow = $m_ambulance->Row_Inserting($rsnew);
	if ($bInsertRow) {
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$AddRow = $conn->Execute($m_ambulance->InsertSQL($rsnew));
		$conn->raiseErrorFn = '';
	} else {
		if ($m_ambulance->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_ambulance->CancelMessage;
			$m_ambulance->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Insert cancelled";
		}
		$AddRow = FALSE;
	}
	if ($AddRow) {

		// Call Row Inserted event
		$m_ambulance->Row_Inserted($rsnew);
	}
	return $AddRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
