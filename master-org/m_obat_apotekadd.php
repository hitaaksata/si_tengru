<?php
define("EW_PAGE_ID", "add", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_obat_apotek', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_obat_apotekinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_obat_apotek->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_obat_apotek->Export; // Get export parameter, used in header
$sExportFile = $m_obat_apotek->TableVar; // Get export file, used in header
?>
<?php

// Load key values from QueryString
$bCopy = TRUE;
if (@$_GET["kode_obat"] != "") {
  $m_obat_apotek->kode_obat->setQueryStringValue($_GET["kode_obat"]);
} else {
  $bCopy = FALSE;
}

// Create form object
$objForm = new cFormObj();

// Process form if post back
if (@$_POST["a_add"] <> "") {
  $m_obat_apotek->CurrentAction = $_POST["a_add"]; // Get form action
  LoadFormValues(); // Load form values
} else { // Not post back
  if ($bCopy) {
    $m_obat_apotek->CurrentAction = "C"; // Copy Record
  } else {
    $m_obat_apotek->CurrentAction = "I"; // Display Blank Record
    LoadDefaultValues(); // Load default values
  }
}

// Perform action based on action code
switch ($m_obat_apotek->CurrentAction) {
  case "I": // Blank record, no action required
		break;
  case "C": // Copy an existing record
   if (!LoadRow()) { // Load record based on key
      $_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
      Page_Terminate($m_obat_apotek->getReturnUrl()); // Clean up and return
    }
		break;
  case "A": // ' Add new record
		$m_obat_apotek->SendEmail = TRUE; // Send email on add success
    if (AddRow()) { // Add successful
      $_SESSION[EW_SESSION_MESSAGE] = "Add New Record Successful"; // Set up success message
      Page_Terminate($m_obat_apotek->KeyUrl($m_obat_apotek->getReturnUrl())); // Clean up and return
    } else {
      RestoreFormValues(); // Add failed, restore form values
    }
}

// Render row based on row type
$m_obat_apotek->RowType = EW_ROWTYPE_ADD;  // Render add type
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "add"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_nama_obat"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - nama obat"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_group_obat"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - group obat"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_harga"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - harga"))
				return false; 
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Add to TABLE: m obat apotek<br><br><a href="<?php echo $m_obat_apotek->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") { // Mesasge in Session, display
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
  $_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
}
?>
<form name="fm_obat_apotekadd" id="fm_obat_apotekadd" action="m_obat_apotekadd.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_add" id="a_add" value="A">
<table class="ewTable">
  <tr class="ewTableRow">
    <td class="ewTableHeader">nama obat<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_obat_apotek->nama_obat->CellAttributes() ?>><span id="cb_x_nama_obat">
<textarea name="x_nama_obat" id="x_nama_obat" cols="35" rows="4"<?php echo $m_obat_apotek->nama_obat->EditAttributes() ?>><?php echo $m_obat_apotek->nama_obat->EditValue ?></textarea>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">group obat<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_obat_apotek->group_obat->CellAttributes() ?>><span id="cb_x_group_obat">
<input type="text" name="x_group_obat" id="x_group_obat"  size="30" maxlength="10" value="<?php echo $m_obat_apotek->group_obat->EditValue ?>"<?php echo $m_obat_apotek->group_obat->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableRow">
    <td class="ewTableHeader">satuan</td>
    <td<?php echo $m_obat_apotek->satuan->CellAttributes() ?>><span id="cb_x_satuan">
<input type="text" name="x_satuan" id="x_satuan"  size="30" maxlength="16" value="<?php echo $m_obat_apotek->satuan->EditValue ?>"<?php echo $m_obat_apotek->satuan->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">harga</td>
    <td<?php echo $m_obat_apotek->harga->CellAttributes() ?>><span id="cb_x_harga">
<input type="text" name="x_harga" id="x_harga"  size="30" value="<?php echo $m_obat_apotek->harga->EditValue ?>"<?php echo $m_obat_apotek->harga->EditAttributes() ?>>
</span></td>
  </tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="    Add    ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load default values
function LoadDefaultValues() {
	global $m_obat_apotek;
	$m_obat_apotek->harga->CurrentValue = 0;
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $m_obat_apotek;
	$m_obat_apotek->nama_obat->setFormValue($objForm->GetValue("x_nama_obat"));
	$m_obat_apotek->group_obat->setFormValue($objForm->GetValue("x_group_obat"));
	$m_obat_apotek->satuan->setFormValue($objForm->GetValue("x_satuan"));
	$m_obat_apotek->harga->setFormValue($objForm->GetValue("x_harga"));
}

// Restore form values
function RestoreFormValues() {
	global $m_obat_apotek;
	$m_obat_apotek->nama_obat->CurrentValue = $m_obat_apotek->nama_obat->FormValue;
	$m_obat_apotek->group_obat->CurrentValue = $m_obat_apotek->group_obat->FormValue;
	$m_obat_apotek->satuan->CurrentValue = $m_obat_apotek->satuan->FormValue;
	$m_obat_apotek->harga->CurrentValue = $m_obat_apotek->harga->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_obat_apotek;
	$sFilter = $m_obat_apotek->SqlKeyFilter();
	if (!is_numeric($m_obat_apotek->kode_obat->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@kode_obat@", ew_AdjustSql($m_obat_apotek->kode_obat->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_obat_apotek->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_obat_apotek->CurrentFilter = $sFilter;
	$sSql = $m_obat_apotek->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_obat_apotek->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_obat_apotek;
	$m_obat_apotek->nama_obat->setDbValue($rs->fields('nama_obat'));
	$m_obat_apotek->kode_obat->setDbValue($rs->fields('kode_obat'));
	$m_obat_apotek->group_obat->setDbValue($rs->fields('group_obat'));
	$m_obat_apotek->satuan->setDbValue($rs->fields('satuan'));
	$m_obat_apotek->harga->setDbValue($rs->fields('harga'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_obat_apotek;

	// Call Row Rendering event
	$m_obat_apotek->Row_Rendering();

	// Common render codes for all row types
	// nama_obat

	$m_obat_apotek->nama_obat->CellCssStyle = "";
	$m_obat_apotek->nama_obat->CellCssClass = "";

	// group_obat
	$m_obat_apotek->group_obat->CellCssStyle = "";
	$m_obat_apotek->group_obat->CellCssClass = "";

	// satuan
	$m_obat_apotek->satuan->CellCssStyle = "";
	$m_obat_apotek->satuan->CellCssClass = "";

	// harga
	$m_obat_apotek->harga->CellCssStyle = "";
	$m_obat_apotek->harga->CellCssClass = "";
	if ($m_obat_apotek->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($m_obat_apotek->RowType == EW_ROWTYPE_ADD) { // Add row

		// nama_obat
		$m_obat_apotek->nama_obat->EditCustomAttributes = "";
		$m_obat_apotek->nama_obat->EditValue = ew_HtmlEncode($m_obat_apotek->nama_obat->CurrentValue);

		// group_obat
		$m_obat_apotek->group_obat->EditCustomAttributes = "";
		$m_obat_apotek->group_obat->EditValue = ew_HtmlEncode($m_obat_apotek->group_obat->CurrentValue);

		// satuan
		$m_obat_apotek->satuan->EditCustomAttributes = "";
		$m_obat_apotek->satuan->EditValue = ew_HtmlEncode($m_obat_apotek->satuan->CurrentValue);

		// harga
		$m_obat_apotek->harga->EditCustomAttributes = "";
		$m_obat_apotek->harga->EditValue = ew_HtmlEncode($m_obat_apotek->harga->CurrentValue);
	} elseif ($m_obat_apotek->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_obat_apotek->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_obat_apotek->Row_Rendered();
}
?>
<?php

// Add record
function AddRow() {
	global $conn, $Security, $m_obat_apotek;

	// Check for duplicate key
	$bCheckKey = TRUE;
	$sFilter = $m_obat_apotek->SqlKeyFilter();
	if (trim(strval($m_obat_apotek->kode_obat->CurrentValue)) == "") {
		$bCheckKey = FALSE;
	} else {
		$sFilter = str_replace("@kode_obat@", ew_AdjustSql($m_obat_apotek->kode_obat->CurrentValue), $sFilter); // Replace key value
	}
	if (!is_numeric($m_obat_apotek->kode_obat->CurrentValue)) {
		$bCheckKey = FALSE;
	}
	if ($bCheckKey) {
		$rsChk = $m_obat_apotek->LoadRs($sFilter);
		if ($rsChk && !$rsChk->EOF) {
			$_SESSION[EW_SESSION_MESSAGE] = "Duplicate value for primary key";
			$rsChk->Close();
			return FALSE;
		}
	}
	$rsnew = array();

	// Field nama_obat
	$m_obat_apotek->nama_obat->SetDbValueDef($m_obat_apotek->nama_obat->CurrentValue, "");
	$rsnew['nama_obat'] =& $m_obat_apotek->nama_obat->DbValue;

	// Field group_obat
	$m_obat_apotek->group_obat->SetDbValueDef($m_obat_apotek->group_obat->CurrentValue, "");
	$rsnew['group_obat'] =& $m_obat_apotek->group_obat->DbValue;

	// Field satuan
	$m_obat_apotek->satuan->SetDbValueDef($m_obat_apotek->satuan->CurrentValue, NULL);
	$rsnew['satuan'] =& $m_obat_apotek->satuan->DbValue;

	// Field harga
	$m_obat_apotek->harga->SetDbValueDef($m_obat_apotek->harga->CurrentValue, NULL);
	$rsnew['harga'] =& $m_obat_apotek->harga->DbValue;

	// Call Row Inserting event
	$bInsertRow = $m_obat_apotek->Row_Inserting($rsnew);
	if ($bInsertRow) {
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$AddRow = $conn->Execute($m_obat_apotek->InsertSQL($rsnew));
		$conn->raiseErrorFn = '';
	} else {
		if ($m_obat_apotek->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_obat_apotek->CancelMessage;
			$m_obat_apotek->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Insert cancelled";
		}
		$AddRow = FALSE;
	}
	if ($AddRow) {
		$m_obat_apotek->kode_obat->setDbValue($conn->Insert_ID());
		$rsnew['kode_obat'] =& $m_obat_apotek->kode_obat->DbValue;

		// Call Row Inserted event
		$m_obat_apotek->Row_Inserted($rsnew);
	}
	return $AddRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
