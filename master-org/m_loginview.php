<?php
define("EW_PAGE_ID", "view", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_login', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_logininfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_login->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_login->Export; // Get export parameter, used in header
$sExportFile = $m_login->TableVar; // Get export file, used in header
?>
<?php
if (@$_GET["NIP"] <> "") {
	$m_login->NIP->setQueryStringValue($_GET["NIP"]);
} else {
	Page_Terminate("m_loginlist.php"); // Return to list page
}

// Get action
if (@$_POST["a_view"] <> "") {
	$m_login->CurrentAction = $_POST["a_view"];
} else {
	$m_login->CurrentAction = "I"; // Display form
}
switch ($m_login->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // Set no record message
			Page_Terminate("m_loginlist.php"); // Return to list
		}
}

// Set return url
$m_login->setReturnUrl("m_loginview.php");

// Render row
$m_login->RowType = EW_ROWTYPE_VIEW;
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "view"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">View TABLE: m login
<br><br>
<a href="m_loginlist.php">Back to List</a>&nbsp;
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_loginadd.php">Add</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_login->EditUrl() ?>">Edit</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_login->CopyUrl() ?>">Copy</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_login->DeleteUrl() ?>">Delete</a>&nbsp;
<?php } ?>
</span>
</p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<p>
<form>
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">NIP</td>
		<td<?php echo $m_login->NIP->CellAttributes() ?>>
<div<?php echo $m_login->NIP->ViewAttributes() ?>><?php echo $m_login->NIP->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">PWD</td>
		<td<?php echo $m_login->PWD->CellAttributes() ?>>
<div<?php echo $m_login->PWD->ViewAttributes() ?>><?php echo $m_login->PWD->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">SES REG</td>
		<td<?php echo $m_login->SES_REG->CellAttributes() ?>>
<div<?php echo $m_login->SES_REG->ViewAttributes() ?>><?php echo $m_login->SES_REG->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">ROLES</td>
		<td<?php echo $m_login->ROLES->CellAttributes() ?>>
<div<?php echo $m_login->ROLES->ViewAttributes() ?>><?php echo $m_login->ROLES->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">KDUNIT</td>
		<td<?php echo $m_login->KDUNIT->CellAttributes() ?>>
<div<?php echo $m_login->KDUNIT->ViewAttributes() ?>><?php echo $m_login->KDUNIT->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">DEPARTEMEN</td>
		<td<?php echo $m_login->DEPARTEMEN->CellAttributes() ?>>
<div<?php echo $m_login->DEPARTEMEN->ViewAttributes() ?>><?php echo $m_login->DEPARTEMEN->ViewValue ?></div>
</td>
	</tr>
</table>
</form>
<p>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_login;
	$sFilter = $m_login->SqlKeyFilter();
	$sFilter = str_replace("@NIP@", ew_AdjustSql($m_login->NIP->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_login->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_login->CurrentFilter = $sFilter;
	$sSql = $m_login->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_login->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_login;
	$m_login->NIP->setDbValue($rs->fields('NIP'));
	$m_login->PWD->setDbValue($rs->fields('PWD'));
	$m_login->SES_REG->setDbValue($rs->fields('SES_REG'));
	$m_login->ROLES->setDbValue($rs->fields('ROLES'));
	$m_login->KDUNIT->setDbValue($rs->fields('KDUNIT'));
	$m_login->DEPARTEMEN->setDbValue($rs->fields('DEPARTEMEN'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_login;

	// Call Row Rendering event
	$m_login->Row_Rendering();

	// Common render codes for all row types
	// NIP

	$m_login->NIP->CellCssStyle = "";
	$m_login->NIP->CellCssClass = "";

	// PWD
	$m_login->PWD->CellCssStyle = "";
	$m_login->PWD->CellCssClass = "";

	// SES_REG
	$m_login->SES_REG->CellCssStyle = "";
	$m_login->SES_REG->CellCssClass = "";

	// ROLES
	$m_login->ROLES->CellCssStyle = "";
	$m_login->ROLES->CellCssClass = "";

	// KDUNIT
	$m_login->KDUNIT->CellCssStyle = "";
	$m_login->KDUNIT->CellCssClass = "";

	// DEPARTEMEN
	$m_login->DEPARTEMEN->CellCssStyle = "";
	$m_login->DEPARTEMEN->CellCssClass = "";
	if ($m_login->RowType == EW_ROWTYPE_VIEW) { // View row

		// NIP
		$m_login->NIP->ViewValue = $m_login->NIP->CurrentValue;
		$m_login->NIP->CssStyle = "";
		$m_login->NIP->CssClass = "";
		$m_login->NIP->ViewCustomAttributes = "";

		// PWD
		$m_login->PWD->ViewValue = $m_login->PWD->CurrentValue;
		$m_login->PWD->CssStyle = "";
		$m_login->PWD->CssClass = "";
		$m_login->PWD->ViewCustomAttributes = "";

		// SES_REG
		$m_login->SES_REG->ViewValue = $m_login->SES_REG->CurrentValue;
		$m_login->SES_REG->CssStyle = "";
		$m_login->SES_REG->CssClass = "";
		$m_login->SES_REG->ViewCustomAttributes = "";

		// ROLES
		$m_login->ROLES->ViewValue = $m_login->ROLES->CurrentValue;
		$m_login->ROLES->CssStyle = "";
		$m_login->ROLES->CssClass = "";
		$m_login->ROLES->ViewCustomAttributes = "";

		// KDUNIT
		$m_login->KDUNIT->ViewValue = $m_login->KDUNIT->CurrentValue;
		$m_login->KDUNIT->CssStyle = "";
		$m_login->KDUNIT->CssClass = "";
		$m_login->KDUNIT->ViewCustomAttributes = "";

		// DEPARTEMEN
		$m_login->DEPARTEMEN->ViewValue = $m_login->DEPARTEMEN->CurrentValue;
		$m_login->DEPARTEMEN->CssStyle = "";
		$m_login->DEPARTEMEN->CssClass = "";
		$m_login->DEPARTEMEN->ViewCustomAttributes = "";

		// NIP
		$m_login->NIP->HrefValue = "";

		// PWD
		$m_login->PWD->HrefValue = "";

		// SES_REG
		$m_login->SES_REG->HrefValue = "";

		// ROLES
		$m_login->ROLES->HrefValue = "";

		// KDUNIT
		$m_login->KDUNIT->HrefValue = "";

		// DEPARTEMEN
		$m_login->DEPARTEMEN->HrefValue = "";
	} elseif ($m_login->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_login->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_login->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_login->Row_Rendered();
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_login;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_login->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_login->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_login->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_login->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_login->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_login->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_login->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
