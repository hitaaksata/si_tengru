<?php
define("EW_PAGE_ID", "view", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_ruang', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_ruanginfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_ruang->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_ruang->Export; // Get export parameter, used in header
$sExportFile = $m_ruang->TableVar; // Get export file, used in header
?>
<?php
if (@$_GET["no"] <> "") {
	$m_ruang->no->setQueryStringValue($_GET["no"]);
} else {
	Page_Terminate("m_ruanglist.php"); // Return to list page
}

// Get action
if (@$_POST["a_view"] <> "") {
	$m_ruang->CurrentAction = $_POST["a_view"];
} else {
	$m_ruang->CurrentAction = "I"; // Display form
}
switch ($m_ruang->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // Set no record message
			Page_Terminate("m_ruanglist.php"); // Return to list
		}
}

// Set return url
$m_ruang->setReturnUrl("m_ruangview.php");

// Render row
$m_ruang->RowType = EW_ROWTYPE_VIEW;
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "view"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">View TABLE: m ruang
<br><br>
<a href="m_ruanglist.php">Back to List</a>&nbsp;
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_ruangadd.php">Add</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_ruang->EditUrl() ?>">Edit</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_ruang->CopyUrl() ?>">Copy</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_ruang->DeleteUrl() ?>">Delete</a>&nbsp;
<?php } ?>
</span>
</p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<p>
<form>
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">no</td>
		<td<?php echo $m_ruang->no->CellAttributes() ?>>
<div<?php echo $m_ruang->no->ViewAttributes() ?>><?php echo $m_ruang->no->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">nama</td>
		<td<?php echo $m_ruang->nama->CellAttributes() ?>>
<div<?php echo $m_ruang->nama->ViewAttributes() ?>><?php echo $m_ruang->nama->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">kelas</td>
		<td<?php echo $m_ruang->kelas->CellAttributes() ?>>
<div<?php echo $m_ruang->kelas->ViewAttributes() ?>><?php echo $m_ruang->kelas->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">ruang</td>
		<td<?php echo $m_ruang->ruang->CellAttributes() ?>>
<div<?php echo $m_ruang->ruang->ViewAttributes() ?>><?php echo $m_ruang->ruang->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">jumlah tt</td>
		<td<?php echo $m_ruang->jumlah_tt->CellAttributes() ?>>
<div<?php echo $m_ruang->jumlah_tt->ViewAttributes() ?>><?php echo $m_ruang->jumlah_tt->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">ket ruang</td>
		<td<?php echo $m_ruang->ket_ruang->CellAttributes() ?>>
<div<?php echo $m_ruang->ket_ruang->ViewAttributes() ?>><?php echo $m_ruang->ket_ruang->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">fasilitas</td>
		<td<?php echo $m_ruang->fasilitas->CellAttributes() ?>>
<div<?php echo $m_ruang->fasilitas->ViewAttributes() ?>><?php echo $m_ruang->fasilitas->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">keterangan</td>
		<td<?php echo $m_ruang->keterangan->CellAttributes() ?>>
<div<?php echo $m_ruang->keterangan->ViewAttributes() ?>><?php echo $m_ruang->keterangan->ViewValue ?></div>
</td>
	</tr>
</table>
</form>
<p>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_ruang;
	$sFilter = $m_ruang->SqlKeyFilter();
	if (!is_numeric($m_ruang->no->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@no@", ew_AdjustSql($m_ruang->no->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_ruang->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_ruang->CurrentFilter = $sFilter;
	$sSql = $m_ruang->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_ruang->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_ruang;
	$m_ruang->no->setDbValue($rs->fields('no'));
	$m_ruang->nama->setDbValue($rs->fields('nama'));
	$m_ruang->kelas->setDbValue($rs->fields('kelas'));
	$m_ruang->ruang->setDbValue($rs->fields('ruang'));
	$m_ruang->jumlah_tt->setDbValue($rs->fields('jumlah_tt'));
	$m_ruang->ket_ruang->setDbValue($rs->fields('ket_ruang'));
	$m_ruang->fasilitas->setDbValue($rs->fields('fasilitas'));
	$m_ruang->keterangan->setDbValue($rs->fields('keterangan'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_ruang;

	// Call Row Rendering event
	$m_ruang->Row_Rendering();

	// Common render codes for all row types
	// no

	$m_ruang->no->CellCssStyle = "";
	$m_ruang->no->CellCssClass = "";

	// nama
	$m_ruang->nama->CellCssStyle = "";
	$m_ruang->nama->CellCssClass = "";

	// kelas
	$m_ruang->kelas->CellCssStyle = "";
	$m_ruang->kelas->CellCssClass = "";

	// ruang
	$m_ruang->ruang->CellCssStyle = "";
	$m_ruang->ruang->CellCssClass = "";

	// jumlah_tt
	$m_ruang->jumlah_tt->CellCssStyle = "";
	$m_ruang->jumlah_tt->CellCssClass = "";

	// ket_ruang
	$m_ruang->ket_ruang->CellCssStyle = "";
	$m_ruang->ket_ruang->CellCssClass = "";

	// fasilitas
	$m_ruang->fasilitas->CellCssStyle = "";
	$m_ruang->fasilitas->CellCssClass = "";

	// keterangan
	$m_ruang->keterangan->CellCssStyle = "";
	$m_ruang->keterangan->CellCssClass = "";
	if ($m_ruang->RowType == EW_ROWTYPE_VIEW) { // View row

		// no
		$m_ruang->no->ViewValue = $m_ruang->no->CurrentValue;
		$m_ruang->no->CssStyle = "";
		$m_ruang->no->CssClass = "";
		$m_ruang->no->ViewCustomAttributes = "";

		// nama
		$m_ruang->nama->ViewValue = $m_ruang->nama->CurrentValue;
		$m_ruang->nama->CssStyle = "";
		$m_ruang->nama->CssClass = "";
		$m_ruang->nama->ViewCustomAttributes = "";

		// kelas
		$m_ruang->kelas->ViewValue = $m_ruang->kelas->CurrentValue;
		$m_ruang->kelas->CssStyle = "";
		$m_ruang->kelas->CssClass = "";
		$m_ruang->kelas->ViewCustomAttributes = "";

		// ruang
		$m_ruang->ruang->ViewValue = $m_ruang->ruang->CurrentValue;
		$m_ruang->ruang->CssStyle = "";
		$m_ruang->ruang->CssClass = "";
		$m_ruang->ruang->ViewCustomAttributes = "";

		// jumlah_tt
		$m_ruang->jumlah_tt->ViewValue = $m_ruang->jumlah_tt->CurrentValue;
		$m_ruang->jumlah_tt->CssStyle = "";
		$m_ruang->jumlah_tt->CssClass = "";
		$m_ruang->jumlah_tt->ViewCustomAttributes = "";

		// ket_ruang
		$m_ruang->ket_ruang->ViewValue = $m_ruang->ket_ruang->CurrentValue;
		$m_ruang->ket_ruang->CssStyle = "";
		$m_ruang->ket_ruang->CssClass = "";
		$m_ruang->ket_ruang->ViewCustomAttributes = "";

		// fasilitas
		$m_ruang->fasilitas->ViewValue = $m_ruang->fasilitas->CurrentValue;
		if (!is_null($m_ruang->fasilitas->ViewValue)) $m_ruang->fasilitas->ViewValue = str_replace("\n", "<br>", $m_ruang->fasilitas->ViewValue); 
		$m_ruang->fasilitas->CssStyle = "";
		$m_ruang->fasilitas->CssClass = "";
		$m_ruang->fasilitas->ViewCustomAttributes = "";

		// keterangan
		$m_ruang->keterangan->ViewValue = $m_ruang->keterangan->CurrentValue;
		if (!is_null($m_ruang->keterangan->ViewValue)) $m_ruang->keterangan->ViewValue = str_replace("\n", "<br>", $m_ruang->keterangan->ViewValue); 
		$m_ruang->keterangan->CssStyle = "";
		$m_ruang->keterangan->CssClass = "";
		$m_ruang->keterangan->ViewCustomAttributes = "";

		// no
		$m_ruang->no->HrefValue = "";

		// nama
		$m_ruang->nama->HrefValue = "";

		// kelas
		$m_ruang->kelas->HrefValue = "";

		// ruang
		$m_ruang->ruang->HrefValue = "";

		// jumlah_tt
		$m_ruang->jumlah_tt->HrefValue = "";

		// ket_ruang
		$m_ruang->ket_ruang->HrefValue = "";

		// fasilitas
		$m_ruang->fasilitas->HrefValue = "";

		// keterangan
		$m_ruang->keterangan->HrefValue = "";
	} elseif ($m_ruang->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_ruang->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_ruang->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_ruang->Row_Rendered();
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_ruang;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_ruang->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_ruang->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_ruang->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_ruang->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_ruang->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_ruang->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_ruang->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
