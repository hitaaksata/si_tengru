<?php
define("EW_PAGE_ID", "view", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_ambulance', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_ambulanceinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_ambulance->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_ambulance->Export; // Get export parameter, used in header
$sExportFile = $m_ambulance->TableVar; // Get export file, used in header
?>
<?php
if (@$_GET["kode"] <> "") {
	$m_ambulance->kode->setQueryStringValue($_GET["kode"]);
} else {
	Page_Terminate("m_ambulancelist.php"); // Return to list page
}

// Get action
if (@$_POST["a_view"] <> "") {
	$m_ambulance->CurrentAction = $_POST["a_view"];
} else {
	$m_ambulance->CurrentAction = "I"; // Display form
}
switch ($m_ambulance->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // Set no record message
			Page_Terminate("m_ambulancelist.php"); // Return to list
		}
}

// Set return url
$m_ambulance->setReturnUrl("m_ambulanceview.php");

// Render row
$m_ambulance->RowType = EW_ROWTYPE_VIEW;
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "view"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">View TABLE: m ambulance
<br><br>
<a href="m_ambulancelist.php">Back to List</a>&nbsp;
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_ambulanceadd.php">Add</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_ambulance->EditUrl() ?>">Edit</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_ambulance->CopyUrl() ?>">Copy</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_ambulance->DeleteUrl() ?>">Delete</a>&nbsp;
<?php } ?>
</span>
</p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<p>
<form>
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">kode</td>
		<td<?php echo $m_ambulance->kode->CellAttributes() ?>>
<div<?php echo $m_ambulance->kode->ViewAttributes() ?>><?php echo $m_ambulance->kode->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">group jasa</td>
		<td<?php echo $m_ambulance->group_jasa->CellAttributes() ?>>
<div<?php echo $m_ambulance->group_jasa->ViewAttributes() ?>><?php echo $m_ambulance->group_jasa->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">kode jasa</td>
		<td<?php echo $m_ambulance->kode_jasa->CellAttributes() ?>>
<div<?php echo $m_ambulance->kode_jasa->ViewAttributes() ?>><?php echo $m_ambulance->kode_jasa->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">nama jasa</td>
		<td<?php echo $m_ambulance->nama_jasa->CellAttributes() ?>>
<div<?php echo $m_ambulance->nama_jasa->ViewAttributes() ?>><?php echo $m_ambulance->nama_jasa->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">jarak tempuh</td>
		<td<?php echo $m_ambulance->jarak_tempuh->CellAttributes() ?>>
<div<?php echo $m_ambulance->jarak_tempuh->ViewAttributes() ?>><?php echo $m_ambulance->jarak_tempuh->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">biaya</td>
		<td<?php echo $m_ambulance->biaya->CellAttributes() ?>>
<div<?php echo $m_ambulance->biaya->ViewAttributes() ?>><?php echo $m_ambulance->biaya->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">tarif</td>
		<td<?php echo $m_ambulance->tarif->CellAttributes() ?>>
<div<?php echo $m_ambulance->tarif->ViewAttributes() ?>><?php echo $m_ambulance->tarif->ViewValue ?></div>
</td>
	</tr>
</table>
</form>
<p>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_ambulance;
	$sFilter = $m_ambulance->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($m_ambulance->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_ambulance->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_ambulance->CurrentFilter = $sFilter;
	$sSql = $m_ambulance->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_ambulance->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_ambulance;
	$m_ambulance->kode->setDbValue($rs->fields('kode'));
	$m_ambulance->group_jasa->setDbValue($rs->fields('group_jasa'));
	$m_ambulance->kode_jasa->setDbValue($rs->fields('kode_jasa'));
	$m_ambulance->nama_jasa->setDbValue($rs->fields('nama_jasa'));
	$m_ambulance->jarak_tempuh->setDbValue($rs->fields('jarak_tempuh'));
	$m_ambulance->biaya->setDbValue($rs->fields('biaya'));
	$m_ambulance->tarif->setDbValue($rs->fields('tarif'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_ambulance;

	// Call Row Rendering event
	$m_ambulance->Row_Rendering();

	// Common render codes for all row types
	// kode

	$m_ambulance->kode->CellCssStyle = "";
	$m_ambulance->kode->CellCssClass = "";

	// group_jasa
	$m_ambulance->group_jasa->CellCssStyle = "";
	$m_ambulance->group_jasa->CellCssClass = "";

	// kode_jasa
	$m_ambulance->kode_jasa->CellCssStyle = "";
	$m_ambulance->kode_jasa->CellCssClass = "";

	// nama_jasa
	$m_ambulance->nama_jasa->CellCssStyle = "";
	$m_ambulance->nama_jasa->CellCssClass = "";

	// jarak_tempuh
	$m_ambulance->jarak_tempuh->CellCssStyle = "";
	$m_ambulance->jarak_tempuh->CellCssClass = "";

	// biaya
	$m_ambulance->biaya->CellCssStyle = "";
	$m_ambulance->biaya->CellCssClass = "";

	// tarif
	$m_ambulance->tarif->CellCssStyle = "";
	$m_ambulance->tarif->CellCssClass = "";
	if ($m_ambulance->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode
		$m_ambulance->kode->ViewValue = $m_ambulance->kode->CurrentValue;
		$m_ambulance->kode->CssStyle = "";
		$m_ambulance->kode->CssClass = "";
		$m_ambulance->kode->ViewCustomAttributes = "";

		// group_jasa
		$m_ambulance->group_jasa->ViewValue = $m_ambulance->group_jasa->CurrentValue;
		$m_ambulance->group_jasa->CssStyle = "";
		$m_ambulance->group_jasa->CssClass = "";
		$m_ambulance->group_jasa->ViewCustomAttributes = "";

		// kode_jasa
		$m_ambulance->kode_jasa->ViewValue = $m_ambulance->kode_jasa->CurrentValue;
		$m_ambulance->kode_jasa->CssStyle = "";
		$m_ambulance->kode_jasa->CssClass = "";
		$m_ambulance->kode_jasa->ViewCustomAttributes = "";

		// nama_jasa
		$m_ambulance->nama_jasa->ViewValue = $m_ambulance->nama_jasa->CurrentValue;
		if (!is_null($m_ambulance->nama_jasa->ViewValue)) $m_ambulance->nama_jasa->ViewValue = str_replace("\n", "<br>", $m_ambulance->nama_jasa->ViewValue); 
		$m_ambulance->nama_jasa->CssStyle = "";
		$m_ambulance->nama_jasa->CssClass = "";
		$m_ambulance->nama_jasa->ViewCustomAttributes = "";

		// jarak_tempuh
		$m_ambulance->jarak_tempuh->ViewValue = $m_ambulance->jarak_tempuh->CurrentValue;
		if (!is_null($m_ambulance->jarak_tempuh->ViewValue)) $m_ambulance->jarak_tempuh->ViewValue = str_replace("\n", "<br>", $m_ambulance->jarak_tempuh->ViewValue); 
		$m_ambulance->jarak_tempuh->CssStyle = "";
		$m_ambulance->jarak_tempuh->CssClass = "";
		$m_ambulance->jarak_tempuh->ViewCustomAttributes = "";

		// biaya
		$m_ambulance->biaya->ViewValue = $m_ambulance->biaya->CurrentValue;
		if (!is_null($m_ambulance->biaya->ViewValue)) $m_ambulance->biaya->ViewValue = str_replace("\n", "<br>", $m_ambulance->biaya->ViewValue); 
		$m_ambulance->biaya->CssStyle = "";
		$m_ambulance->biaya->CssClass = "";
		$m_ambulance->biaya->ViewCustomAttributes = "";

		// tarif
		$m_ambulance->tarif->ViewValue = $m_ambulance->tarif->CurrentValue;
		$m_ambulance->tarif->CssStyle = "";
		$m_ambulance->tarif->CssClass = "";
		$m_ambulance->tarif->ViewCustomAttributes = "";

		// kode
		$m_ambulance->kode->HrefValue = "";

		// group_jasa
		$m_ambulance->group_jasa->HrefValue = "";

		// kode_jasa
		$m_ambulance->kode_jasa->HrefValue = "";

		// nama_jasa
		$m_ambulance->nama_jasa->HrefValue = "";

		// jarak_tempuh
		$m_ambulance->jarak_tempuh->HrefValue = "";

		// biaya
		$m_ambulance->biaya->HrefValue = "";

		// tarif
		$m_ambulance->tarif->HrefValue = "";
	} elseif ($m_ambulance->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_ambulance->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_ambulance->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_ambulance->Row_Rendered();
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_ambulance;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_ambulance->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_ambulance->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_ambulance->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_ambulance->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_ambulance->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_ambulance->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_ambulance->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
