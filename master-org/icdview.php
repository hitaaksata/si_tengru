<?php
define("EW_PAGE_ID", "view", TRUE); // Page ID
define("EW_TABLE_NAME", 'icd', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "icdinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$icd->Export = @$_GET["export"]; // Get export parameter
$sExport = $icd->Export; // Get export parameter, used in header
$sExportFile = $icd->TableVar; // Get export file, used in header
?>
<?php
if (@$_GET["icd_code"] <> "") {
	$icd->icd_code->setQueryStringValue($_GET["icd_code"]);
} else {
	Page_Terminate("icdlist.php"); // Return to list page
}

// Get action
if (@$_POST["a_view"] <> "") {
	$icd->CurrentAction = $_POST["a_view"];
} else {
	$icd->CurrentAction = "I"; // Display form
}
switch ($icd->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // Set no record message
			Page_Terminate("icdlist.php"); // Return to list
		}
}

// Set return url
$icd->setReturnUrl("icdview.php");

// Render row
$icd->RowType = EW_ROWTYPE_VIEW;
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "view"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">View TABLE: icd
<br><br>
<a href="icdlist.php">Back to List</a>&nbsp;
<?php if ($Security->IsLoggedIn()) { ?>
<a href="icdadd.php">Add</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $icd->EditUrl() ?>">Edit</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $icd->CopyUrl() ?>">Copy</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $icd->DeleteUrl() ?>">Delete</a>&nbsp;
<?php } ?>
</span>
</p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<p>
<form>
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">icd code</td>
		<td<?php echo $icd->icd_code->CellAttributes() ?>>
<div<?php echo $icd->icd_code->ViewAttributes() ?>><?php echo $icd->icd_code->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">jenis penyakit</td>
		<td<?php echo $icd->jenis_penyakit->CellAttributes() ?>>
<div<?php echo $icd->jenis_penyakit->ViewAttributes() ?>><?php echo $icd->jenis_penyakit->ViewValue ?></div>
</td>
	</tr>
</table>
</form>
<p>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $icd;
	$sFilter = $icd->SqlKeyFilter();
	$sFilter = str_replace("@icd_code@", ew_AdjustSql($icd->icd_code->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$icd->Row_Selecting($sFilter);

	// Load sql based on filter
	$icd->CurrentFilter = $sFilter;
	$sSql = $icd->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$icd->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $icd;
	$icd->icd_code->setDbValue($rs->fields('icd_code'));
	$icd->jenis_penyakit->setDbValue($rs->fields('jenis_penyakit'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $icd;

	// Call Row Rendering event
	$icd->Row_Rendering();

	// Common render codes for all row types
	// icd_code

	$icd->icd_code->CellCssStyle = "";
	$icd->icd_code->CellCssClass = "";

	// jenis_penyakit
	$icd->jenis_penyakit->CellCssStyle = "";
	$icd->jenis_penyakit->CellCssClass = "";
	if ($icd->RowType == EW_ROWTYPE_VIEW) { // View row

		// icd_code
		$icd->icd_code->ViewValue = $icd->icd_code->CurrentValue;
		$icd->icd_code->CssStyle = "";
		$icd->icd_code->CssClass = "";
		$icd->icd_code->ViewCustomAttributes = "";

		// jenis_penyakit
		$icd->jenis_penyakit->ViewValue = $icd->jenis_penyakit->CurrentValue;
		if (!is_null($icd->jenis_penyakit->ViewValue)) $icd->jenis_penyakit->ViewValue = str_replace("\n", "<br>", $icd->jenis_penyakit->ViewValue); 
		$icd->jenis_penyakit->CssStyle = "";
		$icd->jenis_penyakit->CssClass = "";
		$icd->jenis_penyakit->ViewCustomAttributes = "";

		// icd_code
		$icd->icd_code->HrefValue = "";

		// jenis_penyakit
		$icd->jenis_penyakit->HrefValue = "";
	} elseif ($icd->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($icd->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($icd->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$icd->Row_Rendered();
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $icd;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$icd->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$icd->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $icd->getStartRecordNumber();
		}
	} else {
		$nStartRec = $icd->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$icd->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$icd->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$icd->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
