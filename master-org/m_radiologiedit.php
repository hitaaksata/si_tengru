<?php
define("EW_PAGE_ID", "edit", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_radiologi', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_radiologiinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_radiologi->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_radiologi->Export; // Get export parameter, used in header
$sExportFile = $m_radiologi->TableVar; // Get export file, used in header
?>
<?php

// Load key from QueryString
if (@$_GET["kd_rad"] <> "") {
	$m_radiologi->kd_rad->setQueryStringValue($_GET["kd_rad"]);
}

// Create form object
$objForm = new cFormObj();
if (@$_POST["a_edit"] <> "") {
	$m_radiologi->CurrentAction = $_POST["a_edit"]; // Get action code
	LoadFormValues(); // Get form values
} else {
	$m_radiologi->CurrentAction = "I"; // Default action is display
}

// Check if valid key
if ($m_radiologi->kd_rad->CurrentValue == "") Page_Terminate($m_radiologi->getReturnUrl()); // Invalid key, exit
switch ($m_radiologi->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load Record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
			Page_Terminate($m_radiologi->getReturnUrl()); // Return to caller
		}
		break;
	Case "U": // Update
		$m_radiologi->SendEmail = TRUE; // Send email on update success
		if (EditRow()) { // Update Record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "Update successful"; // Update success
			Page_Terminate($m_radiologi->getReturnUrl()); // Return to caller
		} else {
			RestoreFormValues(); // Restore form values if update failed
		}
}

// Render the record
$m_radiologi->RowType = EW_ROWTYPE_EDIT; // Render as edit
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "edit"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_kd_rad"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - kd rad"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_nama_rad"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - nama rad"))
				return false;
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Edit TABLE: m radiologi<br><br><a href="<?php echo $m_radiologi->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form name="fm_radiologiedit" id="fm_radiologiedit" action="m_radiologiedit.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_edit" id="a_edit" value="U">
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">kd rad<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_radiologi->kd_rad->CellAttributes() ?>><span id="cb_x_kd_rad">
<div<?php echo $m_radiologi->kd_rad->ViewAttributes() ?>><?php echo $m_radiologi->kd_rad->EditValue ?></div>
<input type="hidden" name="x_kd_rad" id="x_kd_rad" value="<?php echo ew_HtmlEncode($m_radiologi->kd_rad->CurrentValue) ?>">
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">nama rad<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_radiologi->nama_rad->CellAttributes() ?>><span id="cb_x_nama_rad">
<input type="text" name="x_nama_rad" id="x_nama_rad"  size="30" maxlength="128" value="<?php echo $m_radiologi->nama_rad->EditValue ?>"<?php echo $m_radiologi->nama_rad->EditAttributes() ?>>
</span></td>
	</tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="   Edit   ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $m_radiologi;
	$m_radiologi->kd_rad->setFormValue($objForm->GetValue("x_kd_rad"));
	$m_radiologi->nama_rad->setFormValue($objForm->GetValue("x_nama_rad"));
}

// Restore form values
function RestoreFormValues() {
	global $m_radiologi;
	$m_radiologi->kd_rad->CurrentValue = $m_radiologi->kd_rad->FormValue;
	$m_radiologi->nama_rad->CurrentValue = $m_radiologi->nama_rad->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_radiologi;
	$sFilter = $m_radiologi->SqlKeyFilter();
	$sFilter = str_replace("@kd_rad@", ew_AdjustSql($m_radiologi->kd_rad->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_radiologi->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_radiologi->CurrentFilter = $sFilter;
	$sSql = $m_radiologi->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_radiologi->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_radiologi;
	$m_radiologi->kd_rad->setDbValue($rs->fields('kd_rad'));
	$m_radiologi->nama_rad->setDbValue($rs->fields('nama_rad'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_radiologi;

	// Call Row Rendering event
	$m_radiologi->Row_Rendering();

	// Common render codes for all row types
	// kd_rad

	$m_radiologi->kd_rad->CellCssStyle = "";
	$m_radiologi->kd_rad->CellCssClass = "";

	// nama_rad
	$m_radiologi->nama_rad->CellCssStyle = "";
	$m_radiologi->nama_rad->CellCssClass = "";
	if ($m_radiologi->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($m_radiologi->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_radiologi->RowType == EW_ROWTYPE_EDIT) { // Edit row

		// kd_rad
		$m_radiologi->kd_rad->EditCustomAttributes = "";
		$m_radiologi->kd_rad->EditValue = $m_radiologi->kd_rad->CurrentValue;
		$m_radiologi->kd_rad->CssStyle = "";
		$m_radiologi->kd_rad->CssClass = "";
		$m_radiologi->kd_rad->ViewCustomAttributes = "";

		// nama_rad
		$m_radiologi->nama_rad->EditCustomAttributes = "";
		$m_radiologi->nama_rad->EditValue = ew_HtmlEncode($m_radiologi->nama_rad->CurrentValue);
	} elseif ($m_radiologi->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_radiologi->Row_Rendered();
}
?>
<?php

// Update record based on key values
function EditRow() {
	global $conn, $Security, $m_radiologi;
	$sFilter = $m_radiologi->SqlKeyFilter();
	$sFilter = str_replace("@kd_rad@", ew_AdjustSql($m_radiologi->kd_rad->CurrentValue), $sFilter); // Replace key value
	$m_radiologi->CurrentFilter = $sFilter;
	$sSql = $m_radiologi->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE)
		return FALSE;
	if ($rs->EOF) {
		$EditRow = FALSE; // Update Failed
	} else {

		// Save old values
		$rsold =& $rs->fields;
		$rsnew = array();

		// Field kd_rad
		// Field nama_rad

		$m_radiologi->nama_rad->SetDbValueDef($m_radiologi->nama_rad->CurrentValue, "");
		$rsnew['nama_rad'] =& $m_radiologi->nama_rad->DbValue;

		// Call Row Updating event
		$bUpdateRow = $m_radiologi->Row_Updating($rsold, $rsnew);
		if ($bUpdateRow) {
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$EditRow = $conn->Execute($m_radiologi->UpdateSQL($rsnew));
			$conn->raiseErrorFn = '';
		} else {
			if ($m_radiologi->CancelMessage <> "") {
				$_SESSION[EW_SESSION_MESSAGE] = $m_radiologi->CancelMessage;
				$m_radiologi->CancelMessage = "";
			} else {
				$_SESSION[EW_SESSION_MESSAGE] = "Update cancelled";
			}
			$EditRow = FALSE;
		}
	}

	// Call Row Updated event
	if ($EditRow) {
		$m_radiologi->Row_Updated($rsold, $rsnew);
	}
	$rs->Close();
	return $EditRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
