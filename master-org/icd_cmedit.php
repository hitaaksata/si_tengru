<?php
define("EW_PAGE_ID", "edit", TRUE); // Page ID
define("EW_TABLE_NAME", 'icd_cm', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "icd_cminfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$icd_cm->Export = @$_GET["export"]; // Get export parameter
$sExport = $icd_cm->Export; // Get export parameter, used in header
$sExportFile = $icd_cm->TableVar; // Get export file, used in header
?>
<?php

// Load key from QueryString
if (@$_GET["kode"] <> "") {
	$icd_cm->kode->setQueryStringValue($_GET["kode"]);
}

// Create form object
$objForm = new cFormObj();
if (@$_POST["a_edit"] <> "") {
	$icd_cm->CurrentAction = $_POST["a_edit"]; // Get action code
	LoadFormValues(); // Get form values
} else {
	$icd_cm->CurrentAction = "I"; // Default action is display
}

// Check if valid key
if ($icd_cm->kode->CurrentValue == "") Page_Terminate($icd_cm->getReturnUrl()); // Invalid key, exit
switch ($icd_cm->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load Record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
			Page_Terminate($icd_cm->getReturnUrl()); // Return to caller
		}
		break;
	Case "U": // Update
		$icd_cm->SendEmail = TRUE; // Send email on update success
		if (EditRow()) { // Update Record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "Update successful"; // Update success
			Page_Terminate($icd_cm->getReturnUrl()); // Return to caller
		} else {
			RestoreFormValues(); // Restore form values if update failed
		}
}

// Render the record
$icd_cm->RowType = EW_ROWTYPE_EDIT; // Render as edit
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "edit"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_kode"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - kode"))
				return false;
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Edit TABLE: icd cm<br><br><a href="<?php echo $icd_cm->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form name="ficd_cmedit" id="ficd_cmedit" action="icd_cmedit.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_edit" id="a_edit" value="U">
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">kode<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $icd_cm->kode->CellAttributes() ?>><span id="cb_x_kode">
<div<?php echo $icd_cm->kode->ViewAttributes() ?>><?php echo $icd_cm->kode->EditValue ?></div>
<input type="hidden" name="x_kode" id="x_kode" value="<?php echo ew_HtmlEncode($icd_cm->kode->CurrentValue) ?>">
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">keterangan</td>
		<td<?php echo $icd_cm->keterangan->CellAttributes() ?>><span id="cb_x_keterangan">
<textarea name="x_keterangan" id="x_keterangan" cols="35" rows="4"<?php echo $icd_cm->keterangan->EditAttributes() ?>><?php echo $icd_cm->keterangan->EditValue ?></textarea>
</span></td>
	</tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="   Edit   ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $icd_cm;
	$icd_cm->kode->setFormValue($objForm->GetValue("x_kode"));
	$icd_cm->keterangan->setFormValue($objForm->GetValue("x_keterangan"));
}

// Restore form values
function RestoreFormValues() {
	global $icd_cm;
	$icd_cm->kode->CurrentValue = $icd_cm->kode->FormValue;
	$icd_cm->keterangan->CurrentValue = $icd_cm->keterangan->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $icd_cm;
	$sFilter = $icd_cm->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($icd_cm->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$icd_cm->Row_Selecting($sFilter);

	// Load sql based on filter
	$icd_cm->CurrentFilter = $sFilter;
	$sSql = $icd_cm->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$icd_cm->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $icd_cm;
	$icd_cm->kode->setDbValue($rs->fields('kode'));
	$icd_cm->keterangan->setDbValue($rs->fields('keterangan'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $icd_cm;

	// Call Row Rendering event
	$icd_cm->Row_Rendering();

	// Common render codes for all row types
	// kode

	$icd_cm->kode->CellCssStyle = "";
	$icd_cm->kode->CellCssClass = "";

	// keterangan
	$icd_cm->keterangan->CellCssStyle = "";
	$icd_cm->keterangan->CellCssClass = "";
	if ($icd_cm->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($icd_cm->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($icd_cm->RowType == EW_ROWTYPE_EDIT) { // Edit row

		// kode
		$icd_cm->kode->EditCustomAttributes = "";
		$icd_cm->kode->EditValue = $icd_cm->kode->CurrentValue;
		$icd_cm->kode->CssStyle = "";
		$icd_cm->kode->CssClass = "";
		$icd_cm->kode->ViewCustomAttributes = "";

		// keterangan
		$icd_cm->keterangan->EditCustomAttributes = "";
		$icd_cm->keterangan->EditValue = ew_HtmlEncode($icd_cm->keterangan->CurrentValue);
	} elseif ($icd_cm->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$icd_cm->Row_Rendered();
}
?>
<?php

// Update record based on key values
function EditRow() {
	global $conn, $Security, $icd_cm;
	$sFilter = $icd_cm->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($icd_cm->kode->CurrentValue), $sFilter); // Replace key value
	$icd_cm->CurrentFilter = $sFilter;
	$sSql = $icd_cm->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE)
		return FALSE;
	if ($rs->EOF) {
		$EditRow = FALSE; // Update Failed
	} else {

		// Save old values
		$rsold =& $rs->fields;
		$rsnew = array();

		// Field kode
		// Field keterangan

		$icd_cm->keterangan->SetDbValueDef($icd_cm->keterangan->CurrentValue, NULL);
		$rsnew['keterangan'] =& $icd_cm->keterangan->DbValue;

		// Call Row Updating event
		$bUpdateRow = $icd_cm->Row_Updating($rsold, $rsnew);
		if ($bUpdateRow) {
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$EditRow = $conn->Execute($icd_cm->UpdateSQL($rsnew));
			$conn->raiseErrorFn = '';
		} else {
			if ($icd_cm->CancelMessage <> "") {
				$_SESSION[EW_SESSION_MESSAGE] = $icd_cm->CancelMessage;
				$icd_cm->CancelMessage = "";
			} else {
				$_SESSION[EW_SESSION_MESSAGE] = "Update cancelled";
			}
			$EditRow = FALSE;
		}
	}

	// Call Row Updated event
	if ($EditRow) {
		$icd_cm->Row_Updated($rsold, $rsnew);
	}
	$rs->Close();
	return $EditRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
