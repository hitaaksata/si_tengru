<?php
define("EW_PAGE_ID", "edit", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_obat', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_obatinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_obat->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_obat->Export; // Get export parameter, used in header
$sExportFile = $m_obat->TableVar; // Get export file, used in header
?>
<?php

// Load key from QueryString
if (@$_GET["kode_obat"] <> "") {
	$m_obat->kode_obat->setQueryStringValue($_GET["kode_obat"]);
}

// Create form object
$objForm = new cFormObj();
if (@$_POST["a_edit"] <> "") {
	$m_obat->CurrentAction = $_POST["a_edit"]; // Get action code
	LoadFormValues(); // Get form values
} else {
	$m_obat->CurrentAction = "I"; // Default action is display
}

// Check if valid key
if ($m_obat->kode_obat->CurrentValue == "") Page_Terminate($m_obat->getReturnUrl()); // Invalid key, exit
switch ($m_obat->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load Record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
			Page_Terminate($m_obat->getReturnUrl()); // Return to caller
		}
		break;
	Case "U": // Update
		$m_obat->SendEmail = TRUE; // Send email on update success
		if (EditRow()) { // Update Record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "Update successful"; // Update success
			Page_Terminate($m_obat->getReturnUrl()); // Return to caller
		} else {
			RestoreFormValues(); // Restore form values if update failed
		}
}

// Render the record
$m_obat->RowType = EW_ROWTYPE_EDIT; // Render as edit
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "edit"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_nama_obat"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - nama obat"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_kode_obat"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - kode obat"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_kode_obat"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - kode obat"))
				return false; 
		}
		elm = fobj.elements["x" + infix + "_group_obat"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - group obat"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_harga"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - harga"))
				return false; 
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Edit TABLE: m obat<br><br><a href="<?php echo $m_obat->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form name="fm_obatedit" id="fm_obatedit" action="m_obatedit.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_edit" id="a_edit" value="U">
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">nama obat<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_obat->nama_obat->CellAttributes() ?>><span id="cb_x_nama_obat">
<textarea name="x_nama_obat" id="x_nama_obat" cols="35" rows="4"<?php echo $m_obat->nama_obat->EditAttributes() ?>><?php echo $m_obat->nama_obat->EditValue ?></textarea>
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">kode obat<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_obat->kode_obat->CellAttributes() ?>><span id="cb_x_kode_obat">
<div<?php echo $m_obat->kode_obat->ViewAttributes() ?>><?php echo $m_obat->kode_obat->EditValue ?></div>
<input type="hidden" name="x_kode_obat" id="x_kode_obat" value="<?php echo ew_HtmlEncode($m_obat->kode_obat->CurrentValue) ?>">
</span></td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">group obat<span class='ewmsg'>&nbsp;*</span></td>
		<td<?php echo $m_obat->group_obat->CellAttributes() ?>><span id="cb_x_group_obat">
<input type="text" name="x_group_obat" id="x_group_obat"  size="30" maxlength="10" value="<?php echo $m_obat->group_obat->EditValue ?>"<?php echo $m_obat->group_obat->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">satuan</td>
		<td<?php echo $m_obat->satuan->CellAttributes() ?>><span id="cb_x_satuan">
<input type="text" name="x_satuan" id="x_satuan"  size="30" maxlength="16" value="<?php echo $m_obat->satuan->EditValue ?>"<?php echo $m_obat->satuan->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">harga</td>
		<td<?php echo $m_obat->harga->CellAttributes() ?>><span id="cb_x_harga">
<input type="text" name="x_harga" id="x_harga"  size="30" value="<?php echo $m_obat->harga->EditValue ?>"<?php echo $m_obat->harga->EditAttributes() ?>>
</span></td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">hide when print</td>
		<td<?php echo $m_obat->hide_when_print->CellAttributes() ?>><span id="cb_x_hide_when_print">
<input type="text" name="x_hide_when_print" id="x_hide_when_print"  size="30" maxlength="1" value="<?php echo $m_obat->hide_when_print->EditValue ?>"<?php echo $m_obat->hide_when_print->EditAttributes() ?>>
</span></td>
	</tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="   Edit   ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $m_obat;
	$m_obat->nama_obat->setFormValue($objForm->GetValue("x_nama_obat"));
	$m_obat->kode_obat->setFormValue($objForm->GetValue("x_kode_obat"));
	$m_obat->group_obat->setFormValue($objForm->GetValue("x_group_obat"));
	$m_obat->satuan->setFormValue($objForm->GetValue("x_satuan"));
	$m_obat->harga->setFormValue($objForm->GetValue("x_harga"));
	$m_obat->hide_when_print->setFormValue($objForm->GetValue("x_hide_when_print"));
}

// Restore form values
function RestoreFormValues() {
	global $m_obat;
	$m_obat->nama_obat->CurrentValue = $m_obat->nama_obat->FormValue;
	$m_obat->kode_obat->CurrentValue = $m_obat->kode_obat->FormValue;
	$m_obat->group_obat->CurrentValue = $m_obat->group_obat->FormValue;
	$m_obat->satuan->CurrentValue = $m_obat->satuan->FormValue;
	$m_obat->harga->CurrentValue = $m_obat->harga->FormValue;
	$m_obat->hide_when_print->CurrentValue = $m_obat->hide_when_print->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_obat;
	$sFilter = $m_obat->SqlKeyFilter();
	if (!is_numeric($m_obat->kode_obat->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@kode_obat@", ew_AdjustSql($m_obat->kode_obat->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_obat->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_obat->CurrentFilter = $sFilter;
	$sSql = $m_obat->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_obat->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_obat;
	$m_obat->nama_obat->setDbValue($rs->fields('nama_obat'));
	$m_obat->kode_obat->setDbValue($rs->fields('kode_obat'));
	$m_obat->group_obat->setDbValue($rs->fields('group_obat'));
	$m_obat->satuan->setDbValue($rs->fields('satuan'));
	$m_obat->harga->setDbValue($rs->fields('harga'));
	$m_obat->hide_when_print->setDbValue($rs->fields('hide_when_print'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_obat;

	// Call Row Rendering event
	$m_obat->Row_Rendering();

	// Common render codes for all row types
	// nama_obat

	$m_obat->nama_obat->CellCssStyle = "";
	$m_obat->nama_obat->CellCssClass = "";

	// kode_obat
	$m_obat->kode_obat->CellCssStyle = "";
	$m_obat->kode_obat->CellCssClass = "";

	// group_obat
	$m_obat->group_obat->CellCssStyle = "";
	$m_obat->group_obat->CellCssClass = "";

	// satuan
	$m_obat->satuan->CellCssStyle = "";
	$m_obat->satuan->CellCssClass = "";

	// harga
	$m_obat->harga->CellCssStyle = "";
	$m_obat->harga->CellCssClass = "";

	// hide_when_print
	$m_obat->hide_when_print->CellCssStyle = "";
	$m_obat->hide_when_print->CellCssClass = "";
	if ($m_obat->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($m_obat->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_obat->RowType == EW_ROWTYPE_EDIT) { // Edit row

		// nama_obat
		$m_obat->nama_obat->EditCustomAttributes = "";
		$m_obat->nama_obat->EditValue = ew_HtmlEncode($m_obat->nama_obat->CurrentValue);

		// kode_obat
		$m_obat->kode_obat->EditCustomAttributes = "";
		$m_obat->kode_obat->EditValue = $m_obat->kode_obat->CurrentValue;
		$m_obat->kode_obat->CssStyle = "";
		$m_obat->kode_obat->CssClass = "";
		$m_obat->kode_obat->ViewCustomAttributes = "";

		// group_obat
		$m_obat->group_obat->EditCustomAttributes = "";
		$m_obat->group_obat->EditValue = ew_HtmlEncode($m_obat->group_obat->CurrentValue);

		// satuan
		$m_obat->satuan->EditCustomAttributes = "";
		$m_obat->satuan->EditValue = ew_HtmlEncode($m_obat->satuan->CurrentValue);

		// harga
		$m_obat->harga->EditCustomAttributes = "";
		$m_obat->harga->EditValue = ew_HtmlEncode($m_obat->harga->CurrentValue);

		// hide_when_print
		$m_obat->hide_when_print->EditCustomAttributes = "";
		$m_obat->hide_when_print->EditValue = ew_HtmlEncode($m_obat->hide_when_print->CurrentValue);
	} elseif ($m_obat->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_obat->Row_Rendered();
}
?>
<?php

// Update record based on key values
function EditRow() {
	global $conn, $Security, $m_obat;
	$sFilter = $m_obat->SqlKeyFilter();
	if (!is_numeric($m_obat->kode_obat->CurrentValue)) {
		return FALSE;
	}
	$sFilter = str_replace("@kode_obat@", ew_AdjustSql($m_obat->kode_obat->CurrentValue), $sFilter); // Replace key value
	$m_obat->CurrentFilter = $sFilter;
	$sSql = $m_obat->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE)
		return FALSE;
	if ($rs->EOF) {
		$EditRow = FALSE; // Update Failed
	} else {

		// Save old values
		$rsold =& $rs->fields;
		$rsnew = array();

		// Field nama_obat
		$m_obat->nama_obat->SetDbValueDef($m_obat->nama_obat->CurrentValue, "");
		$rsnew['nama_obat'] =& $m_obat->nama_obat->DbValue;

		// Field kode_obat
		// Field group_obat

		$m_obat->group_obat->SetDbValueDef($m_obat->group_obat->CurrentValue, "");
		$rsnew['group_obat'] =& $m_obat->group_obat->DbValue;

		// Field satuan
		$m_obat->satuan->SetDbValueDef($m_obat->satuan->CurrentValue, NULL);
		$rsnew['satuan'] =& $m_obat->satuan->DbValue;

		// Field harga
		$m_obat->harga->SetDbValueDef($m_obat->harga->CurrentValue, NULL);
		$rsnew['harga'] =& $m_obat->harga->DbValue;

		// Field hide_when_print
		$m_obat->hide_when_print->SetDbValueDef($m_obat->hide_when_print->CurrentValue, NULL);
		$rsnew['hide_when_print'] =& $m_obat->hide_when_print->DbValue;

		// Call Row Updating event
		$bUpdateRow = $m_obat->Row_Updating($rsold, $rsnew);
		if ($bUpdateRow) {
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$EditRow = $conn->Execute($m_obat->UpdateSQL($rsnew));
			$conn->raiseErrorFn = '';
		} else {
			if ($m_obat->CancelMessage <> "") {
				$_SESSION[EW_SESSION_MESSAGE] = $m_obat->CancelMessage;
				$m_obat->CancelMessage = "";
			} else {
				$_SESSION[EW_SESSION_MESSAGE] = "Update cancelled";
			}
			$EditRow = FALSE;
		}
	}

	// Call Row Updated event
	if ($EditRow) {
		$m_obat->Row_Updated($rsold, $rsnew);
	}
	$rs->Close();
	return $EditRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
