<?php
define("EW_PAGE_ID", "delete", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_obat_apotek', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_obat_apotekinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_obat_apotek->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_obat_apotek->Export; // Get export parameter, used in header
$sExportFile = $m_obat_apotek->TableVar; // Get export file, used in header
?>
<?php

// Load Key Parameters
$sKey = "";
$bSingleDelete = TRUE; // Initialize as single delete
$arRecKeys = array();
$nKeySelected = 0; // Initialize selected key count
$sFilter = "";
if (@$_GET["kode_obat"] <> "") {
	$m_obat_apotek->kode_obat->setQueryStringValue($_GET["kode_obat"]);
	if (!is_numeric($m_obat_apotek->kode_obat->QueryStringValue)) {
		Page_Terminate($m_obat_apotek->getReturnUrl()); // Prevent sql injection, exit
	}
	$sKey .= $m_obat_apotek->kode_obat->QueryStringValue;
} else {
	$bSingleDelete = FALSE;
}
if ($bSingleDelete) {
	$nKeySelected = 1; // Set up key selected count
	$arRecKeys[0] = $sKey;
} else {
	if (isset($_POST["key_m"])) { // Key in form
		$nKeySelected = count($_POST["key_m"]); // Set up key selected count
		$arRecKeys = ew_StripSlashes($_POST["key_m"]);
	}
}
if ($nKeySelected <= 0) Page_Terminate($m_obat_apotek->getReturnUrl()); // No key specified, exit

// Build filter
foreach ($arRecKeys as $sKey) {
	$sFilter .= "(";

	// Set up key field
	$sKeyFld = $sKey;
	if (!is_numeric($sKeyFld)) {
		Page_Terminate($m_obat_apotek->getReturnUrl()); // Prevent sql injection, exit
	}
	$sFilter .= "kode_obat=" . ew_AdjustSql($sKeyFld) . " AND ";
	if (substr($sFilter, -5) == " AND ") $sFilter = substr($sFilter, 0, strlen($sFilter)-5) . ") OR ";
}
if (substr($sFilter, -4) == " OR ") $sFilter = substr($sFilter, 0, strlen($sFilter)-4);

// Set up filter (Sql Where Clause) and get Return Sql
// Sql constructor in m_obat_apotek class, m_obat_apotekinfo.php

$m_obat_apotek->CurrentFilter = $sFilter;

// Get action
if (@$_POST["a_delete"] <> "") {
	$m_obat_apotek->CurrentAction = $_POST["a_delete"];
} else {
	$m_obat_apotek->CurrentAction = "I"; // Display record
}
switch ($m_obat_apotek->CurrentAction) {
	case "D": // Delete
		$m_obat_apotek->SendEmail = TRUE; // Send email on delete success
		if (DeleteRows()) { // delete rows
			$_SESSION[EW_SESSION_MESSAGE] = "Delete Successful"; // Set up success message
			Page_Terminate($m_obat_apotek->getReturnUrl()); // Return to caller
		}
}

// Load records for display
$rs = LoadRecordset();
$nTotalRecs = $rs->RecordCount(); // Get record count
if ($nTotalRecs <= 0) { // No record found, exit
	$rs->Close();
	Page_Terminate($m_obat_apotek->getReturnUrl()); // Return to caller
}
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "delete"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Delete from TABLE: m obat apotek<br><br><a href="<?php echo $m_obat_apotek->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form action="m_obat_apotekdelete.php" method="post">
<p>
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($arRecKeys as $sKey) { ?>
<input type="hidden" name="key_m[]" id="key_m[]" value="<?php echo ew_HtmlEncode($sKey) ?>">
<?php } ?>
<table class="ewTable">
	<tr class="ewTableHeader">
		<td valign="top">kode obat</td>
		<td valign="top">group obat</td>
		<td valign="top">satuan</td>
		<td valign="top">harga</td>
	</tr>
<?php
$nRecCount = 0;
$i = 0;
while (!$rs->EOF) {
	$nRecCount++;

	// Set row class and style
	$m_obat_apotek->CssClass = "ewTableRow";
	$m_obat_apotek->CssStyle = "";

	// Display alternate color for rows
	if ($nRecCount % 2 <> 1) {
		$m_obat_apotek->CssClass = "ewTableAltRow";
	}

	// Get the field contents
	LoadRowValues($rs);

	// Render row value
	$m_obat_apotek->RowType = EW_ROWTYPE_VIEW; // view
	RenderRow();
?>
	<tr<?php echo $m_obat_apotek->DisplayAttributes() ?>>
		<td<?php echo $m_obat_apotek->kode_obat->CellAttributes() ?>>
<div<?php echo $m_obat_apotek->kode_obat->ViewAttributes() ?>><?php echo $m_obat_apotek->kode_obat->ViewValue ?></div>
</td>
		<td<?php echo $m_obat_apotek->group_obat->CellAttributes() ?>>
<div<?php echo $m_obat_apotek->group_obat->ViewAttributes() ?>><?php echo $m_obat_apotek->group_obat->ViewValue ?></div>
</td>
		<td<?php echo $m_obat_apotek->satuan->CellAttributes() ?>>
<div<?php echo $m_obat_apotek->satuan->ViewAttributes() ?>><?php echo $m_obat_apotek->satuan->ViewValue ?></div>
</td>
		<td<?php echo $m_obat_apotek->harga->CellAttributes() ?>>
<div<?php echo $m_obat_apotek->harga->ViewAttributes() ?>><?php echo $m_obat_apotek->harga->ViewValue ?></div>
</td>
	</tr>
<?php
	$rs->MoveNext();
}
$rs->Close();
?>
</table>
<p>
<input type="submit" name="Action" id="Action" value="Confirm Delete">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// ------------------------------------------------
//  Function DeleteRows
//  - Delete Records based on current filter
function DeleteRows() {
	global $conn, $Security, $m_obat_apotek;
	$DeleteRows = TRUE;
	$sWrkFilter = $m_obat_apotek->CurrentFilter;

	// Set up filter (Sql Where Clause) and get Return Sql
	// Sql constructor in m_obat_apotek class, m_obat_apotekinfo.php

	$m_obat_apotek->CurrentFilter = $sWrkFilter;
	$sSql = $m_obat_apotek->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE) {
		return FALSE;
	} elseif ($rs->EOF) {
		$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
		$rs->Close();
		return FALSE;
	}
	$conn->BeginTrans();

	// Clone old rows
	$rsold = ($rs) ? $rs->GetRows() : array();
	if ($rs) $rs->Close();

	// Call row deleting event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$DeleteRows = $m_obat_apotek->Row_Deleting($row);
			if (!$DeleteRows) break;
		}
	}
	if ($DeleteRows) {
		$sKey = "";
		foreach ($rsold as $row) {
			$sThisKey = "";
			if ($sThisKey <> "") $sThisKey .= EW_COMPOSITE_KEY_SEPARATOR;
			$sThisKey .= $row['kode_obat'];
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$DeleteRows = $conn->Execute($m_obat_apotek->DeleteSQL($row)); // Delete
			$conn->raiseErrorFn = '';
			if ($DeleteRows === FALSE)
				break;
			if ($sKey <> "") $sKey .= ", ";
			$sKey .= $sThisKey;
		}
	} else {

		// Set up error message
		if ($m_obat_apotek->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_obat_apotek->CancelMessage;
			$m_obat_apotek->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Delete cancelled";
		}
	}
	if ($DeleteRows) {
		$conn->CommitTrans(); // Commit the changes
	} else {
		$conn->RollbackTrans(); // Rollback changes
	}

	// Call recordset deleted event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$m_obat_apotek->Row_Deleted($row);
		}	
	}
	return $DeleteRows;
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_obat_apotek;

	// Call Recordset Selecting event
	$m_obat_apotek->Recordset_Selecting($m_obat_apotek->CurrentFilter);

	// Load list page sql
	$sSql = $m_obat_apotek->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_obat_apotek->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_obat_apotek;
	$sFilter = $m_obat_apotek->SqlKeyFilter();
	if (!is_numeric($m_obat_apotek->kode_obat->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@kode_obat@", ew_AdjustSql($m_obat_apotek->kode_obat->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_obat_apotek->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_obat_apotek->CurrentFilter = $sFilter;
	$sSql = $m_obat_apotek->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_obat_apotek->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_obat_apotek;
	$m_obat_apotek->nama_obat->setDbValue($rs->fields('nama_obat'));
	$m_obat_apotek->kode_obat->setDbValue($rs->fields('kode_obat'));
	$m_obat_apotek->group_obat->setDbValue($rs->fields('group_obat'));
	$m_obat_apotek->satuan->setDbValue($rs->fields('satuan'));
	$m_obat_apotek->harga->setDbValue($rs->fields('harga'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_obat_apotek;

	// Call Row Rendering event
	$m_obat_apotek->Row_Rendering();

	// Common render codes for all row types
	// kode_obat

	$m_obat_apotek->kode_obat->CellCssStyle = "";
	$m_obat_apotek->kode_obat->CellCssClass = "";

	// group_obat
	$m_obat_apotek->group_obat->CellCssStyle = "";
	$m_obat_apotek->group_obat->CellCssClass = "";

	// satuan
	$m_obat_apotek->satuan->CellCssStyle = "";
	$m_obat_apotek->satuan->CellCssClass = "";

	// harga
	$m_obat_apotek->harga->CellCssStyle = "";
	$m_obat_apotek->harga->CellCssClass = "";
	if ($m_obat_apotek->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode_obat
		$m_obat_apotek->kode_obat->ViewValue = $m_obat_apotek->kode_obat->CurrentValue;
		$m_obat_apotek->kode_obat->CssStyle = "";
		$m_obat_apotek->kode_obat->CssClass = "";
		$m_obat_apotek->kode_obat->ViewCustomAttributes = "";

		// group_obat
		$m_obat_apotek->group_obat->ViewValue = $m_obat_apotek->group_obat->CurrentValue;
		$m_obat_apotek->group_obat->CssStyle = "";
		$m_obat_apotek->group_obat->CssClass = "";
		$m_obat_apotek->group_obat->ViewCustomAttributes = "";

		// satuan
		$m_obat_apotek->satuan->ViewValue = $m_obat_apotek->satuan->CurrentValue;
		$m_obat_apotek->satuan->CssStyle = "";
		$m_obat_apotek->satuan->CssClass = "";
		$m_obat_apotek->satuan->ViewCustomAttributes = "";

		// harga
		$m_obat_apotek->harga->ViewValue = $m_obat_apotek->harga->CurrentValue;
		$m_obat_apotek->harga->CssStyle = "";
		$m_obat_apotek->harga->CssClass = "";
		$m_obat_apotek->harga->ViewCustomAttributes = "";

		// kode_obat
		$m_obat_apotek->kode_obat->HrefValue = "";

		// group_obat
		$m_obat_apotek->group_obat->HrefValue = "";

		// satuan
		$m_obat_apotek->satuan->HrefValue = "";

		// harga
		$m_obat_apotek->harga->HrefValue = "";
	} elseif ($m_obat_apotek->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_obat_apotek->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_obat_apotek->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_obat_apotek->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
