<?php
define("EW_PAGE_ID", "view", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_poly', TRUE);
?>
<?php 
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_polyinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_poly->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_poly->Export; // Get export parameter, used in header
$sExportFile = $m_poly->TableVar; // Get export file, used in header
?>
<?php
if (@$_GET["kode"] <> "") {
	$m_poly->kode->setQueryStringValue($_GET["kode"]);
} else {
	Page_Terminate("m_polylist.php"); // Return to list page
}

// Get action
if (@$_POST["a_view"] <> "") {
	$m_poly->CurrentAction = $_POST["a_view"];
} else {
	$m_poly->CurrentAction = "I"; // Display form
}
switch ($m_poly->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // Set no record message
			Page_Terminate("m_polylist.php"); // Return to list
		}
}

// Set return url
$m_poly->setReturnUrl("m_polyview.php");

// Render row
$m_poly->RowType = EW_ROWTYPE_VIEW;
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "view"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">View TABLE: m poly
<br><br>
<a href="m_polylist.php">Back to List</a>&nbsp;
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_polyadd.php">Add</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_poly->EditUrl() ?>">Edit</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_poly->CopyUrl() ?>">Copy</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_poly->DeleteUrl() ?>">Delete</a>&nbsp;
<?php } ?>
</span>
</p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<p>
<form>
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">kode</td>
		<td<?php echo $m_poly->kode->CellAttributes() ?>>
<div<?php echo $m_poly->kode->ViewAttributes() ?>><?php echo $m_poly->kode->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">nama</td>
		<td<?php echo $m_poly->nama->CellAttributes() ?>>
<div<?php echo $m_poly->nama->ViewAttributes() ?>><?php echo $m_poly->nama->ViewValue ?></div>
</td>
	</tr>
</table>
</form>
<p>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_poly;
	$sFilter = $m_poly->SqlKeyFilter();
	if (!is_numeric($m_poly->kode->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@kode@", ew_AdjustSql($m_poly->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_poly->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_poly->CurrentFilter = $sFilter;
	$sSql = $m_poly->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_poly->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_poly;
	$m_poly->kode->setDbValue($rs->fields('kode'));
	$m_poly->nama->setDbValue($rs->fields('nama'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_poly;

	// Call Row Rendering event
	$m_poly->Row_Rendering();

	// Common render codes for all row types
	// kode

	$m_poly->kode->CellCssStyle = "";
	$m_poly->kode->CellCssClass = "";

	// nama
	$m_poly->nama->CellCssStyle = "";
	$m_poly->nama->CellCssClass = "";
	if ($m_poly->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode
		$m_poly->kode->ViewValue = $m_poly->kode->CurrentValue;
		$m_poly->kode->CssStyle = "";
		$m_poly->kode->CssClass = "";
		$m_poly->kode->ViewCustomAttributes = "";

		// nama
		$m_poly->nama->ViewValue = $m_poly->nama->CurrentValue;
		$m_poly->nama->CssStyle = "";
		$m_poly->nama->CssClass = "";
		$m_poly->nama->ViewCustomAttributes = "";

		// kode
		$m_poly->kode->HrefValue = "";

		// nama
		$m_poly->nama->HrefValue = "";
	} elseif ($m_poly->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_poly->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_poly->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_poly->Row_Rendered();
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_poly;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_poly->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_poly->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_poly->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_poly->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_poly->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_poly->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_poly->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
