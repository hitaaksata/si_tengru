<script src="include/date-functions.js" type="text/javascript"></script>
<script src="include/datechooser.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="include/datechooser.css"/>

<div class="row">
    <div class="col-sm-12">

        <div class="card">
            <div class="card-header"><h5>Cari Berdasarkan Nama atau Nomor peserta</h5></div>
            <div class="card-block">
                <!-- <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <form  method="get" name="cariNama" >
                          <input type="hidden" name="link" value="2f" />

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Asuransi</label>
                                <div class="col-sm-10">
                                    <select name="asuransi" id="asuransi" class="form-control">
                                      <option value="2">ASKES</option>
                                      <option value="3" <?php if ($_GET['asuransi']==3) echo 'selected'?>>JAMKESMAS</option>
                                      <option value="4" <?php if ($_GET['asuransi']==4) echo 'selected'?>>SKTM</option>
                                      <option value="6" <?php if ($_GET['asuransi']==6) echo 'selected'?>>JAMKESDA (GOLD)</option>
                                      <option value="7" <?php if ($_GET['asuransi']==7) echo 'selected'?>>JAMKESDA (SILVER)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" name="nama" id="nama" value="<?=$_GET['nama']?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nomor Peserta</label>
                                <div class="col-sm-10">
                                    <input id="nopeserta" name="nopeserta" type="text" value="<?=$_GET['nopeserta']?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Alamat</label>
                                <div class="col-sm-10">
                                    <input id="alamat" name="alamat" type="text" value="<?=$_GET['alamat']?>" class="form-control">
                                </div>
                            </div>

                            <center>
                              <button type="submit" class="btn btn-xs btn-outline-info waves-effects" name="button" id="button"><i class="ti-search"></i> Cari</button>
                            </center>
                        </form>
                    </div>
                    <div class="col-sm-3"></div>
                </div> -->
                <div class="row">
                    <div class="col-sm-12">
                        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td width="24%" valign="top"></td>
                                <td width="46%">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                        <form  method="get" name="cariNama" >
                                            <input type="hidden" name="link" value="2f" />
                                            <tr>
                                                <!-- <td colspan="3" background="img/frame_title.png" bgcolor="#FFFFFF"> -->
                                                <div align="center"><strong><font color="#FFFFFF">Cari Berdasarkan Nama atau Nomor peserta</font></strong></div></td>
                                            </tr>
                                            <tr>
                                                <td width="34%">&nbsp;</td>
                                                <td width="3%">&nbsp;</td>
                                                <td width="63%">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="34%">Asuransi</td>
                                                <td width="3%">:</td>
                                                <td width="63%"><select name="asuransi" id="asuransi"><option value="2">ASKES</option>
                                                <option value="3" <?php if ($_GET['asuransi']==3) echo 'selected'?>>JAMKESMAS</option>
                                                <option value="4" <?php if ($_GET['asuransi']==4) echo 'selected'?>>SKTM</option>
                                                <option value="6" <?php if ($_GET['asuransi']==6) echo 'selected'?>>JAMKESDA (GOLD)</option>
                                                <option value="7" <?php if ($_GET['asuransi']==7) echo 'selected'?>>JAMKESDA (SILVER)</option>
                                                </select></td>
                                            </tr>
                                            <tr>
                                                <td>Nama</td>
                                                <td>:</td>
                                                <td><input type="text" name="nama" id="nama" value="<?=$_GET['nama']?>"></td>
                                            </tr>
                                            <tr>
                                                <td >Nomor Peserta</td>
                                                <td>:</td>
                                                <td><input id="nopeserta" name="nopeserta" type="text" value="<?=$_GET['nopeserta']?>"></td>
                                            </tr>
                                            <tr>
                                                <td >Alamat</td>
                                                <td>:</td>
                                                <td><input id="alamat" name="alamat" type="text" value="<?=$_GET['alamat']?>"></td>
                                            </tr>            
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td><input type="submit" name="button" id="button" value="cari" /></td>
                                            </tr>
                                            <!-- <tr>
                                                <td colspan="3" background="img/bg_menu_master.png" height="64"></td>
                                            </tr> -->
                                        </form>   
                                    </table>
                                </td>
                                <td width="30%" valign="top"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php 
 include("daftarCariAsuransi.php");
?>