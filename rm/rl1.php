<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Laporan RL1</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <div align="center">
                            <table border="0" width="95%" align="center">
                                  <tr valign="top">
                                    <td colspan="2" align="center">
                                    <fieldset><legend>1. PELAYANAN RAWAT INAP</legend>
                                    <table cellpadding="0" class="tb" width="95%" cellspacing="0">
                                      <tr>
                                        <td colspan="59">DATA KEGIATAN RUMAH SAKIT</td>
                                      </tr>
                                      <tr>
                                        <td colspan="59">Triwulan :………………</td>
                                      </tr>
                                      <tr>
                                        <td colspan="59" align="right">Formulir RL1</td>
                                      </tr>
                                      <tr>
                                        <td colspan="59">Nama    Rumah Sakit :</td>
                                      </tr>
                                      <tr>
                                        <td colspan="59" align="right">No. Kode RS:
                                          <input type="text" size="3" class="text" />
                                          <input type="text" size="3" class="text" />
                                          <input type="text" size="3" class="text" />
                                          <input type="text" size="3" class="text" />
                                          <input type="text" size="3" class="text" /></td>
                                      </tr>
                                    </table>
                                      <table cellspacing="1" cellpadding="1" class="tb" width="95%">
                                      <tr>
                                        <th width="20" rowspan="3">No</th>
                                        <th width="184" rowspan="3">JENIS PELAYANAN</th>
                                        <th width="52">Pasien</th>
                                        <th width="42">Pasien</th>
                                        <th width="41">Pasien</th>
                                        <th colspan="3" rowspan="2">Pasien Keluar Mati</th>
                                        <th width="48">Jumlah</th>
                                        <th colspan="2">Pasien</th>
                                        <th width="51">Jumlah</th>
                                        <th colspan="6">Rincian Hari Perawatan per Kelas</th>
                                      </tr>
                                      <tr>
                                        <th>Awal</th>
                                        <th rowspan="2">Masuk</th>
                                        <th>Keluar</th>
                                        <th>Lama</th>
                                        <th colspan="2">Akhir</th>
                                        <th>Hari Pe-</th>
                                        <th width="40">Kelas</th>
                                        <th width="43" rowspan="2">Kelas I</th>
                                        <th width="47" rowspan="2">Kelas II</th>
                                        <th width="52" rowspan="2">Kelas III</th>
                                        <th width="72" rowspan="2">Tanpa Kelas</th>
                                        <th width="72" rowspan="2">No.</th>
                                      </tr>
                                      <tr>
                                        <th>Triwulan</th>
                                        <th>Hidup</th>
                                        <th width="41">&lt;48 jam</th>
                                        <th width="43">&gt;/ 48 jam</th>
                                        <th width="41">Jumlah</th>
                                        <th>Dirawat</th>
                                        <th colspan="2">Triwulan</th>
                                        <th>rawatan</th>
                                        <th>Utama</th>
                                      </tr>
                                      <tr>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>5</td>
                                        <td>6</td>
                                        <td>7</td>
                                        <td>8</td>
                                        <td>9</td>
                                        <td colspan="2">10</td>
                                        <td>11</td>
                                        <td>12</td>
                                        <td>13</td>
                                        <td>14</td>
                                        <td>15</td>
                                        <td>16</td>
                                        <td width="17">17</td>
                                      </tr>
                                      <tr>
                                        <td>1</td>
                                        <td>Penyakit Dalam</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>1</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Bedah</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>2</td>
                                      </tr>
                                      <tr>
                                        <td>3</td>
                                        <td>Kesehatan Anak</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>3</td>
                                      </tr>
                                      <tr>
                                        <td>4</td>
                                        <td>Obstetri</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>4</td>
                                      </tr>
                                      <tr>
                                        <td>5</td>
                                        <td>Ginekologi</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>5</td>
                                      </tr>
                                      <tr>
                                        <td>6</td>
                                        <td>Bedah Saraf</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>6</td>
                                      </tr>
                                      <tr>
                                        <td>7</td>
                                        <td>Saraf</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>7</td>
                                      </tr>
                                      <tr>
                                        <td>8</td>
                                        <td>Jiwa</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>8</td>
                                      </tr>
                                      <tr>
                                        <td>9</td>
                                        <td>THT</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>9</td>
                                      </tr>
                                      <tr>
                                        <td>10</td>
                                        <td>Mata</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>10</td>
                                      </tr>
                                      <tr>
                                        <td>11</td>
                                        <td>Kulit &amp; Kelamin</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>11</td>
                                      </tr>
                                      <tr>
                                        <td>12</td>
                                        <td>Gigi &amp; Mulut</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>12</td>
                                      </tr>
                                      <tr>
                                        <td>13</td>
                                        <td>Kardiologi</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>13</td>
                                      </tr>
                                      <tr>
                                        <td>14</td>
                                        <td>Radioterapi</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>14</td>
                                      </tr>
                                      <tr>
                                        <td>15</td>
                                        <td>Bedah Orthopedi</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>15</td>
                                      </tr>
                                      <tr>
                                        <td>16</td>
                                        <td>Paru-paru</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>16</td>
                                      </tr>
                                      <tr>
                                        <td>17</td>
                                        <td>Kusta</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>17</td>
                                      </tr>
                                      <tr>
                                        <td>18</td>
                                        <td>Umum</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>18</td>
                                      </tr>
                                      <tr>
                                        <td>19</td>
                                        <td>Pelayanan Gawat Darurat</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>19</td>
                                      </tr>
                                      <tr>
                                        <td>20</td>
                                        <td>Rehabilitasi Medik</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>20</td>
                                      </tr>
                                      <tr>
                                        <td>21</td>
                                        <td>Isolasi</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>21</td>
                                      </tr>
                                      <tr>
                                        <td>22</td>
                                        <td>Luka Bakar</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>22</td>
                                      </tr>
                                      <tr>
                                        <td>23</td>
                                        <td>ICU</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>23</td>
                                      </tr>
                                      <tr>
                                        <td>24</td>
                                        <td>ICCU</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>24</td>
                                      </tr>
                                      <tr>
                                        <td>25</td>
                                        <td>NICU / PICU</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>25</td>
                                      </tr>
                                      <tr>
                                        <td>26</td>
                                        <td>Penatalaksana Pnyguna    NAPZA</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>26</td>
                                      </tr>
                                      <tr>
                                        <td>27</td>
                                        <td>Kedokteran Nuklir</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>27</td>
                                      </tr>
                                      <tr>
                                        <td>77</td>
                                        <td>SUB TOTAL</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>77</td>
                                      </tr>
                                      <tr>
                                        <td>88</td>
                                        <td>Perinatalogi</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>88</td>
                                      </tr>
                                      <tr>
                                        <td>99</td>
                                        <td>TOTAL</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>99</td>
                                      </tr>
                                      <tr>
                                        <td colspan="29">*) Pasien awal triwulan    = Pasien akhir triwulan periode sebelumnya : Kolom 10 = (kolom 3 + kolom 4) -    (kolom 5+kolom 8)</td>
                                      </tr>
                                    </table></fieldset>
                                    </td>
                                    

                                  </tr>
                              <tr valign="top">
                                <td align="center">
                            <fieldset>
                                        <legend>2. PENGUNJUNG RUMAH SAKIT</legend>
                                        <table cellpadding="1" cellspacing="1" class="tb">
                                          <tr>
                                            <td width="24">1</td>
                                            <th colspan="8">Pengunjung Baru</th>
                                            <td width="52" >&nbsp;</td>
                                            <td width="52" colspan="4" >Orang</td>
                                          </tr>
                                          <tr>
                                            <td >2</td>
                                            <th colspan="8">Pengunjung Lama</th>
                                            <td>&nbsp;</td>
                                            <td colspan="4">Orang</td>
                                          </tr>
                                        </table>
                                        </fieldset>
                                        <fieldset><legend>6. KESEHATAN JIWA</legend>
                                      <table cellspacing="1" cellpadding="1" class="tb">
                                        <col width="19" span="10" />
                                        <tr>
                                          <td colspan="7" width="133"></td>
                                          <td width="19"></td>
                                          <td width="19"></td>
                                          <td width="19"></td>
                                        </tr>
                                        <tr>
                                          <th colspan="2" rowspan="2" width="38">No</th>
                                          <th colspan="5">Jenis</th>
                                          <th colspan="3" rowspan="2" width="57">Kunjungan</th>
                                        </tr>
                                        <tr>
                                          <th colspan="5">Pelayanan</th>
                                        </tr>
                                        <tr>
                                          <td colspan="2">1</td>
                                          <td colspan="5">Psikotes</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">2</td>
                                          <td colspan="5">Konsultasi</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2" rowspan="2">3</td>
                                          <td colspan="5" rowspan="2" width="95">Terapi    Medikamentosa</td>
                                          <td colspan="3" rowspan="2">&nbsp;</td>
                                        </tr>
                                        <tr> </tr>
                                        <tr>
                                          <td colspan="2">4</td>
                                          <td colspan="5">Elektro Medik</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">5</td>
                                          <td colspan="5">Psikoterapi</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">6</td>
                                          <td colspan="5">Play Therapy</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2" rowspan="2">7</td>
                                          <td colspan="5" rowspan="2" width="95">Rehabilitasi    Medik Psikiatrik</td>
                                          <td colspan="3" rowspan="2">&nbsp;</td>
                                        </tr>
                                        <tr> </tr>
                                        <tr>
                                          <td colspan="2" rowspan="2">99</td>
                                          <td colspan="5" rowspan="2">TOTAL</td>
                                          <td colspan="3" rowspan="2">&nbsp;</td>
                                        </tr>
                                        <tr> </tr>
                                      </table>
                                      </fieldset>
                                      <fieldset><legend>7. PELAYANAN RAWAT DARURAT</legend>
                                      <table cellspacing="1" cellpadding="1" class="tb">
                                        <col width="19" span="21" />
                                        <tr>
                                          <td colspan="11" width="209"></td>
                                          <td width="19"></td>
                                          <td width="19"></td>
                                          <td width="19"></td>
                                          <td width="19"></td>
                                          <td width="19"></td>
                                          <td width="19"></td>
                                          <td width="19"></td>
                                          <td width="19"></td>
                                          <td width="19"></td>
                                          <td width="19"></td>
                                        </tr>
                                        <tr>
                                          <th colspan="2" rowspan="4">No</th>
                                          <th colspan="4" rowspan="4" width="76">Jenis    Pelayanan</th>
                                          <th colspan="6" rowspan="2">Total Pasien</th>
                                          <th colspan="6" rowspan="2" width="114">Tindak Lanjut Pelayanan</th>
                                          <th colspan="3" rowspan="4" width="57">Mati sebelum dirawat</th>
                                        </tr>
                                        <tr> </tr>
                                        <tr>
                                          <th colspan="3" rowspan="2">Rujukan</th>
                                          <th colspan="3" rowspan="2" width="57">Non Rujukan</th>
                                          <th colspan="2" rowspan="2" width="38">Dirawat</th>
                                          <th colspan="2" rowspan="2" width="38">Dirujuk</th>
                                          <th colspan="2" rowspan="2" width="38">Pulang</th>
                                        </tr>
                                        <tr> </tr>
                                        <tr>
                                          <td colspan="2">1</td>
                                          <td colspan="4">Bedah</td>
                                          <td colspan="3">&nbsp;</td>
                                          <td colspan="3">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">2</td>
                                          <td colspan="4">Non Bedah</td>
                                          <td colspan="3">&nbsp;</td>
                                          <td colspan="3">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">3</td>
                                          <td colspan="4">Kebidanan</td>
                                          <td colspan="3">&nbsp;</td>
                                          <td colspan="3">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">4</td>
                                          <td colspan="4">Psikiatrik</td>
                                          <td colspan="3">&nbsp;</td>
                                          <td colspan="3">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">5</td>
                                          <td colspan="4">Anak</td>
                                          <td colspan="3">&nbsp;</td>
                                          <td colspan="3">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">99</td>
                                          <td colspan="4">Total</td>
                                          <td colspan="3">&nbsp;</td>
                                          <td colspan="3">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="2">&nbsp;</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                      </table>
                                      </fieldset>
                                      <fieldset><legend>8. KUNJUNGAN RUMAH</legend>
                                      <table cellspacing="1" cellpadding="1" class="tb">
                                        <tr>
                                          <td colspan="4"></td>
                                          <td width="57"></td>
                                          <td width="57"></td>
                                        </tr>
                                        <tr>
                                          <th colspan="2" rowspan="2">No</th>
                                          <th>Jenis</th>
                                          <th colspan="3" rowspan="2">Kunjungan</th>
                                        </tr>
                                        <tr>
                                          <th>Pelayanan</th>
                                        </tr>
                                        <tr>
                                          <td colspan="2">1</td>
                                          <td>Penyakit Dalam</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">2</td>
                                          <td>Kesehatan Anak</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">&nbsp;</td>
                                          <td>a. Neonatal</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">&nbsp;</td>
                                          <td>b. Lain-lain</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">3</td>
                                          <td width="228">Obstetri dan    Ginekologi</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">4</td>
                                          <td>Saraf</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">5</td>
                                          <td width="228">Jiwa</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">99</td>
                                          <td>TOTAL</td>
                                          <td colspan="3">&nbsp;</td>
                                        </tr>
                                        </table>
                                    </fieldset>    
                                </td>
                                <td rowspan="2" align="center">
                                    <fieldset><legend>3. KUNJUNGAN RAWAT JALAN</legend>
                                    <table cellspacing="1" cellpadding="1" class="tb">
                                      <tr>
                                        <th width="20">No</th>
                                        <th width="193">Jenis Pelayanan Rawat Jalan</th>
                                        <th colspan="2">Kunjungan Baru</th>
                                        <th colspan="3" width="115">Kunjungan Lama</th>
                                        </tr>
                                      <tr>
                                        <td>1</td>
                                        <td>Penyakit Dalam</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Bedah</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>3</td>
                                        <td>Kesehatan Anak</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td>a. Neonatal</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td>b. Lain-lain</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>4</td>
                                        <td>Obstetri &amp; Ginekologi</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td>a. Ibu Hamil</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td>b. Lain-lain</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>5</td>
                                        <td>Keluarga Berencana</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>6</td>
                                        <td>Bedah Saraf</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>7</td>
                                        <td>Saraf</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>8</td>
                                        <td>Jiwa</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td>a. Napza</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>9</td>
                                        <td>THT</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>10</td>
                                        <td>Mata</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>11</td>
                                        <td>Kulit dan Kelamin</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>12</td>
                                        <td>Gigi dan Mulut</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>13</td>
                                        <td>Kardiologi</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>14</td>
                                        <td>Radiologi</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>15</td>
                                        <td>Bedah Orthopedi</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>16</td>
                                        <td>Paru-paru</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>17</td>
                                        <td>Kusta</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>18</td>
                                        <td>Umum</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>19</td>
                                        <td>Rawat Darurat</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>20</td>
                                        <td>Rehabilitasi Medik</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>21</td>
                                        <td>Akupungtur Medik</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>22</td>
                                        <td>Konsultasi Gizi</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>23</td>
                                        <td>Day care</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>24</td>
                                        <td>Lain-lain</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      <tr>
                                        <td>99</td>
                                        <td>TOTAL</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="3">&nbsp;</td>
                                        </tr>
                                      </table>
                                    </fieldset>
                                <fieldset><legend>4. KEGIATAN KEBIDANAN &amp; PERINATALOGI</legend>
                                  <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <td colspan="13" width="247"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      </tr>
                                    <tr>
                                      <th colspan="2" rowspan="2" width="34">No</th>
                                      <th colspan="7" rowspan="2" width="137">Jenis    Kegiatan</th>
                                      <th colspan="4">Berat Bayi</th>
                                      <th colspan="4">Rujukan</th>
                                      <th colspan="4">Non Rujukan</th>
                                      <th colspan="3">Dirujuk</th>
                                      </tr>
                                    <tr>
                                      <th>&gt;/2500</th>
                                      <th>&nbsp;</th>
                                      <th>&lt;2500</th>
                                      <th>&nbsp;</th>
                                      <th colspan="2">Jml</th>
                                      <th colspan="2">Mati</th>
                                      <th colspan="2">Jml</th>
                                      <th colspan="2">Mati</th>
                                      <th colspan="3">Keatas</th>
                                      </tr>
                                    <tr>
                                      <td colspan="2">1</td>
                                      <td colspan="7">Persalinan (a+b)</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="7">a. Persalinan Normal</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="7">b. Pers dg komplikasi</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Perd sbl    Persalinan</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Perd sdh    Persalinan</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Pre Eclampsi</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Eclampsi</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Infeksi</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Lain-lain</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">2</td>
                                      <td colspan="7">Sectio caesaria</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">3</td>
                                      <td colspan="7">Abortus</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">4</td>
                                      <td colspan="7">Kematian Perinatal</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Kelahiran Mati</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Mati Neonatal &lt; 7 Hari</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">5</td>
                                      <td colspan="7">Sebab Kematian</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Asphyxia</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Trauma Kelahiran</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-BBLR</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Tetanus Neonatorum</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Kelainan Congenital</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-ISPA</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Diare</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2">&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">-Lain-lain</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="2" rowspan="2">6</td>
                                      <td colspan="4" rowspan="2" width="76">Immunisasi</td>
                                      <td colspan="3">-TT1</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    <tr>
                                      <td colspan="3">-TT2</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      </tr>
                                    </table>
                                </fieldset>
                                </td>
                              </tr>
                              <tr valign="top">
                                <td>&nbsp;</td>
                                </tr>
                              <tr valign="top">
                                <td colspan="2" align="center">
                                <fieldset><legend>5. KEGIATAN PEMBEDAHAN (MENURUT GOLONGAN DAN    SPESIALISASI)</legend>
                                <table cellspacing="1" cellpadding="1" class="tb">
                                  <tr>
                                    <td colspan="22" width="418"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                  </tr>
                                  <tr>
                                    <th colspan="2" rowspan="3" width="34">No</th>
                                    <th colspan="14" rowspan="3" width="270">Spesialisasi</th>
                                    <th colspan="3" rowspan="3" width="57">Total</th>
                                    <th colspan="6">Khusus</th>
                                    <th colspan="6">Besar</th>
                                    <th colspan="6">Sedang</th>
                                    <th colspan="9">Kecil</th>
                                  </tr>
                                  <tr>
                                    <th colspan="3">Kamar</th>
                                    <th colspan="3">Unit</th>
                                    <th colspan="3">Kamar</th>
                                    <th colspan="3">Unit</th>
                                    <th colspan="3">Kamar</th>
                                    <th colspan="3">Unit</th>
                                    <th colspan="3">Kamar</th>
                                    <th colspan="3">Unit</th>
                                    <th colspan="3" rowspan="2">Poliklinik</th>
                                  </tr>
                                  <tr>
                                    <th colspan="3">Bedah</th>
                                    <th colspan="3">Darurat</th>
                                    <th colspan="3">Bedah</th>
                                    <th colspan="3">Darurat</th>
                                    <th colspan="3">Bedah</th>
                                    <th colspan="3">Darurat</th>
                                    <th colspan="3">Bedah</th>
                                    <th colspan="3">Darurat</th>
                                  </tr>
                                  <tr>
                                    <td colspan="2">1</td>
                                    <td colspan="14">Bedah</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">2</td>
                                    <td colspan="14">Obstetrik &amp; Ginekologi</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">3</td>
                                    <td colspan="14">Bedah Saraf</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">4</td>
                                    <td colspan="14">THT</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">5</td>
                                    <td colspan="14">Mata</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">6</td>
                                    <td colspan="14">Kulit &amp; Kelamin</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">7</td>
                                    <td colspan="14">Gigi &amp; Mulut</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">8</td>
                                    <td colspan="14">Kardiologi</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">9</td>
                                    <td colspan="14">Bedah Orthopedi</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">10</td>
                                    <td colspan="14">Paru-paru</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">11</td>
                                    <td colspan="14">Lain-lain</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">99</td>
                                    <td colspan="14">TOTAL</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                  </tr>
                                </table>
                                </fieldset>
                                </td>
                                </tr>
                              <tr valign="top">
                                <td>
                                <fieldset><legend>9. KEGIATAN RADIOLOGI</legend>
                                <table cellspacing="1" cellpadding="1" class="tb">
                                  <tr>
                                    <td colspan="12">A. RADIODIAGNOSTIK</td>
                                    <td colspan="5">Jumlah</td>
                                  </tr>
                                  <tr>
                                    <td width="152">1</td>
                                    <td colspan="10">Foto tanpa bahan kontras</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                  <tr>
                                    <td>2</td>
                                    <td colspan="10">Foto dengan bahan kontras</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                  <tr>
                                    <td>3</td>
                                    <td colspan="10">Foto dengan rol film</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                  <tr>
                                    <td>4</td>
                                    <td colspan="10">Flouroskopi</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                  <tr>
                                    <td>5</td>
                                    <td colspan="2">Foto gigi</td>
                                    <td>:</td>
                                    <td colspan="5">a) Dento alveolair</td>
                                    <td width="19">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="4">b) Panoramic</td>
                                    <td>&nbsp;</td>
                                    <td width="19">&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="5">c) Cephalographi</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                  <tr>
                                    <td>6</td>
                                    <td colspan="2">C.T Scan</td>
                                    <td>:</td>
                                    <td colspan="3">a) Di kepala</td>
                                    <td>&nbsp;</td>
                                    <td width="19">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="4">b) Di luar kepala</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                  <tr>
                                    <td>7</td>
                                    <td colspan="10">Lymphografi</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                  <tr>
                                    <td>8</td>
                                    <td colspan="10">Angiograpi</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                  <tr>
                                    <td>9</td>
                                    <td colspan="10">Lain-lain</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                  <tr>
                                    <td>99</td>
                                    <td colspan="10">TOTAL</td>
                                    <td colspan="3">&nbsp;</td>
                                    <td colspan="2">kali</td>
                                  </tr>
                                </table>
                                  <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <td colspan="4">B. RADIOTHERAPI</td>
                                      </tr>
                                    <tr>
                                      <td width="114">1</td>
                                      <td width="133">Jumlah Kegiatan    Radiotherapi</td>
                                      <td width="95">&nbsp;</td>
                                      <td>Org</td>
                                    </tr>
                                    <tr>
                                      <td>2</td>
                                      <td>Lain-lain</td>
                                      <td>&nbsp;</td>
                                      <td>Org</td>
                                    </tr>
                                  </table>
                                    <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <td colspan="12">C. KEDOKTERAN NUKLIR</td>
                                      <td colspan="5" width="95">Jumlah</td>
                                    </tr>
                                    <tr>
                                      <td width="152">1</td>
                                      <td colspan="10">Jumlah Kegiatan Diagnostik</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Org</td>
                                    </tr>
                                    <tr>
                                      <td>2</td>
                                      <td colspan="10">Jumlah Kegiatan Therapi</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Org</td>
                                    </tr>
                                    <tr>
                                      <td>3</td>
                                      <td colspan="10">Lain-lain</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Org</td>
                                    </tr>
                                  </table>
                                  <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <td colspan="12">D. IMAGING / PENCITRAAN</td>
                                      <td colspan="5" width="95">Jumlah</td>
                                    </tr>
                                    <tr>
                                      <td width="152">1</td>
                                      <td colspan="10">USG</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td>2</td>
                                      <td colspan="10">MRI</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td>3</td>
                                      <td colspan="10">Lain-lain</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <fieldset><legend>10. KEGIATAN PELAYANAN KHUSUS</legend>
                                  <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <td colspan="11" width="209"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                    </tr>
                                    <tr>
                                      <th colspan="2">NO</th>
                                      <th colspan="11">JENIS KEGIATAN</th>
                                      <th colspan="5">JUMLAH</th>
                                    </tr>
                                    <tr>
                                      <td colspan="2">1</td>
                                      <td colspan="11">Electro Encephalografi    (EEG)</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">2</td>
                                      <td colspan="11">Electro Kardiographi (EKG)</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">3</td>
                                      <td colspan="11">Endoskopi (semua bentuk)</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">4</td>
                                      <td colspan="11">Hemodialisa</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">5</td>
                                      <td colspan="11">Densitometri Tulang</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">6</td>
                                      <td colspan="11">Koreksi Fraktur /    Dislokasi non Bedah</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">7</td>
                                      <td colspan="11">Pungsi</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">8</td>
                                      <td colspan="11">Spirometri</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">9</td>
                                      <td colspan="11">Tes Kulit/Alergi/Histamin</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">10</td>
                                      <td colspan="11">Topometri</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">11</td>
                                      <td colspan="11">Treadmill/Exercise Test</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">12</td>
                                      <td colspan="11">Lain-lain</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">Kali</td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <fieldset><legend>13. PELAYANAN REHABILITASI MEDIK</legend>
                                  <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <td colspan="12" width="228"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                    </tr>
                                    <tr>
                                      <th colspan="2">No</th>
                                      <th colspan="9">JENIS TINDAKAN</th>
                                      <th colspan="3">Jumlah</th>
                                      <th colspan="2">No</th>
                                      <th colspan="10">JENIS TINDAKAN</th>
                                      <th colspan="3">Jumlah</th>
                                    </tr>
                                    <tr>
                                      <td colspan="2">1</td>
                                      <td colspan="9">Media</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">3.4</td>
                                      <td colspan="10">Proper Body Mekanik</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2" rowspan="2">1.1</td>
                                      <td colspan="9" rowspan="2" width="171">Gait    Analyzer</td>
                                      <td colspan="3" rowspan="2">&nbsp;</td>
                                      <td colspan="2" rowspan="2">&nbsp;</td>
                                      <td colspan="10">Pembuatan Alat Lontar    &amp; Adaptasi</td>
                                      <td colspan="3" rowspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="10">Alat</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">1.2</td>
                                      <td colspan="9">EMG</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Analisa Persiapan Kerja</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">1.3</td>
                                      <td colspan="9">Uro Dinamic</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Latihan Relaksasi</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">1.4</td>
                                      <td colspan="9">Side Back</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2" rowspan="2">&nbsp;</td>
                                      <td colspan="10">Analisa &amp; Intervensi,    Persepsi,</td>
                                      <td colspan="3" rowspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">1.5</td>
                                      <td colspan="9">EN tere</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="10">Kognitif, Psikomotor</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">1.6</td>
                                      <td colspan="9">Spirometer</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Terapi Wicara</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">1.7</td>
                                      <td colspan="9">Static Bicycle</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Fungsi Bicara</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">1.8</td>
                                      <td colspan="9">Tread Mill</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Fungsi Bahasa/ laku</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">1.9</td>
                                      <td colspan="9">Body Platysmograf</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Fungsi Menelan</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">2</td>
                                      <td colspan="9">Fisioterapi</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Psikologi</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">2.1</td>
                                      <td colspan="9">Latihan Fisik</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Psikologi Anak</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">2.2</td>
                                      <td colspan="9">Aktinoterapi</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Psikolog Dewasa</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">2.3</td>
                                      <td colspan="9">Elektroterapi</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Sosial Medis</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">2.4</td>
                                      <td colspan="9">Hidroterapi</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Evaluasi Lingkungan Rumah</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">2.5</td>
                                      <td colspan="9">Traksi Lumbal &amp;    Cervical</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Evaluasi Ekonomi</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">2.6</td>
                                      <td colspan="9">Lain-lain</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Evaluasi Pekerjaan</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">3</td>
                                      <td colspan="9">Okupasiterapi</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Ortotik Prostetik</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">3.1</td>
                                      <td colspan="9">Snoosien Room</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Pembuatan Alat Bantu</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">3.2</td>
                                      <td colspan="9">Sensor Integrasi</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Pembuatan Alat Anggota    Tiruan</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2" rowspan="2">3.3</td>
                                      <td colspan="9">Latihan aktivitas kehidupan </td>
                                      <td colspan="3" rowspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Lain-lain</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="9">sehari-hari</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="10">Kunjungan Rumah</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <fieldset><legend>16. KEGIATAN KESEHATAN GIGI</legend>
                                    <table cellspacing="1" class="tb" cellpadding="1">
                                      <col width="19" span="14" />
                                      <tr>
                                        <td colspan="2" width="38">1</td>
                                        <td colspan="8" width="152">Tumpatan    Gigi Tetap</td>
                                        <td colspan="2" width="38">&nbsp;</td>
                                        <td colspan="2" width="38">gigi</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">2</td>
                                        <td colspan="8">Tumpatan Gigi Sulung</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">gigi</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">3</td>
                                        <td colspan="8">Pengobatan Pulpa</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">kali</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">4</td>
                                        <td colspan="8">Pencabutan Gigi Tetap</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">gigi</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">5</td>
                                        <td colspan="8">Pencabutan Gigi Sulung</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">gigi</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">6</td>
                                        <td colspan="8">Pengobatan Periodontal</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">kali</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">7</td>
                                        <td colspan="8">Pengobatan Abses</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">kali</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">8</td>
                                        <td colspan="8">Pembersihan Karang Gigi</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">kali</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">9</td>
                                        <td colspan="8">Prothese Lengkap</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">buah</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">10</td>
                                        <td colspan="8">Prothese Sebagian</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">buah</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">11</td>
                                        <td colspan="8">Prothese Cekat</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">buah</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">12</td>
                                        <td colspan="8">Orthodonti</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">buah</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">13</td>
                                        <td colspan="8">Bedah Mulut</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="2">kali</td>
                                      </tr>
                                    </table>
                                  </fieldset>
                                  <fieldset><legend>18. TRANSFUSI DARAH</legend>
                                  <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <td colspan="7" width="133"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                    </tr>
                                    <tr>
                                      <td>1</td>
                                      <td colspan="5">Jumlah Pasien</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">Org</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="7">- Obstetrik /Kebidanan</td>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">Org</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="5">- Cidera (Injury)</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">Org</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="4">- Lain-Lain</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">Org</td>
                                    </tr>
                                    <tr>
                                      <td>2</td>
                                      <td colspan="6">Penerimaan darah</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="5">- Dari PMI (UTD)</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">Ktg</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="8">-    Diambil di Rumah Sakit</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">Ktg</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="5">- Dari RS Lain</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">Ktg</td>
                                    </tr>
                                    <tr>
                                      <td>3</td>
                                      <td colspan="4">Pemakaian</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="5">- Whole Blood</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">Ktg</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="6">- Packed Red Cell</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">Ktg</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="4">- Thrombo</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">Ktg</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="4">- Lain-Lain</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">Ktg</td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  </td>
                                <td>
                                <fieldset><legend>11. PEMERIKSAAN LABORATORIUM</legend>
                                  <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <td colspan="10"></td>
                                      <td width="106"></td>
                                      <td width="15"></td>
                                      <td width="15"></td>
                                      <td width="15"></td>
                                      <td width="15"></td>
                                      <td width="15"></td>
                                      <td width="14"></td>
                                      <td width="10"></td>
                                      <td width="10"></td>
                                      <td width="10"></td>
                                      <td width="10"></td>
                                      <td width="10"></td>
                                      <td width="8"></td>
                                      <td width="11"></td>
                                      <td width="11"></td>
                                      <td width="11"></td>
                                      <td width="11"></td>
                                      <td width="11"></td>
                                      <td width="12"></td>
                                      <td width="7"></td>
                                      <td width="7"></td>
                                      <td width="7"></td>
                                      <td width="7"></td>
                                      <td width="7"></td>
                                      <td width="7"></td>
                                      <td width="8"></td>
                                      <td width="10"></td>
                                      <td width="10"></td>
                                      <td width="3"></td>
                                      <td width="3"></td>
                                      <td width="65"></td>
                                    </tr>
                                    <tr>
                                      <td colspan="36">A. PATOLOGI KLINIK</td>
                                    </tr>
                                    <tr>
                                      <th width="24">NO</th>
                                      <th colspan="10">JENIS PEMERIKSAAN</th>
                                      <th colspan="6">SEDERHANA</th>
                                      <th colspan="6">SEDANG</th>
                                      <th colspan="6">CANGGIH</th>
                                      <th colspan="7">TOTAL</th>
                                    </tr>
                                    <tr>
                                      <td>1</td>
                                      <td colspan="10">Kimia</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>2</td>
                                      <td colspan="10">Gula Darah</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>3</td>
                                      <td colspan="10">Hematologi</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>4</td>
                                      <td colspan="10">Serologi</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>5</td>
                                      <td colspan="10">Bakteriologi</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>6</td>
                                      <td colspan="10">Liquor</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>7</td>
                                      <td colspan="10">Transudat / Exsudat</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>8</td>
                                      <td colspan="10">Urine</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>9</td>
                                      <td colspan="10">Tinja</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>10</td>
                                      <td colspan="10">Analisa Gas Darah</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>11</td>
                                      <td colspan="10">Radio Assay</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>12</td>
                                      <td colspan="10">Cairan Otak</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>13</td>
                                      <td colspan="10">Cairan Tubuh Lainnya</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>14</td>
                                      <td colspan="10">Immunologi</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>15</td>
                                      <td colspan="10">Mikrobiologi Klinik</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>16</td>
                                      <td colspan="10">Lain-lain</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>99</td>
                                      <td colspan="10">TOTAL</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                    </tr>
                                  </table>
                                  <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <td colspan="41">B. PATOLOGI ANATOMI</td>
                                      </tr>
                                    <tr>
                                      <th width="19">NO</th>
                                      <th colspan="10">JENIS PEMERIKSAAN</th>
                                      <th colspan="7">SEDERHANA</th>
                                      <th colspan="7">SEDANG</th>
                                      <th colspan="7">CANGGIH</th>
                                      <th colspan="8">TOTAL</th>
                                    </tr>
                                    <tr>
                                      <td>1</td>
                                      <td colspan="10">Pemeriksaan Sitologi</td>
                                      <td colspan="7">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                      <td colspan="8">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>2</td>
                                      <td colspan="10">Pemeriksaan Histologi</td>
                                      <td colspan="7">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                      <td colspan="8">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>3</td>
                                      <td colspan="10">Lain - lain</td>
                                      <td colspan="7">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                      <td colspan="8">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>4</td>
                                      <td colspan="10">TOTAL</td>
                                      <td colspan="7">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                      <td colspan="7">&nbsp;</td>
                                      <td colspan="8">&nbsp;</td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <fieldset><legend>12. KEGIATAN FARMASI RUMAH SAKIT</legend>
                                  <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <td colspan="11"></td>
                                      <td width="84"></td>
                                    </tr>
                                    <tr>
                                      <td colspan="49">A. PENGADAAN OBAT</td>
                                      </tr>
                                    <tr>
                                      <th width="28" rowspan="2">NO</th>
                                      <th colspan="10" rowspan="2">GOLONGAN OBAT</th>
                                      <th colspan="6">Jumlah Item Obat</th>
                                      <th colspan="12">Yang Tersedia di Rumah Sakit</th>
                                      <th width="151" colspan="18" rowspan="2">Keterangan</th>
                                    </tr>
                                    <tr>
                                      <th colspan="6">Sesuai Kebutuhan</th>
                                      <th colspan="6">Jumlah Item</th>
                                      <th colspan="6">% Ketersediaan</th>
                                    </tr>
                                    <tr>
                                      <td>1</td>
                                      <td colspan="10">FORMULARIUM</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="19">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="10">a. Generik</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="19">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="10">b. Non Generik</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="19">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>2</td>
                                      <td colspan="10">NON FORMULARIUM</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="19">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="10">a. Generik</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="19">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="10">b. Non Generik</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="19">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>99</td>
                                      <td colspan="10">TOTAL</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="19">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="10">a. Generik</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="19">Total Obat Formul.&amp; Non Formul</td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="10">b. Non Generik</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="6">&nbsp;</td>
                                      <td colspan="19">Total Obat Formul.&amp; Non Formul</td>
                                    </tr>
                                  </table>
                                  <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <td colspan="41">B. PENULISAN DAN PELAYANAN RESEP (R/)</td>
                                      </tr>
                                    <tr>
                                      <th width="247" rowspan="2">NO</th>
                                      <th colspan="10" rowspan="2" width="209">GOLONGAN OBAT</th>
                                      <th colspan="3" rowspan="2" width="76">Rawat Jalan</th>
                                      <th colspan="4" rowspan="2" width="76">UGD</th>
                                      <th colspan="4" rowspan="2" width="76">Rawat Inap</th>
                                      <th colspan="8">TOTAL</th>
                                      <th colspan="9">R/ Yang Dilayani Rumah Sakit</th>
                                    </tr>
                                    <tr>
                                      <th colspan="4">Jumlah    R/</th>
                                      <th colspan="4">% thd total</th>
                                      <th colspan="4">Jumlah R/</th>
                                      <th colspan="5">% thd total</th>
                                    </tr>
                                    <tr>
                                      <td>1</td>
                                      <td colspan="10">2</td>
                                      <td colspan="3">3</td>
                                      <td colspan="4">4</td>
                                      <td colspan="4">5</td>
                                      <td colspan="4">6=(3+4+5)</td>
                                      <td colspan="4">7</td>
                                      <td colspan="4">8</td>
                                      <td colspan="5">9=8/6</td>
                                    </tr>
                                    <tr>
                                      <td>1</td>
                                      <td colspan="10">Obat Generik</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>2</td>
                                      <td colspan="10">Obat Generik Formularium</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>3</td>
                                      <td colspan="10">Obat Non Generik</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>99</td>
                                      <td colspan="10">TOTAL</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <fieldset><legend>14. KEGIATAN KELUARGA BERENCANA</legend>
                                  <table width="578" cellpadding="1" cellspacing="1" class="tb">
                                    <tr>
                                      <th width="20" rowspan="3">No</th>
                                      <th colspan="5" rowspan="3">METODA</th>
                                      <th colspan="10">Peserta KB Baru</th>
                                      <th colspan="2">Kunju</th>
                                      <th colspan="8">Keluhan Efek</th>
                                    </tr>
                                    <tr>
                                      <th colspan="2">Bukan </th>
                                      <th colspan="2">Rujukan</th>
                                      <th colspan="3">Rujukan</th>
                                      <th colspan="2" rowspan="2">Total</th>
                                      <th colspan="2">ngan</th>
                                      <th colspan="8">Samping</th>
                                    </tr>
                                    <tr>
                                      <th colspan="2">Rujukan</th>
                                      <th colspan="2">R. Inap</th>
                                      <th colspan="3">R. Jalan</th>
                                      <th colspan="2">ulang</th>
                                      <th colspan="3">Jumlah</th>
                                      <th width="100" colspan="5">Dirujuk Keatas</th>
                                    </tr>
                                    <tr>
                                      <td>1</td>
                                      <td colspan="5">IUD</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>2</td>
                                      <td colspan="5">Pil</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>3</td>
                                      <td colspan="5">Kondom</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>4</td>
                                      <td colspan="5">Obat Vaginal</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>5</td>
                                      <td colspan="5">MO Pria</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>6</td>
                                      <td colspan="5">MO Wanita</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>7</td>
                                      <td colspan="5">Suntikan</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>8</td>
                                      <td colspan="5">Implant</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td>99</td>
                                      <td colspan="5">TOTAL</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="2">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <fieldset><legend>15. KEGIATAN PENYULUHAN KESEHATAN</legend>
                                  <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <td colspan="13" width="247"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                      <td width="19"></td>
                                    </tr>
                                    <tr>
                                      <th colspan="2" rowspan="2" width="38">No</th>
                                      <th colspan="4">Topik</th>
                                      <th colspan="5">Pemasangan</th>
                                      <th colspan="4">Pemutaran</th>
                                      <th colspan="3">Ceramah</th>
                                      <th colspan="3">Demons</th>
                                      <th colspan="3">Pameran</th>
                                      <th colspan="3">Pelatihan</th>
                                      <th colspan="3">Lain-lain</th>
                                    </tr>
                                    <tr>
                                      <th colspan="4">Penyuluhan</th>
                                      <th colspan="5">Poster (Ya/Tdk)</th>
                                      <th colspan="4">Kaset(kali)</th>
                                      <th colspan="3">(kali)</th>
                                      <th colspan="3">trasi(kali)</th>
                                      <th colspan="3">(kali)</th>
                                      <th colspan="3">(kali)</th>
                                      <th colspan="3">(kali)</th>
                                    </tr>
                                    <tr>
                                      <td colspan="2">1</td>
                                      <td colspan="4">Kes. Umum</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">2</td>
                                      <td colspan="4">KB</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">3</td>
                                      <td colspan="4">KIA</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">4</td>
                                      <td colspan="4">Gizi</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">5</td>
                                      <td colspan="4">Immunisasi</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">6</td>
                                      <td colspan="4">Usila</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">7</td>
                                      <td colspan="4">Peny. Diare</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">8</td>
                                      <td colspan="4">Gigi &amp; Mulut</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">9</td>
                                      <td colspan="4">Jiwa</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">10</td>
                                      <td colspan="4">Napza</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">11</td>
                                      <td colspan="4">Lain-lain</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <fieldset><legend>17. PEMANTAUAN DOKTER &amp; TENAGA KESEHATAN ASING LAINNYA</legend>
                                  <table cellspacing="1" cellpadding="1" class="tb">
                                    <tr>
                                      <th width="760" colspan="2" rowspan="2">No</th>
                                      <th width="1520" colspan="4">Jenis</th>
                                      <th width="1140" colspan="3">Asal</th>
                                      <th width="1900" colspan="5">Status</th>
                                      <th width="1140" colspan="3">Lama</th>
                                      <th width="1159" colspan="4">Jenis </th>
                                      <th colspan="3" rowspan="2" width="57">Jumlah</th>
                                    </tr>
                                    <tr>
                                      <th colspan="4">Keahlian</th>
                                      <th colspan="3">Negara</th>
                                      <th colspan="5">Kepegawaian</th>
                                      <th colspan="3">Domisili</th>
                                      <th colspan="4">Pelayanan</th>
                                    </tr>
                                    <tr>
                                      <td colspan="2">1</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">2</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">3</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">4</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">5</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">dst</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="5">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                      <td colspan="4">&nbsp;</td>
                                      <td colspan="3">&nbsp;</td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  </td>
                              </tr>
                              <tr valign="top">
                                <td colspan="2">
                                <fieldset><legend>19. LATIHAN / KURSUS / PENATARAN (KALAKARYA / INSERVICE TRAINING) YANG BERAKHIR DALAM TRIWULAN INI</legend>
                                <table align="center" cellspacing="1" cellpadding="1" width="95%" class="tb">
                                  <tr>
                                    <th colspan="2" rowspan="2">No</th>
                                    <th colspan="17" rowspan="2">Kategori Pelatihan</th>
                                    <th colspan="21">Rumah Sakit Sendiri</th>
                                    <th colspan="21">Rumah Sakit / Instansi Lain</th>
                                  </tr>
                                  <tr>
                                    <th colspan="7">Dokter</th>
                                    <th colspan="7">Tenaga Kes. Lainnya</th>
                                    <th colspan="7">Non Kes. Lainnya</th>
                                    <th colspan="7">Dokter</th>
                                    <th colspan="7">Tenaga Kes. Lainnya</th>
                                    <th width="47" colspan="7">Non Kes. Lainnya</th>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="17">Teknis</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="17">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="17">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="17">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="17">Teknis Fungsional</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="17">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="17">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="17">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                  </tr>
                                </table>
                                </fieldset>
                                </td>
                                </tr>
                              <tr valign="top">
                              <td>
                              <fieldset><legend>20. PEMBEDAHAN MATA</legend>
                                <table cellspacing="1" cellpadding="1" class="tb">
                                  <tr>
                                    <td colspan="8" width="152"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                    <td width="19"></td>
                                  </tr>
                                  <tr>
                                    <th colspan="2" rowspan="4" width="38">No</th>
                                    <th colspan="9" rowspan="4" width="171">Jenis Kegiatan</th>
                                    <th colspan="12">Kualitas</th>
                                    <th colspan="4" rowspan="2" width="76">Kuantitas</th>
                                  </tr>
                                  <tr>
                                    <th colspan="12">Hasil    (BCVS)</th>
                                  </tr>
                                  <tr>
                                    <th colspan="4">Baik</th>
                                    <th colspan="4">Sedang</th>
                                    <th colspan="4">Buruk</th>
                                    <th colspan="4" rowspan="2" width="76">Jumlah</th>
                                  </tr>
                                  <tr>
                                    <th colspan="4">&gt;6/12</th>
                                    <th colspan="4">6/18 - 6/12</th>
                                    <th colspan="4">&lt;6/18</th>
                                  </tr>
                                  <tr>
                                    <td colspan="2">1</td>
                                    <td colspan="9">Katarak / Refraktif</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="9">- Ekstraksi Katarak</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2" rowspan="2">&nbsp;</td>
                                    <td colspan="9" rowspan="2" width="171">- Ekstraksi    Katarak (Implantasi Lensa)</td>
                                    <td colspan="4" rowspan="2">&nbsp;</td>
                                    <td colspan="4" rowspan="2">&nbsp;</td>
                                    <td colspan="4" rowspan="2">&nbsp;</td>
                                    <td colspan="4" rowspan="2">&nbsp;</td>
                                  </tr>
                                  <tr> </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="9">-Refraktip</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                </table>
                                <table cellspacing="1" cellpadding="1" class="tb">
                                  <tr>
                                    <th colspan="2" rowspan="4" width="38">No</th>
                                    <th colspan="9" rowspan="4" width="171">Jenis    Kegiatan</th>
                                    <th colspan="12" width="228">Kualitas</th>
                                    <th colspan="4" rowspan="2" width="76">Kuantitas</th>
                                  </tr>
                                  <tr>
                                    <th colspan="12">TIO</th>
                                  </tr>
                                  <tr>
                                    <th colspan="4">Turun    Obat</th>
                                    <th colspan="4">Turun Obat</th>
                                    <th colspan="4">Tetap</th>
                                    <th colspan="4" rowspan="2" width="76">Jumlah</th>
                                  </tr>
                                  <tr>
                                    <th colspan="4">(-)</th>
                                    <th colspan="4">(+)</th>
                                    <th colspan="4">/ Meningkat</th>
                                  </tr>
                                  <tr>
                                    <td colspan="2">2</td>
                                    <td colspan="9">Glaukoma</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="9">- Skletal Buckling</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="9"> - Vitrektomi</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                </table>
                                <table cellspacing="1" cellpadding="1" class="tb">
                                  <tr>
                                    <th colspan="2" rowspan="2" width="38">No</th>
                                    <th colspan="7" rowspan="2" width="133">Jenis Kegiatan</th>
                                    <th colspan="14" width="266">Kualitas</th>
                                    <th colspan="4" width="76">Kuantitas</th>
                                  </tr>
                                  <tr>
                                    <th colspan="7">Perbaikan    Anatromik</th>
                                    <th colspan="7">Perbaikan Visus</th>
                                    <th colspan="4" width="76">Jumlah</th>
                                  </tr>
                                  <tr>
                                    <td colspan="2">3</td>
                                    <td colspan="7">Retina</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">- Skletal Buckling</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7"> - Vitrektomi</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                </table>
                                <table cellspacing="1" cellpadding="1" class="tb">
                                  <tr>
                                    <th colspan="2" rowspan="2" width="38">No</th>
                                    <th colspan="7" rowspan="2" width="133">Jenis Kegiatan</th>
                                    <th colspan="14" width="266">Kualitas</th>
                                    <th colspan="4" width="76">Kuantitas</th>
                                  </tr>
                                  <tr>
                                    <th colspan="7">Perbaikan -</th>
                                    <th colspan="7">Perbaikan +</th>
                                    <th colspan="4" width="76">Jumlah</th>
                                  </tr>
                                  <tr>
                                    <td colspan="2">4</td>
                                    <td colspan="7">Kornea / Infeksi</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">Pterygium</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">Conjungtival Flap</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">Transplantasi Ammon</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">Transplantasi Kornea</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">- Stabismus</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">- Orbita</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">+ Keganasan / Tumor</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">+ Rekonstruktif</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">Eviserasi / Enucleasi</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">Adneksa</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">- Tumor</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">- Relansmukaf</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="7">- Estetika</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="7">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                  </tr>
                                </table>
                              </fieldset></td>
                                <td></td>
                              </tr>
                              <tr valign="top">
                                <td>&nbsp;</td>
                                <td></td>
                              </tr>
                              <tr valign="top">
                                <td>&nbsp;</td>
                                <td>
                                </td>
                              </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>