<style>
	thead th, thead td{text-align:center;}
	thead tr:last{border-bottom :1px solid #999;}
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Laporan RL 3.2</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                    	<table border="0" width="100%">
							<tr valign="top">
								<td align="center">
									<table cellpadding="0" class="tb" width="100%" cellspacing="0" class="table-responsive">
										<tr>
											<td rowspan="2" style="width:110px;">
												<img src="<?php echo _BASE_;?>/img/logobaktihusda.gif"></td>
											<td><h2>Formulir 3.2</h2></td>
											<td rowspan="2">
												<div style="border:1px dashed #999; padding:10px; display:block; font-style:italic; width:170px;">Ditjen Bina Upaya Kesehatan <br />Kementrian Kesehatan RI</div> 
											</td>
										</tr>
										<tr><td><h1>KUNJUNGAN GAWAT DARURAT</h1></td></tr>
									</table>
								
									<table cellpadding="0" class="tb" width="100%" cellspacing="0" class="table-responsive">
										<tr><td> Kode RS </td><td>: <input type="text" name="kode_rs" class="inputrl12" /></td></tr>
						                <tr><td> Nama RS </td><td>: <input type="text" name="nama_rs" class="inputrl12" /></td></tr>
						                <tr><td> Tahun </td><td>: <input type="text" name="tahun" class="inputrl12" /></td></tr>
						                <tr><td colspan="2">&nbsp;</td></tr>
						                <tr><td colspan="2">&nbsp;</td></tr>
						                <!--<tr><td colspan="2"><h2>RL 1.2 Indikator Pelayanan Rumah Sakit</h2></td></tr>-->
									</table>
									<br>
									<table cellspacing="1" cellpadding="1" class="tb" width="100%" class="table-responsive">
										<thead>
											<tr>
												<th rowspan="2">NO</th>
												<th rowspan="2">JENIS PELAYANAN</th>
												<th colspan="2">TOTAL PASIEN</th>
												<th colspan="3">TINDAK LANJUT PELAYANAN</th>
												<th rowspan="2">MATI DI IGD</th>
												<th rowspan="2">DOA</th>
											</tr>
											<tr>
												<th>RUJUKAN</th>
												<th>NON RUJUKAN</th>
												<th>DIRAWAT</th>
												<th>DIRUJUK</th>
												<th>PULANG</th></tr>
											<tr>
												<td>1</td>
												<td>2</td>
												<td>3</td>
												<td>4</td>
												<td>5</td>
												<td>6</td>
												<td>7</td>
												<td>8</td>
												<td>9</td>
											</tr>
										</thead>
										<tbody>
											<?php
												$sql	= mysql_query('select * from m_unit where kode_unit <> 0 and kode_unit <> 14 and kode_unit <> 32');
												if(mysql_num_rows($sql) > 0){
													$i	= 1;
													while($data	= mysql_fetch_array($sql)){ ?>
														<tr>
															<td align="center"><?php echo $i++ ?></td>
															<td><?php echo $data['nama_unit'] ?></td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<!-- <td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td> -->
														</tr>
											<?
													}
												}
											?>
										</tbody>
									</table>
						        </td>
						    </tr>
						</table>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
