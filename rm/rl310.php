<style>
	thead th, thead td{text-align:center;}
	thead tr:last{border-bottom :1px solid #999;}
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Laporan RL 3.10</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <table border="0" width="100%">
							<tr valign="top">
								<td align="center">
									<table cellpadding="0" class="tb" width="100%" cellspacing="0" class="table-responsive">
										<tr>
											<td rowspan="2" style="width:110px;"><img src="<?php echo _BASE_;?>/img/logobaktihusda.gif"></td>
											<td><h2>Formulir 3.10</h2></td>
											<td rowspan="2">
												<div style="border:1px dashed #999; padding:10px; display:block; font-style:italic; width:170px;">Ditjen Bina Upaya Kesehatan <br />Kementrian Kesehatan RI</div> 
											</td>
										</tr>
										<tr><td><h1>KEGIATAN PELAYANAN KHUSUS</h1></td></tr>
									</table>
								
									<table cellpadding="0" class="tb" width="100%" cellspacing="0" class="table-responsive">
										<tr><td> Kode RS </td><td>: <input type="text" name="kode_rs" class="inputrl12" /></td></tr>
						                <tr><td> Nama RS </td><td>: <input type="text" name="nama_rs" class="inputrl12" /></td></tr>
						                <tr><td> Tahun </td><td>: <input type="text" name="tahun" class="inputrl12" /></td></tr>
						                <tr><td colspan="2">&nbsp;</td></tr>
						                <tr><td colspan="2">&nbsp;</td></tr>
						                <!--<tr><td colspan="2"><h2>RL 1.2 Indikator Pelayanan Rumah Sakit</h2></td></tr>-->
									</table>
									<br>
									<table cellspacing="1" cellpadding="1" class="tb" width="100%" class="table-responsive">
									<thead>
										<tr>
											<th>NO</th>
											<th>JENIS KEGIATAN</th>
											<th>JUMLAH</th>
										</tr>
										<tr>
											<td>1</td>
											<td>2</td>
											<td>3</td>
										</tr>
									</thead>
									<tbody>
										<tr><td>1</td><td>Elektro Encephalografi (EEG)</td><td></td></tr>
										<tr><td>2</td><td>Elektro Kardiographi (EKG)</td><td></td></tr>
										<tr><td>3</td><td>Elektro Myographi (EMG)</td><td></td></tr>
										<tr><td>4</td><td>Echo Cardiographi (ECG)</td><td></td></tr>
										<tr><td>5</td><td>Endoskopi (semua bentuk)</td><td></td></tr>
										<tr><td>6</td><td>Hemodialisa</td><td></td></tr>
										<tr><td>7</td><td>Densometri Tulang</td><td></td></tr>
										<tr><td>8</td><td>Koreksi Fraktur/Dislokasi non Bedah</td><td></td></tr>
										<tr><td>9</td><td>Pungsi</td><td></td></tr>
										<tr><td>10</td><td>Spirometri</td><td></td></tr>
										<tr><td>11</td><td>Tes Kulit/Alergi/Histamin</td><td></td></tr>
										<tr><td>12</td><td>Topometri</td><td></td></tr>
										<tr><td>13</td><td>Tredmill/ Exercise Test</td><td></td></tr>
										<tr><td>14</td><td>Akupuntur</td><td></td></tr>
										<tr><td>15</td><td>Hiperbarik</td><td></td></tr>
										<tr><td>16</td><td>Herbal / jamu</td><td></td></tr>
										<tr><td>88</td><td>Lain-Lain</td><td></td></tr>
										<tr><td>99</td><td>Total</td><td></td></tr>
									</tbody>
									</table>
						        </td>
						    </tr>
						</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>