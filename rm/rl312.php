<style>
	thead th, thead td{text-align:center;}
	thead tr:last{border-bottom :1px solid #999;}
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Laporan RL 3.12</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <table border="0" width="100%">
							<tr valign="top">
								<td align="center">
									<table cellpadding="0" class="tb" width="100%" cellspacing="0" class="table-responsive">
										<tr>
											<td rowspan="2" style="width:110px;"><img src="<?php echo _BASE_;?>/img/logobaktihusda.gif"></td>
											<td><h2>Formulir 3.12</h2></td>
											<td rowspan="2">
												<div style="border:1px dashed #999; padding:10px; display:block; font-style:italic; width:170px;">Ditjen Bina Upaya Kesehatan <br />Kementrian Kesehatan RI</div> 
											</td>
										</tr>
										<tr><td><h1>KEGIATAN KELUARGA BERENCANA</h1></td></tr>
									</table>
								
									<table cellpadding="0" class="tb" width="100%" cellspacing="0" class="table-responsive">
										<tr><td> Kode RS </td><td>: <input type="text" name="kode_rs" class="inputrl12" /></td></tr>
						                <tr><td> Nama RS </td><td>: <input type="text" name="nama_rs" class="inputrl12" /></td></tr>
						                <tr><td> Tahun </td><td>: <input type="text" name="tahun" class="inputrl12" /></td></tr>
						                <tr><td colspan="2">&nbsp;</td></tr>
						                <tr><td colspan="2">&nbsp;</td></tr>
						                <!--<tr><td colspan="2"><h2>RL 1.2 Indikator Pelayanan Rumah Sakit</h2></td></tr>-->
									</table>
									<br>
									<table cellspacing="1" cellpadding="1" class="tb" width="100%" class="table-responsive">
										<thead>
											<tr><th rowspan="2">NO</th><th rowspan="2">METODA</th><th colspan="2">KONSELING</th><th colspan="4">KB BARU DENGAN CARA MASUK</th><th colspan="3">KB BARU DENGAN KONDISI</th><th rowspan="2">KUNJUNGAN ULANG</th><th colspan="2">KELUHAN EFEK SAMPING</th></tr>
											<tr><th>ANC</th><th>PASCA PERSALINAN</th><th>BUKAN RUJUKAN</th><th>RUJUKAN R.INAP</th><th>RUJUKAN R.JALAN</th><th>TOTAL</th><th>PASCA PERSALINAN / NIFAS</th><th>ABORTUS</th><th>LAINNYA</th><th>JUMLAH</th><th>DIRUJUK</th></tr>
											<tr><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td></tr>
										</thead>
										<tbody>
											<tr><td>1</td><td>I U D</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
											<tr><td>2</td><td>P i l</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
											<tr><td>3</td><td>Kondom</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
											<tr><td>4</td><td>Obat Vaginal</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
											<tr><td>5</td><td>MO Pria</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
											<tr><td>6</td><td>MO Wanita</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
											<tr><td>7</td><td>Suntikan</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
											<tr><td>8</td><td>Implant</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
											<tr><td>99</td><td>TOTAL</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
										</tbody>
									</table>
						        </td>
						    </tr>
						</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>