<style>
	thead th, thead td{text-align:center;}
	thead tr:last{border-bottom :1px solid #999;}
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Laporan RL 3.9</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <table border="0" width="100%">
							<tr valign="top">
								<td align="center">
									<table cellpadding="0" class="tb" width="100%" cellspacing="0" class="table-responsive">
										<tr>
											<td rowspan="2" style="width:110px;"><img src="<?php echo _BASE_;?>/img/logobaktihusda.gif"></td>
											<td><h2>Formulir 3.9</h2></td>
											<td rowspan="2">
												<div style="border:1px dashed #999; padding:10px; display:block; font-style:italic; width:170px;">Ditjen Bina Upaya Kesehatan <br />Kementrian Kesehatan RI</div> 
											</td>
										</tr>
										<tr><td><h1>PELAYANAN REHABILITASI MEDIK</h1></td></tr>
									</table>
								
									<table cellpadding="0" class="tb" width="100%" cellspacing="0" class="table-responsive">
										<tr><td> Kode RS </td><td>: <input type="text" name="kode_rs" class="inputrl12" /></td></tr>
						                <tr><td> Nama RS </td><td>: <input type="text" name="nama_rs" class="inputrl12" /></td></tr>
						                <tr><td> Tahun </td><td>: <input type="text" name="tahun" class="inputrl12" /></td></tr>
						                <tr><td colspan="2">&nbsp;</td></tr>
						                <tr><td colspan="2">&nbsp;</td></tr>
						                <!--<tr><td colspan="2"><h2>RL 1.2 Indikator Pelayanan Rumah Sakit</h2></td></tr>-->
									</table>
									<br>
									<table cellspacing="1" cellpadding="1" class="tb" width="100%" class="table-responsive">
										<thead>
											<tr>
												<th>NO</th>
												<th>JENIS TINDAKAN</th>
												<th>JUMLAH</th>
												<th>NO</th>
												<th>JENIS TINDAKAN</th>
												<th>JUMLAH</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1
												</td><td>Medis
												</td><td>&nbsp;
												</td><td>3.6
												</td><td>Analisa Persiapan Kerja
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>1.1
												</td><td>Gait Analyzer
												</td><td>&nbsp;
												</td><td>3.7
												</td><td>Latihan Relaksasi
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>1.2
												</td><td>E M G
												</td><td>&nbsp;
												</td><td>3.8
												</td><td>Analisa & Intervensi, Persepsi, Kognitif, Psikomotor
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>1.3
												</td><td>Uro Dinamic
												</td><td>&nbsp;
												</td><td>3.9
												</td><td>Lain Lain
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>1.4
												</td><td>Side Back
												</td><td>&nbsp;
												</td><td>4
												</td><td>Terapi Wicara
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>1.5
												</td><td>E N Tree
												</td><td>&nbsp;
												</td><td>4.1
												</td><td>Fungsi Bicara
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>1.6
												</td><td>Spyrometer
												</td><td>&nbsp;
												</td><td>4.2
												</td><td>Fungsi Bahasa / Laku
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>1.7
												</td><td>Static Bicycle
												</td><td>&nbsp;
												</td><td>4.3
												</td><td>Fungsi Menelan
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>1.8
												</td><td>Tread Mill
												</td><td>&nbsp;
												</td><td>4.4
												</td><td>Lain Lain
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>1.9
												</td><td>Body Platysmograf
												</td><td>&nbsp;
												</td><td>5
												</td><td>Psikologi
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>1.10
												</td><td>Lain Lain
												</td><td>&nbsp;
												</td><td>5.1
												</td><td>Psikolog Anak
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>2
												</td><td>Fisioterapi
												</td><td>&nbsp;
												</td><td>5.2
												</td><td>Psikolog Dewasa
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>2.1
												</td><td>Latihan Fisik
												</td><td>&nbsp;
												</td><td>5.3
												</td><td>Lain Lain
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>2.2
												</td><td>Aktinoterapi
												</td><td>&nbsp;
												</td><td>6
												</td><td>Sosial Medis
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>2.3
												</td><td>Elektroterapi
												</td><td>&nbsp;
												</td><td>6.1
												</td><td>Evaluasi Lingkungan Rumah
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>2.4
												</td><td>Hidroterapi
												</td><td>&nbsp;
												</td><td>6.2
												</td><td>Evaluasi Ekonomi
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>2.5
												</td><td>Traksi Lumbal & Cervical
												</td><td>&nbsp;
												</td><td>6.3
												</td><td>Evaluasi Pekerjaan
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>2.6
												</td><td>Lain-Lain
												</td><td>&nbsp;
												</td><td>6.4
												</td><td>Lain-lain
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>3
												</td><td>Okupasiterapi
												</td><td>&nbsp;
												</td><td>7
												</td><td>Ortotik Prostetik
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>3.1
												</td><td>Snoosien Room
												</td><td>&nbsp;
												</td><td>7.1
												</td><td>Pembuatan  Alat Bantu
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>3.2
												</td><td>Sensori Integrasi
												</td><td>&nbsp;
												</td><td>7.2
												</td><td>Pembuatan Alat Anggota Tiruan
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>3.3
												</td><td>Latihan aktivitas kehidupan sehari-hari
												</td><td>&nbsp;
												</td><td>7.3
												</td><td>Lain-Lain
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>3.4
												</td><td>Proper Body Mekanik
												</td><td>&nbsp;
												</td><td>8
												</td><td>Kunjungan Rumah
												</td><td>&nbsp;
												</td>
											</tr>
											<tr>
												<td>3.5
												</td><td>Pembuatan Alat Lontar & Adaptasi Alat
												</td><td>&nbsp;
												</td><td>99
												</td><td>TOTAL
												</td><td>&nbsp;
												</td>
											</tr>
										</tbody>
									</table>
						        </td>
						    </tr>
						</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>