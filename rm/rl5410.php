<style>
	thead th, thead td{text-align:center;}
	thead tr:last{border-bottom :1px solid #999;}
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Laporan RL 5.4</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-9"></div>
                    <div class="col-sm-3">
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table border="0" width="100%">
							<tr valign="top">
								<td align="center">
									<table cellpadding="0" class="tb" width="100%" cellspacing="0" class="table-responsive">
										<tr>
											<td rowspan="2" style="width:110px;"><img src="<?php echo _BASE_;?>/img/logobaktihusda.gif"></td>
											<td><h2>Formulir 5.4</h2></td>
											<td rowspan="2">
												<div style="border:1px dashed #999; padding:10px; display:block; font-style:italic; width:170px;">Ditjen Bina Upaya Kesehatan <br />Kementrian Kesehatan RI</div> 
											</td>
										</tr>
										<tr><td><h1>Daftar 10 Besar Penyakit Rawat Jalan</h1></td></tr>
									</table>
									
									<form action="<?php $_SERVER['PHP_SELF'];?>" method="get">
										<?php 
											$date = date('Y') - 10;
											$koders	= isset($_REQUEST['kode_rs']) ? $_REQUEST['kode_rs'] : '';
											$namars	= isset($_REQUEST['nama_rs']) ? $_REQUEST['nama_rs'] : '';
											$tahun	= isset($_REQUEST['tahun']) ? $_REQUEST['tahun'] : date('Y');
											$bulan	= isset($_REQUEST['bulan']) ? $_REQUEST['bulan'] : date('m');
											
											$blnname	= array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
										?>
										<table cellpadding="0" class="tb" width="100%" cellspacing="0" class="table-responsive">
											<tr>
												<td> Kode RS </td>
												<td>: <input type="text" name="kode_rs" class="inputrl12" value="<?php echo $koders;?>" /></td>
											</tr>
							                <tr>
							                	<td> Nama RS </td>
							                	<td>: <input type="text" name="nama_rs" class="inputrl12" value="<?php echo $namars;?>" /></td>
							                </tr>
							                <tr>
							                	<td> Tahun </td>
							                	<td>: 
							                		<select name="tahun" id="tahun" class="selectbox">
														<?php
															for($i=$date; $i<=date('Y'); $i++){
																$selected	= ($i == $tahun) ? 'selected="selected"' : date('Y') ;
																echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
															}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td> Bulan </td>
												<td>: 
													<select name="bulan" id="bulan" class="selectbox">
														<?php
															for($i=1; $i<=12; $i++) {
																$namabulan	= $blnname[$i - 1];
																$selected_bulan	= ($i == $bulan) ? 'selected="selected"' : date('m') ;
																echo '<option value="'.$i.'" '.$selected_bulan.'>'.$namabulan.'</option>';
															}
														?>
													</select>
												</td>
											</tr>
							                <tr><td colspan="2"><input type="submit" name="submit" value="Prosess"></td></tr>
							                <tr><td colspan="2">&nbsp;</td></tr>
							                <!--<tr><td colspan="2"><h2>RL 1.2 Indikator Pelayanan Rumah Sakit</h2></td></tr>-->
										</table>
										<input type="hidden" name="link" value="rl5410">
									</form>

									<table cellspacing="1" cellpadding="1" class="tb" width="100%" class="table-responsive">
										<thead>
											<tr><th rowspan="2">No Urut</th><th rowspan="2">Kode ICD</th><th rowspan="2">Deskripsi</th><th colspan="2">Kasus Baru Menurut Jenis Kelamin</th><th rowspan="2">Jumlah Kasus Baru</th><th rowspan="2">Jumlah Kunjungan</th></tr>
											<tr><th>LK</th><th>PR</th></tr>
											<tr><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td></tr>
										</thead>
										<tbody>
											<?php
												$sql = mysql_query("SELECT a.NOMR,a.ICD_CODE,b.jenis_penyakit, COUNT(a.ICD_CODE) AS jumlah, SUM(IF(c.JENISKELAMIN = 'P',1,0)) AS pr, SUM(IF(c.JENISKELAMIN = 'L',1,0)) AS lk
													,SUM(IF(KDTUJUANRUJUK <> 2 OR KDTUJUANRUJUK <> 6, 1, 0)) AS hidup, SUM(IF(KDTUJUANRUJUK = 2 OR KDTUJUANRUJUK = 6, 1, 0)) AS mati
													FROM t_diagnosadanterapi a
													JOIN icd b ON a.icd_code=b.icd_code 
													JOIN m_pasien c ON a.NOMR = c.NOMR
													WHERE YEAR(a.TANGGAL) = ".$tahun." AND MONTH (a.TANGGAL) = ".$bulan."
													GROUP BY a.ICD_CODE ORDER BY jumlah DESC LIMIT 10");
												if(mysql_num_rows($sql) > 0)
												{
													while($data = mysql_fetch_array($sql)){
														echo '<tr><td>1</td><td>'.$data['ICD_CODE'].'</td><td>'.$data['jenis_penyakit'].'</td><td>'.$data['lk'].'</td><td>'.$data['pr'].'</td><td>'.$data['hidup'].'</td><td>'.$data['mati'].'</td></tr>';
													}
												}
											?>
										</tbody>
									</table>
						        </td>
						    </tr>
						</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>