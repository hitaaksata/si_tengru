<link rel="stylesheet" type="text/css" href="style.css" />
<?php 
include("include/connect.php");
include('ps_pagination.php');
?>

<form name="formugd" method="post" action="ugd/saveugd.php" >

<div class="row">
  <div class="col-sm-12">
    <h5>PEMERIKSAAN</h5> <br>
    <div class="card">
      <div class="card-header">
        <h5>Form Pemeriksaan</h5>
      </div>
      <div class="card-block">
        <div class="row">
          <div class="col-sm-12">
            <div>
              <center>
                <?php include("ugd/pasien.php")?>
              </center>
            </div>
          </div>
        </div>
        <div id="list_data"></div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h5>Detail Pemeriksaan</h5>
      </div>
      <div class="card-block">
        <div class="row">
          <div class="col-sm-12">
            <!-- <ul class="nav nav-tabs md-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tabglassgow" role="tab">GLASSGOW COMA SCALE</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tabanamnesa" role="tab">ANAMNESA</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tabjasmani" role="tab">PEMERIKSAAN JASMANI</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tabbedah" role="tab">STATUS LOKAL/ BEDAH</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tabrontgent" role="tab">RONTGEN</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#settings3" role="tab">OBSERVASI</a>
                    <div class="slide"></div>
                </li>
            </ul>
            <div class="tab-content card-block">
                <div class="tab-pane active" id="tabglassgow" role="tabpanel">
                  <?php include("ugd/glasgow.php")?>  
                </div>
                <div class="tab-pane" id="tabanamnesa" role="tabpanel">
                  <?php include("ugd/anamnesa.php")?>  
                </div>
                <div class="tab-pane" id="tabjasmani" role="tabpanel">
                  <?php include("ugd/jasmani.php")?>  
                </div>
                <div class="tab-pane" id="tabbedah" role="tabpanel">
                  <?php include("ugd/bedah.php")?>  
                </div>
                <div class="tab-pane" id="tabrontgent" role="tabpanel">
                  <?php include("ugd/rontgen.php")?>  
                </div>
                <div class="tab-pane" id="tabobservasi" role="tabpanel">
                  <?php include("ugd/observasi.php")?>  
                </div>
            </div> -->


            <div id="wrapper">
              <div id="content">
                <div class="tab"><h3 class="tabtxt" title="second"><a href="#">GLASSGOW COMA SCALE</a></h3></div>
                <div class="tab"><h3 class="tabtxt" title="third"><a href="#">ANAMNESA</a></h3></div>
                <div class="tab"><h3 class="tabtxt" title="fourth"><a href="#">PEMERIKSAAN JASMANI</a></h3></div>
                <div class="tab"><h3 class="tabtxt" title="five"><a href="#">STATUS LOKAL/ BEDAH</a></h3></div>
                <div class="tab"><h3 class="tabtxt" title="six"><a href="#">RONTGEN</a></h3></div>
                <div class="tab"><h3 class="tabtxt" title="seven"><a href="#">OBSERVASI</a></h3></div>
                <div class="boxholder">
                  <div class="box">
                    <?php include("ugd/glasgow.php")?>
                  </div>  
                  <div class="box">
                    <?php include("ugd/anamnesa.php")?>
                  </div>
                  <div class="box">
                    <?php include("ugd/jasmani.php")?>
                  </div>
                  <div class="box">
                    <?php include("ugd/bedah.php")?>
                  </div>
                  <div class="box">
                    <?php include("ugd/rontgen.php")?>
                  </div>
                  <div class="box">
                    <?php include("ugd/observasi.php")?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</form>


<script>
<!--
/*By George Chiang (JK's JavaScript tutorial)
http://javascriptkit.com
Credit must stay intact for use*/
function show(){
var Digital=new Date()
var hours=Digital.getHours()
var minutes=Digital.getMinutes()
var seconds=Digital.getSeconds()
var curTime = 
    ((hours < 10) ? "0" : "") + hours + ":" 
    + ((minutes < 10) ? "0" : "") + minutes + ":" 
    + ((seconds < 10) ? "0" : "") + seconds 
var dn="AM"

if (hours>12){
dn="PM"
hours=hours-12
}
if (hours==0)
hours=12
if (minutes<=9)
minutes="0"+minutes
if (seconds<=9)
seconds="0"+seconds
document.pasien_masuk.Masuk.value=curTime
document.pasien_keluar.Keluar.value=curTime
setTimeout("show()",1000)
}
show()
//-->
<!-- hide from old browsers
  var curDateTime = new Date()
  var curHour = curDateTime.getHours()
  var curMin = curDateTime.getMinutes()
  var curSec = curDateTime.getSeconds()
  var curTime = 
    ((curHour < 10) ? "0" : "") + curHour + ":" 
    + ((curMin < 10) ? "0" : "") + curMin + ":" 
    + ((curSec < 10) ? "0" : "") + curSec 
//-->
</script>  

  <script type="text/javascript">
	Element.cleanWhitespace('content');
	init();
</script>
