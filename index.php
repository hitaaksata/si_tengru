<?php session_start();
if(!isset($_SESSION['SES_REG'])) {
    header("location:login.php");
}
include("include/connect.php");
include("include/function.php");
include('include/phpMyBorder2.class.php'); 
$pmb = new PhpMyBorder(false);

if(isset($_GET["link"])){
    $link = $_GET["link"];
}else{
    $link = "";
}
  //echo $link;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?= ucwords($rstitle)?></title>

        <link rel="stylesheet" type="text/css" href="rajal/style.css"/>
        <link href="dq_sirs.css" type="text/css" rel="stylesheet" />
        <link rel="shortcut icon" href="rsud.ico" />
        <link rel="stylesheet" type="text/css" href="include/jquery.autocomplete.css" />
        <link rel="stylesheet" href="css/autosuggest_inquisitor.css" type="text/css" media="screen" charset="utf-8" />

        <!-- Google font-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
        <!-- Required Fremwork -->
        <link rel="stylesheet" type="text/css" href="assets/files/bower_components/bootstrap/css/bootstrap.min.css">
        <!-- waves.css -->
        <link rel="stylesheet" href="assets/files/assets/pages/waves/css/waves.min.css" type="text/css" media="all">
        <!-- themify-icons line icon -->
        <link rel="stylesheet" type="text/css" href="assets/files/assets/icon/themify-icons/themify-icons.css">
        <!-- ico font -->
        <link rel="stylesheet" type="text/css" href="assets/files/assets/icon/icofont/css/icofont.css">
        <!-- font awesome -->
        <link rel="stylesheet" type="text/css" href="assets/files/assets/icon/font-awesome/css/font-awesome.min.css">
        <!-- Switch component css -->
        <link rel="stylesheet" type="text/css" href="assets/files/bower_components/switchery/css/switchery.min.css">
        <!-- Tags css -->
        <link rel="stylesheet" type="text/css" href="assets/files/bower_components/bootstrap-tagsinput/css/bootstrap-tagsinput.css" />
        <!-- Data Table Css -->
        <link rel="stylesheet" type="text/css" href="assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
        <!-- Style.css -->
        <link rel="stylesheet" type="text/css" href="assets/files/assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/files/assets/css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" type="text/css" href="assets/files/assets/css/pcoded-horizontal.min.css">

        <!-- ======================== -->
        <!-- Required Jquery -->
        <script type="text/javascript" src="assets/files/bower_components/jquery/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="assets/files/bower_components/popper.js/js/popper.min.js"></script>
        <script type="text/javascript" src="assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
        <!-- jquery slimscroll js -->
        <script type="text/javascript" src="assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
        <!-- waves js -->
        <script src="assets/files/assets/pages/waves/js/waves.min.js"></script>
        <!-- modernizr js -->
        <script type="text/javascript" src="assets/files/bower_components/modernizr/js/modernizr.js"></script>
        <script type="text/javascript" src="assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
        <!-- Switch component js -->
        <!-- <script type="text/javascript" src="assets/files/bower_components/switchery/js/switchery.min.js"></script> -->
        <!-- Tags js -->
        <script type="text/javascript" src="assets/files/bower_components/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script> -->
        <!-- Max-length js -->
        <script type="text/javascript" src="assets/files/bower_components/bootstrap-maxlength/js/bootstrap-maxlength.js"></script>

        <!-- data-table js -->
        <script src="assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="assets/files/assets/pages/data-table/js/jszip.min.js"></script>
        <script src="assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
        <script src="assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
        <script src="assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
        <!-- Custom js -->
        <!-- <script type="text/javascript" src="assets/files/assets/pages/advance-elements/swithces.js"></script> -->

        <script src="assets/files/assets/pages/data-table/js/data-table-custom.js"></script>
        <script src="assets/files/assets/js/pcoded.min.js"></script>
        <script src="assets/files/assets/js/vertical/horizontal-layout.min.js"></script>
        <script src="assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="assets/files/assets/js/script.js"></script>


        <script language="javascript" type="text/javascript" src="rajal/functions.js"></script>
        <script language="javascript" type="text/javascript" src="rajal/xmlhttp.js"></script>
        <script type="text/javascript" language="javascript" src="include/ajaxrequest.js"></script>
       
        <script type="text/javascript" src="include/js.js"></script>
        <script language="javascript" src="include/cal2.js"></script>
        <script language="javascript" src="include/cal_conf2.js"></script>
        <!-- JQUERY -->
        <script src="js/jquery-1.7.min.js" language="JavaScript" type="text/javascript"></script>
        <script src="js/jquery.validate.js" language="JavaScript" type="text/javascript"></script>
        
        <script src="js/jqclock_201.js" language="JavaScript" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery.noConflict();
        </script>
        <!--
        <script type="text/javascript" src="include/scripts/prototype.lite.js"></script>
        <script src="include/prototype.js" type="text/javascript"></script>
        -->
        <!--Notifikasi-->
        <!--<script src="include/jquery.js" language="JavaScript" type="text/javascript"></script>-->
        <script src="include/notification.js" language="JavaScript" type="text/javascript"></script>

        <!--autocomplete-->
        <!--<script type="text/javascript" src="include/jquery-1.2.6.pack.js"></script>-->
        <script type='text/javascript' src='include/jquery.autocomplete.pack.js'></script>
        <script type="text/javascript" src="rajal/jscripts/nicEdit.js"></script>


        <script type="text/javascript">
            bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
        </script>

        <script type="text/javascript">
            function init(){
                var stretchers = document.getElementsByClassName('box');
                var toggles = document.getElementsByClassName('tab');
                var myAccordion = new fx.Accordion(
                toggles, stretchers, {opacity: false, height: true, duration: 600}
            );
                //hash functions
                var found = false;
                toggles.each(function(h3, i){
                    var div = Element.find(h3, 'nextSibling');
                    if (window.location.href.indexOf(h3.title) > 0) {
                        myAccordion.showThisHideOpen(div);
                        found = true;
                    }
                });
                if (!found) myAccordion.showThisHideOpen(stretchers[0]);
            }


            function jumpTo (link)
            {
                var new_url=link;
                if (  (new_url != "")  &&  (new_url != null)  )
                    window.location=new_url;
            }
        </script>

        <!-- admission pasien-->
        <script type="text/javascript">
            jQuery(document).ready(function() {
                //pendaftaran
                //$("#NAMA").autocomplete("include/scripts/auto_nama.php", { width: 260, selectFirst: true });
                <!-- OK-->
                jQuery("#nomroperasi").autocomplete("operasi/nomroperasi.php",{width:260});
                <!-- VK-->
                jQuery("#icdv").autocomplete("vk/autocomplete_vk.php",{width:260});
                <!-- Rawat Inap-->
                jQuery("#namaobat1").autocomplete("ranap/auto_icd.php",{width:260});
            });
        </script>

        <!--auto refresh jumlah pasien-->
        <script type="text/javascript">
            var auto_refresh = setInterval(
            function ()
            {
                jQuery('#totalpasienhariini').load('admission/jmlpasien.php').fadeIn("slow");
            }, 5000); // refresh every 10000 milliseconds
        </script>
        <!--auto refresh jumlah pasien-->



        <script type="text/javascript">
            function enter_pressed(e){
                var keycode;
                if (window.event) keycode = window.event.keyCode;
                else if (e) keycode = e.which;
                else return false;
                return (keycode == 13);
            }

        </script>
        <script type="text/javascript" src="js/bsn.AutoSuggest_c_2.0.js"></script>
        <script>
                    //                  var today;
                    //jQuery(document).ready(function(){
                        //jQuery("#clock4").clock({"format":"24","calendar":"false"});
                        //servertime = parseFloat( jQuery("#servertime").val() ) * 1000;
                        //jQuery("#clockn").clock({"format":"24","calendar":"false"});
                        //jQuery.data = function(success){
                        //  jQuery.get("http://json-time.appspot.com/time.json?callback=?", function(response){
                        //      success(new Date(response.datetime));
                        //  }, "json");
                        //};
                    //});
                    /*
                    function update(){
                        var start = new Date("March 25, 2011 17:00:00");
                        //var today = new Date();
                        jQuery.data(function(time){
                            today = time;
                        });
                        var bla = today.getTime() - start.getTime();
                        jQuery("#milliseconds").text(bla);
                    }
            */
                    //setInterval("update()", 1);

        </script>
        <link rel="stylesheet" href="css/autosuggest_inquisitor.css" type="text/css" media="screen" charset="utf-8" />

    </head>
    
    </HEAD>
    <!--<BODY onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">-->
    

        <!-- <?php //include("notification.php"); ?>
        <div id="header">
            <div id="bg_variation">
                <div id="logo">
                </div>
                <div id="info_header">
                    <div id="info_isi">
                        <div><?=strtoupper($singrstitl)?> <?=strtoupper($singhead1)?></div>
                        <div>
                            <?php
                            if($_SESSION['SES_REG']) {
                                echo '<a href="log_out.php">Sign Out</a> | User : <span class="date">'.$_SESSION['NIP'].'</span>';
                            }else {
                                echo '<a href="login.php">Sign In</a> | guest';
                            }
                            ?>
                        </div>
                        <div class="date"><?php echo date("l, F Y"); ?></div>
                        <div class="date">
                            <?php
                            $dep  = "SELECT * FROM m_login WHERE KDUNIT = '".$_SESSION['KDUNIT']."' AND ROLES = '".$_SESSION['ROLES']."'";
                            $qe   = mysql_query($dep);
                            if($qe) {
                                $deps = mysql_fetch_assoc($qe);
                                echo "<div><b>".$deps['DEPARTEMEN']."</b></div>";
                                echo "<div style='position:absolute;'>";
                                //include("chat/chatroom.php");
                                echo "</div>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php include("menu.php"); ?>
        </div> -->

        <!-- Pre-loader start -->
        <div class="theme-loader">
            <div class="loader-track">
                <div class="preloader-wrapper">
                    <div class="spinner-layer spinner-blue">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="gap-patch">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                    <div class="spinner-layer spinner-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="gap-patch">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                    
                    <div class="spinner-layer spinner-yellow">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="gap-patch">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                    
                    <div class="spinner-layer spinner-green">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="gap-patch">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="pcoded" class="pcoded">
            <?php include("notification.php"); ?>
            <div class="pcoded-container">
                <nav class="navbar header-navbar pcoded-header">
                    <div class="navbar-wrapper">
                        <div class="navbar-logo" style="width: auto;">
                            <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                                <i class="ti-menu"></i>
                            </a>
                            <div class="mobile-search waves-effect waves-light">
                                <div class="header-search">
                                    <div class="main-search morphsearch-search">
                                        <div class="input-group">
                                            <span class="input-group-prepend search-close"><i class="ti-close input-group-text"></i></span>
                                            <input type="text" class="form-control" placeholder="Enter Keyword">
                                            <span class="input-group-append search-btn"><i class="ti-search input-group-text"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="index.html">
                                <img class="img-fluid" src="img/logo.png" alt="Theme-Logo" />
                                <label style="font-size: 18px;"><b><?= ucwords($rstitle)?></b></label>
                            </a>
                            <a class="mobile-options waves-effect waves-light">
                                <i class="ti-more"></i>
                            </a>
                        </div>
                        
                        <div class="navbar-container container-fluid">
                            <ul class="nav-right">
                                <?php
                                if($_SESSION['SES_REG']) { ?>
                                    <?php
                                        $dep  = "SELECT * FROM m_login WHERE KDUNIT = '".$_SESSION['KDUNIT']."' AND ROLES = '".$_SESSION['ROLES']."'";
                                        $qe   = mysql_query($dep);
                                        if($qe) {
                                            $deps = mysql_fetch_assoc($qe);
                                    ?>
                                    <li class="user-profile header-notification">
                                        <a href="#!" class="waves-effect waves-light">
                                            <img src="assets/files/assets/images/avatar-blank.jpg" class="img-radius" alt="User-Profile-Image">
                                            <span><?php echo $deps['DEPARTEMEN'] ?></span>
                                            <i class="ti-angle-down"></i>
                                        </a>
                                        <ul class="show-notification profile-notification">
                                            <li style="text-align: center;">
                                                <font><?=strtoupper($singrstitl)?> <?=strtoupper($singhead1)?></font> <br>
                                                <font><?php echo date("l, d F Y"); ?></font>
                                            </li>
                                            <li class="waves-effect waves-light">
                                                <a href="log_out.php">
                                                    <i class="ti-layout-sidebar-left"></i> Logout
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } } else {
                                    echo '<a href="login.php">Sign In</a> | guest';
                                } ?>
                            </ul>
                        </div>
                    </div>
                </nav>
                
                <!-- Sidebar inner chat end-->
                <div class="pcoded-main-container">
                    <div class="pcoded-wrapper">
                        <nav class="pcoded-navbar">
                            <div class="pcoded-inner-navbar">
                                <?php include("menu.php"); ?>
                            </div>
                        </nav>
                        <div class="pcoded-wrapper">
                            <div class="pcoded-content">
                                <div class="pcoded-inner-content">
                                    <div class="main-body">
                                        <div class="page-wrapper">
                                            <div class="page-body m-t-40">
                                                <?php include("switch.php"); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer">
                    <div class="pull-left"><?=$singrstitl?> <?=strtoupper($singhead1)?> System Application Development</div>
                    <div class="pull-right"><?= ucwords($rstitle)?> &copy; <?php echo date("Y"); ?>
                    <?php 
                    $timestamp = time();
                    echo "<input id='servertime' type='hidden' value='".$timestamp."' />";
                    echo "<input id='clockn' type='hidden' />";
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<?php mysql_close($connect);?>