<?php include("../include/connect.php"); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_r = 20;
$pageNum_r = 0;
if (isset($_GET['pageNum_r'])) {
  $pageNum_r = $_GET['pageNum_r'];
}
$startRow_r = $pageNum_r * $maxRows_r;

$query_r = "select * from t_jamkesmas_klaim_b where status='2C' and tanggal between '".$_GET['dari']."' and '".$_GET['sampai']."'";
$query_limit_r = sprintf("%s LIMIT %d, %d", $query_r, $startRow_r, $maxRows_r);
$r = mysql_query($query_limit_r) or die(mysql_error());
$row_r = mysql_fetch_assoc($r);

if (isset($_GET['totalRows_r'])) {
  $totalRows_r = $_GET['totalRows_r'];
} else {
  $all_r = mysql_query($query_r);
  $totalRows_r = mysql_num_rows($all_r);
}
$totalPages_r = ceil($totalRows_r/$maxRows_r)-1;

$queryString_r = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_r") == false && 
        stristr($param, "totalRows_r") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_r = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_r = sprintf("&totalRows_r=%d%s", $totalRows_r, $queryString_r);
?><body>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h5>FORM DETAIL KLAIM RAWAT INAP</h5>
      </div>
      <div class="card-block">
        <div class="row">
          <div class="col-sm-12">
            <table border="0" cellpadding="2" cellspacing="2" width="95%">
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Nama</th>
                <th>LOS</th>
                <th>INA-DRG</th>
                <th>Tarif</th>
                <th>No. BHP</th>
                <th>Layanan</th>
                <th>Total</th>
                <th>Status</th>
              </tr>
              <?php do { ?>
                <tr>
                  <td><?php echo $row_r['no']; ?></td>
                  <td><?php echo $row_r['tanggal']; ?></td>
                  <td><?php echo $row_r['nama']; ?></td>
                  <td><?php echo $row_r['los']; ?></td>
                  <td><?php echo $row_r['ina']; ?></td>
                  <td><?php echo $row_r['tarif']; ?></td>
                  <td><?php echo $row_r['bhp']; ?></td>
                  <td><?php echo $row_r['layanan']; ?></td>
                  <td><?php echo $row_r['total']; ?></td>
                  <td><?php echo $row_r['status']; ?></td>
                </tr>
                <?php } while ($row_r = mysql_fetch_assoc($r)); ?>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <table border="0">
              <tr>
                <td><?php if ($pageNum_r > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_r=%d%s", $currentPage, 0, $queryString_r); ?>">First</a>
                      <?php } // Show if not first page ?>
                </td>
                <td><?php if ($pageNum_r > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_r=%d%s", $currentPage, max(0, $pageNum_r - 1), $queryString_r); ?>">Previous</a>
                      <?php } // Show if not first page ?>
                </td>
                <td><?php if ($pageNum_r < $totalPages_r) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_r=%d%s", $currentPage, min($totalPages_r, $pageNum_r + 1), $queryString_r); ?>">Next</a>
                      <?php } // Show if not last page ?>
                </td>
                <td><?php if ($pageNum_r < $totalPages_r) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_r=%d%s", $currentPage, $totalPages_r, $queryString_r); ?>">Last</a>
                      <?php } // Show if not last page ?>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<?php
mysql_free_result($r);
?>
