<?php include("../include/connect.php");
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_a = 25;
$pageNum_a = 0;
if (isset($_GET['pageNum_a'])) {
  $pageNum_a = $_GET['pageNum_a'];
}
$startRow_a = $pageNum_a * $maxRows_a;

$query_a = "select * from t_jamkesmas_master_verifikasi where status='1B'";
$query_limit_a = sprintf("%s LIMIT %d, %d", $query_a, $startRow_a, $maxRows_a);
$a = mysql_query($query_limit_a) or die(mysql_error());
$row_a = mysql_fetch_assoc($a);

if (isset($_GET['totalRows_a'])) {
  $totalRows_a = $_GET['totalRows_a'];
} else {
  $all_a = mysql_query($query_a);
  $totalRows_a = mysql_num_rows($all_a);
}
$totalPages_a = ceil($totalRows_a/$maxRows_a)-1;

$queryString_a = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_a") == false && 
        stristr($param, "totalRows_a") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_a = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_a = sprintf("&totalRows_a=%d%s", $totalRows_a, $queryString_a);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h5>VERIFIKASI RAWAT JALAN</h5>
      </div>
      <div class="card-block">
        <div class="row">
          <div class="col-sm-12">
            <table border="0" cellpadding="2" cellspacing="2" width="90%">
              <tr>
                <th>Tanggal</th>
                <th>Nama</th>
                <th>Kode Rumah Sakit</th>
                <th>Kelas</th>
                <th>Contact Person</th>
                <th>Jumlah Pasien</th>
                <th>Verifikator</th>
                <th>Direktur</th>
                <th>Status</th>
                <th>&nbsp;</th>
                </tr>
                <?php do { ?>
                  <tr>
                  <td><?php echo $row_a['tanggal']; ?></td>
                  <td><?php echo $row_a['nama']; ?></td>
                  <td><?php echo $row_a['koder']; ?></td>
                  <td><?php echo $row_a['kelas']; ?></td>
                  <td><?php echo $row_a['kontak']; ?></td>
                  <td><?php echo $row_a['jeniskelamin']; ?></td>
                  <td><?php echo $row_a['verifikator']; ?></td>
                  <td><?php echo $row_a['direktur']; ?></td>
                  <td><?php echo $row_a['status']; ?></td>
                  <td><a href="index.php?link=151&kode=<?php echo $row_a['status']; ?>" class="text">LIHAT_DETAIL</a></td>
                </tr>
                <?php } while ($row_a = mysql_fetch_assoc($a)); ?>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <table border="0">
              <tr>
                <td><?php if ($pageNum_a > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_a=%d%s", $currentPage, 0, $queryString_a); ?>">First</a>
                      <?php } // Show if not first page ?>
                </td>
                <td><?php if ($pageNum_a > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_a=%d%s", $currentPage, max(0, $pageNum_a - 1), $queryString_a); ?>">Previous</a>
                      <?php } // Show if not first page ?>
                </td>
                <td><?php if ($pageNum_a < $totalPages_a) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_a=%d%s", $currentPage, min($totalPages_a, $pageNum_a + 1), $queryString_a); ?>">Next</a>
                      <?php } // Show if not last page ?>
                </td>
                <td><?php if ($pageNum_a < $totalPages_a) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_a=%d%s", $currentPage, $totalPages_a, $queryString_a); ?>">Last</a>
                      <?php } // Show if not last page ?>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
<?php
mysql_free_result($a);
?>
