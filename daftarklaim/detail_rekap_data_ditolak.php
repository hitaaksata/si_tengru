<?php include("../include/connect.php");
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_rsd = 20;
$pageNum_rsd = 0;
if (isset($_GET['pageNum_rsd'])) {
  $pageNum_rsd = $_GET['pageNum_rsd'];
}
$startRow_rsd = $pageNum_rsd * $maxRows_rsd;

if($_GET['kode']=='1A')
{
$query_rsd = "select * from t_jamkesmas_a where tanggal='".$_GET['tgl']."' and status='DITOLAK'";
}
elseif($_GET['kode']=='2A')
{
$query_rsd = "select * from t_jamkesmas_b where tanggal='".$_GET['tgl']."' and status='DITOLAK'";

}
$query_limit_rsd = sprintf("%s LIMIT %d, %d", $query_rsd, $startRow_rsd, $maxRows_rsd);
$rsd = mysql_query($query_limit_rsd) or die(mysql_error());
$row_rsd = mysql_fetch_assoc($rsd);

if (isset($_GET['totalRows_rsd'])) {
  $totalRows_rsd = $_GET['totalRows_rsd'];
} else {
  $all_rsd = mysql_query($query_rsd);
  $totalRows_rsd = mysql_num_rows($all_rsd);
}
$totalPages_rsd = ceil($totalRows_rsd/$maxRows_rsd)-1;

$queryString_rsd = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsd") == false && 
        stristr($param, "totalRows_rsd") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsd = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsd = sprintf("&totalRows_rsd=%d%s", $totalRows_rsd, $queryString_rsd);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h5>REKAP DETAIL DATA DITOLAK</h5>
      </div>
      <div class="card-block">
        <div class="row">
          <div class="col-sm-12">
            <table border="0" cellpadding="2" cellspacing="2" width="95%">
              <tr>
                <th>No</th>
                <th>Nama Pasien</th>
                <th>NOMR</th>
                <th>Tahun</th>
                <th>Hari</th>
                <th>Jenis Kelamin</th>
                <th>No.SKP</th>
                <th>Surat Rujukan</th>
                <th>Nama</th>
                <th>TTD</th>
                <th>Diagnosa Utama</th>
                <th>Diagnosa Skunder</th>
                <th>Prosedur</th>
                <th>Kode INA-DRG</th>
                <th>Tarif</th>
                <th>BHP Khusus</th>
                <th>Layanan Medis Lainnya</th>
                <th>Total</th>
                <th>Keterangan BHP dan Layanan Medis Laiinya</th>
               
              </tr>
              <?php do { ?>
                <tr>
                  <td><?php echo $row_rsd['no']; ?></td>
                  <td><?php echo $row_rsd['namapasien']; ?></td>
                  <td><?php echo $row_rsd['nomr']; ?></td>
                  <td><?php echo $row_rsd['tahun']; ?></td>
                  <td><?php echo $row_rsd['hari']; ?></td>
                  <td><?php echo $row_rsd['jk']; ?></td>
                  <td><?php echo $row_rsd['noskp']; ?></td>
                  <td><?php echo $row_rsd['rujukan']; ?></td>
                  <td><?php echo $row_rsd['nama']; ?></td>
                  <td><?php echo $row_rsd['ttd']; ?></td>
                  <td><?php echo $row_rsd['diagnosautama']; ?></td>
                  <td><?php echo $row_rsd['diagnosaskunder']; ?></td>
                  <td><?php echo $row_rsd['prosedur']; ?></td>
                  <td><?php echo $row_rsd['kode_ina_drg']; ?></td>
                  <td><?php echo $row_rsd['tarif']; ?></td>
                  <td><?php echo $row_rsd['bhp']; ?></td>
                  <td><?php echo $row_rsd['layanan']; ?></td>
                  <td><?php echo $row_rsd['total']; ?></td>
                  <td><?php echo $row_rsd['keterangan']; ?></td>
                  
                </tr>
                <?php } while ($row_rsd = mysql_fetch_assoc($rsd)); ?>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <table border="0" align="center">
              <tr>
                <td><?php if ($pageNum_rsd > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_rsd=%d%s", $currentPage, 0, $queryString_rsd); ?>">First</a>
                      <?php } // Show if not first page ?>
                </td>
                <td><?php if ($pageNum_rsd > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_rsd=%d%s", $currentPage, max(0, $pageNum_rsd - 1), $queryString_rsd); ?>">Previous</a>
                      <?php } // Show if not first page ?>
                </td>
                <td><?php if ($pageNum_rsd < $totalPages_rsd) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_rsd=%d%s", $currentPage, min($totalPages_rsd, $pageNum_rsd + 1), $queryString_rsd); ?>">Next</a>
                      <?php } // Show if not last page ?>
                </td>
                <td><?php if ($pageNum_rsd < $totalPages_rsd) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_rsd=%d%s", $currentPage, $totalPages_rsd, $queryString_rsd); ?>">Last</a>
                      <?php } // Show if not last page ?>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
<?php
mysql_free_result($rsd);
?>
