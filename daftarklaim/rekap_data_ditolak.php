<?php include("../include/connect.php");
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_rtolak = 20;
$pageNum_rtolak = 0;
if (isset($_GET['pageNum_rtolak'])) {
  $pageNum_rtolak = $_GET['pageNum_rtolak'];
}
$startRow_rtolak = $pageNum_rtolak * $maxRows_rtolak;

$query_rtolak = "select * from t_jamkesmas_master where status='DITOLAK'";
$query_limit_rtolak = sprintf("%s LIMIT %d, %d", $query_rtolak, $startRow_rtolak, $maxRows_rtolak);
$rtolak = mysql_query($query_limit_rtolak) or die(mysql_error());
$row_rtolak = mysql_fetch_assoc($rtolak);

if (isset($_GET['totalRows_rtolak'])) {
  $totalRows_rtolak = $_GET['totalRows_rtolak'];
} else {
  $all_rtolak = mysql_query($query_rtolak);
  $totalRows_rtolak = mysql_num_rows($all_rtolak);
}
$totalPages_rtolak = ceil($totalRows_rtolak/$maxRows_rtolak)-1;

$queryString_rtolak = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rtolak") == false && 
        stristr($param, "totalRows_rtolak") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rtolak = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rtolak = sprintf("&totalRows_rtolak=%d%s", $totalRows_rtolak, $queryString_rtolak);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h5>REKAP DATA DITOLAK</h5>
      </div>
      <div class="card-block">
        <div class="row">
          <div class="col-sm-12">
            <table border="0" cellpadding="3" cellspacing="3" width="98%">
              <tr>
                <th>Tanggal</td>
                <th>Nama Rumah Sakit</th>
                <th>Kode Rumah Sakit</th>
                <th>Kelas Rumah Sakit</th>
                <th>Contact Person</th>
                <th>Jumlah Pasien</th>
                <th>Direktur</th>
                <th>Kategori</th>
                <th>Status</th>
                <th>&nbsp;</th>
              </tr>
              <?php do { ?>
                <tr>
                  <td><?php echo $row_rtolak['tanggal']; ?></td>
                  <td><?php echo $row_rtolak['namars']; ?></td>
                  <td><?php echo $row_rtolak['koders']; ?></td>
                  <td><?php echo $row_rtolak['kelasrs']; ?></td>
                  <td><?php echo $row_rtolak['kontakrs']; ?></td>
                  <td><?php echo $row_rtolak['jumlahpasienrs']; ?></td>
                  <td><?php echo $row_rtolak['direkturrs']; ?></td>
                  <td><?php echo $row_rtolak['kategori']; ?></td>
                  <td><?php echo $row_rtolak['status']; ?></td>
                  <td><a href="index.php?link=147&tgl=<?php echo $row_rtolak['tanggal']; ?>&kode=<?php echo $row_rtolak['kategori']; ?>" class="text">LIHAT_DETAIL</a></td>
                </tr>
                <?php } while ($row_rtolak = mysql_fetch_assoc($rtolak)); ?>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <table border="0">
              <tr>
                <td><?php if ($pageNum_rtolak > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_rtolak=%d%s", $currentPage, 0, $queryString_rtolak); ?>">First</a>
                      <?php } // Show if not first page ?>
                </td>
                <td><?php if ($pageNum_rtolak > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_rtolak=%d%s", $currentPage, max(0, $pageNum_rtolak - 1), $queryString_rtolak); ?>">Previous</a>
                      <?php } // Show if not first page ?>
                </td>
                <td><?php if ($pageNum_rtolak < $totalPages_rtolak) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_rtolak=%d%s", $currentPage, min($totalPages_rtolak, $pageNum_rtolak + 1), $queryString_rtolak); ?>">Next</a>
                      <?php } // Show if not last page ?>
                </td>
                <td><?php if ($pageNum_rtolak < $totalPages_rtolak) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_rtolak=%d%s", $currentPage, $totalPages_rtolak, $queryString_rtolak); ?>">Last</a>
                      <?php } // Show if not last page ?>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
<?php
mysql_free_result($rtolak);
?>
