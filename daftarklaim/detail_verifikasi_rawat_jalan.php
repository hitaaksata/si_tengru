<?php include("../include/connect.php"); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_r = 20;
$pageNum_r = 0;
if (isset($_GET['pageNum_r'])) {
  $pageNum_r = $_GET['pageNum_r'];
}
$startRow_r = $pageNum_r * $maxRows_r;

$query_r = "select * from t_jamkesmas_verifikasi_a where status='".$_GET['kode']."'";
$query_limit_r = sprintf("%s LIMIT %d, %d", $query_r, $startRow_r, $maxRows_r);
$r = mysql_query($query_limit_r) or die(mysql_error());
$row_r = mysql_fetch_assoc($r);

if (isset($_GET['totalRows_r'])) {
  $totalRows_r = $_GET['totalRows_r'];
} else {
  $all_r = mysql_query($query_r);
  $totalRows_r = mysql_num_rows($all_r);
}
$totalPages_r = ceil($totalRows_r/$maxRows_r)-1;

$queryString_r = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_r") == false && 
        stristr($param, "totalRows_r") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_r = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_r = sprintf("&totalRows_r=%d%s", $totalRows_r, $queryString_r);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h5>DETAIL VERIFIKASI RAWAT JALAN</h5>
      </div>
      <div class="card-block">
        <div class="row">
          <div class="col-sm-12">
            <table width="100%" border="0" cellpadding="2" cellspacing="2">
              <tr>
                <th valign="top">No</th>
                <th valign="top">Nama</th>
                <th valign="top">NOMR</th>
                <th valign="top">Umur</th>
                <th valign="top">Jenis Kelamin</th>
                <th valign="top">No. SKP</th>
                <th valign="top">Rujukan</th>
                <th valign="top">Nama Dokter</th>
                <th valign="top">TTD</th>
                <th valign="top">Diagnosa Utama</th>
                <th valign="top">Deskripsi</th>
                <th valign="top">Prosedur</th>
                <th valign="top">Dekskripsi</th>
                <th valign="top">Kode INA</th>
                <th valign="top">Deksripsi</th>
                <th valign="top">Tarif</th>
                <th valign="top">Total</th>
                <th valign="top">Kelayakan</th>
              </tr>
              <?php do { ?>
               <tr <?   echo "class =";
                            $count++;
                            if ($count % 2) {
                            echo "tr1"; }
                            else {
                            echo "tr2";
                            }
                    ?>>
               
                  <td valign="top"><?php echo $row_r['no']; ?></td>
                  <td valign="top"><?php echo $row_r['nama']; ?></td>
                  <td valign="top"><?php echo $row_r['nomr']; ?></td>
                  <td valign="top"><?php echo $row_r['tahun']; ?></td>
                  <td valign="top"><?php echo $row_r['jeniskelamin']; ?></td>
                  <td valign="top"><?php echo $row_r['skp']; ?></td>
                  <td valign="top"><?php echo $row_r['rujukan']; ?></td>
                  <td valign="top"><?php echo $row_r['namadokter']; ?></td>
                  <td valign="top"><?php echo $row_r['ttd']; ?></td>
                  <td valign="top"><?php echo $row_r['diagnosautama']; ?></td>
                  <td valign="top"><?php echo $row_r['deskripsi1']; ?></td>
                  <td valign="top"><?php echo $row_r['prosedur']; ?></td>
                  <td valign="top"><?php echo $row_r['deskripsi3']; ?></td>
                  <td valign="top"><?php echo $row_r['kodeina']; ?></td>
                  <td valign="top"><?php echo $row_r['deskripsi4']; ?></td>
                  <td valign="top"><?php echo $row_r['tarif']; ?></td>
                  <td valign="top"><?php echo $row_r['total']; ?></td>
                  <td valign="top"><div align="center"><?php echo $row_r['layak']; ?></div></td>
                </tr>
                <?php } while ($row_r = mysql_fetch_assoc($r)); ?>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <table border="0">
              <tr>
                <td><?php if ($pageNum_r > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_r=%d%s", $currentPage, 0, $queryString_r); ?>">First</a>
                      <?php } // Show if not first page ?>
                </td>
                <td><?php if ($pageNum_r > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_r=%d%s", $currentPage, max(0, $pageNum_r - 1), $queryString_r); ?>">Previous</a>
                      <?php } // Show if not first page ?>
                </td>
                <td><?php if ($pageNum_r < $totalPages_r) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_r=%d%s", $currentPage, min($totalPages_r, $pageNum_r + 1), $queryString_r); ?>">Next</a>
                      <?php } // Show if not last page ?>
                </td>
                <td><?php if ($pageNum_r < $totalPages_r) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_r=%d%s", $currentPage, $totalPages_r, $queryString_r); ?>">Last</a>
                      <?php } // Show if not last page ?>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
<?php
mysql_free_result($r);
?>
