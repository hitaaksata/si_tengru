<script language="javascript" src="include/cal3.js"></script>
<script language="javascript" src="include/cal_conf3.js"></script>

<!-- <div align="center">
    <div id="frame">
   		<div id="frame_title"><h3 align="left">IDENTITAS PASIEN</h3></div> -->
        <script>
		jQuery(document).ready(function(){
			jQuery("#myform").validate();
			jQuery('#parent_nomr').blur(function(){
				var val	= jQuery(this).val();
				jQuery.post('<?php echo _BASE_;?>include/ajaxload.php',{loadDetailPasien:true,nomr:val},function(data){
					var d = data.split('|');
					jQuery('#parent_nama').empty().append(d[2]);
				});
			});
			jQuery('.ruang').click(function(){
				var val = jQuery(this).val();
				if(val == 'vk'){
				   	jQuery.post('<?php echo _BASE_;?>include/ajaxload.php',{load_dokterjaga:true,kdpoly:10},function(data){
						jQuery('#load_dokterjaga').empty().html(data);
					});
				}else{
					jQuery('#load_dokterjaga').empty();
				}
			});
			jQuery('.carabayar').change(function(){
				var val = jQuery(this).val();
				if(val == 5){
					jQuery('#carabayar_lain').show().addClass('required');
				}else{
					jQuery('#carabayar_lain').hide().removeClass('required');
				}
			});
			jQuery("#KDPROVINSI").change(function(){
				var selectValues = jQuery("#KDPROVINSI").val();
				jQuery.post('<?php echo _BASE_;?>include/ajaxload.php',{kdprov:selectValues, load_kota:'true'},function(data){
					jQuery('#kotapilih').html(data);
					jQuery('#kecamatanpilih').html("<select name=\"KDKECAMATAN\" class=\"form-control\" title=\"*\" id=\"KDKECAMATAN\" required><option value=\"0\"> --pilih-- </option></select>");
					jQuery('#kelurahanpilih').html("<select name=\"KELURAHAN\" class=\"form-control\" title=\"*\" id=\"KELURAHAN\" required><option value=\"0\"> --pilih-- </option></select>");
				});
			});
		});
		</script>
        <style type="text/css">
.loader{background:url(js/loading.gif) no-repeat; width:16px; height:16px; float:right; margin-right:30px;}
input.error{ border:1px solid #F00;}
label.error{ color:#F00; font-weight:bold;}

</style>

<form action="models/pendaftaran_bayi.php" method="post" id="myform">
  <? unset($_SESSION['register_nomr']); unset($_SESSION['register_nama']); #  echo $pmb -> begin_round("100%","FFF","CCC","CCC"); //  (width, fillcolor, edgecolor, shadowcolor) ?>
<div class="row">
    <div class="col-sm-12">
      <h5>IDENTITAS PASIEN</h5>
      <br>
        <div class="card">
            <div class="card-header">
                <h5>DAFTAR</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-1">
                        <img src="img/pendaftaran.png" />
                    </div>
                    <div class="col-sm-9">

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Nomor Orang Tua</label>
                                <div class="col-sm-9">
                                    <input type="text" name="parent_nomr" id="parent_nomr" class="form-control" title="*" required/><span id="parent_nama" style="font-weight:bold;"></span>
                                </div>
                            </div>

                            <div class="form-radio form-group row">
                                <label class="col-sm-3 col-form-label" style="margin-left: -10px;">Ruang Lahir</label>
                                <div class="col-sm-9" style="margin-left: 10px;">
                                    <div class="radio radio-inline">
                                        <label>
                                            <input type="radio" name="ruang" value="ok" class="ruang required" title="*" />
                                            <i class="helper"></i>Ruang OK
                                        </label>
                                    </div>
                                    <div class="radio radio-inline">
                                        <label>
                                            <input type="radio" class="ruang required" name="ruang" value="vk" title="*" />
                                            <i class="helper"></i>Ruang VK
                                            <span id="load_dokterjaga"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-radio form-group row">
                                <label class="col-sm-3 col-form-label" style="margin-left: -10px;">Cara bayar</label>
                                <div class="col-sm-9" style="margin-left: 10px;">
                                  <?php
                                    $ss  = mysql_query('select * from m_carabayar order by ORDERS ASC');
                                    while($ds = mysql_fetch_array($ss)){
                                      if($_GET['KDCARABAYAR'] == $ds['KODE']): $sel = "Checked"; else: $sel = ''; endif;
                                      echo '
                                        <div class="radio radio-inline">
                                            <label>
                                                <input type="radio" name="KDCARABAYAR" id="carabayar_'.$ds['KODE'].'" title="*" class="carabayar required" '.$sel.' value="'.$ds['KODE'].'" />
                                                <i class="helper"></i>'.$ds['NAMA'].'&nbsp
                                            </label>
                                        </div>
                                      ';
                                    }

                                    $cssb ='style="display:none;"';
                                    if($_GET['KETBAYAR'] != ''): 
                                      $cssb = 'style="display:inline;"';
                                    endif;
                                  ?>
                                </div>
                                <div class="col-sm-3">
                                    <div class="radio radio-inline">
                                        <label>
                                            <input type="text" name="KETBAYAR" title="*" id="carabayar_lain" <?php echo $cssb;?> value="<?php echo $_REQUEST['KETBAYAR']; ?>" class="text"/>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-radio form-group row">
                                <label class="col-sm-3 col-form-label" style="margin-left: -10px;">Shift</label>
                                <div class="col-sm-9" style="margin-left: 10px;">
                                  <div class="radio radio-inline">
                                      <label>
                                          <input type="radio" name="SHIFT" class="required" title="*" value="1" <? if($_GET['SHIFT']=="1")echo "Checked";?>/> 
                                          <i class="helper"></i> 1
                                      </label>
                                  </div>
                                  <div class="radio radio-inline">
                                      <label>
                                          <input type="radio" name="SHIFT" class="required" title="*" value="2" <? if($_GET['SHIFT']=="2")echo "Checked";?>/>
                                          <i class="helper"></i> 2
                                      </label>
                                  </div>
                                  <div class="radio radio-inline">
                                      <label>
                                          <input type="radio" name="SHIFT" class="required" title="*" value="3" <? if($_GET['SHIFT']=="3")echo "Checked";?>/>
                                          <i class="helper"></i> 3
                                      </label>
                                  </div>
                                </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Nama Lengkap Bayi</label>
                              <div class="col-sm-9">
                                <input class="form-control" title="*" type="text" name="NAMA" size="30" value="<? if(!empty($_GET['NAMA'])){ echo $_GET['NAMA']; }?>" id="NAMA" required />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Tempat Tanggal Lahir</label>
                              <div class="col-sm-7">
                                <div class="form-group row">
                                  <label class="col-sm-2 col-form-label">Tempat</label>
                                  <div class="col-sm-5">
                                    <input type="text" value="<? if(!empty($_GET['TEMPAT'])){ echo $_GET['TEMPAT']; }else{ echo $m_pasien->TEMPAT;} ?>" class="form-control" title="*" name="TEMPAT" size="15" id="TEMPAT" required style="width: 200px;" />
                                  </div>
                                  <div class="col-sm-5">
                                    <input onblur="calage1(this.value,'umur');" type="text" class="form-control" title="*" value="<? if(!empty($_GET['TGLLAHIR'])){ echo date('d/m/Y', strtotime($_GET['TGLLAHIR'])); }else{ echo date('d/m/Y', strtotime($m_pasien->TGLLAHIR));} ?>" name="TGLLAHIR" id="TGLLAHIR" size="20" required style="width: 200px;"/>
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-2">
                                <a href="javascript:showCal1('Calendar1')"><img align="top" src="img/date.png" border="0" /></a> ex : 29/09/1999
                              </div>
                            </div>
                            
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Umur</label>
                              <div class="col-sm-9">
                                <?php 
                                  if ($m_pasien->TGLLAHIR==""){
                                    $a = datediff(date("Y/m/d"), date("Y/m/d"));
                                  } else {
                                    $a = datediff($m_pasien->TGLLAHIR, date("Y/m/d"));
                                  } ?>
                                <span id="umurc">
                                  <input class="form-control" type="text" value="<?php echo 'umur '.$a[years].' tahun '.$a[months].' bulan '.$a[days].' hari'; ?>" name="umur" id="umur" size="45" />
                                </span>
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Alamat Sekarang</label>
                              <div class="col-sm-9">
                                <input name="ALAMAT" id="ALAMAT" class="form-control" type="text" value="<? if(!empty($_GET['ALAMAT'])){ echo $_GET['ALAMAT']; } ?>" title="*" size="45" required />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Alamat KTP</label>
                              <div class="col-sm-9">
                                <input name="ALAMAT_KTP" class="form-control" type="text" value="<? if(!empty($_GET['ALAMAT_KTP'])){ echo $_GET['ALAMAT_KTP']; } ?>" size="45" id="ALAMAT_KTP" />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Provinsi</label>
                              <div class="col-sm-9">
                                <select name="KDPROVINSI" class="form-control" title="*" id="KDPROVINSI" required>
                                  <option value="0"> --pilih-- </option>
                                  <?php
                                    $ss = mysql_query('select * from m_provinsi order by idprovinsi ASC');
                                    while($ds = mysql_fetch_array($ss)){
                                      if($_GET['KDPROVINSI'] == $ds['idprovinsi']): $sel = "selected=Selected"; else: $sel = ''; endif;
                                        echo '<option value="'.$ds['idprovinsi'].'" '.$sel.' > '.$ds['namaprovinsi'].'</option>';
                                    } ?>
                                </select>
                                <input class="form-control" value="<?=$_GET['KOTA']?>" type="hidden" name="KOTAHIDDEN" id="KOTAHIDDEN" >
                                <input class="form-control" value="<?=$_GET['KECAMATAN']?>" type="hidden" name="KECAMATANHIDDEN" id="KECAMATANHIDDEN" >
                                <input class="form-control" value="<?=$_GET['KELURAHAN']?>" type="hidden" name="KELURAHANHIDDEN" id="KELURAHANHIDDEN" >
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Kabupaten / Kota</label>
                              <div class="col-sm-9">
                                <div id="kotapilih">
                                  <select name="KOTA" class="form-control" title="*" id="KOTA" required>
                                    <option value="0"> --pilih-- </option>
                                    <?php
                                      $ss = mysql_query('select * from m_kota where idprovinsi = "'.$_GET['KDPROVINSI'].'" order by idkota ASC');
                                      while($ds = mysql_fetch_array($ss)){
                                        if($_GET['KOTA'] == $ds['idkota']): $sel = "selected=Selected"; else: $sel = ''; endif;
                                          echo '<option value="'.$ds['idkota'].'" '.$sel.' > '.$ds['namakota'].'</option>';
                                    } ?>
                                  </select>
                                </div>
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Kecamatan</label>
                              <div class="col-sm-9">
                                <div id="kecamatanpilih">
                                  <select name="KDKECAMATAN" class="form-control" title="*" id="KDKECAMATAN" required>
                                    <option value="0"> --pilih-- </option>
                                    <?php
                                      $ss = mysql_query('select * from m_kecamatan where idkota = "'.$_GET['KOTA'].'" order by idkecamatan ASC');
                                      while($ds = mysql_fetch_array($ss)){
                                        if($_GET['KDKECAMATAN'] == $ds['idkecamatan']): $sel = "selected=Selected"; else: $sel = ''; endif;
                                          echo '<option value="'.$ds['idkecamatan'].'" '.$sel.' /> '.$ds['namakecamatan'].'</option>&nbsp;';
                                    } ?>
                                  </select>
                                </div>
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Kelurahan</label>
                              <div class="col-sm-9">
                                <div id="kelurahanpilih">
                                  <select name="KELURAHAN" class="form-control" title="*" id="KELURAHAN" required>
                                    <option value="0"> --pilih-- </option>
                                    <?php
                                      $ss = mysql_query('select * from m_kelurahan where idkecamatan = "'.$_GET['KDKECAMATAN'].'" order by idkelurahan ASC');
                                      while($ds = mysql_fetch_array($ss)){
                                        if($_GET['KELURAHAN'] == $ds['idkelurahan']): $sel = "selected=Selected"; else: $sel = ''; endif;
                                          echo '<option value="'.$ds['idkelurahan'].'" '.$sel.' /> '.$ds['namakelurahan'].'</option>&nbsp;';
                                    } ?>
                                  </select>
                                </div>
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">No. Telepon / Hp</label>
                              <div class="col-sm-9">
                                <input  class="form-control" value="<? if(!empty($_GET['NOTELP'])){ echo $_GET['NOTELP']; }else{ echo $m_pasien->NOTELP;} ?>" type="text" name="NOTELP" size="25" id="notelp" />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">No. KTP</label>
                              <div class="col-sm-9">
                                <input  class="form-control" value="<? if(!empty($_GET['NOKTP'])){ echo $_GET['NOKTP']; }else{ echo $m_pasien->NOKTP;} ?>" type="text" name="NOKTP" id="NOKTP" size="25" />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Nama Suami / Orang Tua</label>
                              <div class="col-sm-9">
                                <input class="form-control" type="text" value="<? if(!empty($_GET['SUAMI_ORTU'])){ echo $_GET['SUAMI_ORTU']; }else{ echo $m_pasien->SUAMI_ORTU;} ?>" name="SUAMI_ORTU" id="SUAMI_ORTU" size="25" />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Pekerjaan Pasien / Orang Tua</label>
                              <div class="col-sm-9">
                                <input class="form-control" type="text" value="<? if(!empty($_GET['PEKERJAAN'])){ echo $_GET['PEKERJAAN']; }else{ echo $m_pasien->PEKERJAAN;} ?>" name="PEKERJAAN" size="25" id="PEKERJAAN" />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Nama Penanggung Jawab</label>
                              <div class="col-sm-9">
                                <input class="form-control" type="text" name="nama_penanggungjawab" size="30" value="<? if(!empty($_GET['nama_penanggungjawab'])){ echo $_GET['nama_penanggungjawab']; } ?>" id="nama_penanggungjawab"  />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Hubungan Dengan Pasien</label>
                              <div class="col-sm-9">
                                <input class="form-control" type="text" name="hubungan_penanggungjawab" size="30" value="<? if(!empty($_GET['hubungan_penanggungjawab'])){ echo $_GET['hubungan_penanggungjawab']; } ?>" id="hubungan_penanggungjawab" />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Alamat</label>
                              <div class="col-sm-9">
                                <input name="alamat_penanggungjawab" class="form-control" type="text" size="45" value="<? if(!empty($_GET['alamat_penanggungjawab'])){ echo $_GET['alamat_penanggungjawab']; } ?>" id="alamat_penanggungjawab" />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">No. Telepon / HP</label>
                              <div class="col-sm-9">
                                <input class="form-control" type="text" name="phone_penanggungjawab" size="25" value="<? if(!empty($_GET['phone_penanggungjawab'])){ echo $_GET['phone_penanggungjawab']; } ?>" id="phone_penanggungjawab" />
                              </div>
                            </div>
                            
                    </div>
                    <div class="col-sm-2">
                        <div>
                          Jenis Kelamin : <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="JENISKELAMIN" id="JENISKELAMIN_L" title="*" class="required" value="L" <? if(strtoupper($_GET['JENISKELAMIN'])=="L") echo "Checked";?>/>
                                  <i class="helper"></i>Laki-laki
                              </label>
                          </div>
                          <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="JENISKELAMIN" id="JENISKELAMIN_P" title="*" class="required" title="*" value="P" <? if(strtoupper($_GET['JENISKELAMIN'])=="P") echo "Checked";?>/>
                                  <i class="helper"></i>Perempuan
                              </label>
                          </div>
                        </div>
                          <br>
                        <div>
                          Status Perkawinan : <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="STATUS" id="status_1" value="1" <? if($m_pasien->STATUS=="1" || $_GET['STATUS']=="1") echo "Checked";?>/>
                                  <i class="helper"></i>Belum Kawin
                              </label>
                          </div>
                          <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="STATUS" id="status_2" value="2" <? if($m_pasien->STATUS=="2" || $_GET['STATUS']=="2") echo "Checked";?> />
                                  <i class="helper"></i>Kawin
                              </label>
                          </div>
                          <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="STATUS" id="status_3" value="3" <? if($m_pasien->STATUS=="3" || $_GET['STATUS']=="3") echo "Checked";?>/>
                                  <i class="helper"></i>Janda / Duda
                              </label>
                          </div>
                        </div>
                          <br>
                        <div>
                          Pendidikan Terakhir : <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_1" value="1" <? if($m_pasien->PENDIDIKAN=="1" || $_GET['PENDIDIKAN']=="1") echo "Checked";?> />
                                  <i class="helper"></i>SD
                              </label>
                          </div>
                          <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_2" value="2" <? if($m_pasien->PENDIDIKAN=="2" || $_GET['PENDIDIKAN']=="2") echo "Checked";?> />
                                  <i class="helper"></i>SLTP
                              </label>
                          </div>
                          <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_3" value="3" <? if($m_pasien->PENDIDIKAN=="3" || $_GET['PENDIDIKAN']=="3") echo "Checked";?> />
                                  <i class="helper"></i>SMU
                              </label>
                          </div>
                          <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_4" value="4" <? if($m_pasien->PENDIDIKAN=="4" || $_GET['PENDIDIKAN']=="4") echo "Checked";?> />
                                  <i class="helper"></i>D3/Akademik
                              </label>
                          </div>
                          <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_5" value="5" <? if($m_pasien->PENDIDIKAN=="5" || $_GET['PENDIDIKAN']=="5") echo "Checked";?> />
                                  <i class="helper"></i>Universitas
                              </label>
                          </div>
                        </div>
                          <br>
                        <div>
                          Agama : <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="AGAMA" id="AGAMA_1" value="1" <? if($m_pasien->AGAMA=="1" || $_GET['AGAMA']=="1") echo "Checked";?> />
                                  <i class="helper"></i>Islam
                              </label>
                          </div>
                          <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="AGAMA" id="AGAMA_2" value="2" <? if($m_pasien->AGAMA=="2" || $_GET['AGAMA']=="2") echo "Checked";?>/>
                                  <i class="helper"></i>Kristen Protestan
                              </label>
                          </div>
                          <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="AGAMA" id="AGAMA_3" value="3" <? if($m_pasien->AGAMA=="3" || $_GET['AGAMA']=="3") echo "Checked";?>/>
                                  <i class="helper"></i>Katholik
                              </label>
                          </div>
                          <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="AGAMA" id="AGAMA_4" value="4" <? if($m_pasien->AGAMA=="4" || $_GET['AGAMA']=="4") echo "Checked";?>/>
                                  <i class="helper"></i>Hindu
                              </label>
                          </div>
                          <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="AGAMA" id="AGAMA_5" value="5" <? if($m_pasien->AGAMA=="5" || $_GET['AGAMA']=="5") echo "Checked";?>/>
                                  <i class="helper"></i>Budha
                              </label>
                          </div>
                          <br>
                          <div class="form-radio radio radio-inline">
                              <label>
                                  <input type="radio" name="AGAMA" id="AGAMA_6" value="6" <? if($m_pasien->AGAMA=="6" || $_GET['AGAMA']=="6") echo "Checked";?>/>
                                  <i class="helper"></i>Lain - lain
                              </label>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <center>
      <button type="submit" name="daftar" class="btn btn-outline-primary waves-effect" onclick="stopjam();"><i class="ti-save"></i> S a v e</button>
      <button type="button" name="print" class="btn btn-outline-success waves-effect" onclick="cetak();"><i class="ti-printer"></i> Print Kartu Pasien</button>
      <input type="text" id="msgid" name="msgid" style="border:1px #FFF solid; width:0px; height:0px;">
    </center>
    <br><br>
    </div>
</div>
</form>