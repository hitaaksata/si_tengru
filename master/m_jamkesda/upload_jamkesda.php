<?
	session_start();
if(empty($_SESSION['u_name']))
	header("Location:../index.php");	

if(isset($_GET['logout']))
{
	session_destroy();
	header("Location:index.php");
}	
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Form Upload</title>
    <link rel="stylesheet" type="text/css" href="../master.css" />	
</head>

<body>
<div id="masthead"> <div id="bg_variation"> <div id="logo"></div></div></div>
	<ol id="navlinks">
        <li class="last"> <a href="m_jamkesda/upload_jamkesda.php">Upload .XLS Jamkesda</a></li>    	
		<li> <a href="../m_icd/index.php">ICD</a></li>
		<li> <a href="../m_login/index.php">USER LOGIN</li>
		<li> <a href="../m_poly/index.php">POLY</a></li>
		<li> <a href="../m_tarif/index.php">TARIF</a></li>
		<li> <a href="../m_dokter/index.php">DOKTER</a></li>
		<li> <a href="../secure.php?logout">LOGOUT</a></li>
	</ol>

<div align="center">
  <div id="frame">
<? if($_GET['psn'] != '')
{
echo $_GET['psn'];
}
else
{
echo "";
}
?>
<table>
<tr>
<td width="462">
<BR><form name="form1" action="ins_jamkesda.php" method="post" enctype="multipart/form-data">

<table width="251">
<tr>
  <td colspan="2" align="center" bgcolor="#CCCCCC" >UPLOAD FILE JAMKESMAS GOLD</td></tr>
<tr><td width="95">Mulai baris</td>
<td width="302"><input type="text" name="baris" id="baris"></td>
</tr>

<tr align="center" bgcolor="#CCCCCC">
<td>NAMA KOLOM</td>
<td>KOLOM KE</td>
</tr>
<tr><td>DPSNOKA</td>
<td><input type="text" name="DPSNOKA" id="DPSNOKA"></td>
</tr>
<tr><td>NOKK</td>
<td><input type="text" name="NOKK" id="NOKK"></td>
</tr>
<tr><td>NOPEN</td>
<td><input type="text" name="NOPEN" id="NOPEN"></td>
</tr>
<tr><td>KDDESA</td>
<td><input type="text" name="KDDESA" id="KDDESA"></td>
</tr>
<tr><td>KDKC</td>
<td><input type="text" name="KDKC" id="KDKC"></td>
</tr>
<tr><td>DPSKDKEC</td>
<td><input type="text" name="DPSKDKEC" id="DPSKDKEC"></td>
</tr>
<tr><td>DPSNMCTK</td>
<td><input type="text" name="DPSNMCTK" id="DPSNMCTK"></td>
</tr>
<tr><td>DPSTGLLHR</td>
<td><input type="text" name="DPSTGLLHR" id="DPSTGLLHR"></td>
</tr>
<tr><td>DPSJK</td>
<td><input type="text" name="DPSJK" id="DPSJK"></td>
</tr>
<tr><td>DPSSTSKWN</td>
<td><input type="text" name="DPSSTSKWN" id="DPSSTSKWN"></td>
</tr>
<tr><td>DPSJLN</td>
<td><input type="text" name="DPSJLN" id="DPSJLN"></td>
</tr>
<tr><td>DPSRTRW</td>
<td><input type="text" name="DPSRTRW" id="DPSRTRW"></td>
</tr>
<tr><td>DPSTGLREG</td>
<td><input type="text" name="DPSTGLREG" id="DPSTGLREG"></td>
</tr>
<tr><td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr><td colspan="2">  <br>
<input type="file" name="upload_file" id="upload_file" size="50"/>
<input type="submit" name="upload" value="Upload JMKS GOLD" class='submit' />
<br><br>

</td></tr>
</table>
</form>
</td>
<td width="307"><BR><form name="form1" action="ins_jamkesda_silver.php" method="post" enctype="multipart/form-data">

<table width="251">
<tr>
  <td colspan="2" align="center" bgcolor="#CCCCCC">UPLOAD FILE JAMKESMAS SILVER</td></tr>
<tr><td width="95">Mulai baris</td>
<td width="302"><input type="text" name="baris" id="baris"></td>
</tr>

<tr align="center" bgcolor="#CCCCCC"><td>NAMA KOLOM</td>
<td>KOLOM KE</td>
</tr>
<tr><td>NOKK</td>
<td><input type="text" name="NOKK" id="NOKK"></td>
</tr>
<tr>
  <td>NAMA LENGKAP</td>
<td><input type="text" name="NAMALKP" id="NAMALKP"></td>
</tr>
<tr>
  <td>TGL. LAHIR</td>
<td><input type="text" name="TGLLAHIR" id="TGLLAHIR"></td>
</tr>
<tr>
  <td>JENIS KELAMN</td>
<td><input type="text" name="JK" id="JK"></td>
</tr>
<tr>
  <td>STATUS KAWIN</td>
<td><input type="text" name="STKAWIN" id="STKAWIN"></td>
</tr>
<tr>
  <td>ALAMAT</td>
<td><input type="text" name="ALAMAT" id="ALAMAT"></td>
</tr>
<tr>
  <td>RT</td>
<td><input type="text" name="RT" id="RT"></td>
</tr>
<tr>
  <td>RW</td>
<td><input type="text" name="RW" id="RW"></td>
</tr>
<tr>
  <td colspan="2" align="center" bgcolor="#CCCCCC">IDENTITAS WILAYAH</td>
</tr>
<tr>
  <td>KABUPATEN</td>
<td><input type="text" name="KAB" id="KAB"></td>
</tr>
<tr>
  <td>KECAMATAN</td>
<td><input type="text" name="KEC" id="KEC"></td>
</tr>
<tr>
  <td>DESA</td>
<td><input type="text" name="DESA" id="DESA"></td>
</tr>
<tr>
  <td>WILAYAH PUSKESMAS</td>
<td><input type="text" name="PUSKESMAS" id="PUSKESMAS"></td>
</tr>
<tr>
  <td>KODE DESA</td>
<td><input type="text" name="KDDESA" id="KDDESA"></td>
</tr>
<tr><td colspan="2">  <br>
<input type="file" name="upload_file" id="upload_file" size="50"/>
<input type="submit" name="upload" value="Upload JMKS SILVER" class='submit' />
<br><br>

</td></tr>
</table>
</form>
</td></tr>

</table>

</div></div>
</body>
</html>