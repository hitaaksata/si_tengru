<?php include("../include/connect.php");
include '../include/function.php';?>
<script>
jQuery(document).ready(function(){
	jQuery(".tab_content").hide(); //Hide all content
	jQuery("ul.tabs li:first").addClass("active").show(); //Activate first tab
	jQuery(".tab_content:first").show(); //Show first tab content
	//On Click Event
	jQuery("ul.tabs li").click(function() {
		jQuery("ul.tabs li").removeClass("active"); //Remove any "active" class
		jQuery(this).addClass("active"); //Add "active" class to selected tab
		jQuery(".tab_content").hide(); //Hide all tab content
		var activeTab = jQuery(this).find("span").attr("id"); //Find the rel attribute value to identify the active tab + content
		jQuery(activeTab).fadeIn(); //Fade in the active content
		return false;
	});
	
	jQuery('#simpan').click(function(){
		jQuery.post('<?php echo _BASE_;?>radiologi/save_order_rad.php',jQuery('#order_lab').serialize(),function(data){
			if(!data){
				window.location ='<?php echo _BASE_;?>index.php?link=7order';
			}
		});
	});
	
	jQuery('.checkbox_lab').click(function(){
		var loc = jQuery(this).is(':checked');
		if(loc == true){
			var kd_tindakan	= jQuery(this).attr('id');
			jQuery.post('<?php echo _BASE_;?>include/ajaxload.php',{kd_tindakan:kd_tindakan,tindakanlab:true},function(data){
				jQuery('#listordersss').empty().append(data);
			});
		}else{
			var kd_tindakan	= jQuery(this).attr('id');
			jQuery.post('<?php echo _BASE_;?>include/ajaxload.php',{kd_tindakan:kd_tindakan,rem_tindakanlab:true},function(data){
				jQuery('#listordersss').empty().append(data);
			});
		}
	});
	jQuery('.addtindakan').click(function(){
		var kd_tindakan	= jQuery(this).attr('id');
		jQuery.post('<?php echo _BASE_;?>include/ajaxload.php',{kd_tindakan:kd_tindakan,tindakanlab:true},function(data){
				jQuery('#listordersss').empty().html(data);
			});
	});
});
</script>
<style type="text/css">
ul.tabs {margin: 0;padding: 0;float: left;list-style: none;height: 32px;border-bottom: 1px solid #999;border-left: 1px solid #999;width: 100%;}
ul.tabs li {float: left;margin: 0;padding: 0 3px;height: 31px;line-height: 31px;border: 1px solid #999;border-left: none;margin-bottom: -1px;background: #e0e0e0;overflow: hidden;position: relative;}
ul.tabs li:hover{ background:#FF9; display:block; cursor:pointer;}
ul.tabs li a {text-decoration: none;color: #000;display: block;font-size: 10px;padding: 0 10px;border: 1px solid #fff;outline: none;}
ul.tabs li a:hover {background: #ccc;}	
html ul.tabs li.active, html ul.tabs li.active a:hover  {background: #fff;border-bottom: 1px solid #fff;}
.tab_container {border: 1px solid #999;	border-top: none;clear: both;float: left; width: 100%;background: #fff;	-moz-border-radius-bottomright: 5px;-khtml-border-radius-bottomright: 5px;	-webkit-border-bottom-right-radius: 5px;-moz-border-radius-bottomleft: 5px;	-khtml-border-radius-bottomleft: 5px;	-webkit-border-bottom-left-radius: 5px;}
.tab_content {padding: 5px;font-size: 11px; text-align:left;}
</style>

<div class="row">
  	<div class="col-sm-4">
    	<div class="card">
      		<div class="card-header">
        		<h5>DATA PASIEN RADIOLOGI</h5>
  			</div>
      		<div class="card-block">
        		<div class="row">
        			<div class="col-sm-12">
        				<?php
							mysql_query('DELETE FROM tmp_orderpenunjang where ip = "'.getRealIpAddr().'"');
							$sql = mysql_query('select a.NOMR, a.APS, a.TGLDAFTAR, c.nama_unit as POLY, a.DIAGNOSA,
									CASE aps WHEN 1 THEN (SELECT nama FROM m_pasien_aps b WHERE b.NOMR=a.NOMR) ELSE
									(SELECT nama FROM m_pasien b WHERE b.NOMR=a.NOMR) END AS nama,
									CASE aps WHEN 1 THEN (SELECT alamat FROM m_pasien_aps b WHERE b.NOMR=a.NOMR) ELSE
									(SELECT alamat FROM m_pasien b WHERE b.NOMR=a.NOMR) END AS alamat,
									CASE aps WHEN 1 THEN (SELECT jeniskelamin FROM m_pasien_aps b WHERE b.NOMR=a.NOMR) ELSE
									(SELECT jeniskelamin FROM m_pasien b WHERE b.NOMR=a.NOMR) END AS jeniskelamin,
									CASE aps WHEN 1 THEN (SELECT tgllahir FROM m_pasien_aps b WHERE b.NOMR=a.NOMR) ELSE
									(SELECT tgllahir FROM m_pasien b WHERE b.NOMR=a.NOMR) END AS tgllahir,
									CASE aps WHEN 1 THEN (SELECT kdcarabayar FROM m_pasien_aps b WHERE b.NOMR=a.NOMR) ELSE
									(SELECT kdcarabayar FROM m_pasien b WHERE b.NOMR=a.NOMR) END AS kdcarabayar
									from tmp_cartorderrad a join m_unit c on c.kode_unit = a.UNIT where a.NOMR = "'.$_REQUEST['nomr'].'" and a.IDXDAFTAR = "'.$_REQUEST['idx'].'"');
							$d	 = mysql_fetch_array($sql);
						?>
						<table width="100%" border="0" cellspacing="0" class="tb">
							<tr>
								<td width="100px">No RM</td>
								<td width="192"><?php echo $d['NOMR']; ?></td>
							</tr>
							<tr>
								<td>Nama</td>
								<td><?php echo $d['nama']; ?></td>
							</tr>
				            <tr>
				            	<td>Alamat</td>
				            	<td><?php echo $d['alamat']; ?></td>
				            </tr>
				            <tr>
				            	<td>Tanggal Lahir</td>
				            	<td><?php echo $d['tgllahir']; ?></td>
				            </tr>
				            <tr>
				            	<td>Umur</td>
				            	<td>
				            		<?php
				             			$a = datediff($d['tgllahir'],date('Y-m-d'));
						  				echo $a[years]." tahun ".$a[months]." bulan ".$a[days]." hari"; ?>
					  				</td>
					  			</tr>
				            <tr>
				            	<td>Jenis Kelamin</td>
				            	<td><?php echo jeniskelamin($d['jeniskelamin']); ?></td>
				            </tr>
							<tr>
								<td>Tanggal Daftar</td>
								<td><?php echo $d['TGLDAFTAR'];?></td>
							</tr>
				            <tr>
				            	<td>Poly</td>
				            	<td><?php echo $d['POLY'];?></td>
				            </tr>
				            <tr>
				            	<td>Diagnosa Klinik</td>
				            	<td><?php echo $d['DIAGNOSA'];?></td>
				            </tr>
			            </table>
			            <br>
			            <div id="listordersss"></div>
        			</div>
        		</div>
        	</div>
        </div>
    </div>
    <div class="col-sm-8">
    	<div class="card">
      		<div class="card-header">
        		<h5>FORM ORDER RADIOLOGI</h5>
  			</div>
      		<div class="card-block">
        		<div class="row">
        			<div class="col-sm-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                        	<?php
					            $sql=mysql_query('select * from m_tarif2012 where kode_unit= "17"');
					            $i = 1;
					            while($data = mysql_fetch_array($sql)){
					                if(strlen($data['kode_gruptindakan']) == 2):
					                    // echo '<li><span id="#'.$i.'">'.$data['nama_tindakan'].'</span></li>';
					                    echo '
					                    	<li class="nav-item">
				                                <a class="nav-link " data-toggle="tab" href="#'.$i.'" role="tab">'.$data['nama_tindakan'].'</a>
				                                <div class="slide"></div>
				                            </li>
					                    ';
					                    $i++;
					                endif;
					        }?>

                            <!-- <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home3" role="tab">Radiologi</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#profile3" role="tab">Elektromedis</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#messages3" role="tab">CT Scan, MRI</a>
                                <div class="slide"></div>
                            </li> -->
                        </ul>
                        <form id="order_lab">
				            <?php
								$poly_ruang = '';
								$nott	= '';
					            $sql_rajal	= mysql_query('select * from tmp_cartorderrad where NOMR = "'.$_REQUEST['nomr'].'" and IDXDAFTAR = "'.$_REQUEST['idx'].'"');
					            $ddaftar	= mysql_fetch_array($sql_rajal);
								if($ddaftar['RAJAL'] == 0){
								$sql = mysql_query('select * from t_admission where nomr = "'.$_REQUEST['nomr'].'" and id_admission = "'.$_REQUEST['idx'].'"');
								$dsql = mysql_fetch_array($sql);
									$poly_ruang = $dsql['noruang'];
									$nott	= $dsql['nott'];
							} ?>
				            <input type="hidden" name="ruang" value="<?php echo $poly_ruang; ?>" />
				            <input type="hidden" name="nott" value="<?php echo $nott; ?>" />
				            <input type="hidden" name="diagnosa" value="<?php echo $d['DIAGNOSA'];?>" />
				            <input type="hidden" name="tglorder" value="<?php echo $d['TGLDAFTAR']; ?>" />
				            <input type="hidden" name="aps" value="<?php echo $d['APS']; ?>" />
				            <input type="hidden" name="carabayar" value="<?php echo $ddaftar['CARABAYAR']; ?>" />
				            <input type="hidden" name="nomr" value="<?php echo $_REQUEST['nomr']; ?>" />
				            <input type="hidden" name="idxdaftar" value="<?php echo $_REQUEST['idx']; ?>" />
				            <input type="hidden" name="kddokter" value="0" />
				            <input type="hidden" name="unit" value="<?php echo $ddaftar['UNIT']; ?>" />
				            <!-- <div class="tab_container"> -->
				            <div class="tab-content card-block">
                        	<?php
								$sql2=mysql_query('select * from m_tarif2012 where kode_unit= "17"');
								$i = 1;
								while($datas = mysql_fetch_array($sql2)){	
									if(strlen($datas['kode_gruptindakan']) == 2):
        								// echo '<div id="'.$i.'" class="tab_content" style="height: 250px; overflow-x: hidden; overflow-y: scroll;">';
        								echo '
        									<div class="tab-pane" id="'.$i.'" role="tabpanel" style="height: 250px; overflow-x: hidden; overflow-y: scroll;">
        								';
										$sql_sub = mysql_query('select * from m_tarif2012 where kode_gruptindakan = "'.$datas['kode_tindakan'].'" order by nama_tindakan asc');
							 				if(mysql_num_rows($sql_sub) > 0){
								 				echo '<table style="width:100%;">';
								 				echo '<tr><th>No</th><th>Nama Tindakan</th><th>Tarif</th><th>Aksi</th></tr>';
								 				$x = 1;
								 				while($dsub = mysql_fetch_array($sql_sub)){
									 				echo '<tr><td>'.$x.'</td><td>'.$dsub['nama_tindakan'].'</td><td align="right">'.curformat($dsub['tarif']).'</td><td><input type="button" name="add" value="add" id="'.$dsub['kode_tindakan'].'" class="text addtindakan"></td></tr>';
													 /*
													 if($x == 5){
														 echo '</tr><tr>';
														 $x = 0;
													 }
													 $last = checkLastLevel($dsub['kode_tindakan']);
													 if($last > 0){
														$ll = getLastLevel($dsub['kode_tindakan']);
														while($dll = mysql_fetch_array($ll)){
															if($x == 5){
																 echo '</tr><tr>';
																 $x = 0;
															 }
															echo '<td style="width:20%;"><input type="checkbox" name="checkbox[]" value="'.$dll['kode_tindakan'].'" id="'.$dll['kode_tindakan'].'" class="checkbox_lab"> '.$dll['nama_tindakan'].'</td>';	 
															$x++;
														}
													 }else{
														echo '<td style="width:20%;"><input type="checkbox" name="checkbox[]" value="'.$dsub['kode_tindakan'].'" id="'.$dsub['kode_tindakan'].'" class="checkbox_lab"> '.$dsub['nama_tindakan'].'</td>';	 
														$x++;
													 }
													 */
									 				$x++;
								 				}
												#echo '</tr>';
												echo '</table>';
							 				}
										echo '</div>';
									$i++;
								endif;
							} ?>
							</div>
					            <br clear="all" />
					            <input type="button" name="simpan" value="S I M P A N" id="simpan" class="text" />
					            </form>
							</div>

                            <!-- Tab panes -->
                            <!-- <div class="tab-content card-block">
                                <div class="tab-pane active" id="home3" role="tabpanel">
                                    <p class="m-0">1. This is Photoshop's version of Lorem IpThis is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean mas Cum sociis natoque penatibus et magnis dis.....</p>
                                </div>
                                <div class="tab-pane" id="profile3" role="tabpanel">
                                    <p class="m-0">2.Cras consequat in enim ut efficitur. Nulla posuere elit quis auctor interdum praesent sit amet nulla vel enim amet. Donec convallis tellus neque, et imperdiet felis amet.</p>
                                </div>
                                <div class="tab-pane" id="messages3" role="tabpanel">
                                    <p class="m-0">3. This is Photoshop's version of Lorem IpThis is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean mas Cum sociis natoque penatibus et magnis dis.....</p>
                                </div>
                            </div> -->
        			</div>
        		</div>
        	</div>
        </div>
    </div>
</div>