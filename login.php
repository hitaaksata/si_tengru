<? include("include/connect.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= ucwords($rstitle)?></title>
      <!-- Meta -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="description" content="Gradient Able Bootstrap admin template made using Bootstrap 4 and it has huge amount of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
      <meta name="keywords" content="bootstrap, bootstrap admin template, admin theme, admin dashboard, dashboard template, admin template, responsive" />
      <meta name="author" content="Phoenixcoded" />
      <!-- Favicon icon -->
      <link rel="icon" href="img/log.png" type="image/x-icon">
      <!-- Google font-->     <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
      <!-- Required Fremwork -->
      <link rel="stylesheet" type="text/css" href="assets/files/bower_components/bootstrap/css/bootstrap.min.css">
      <!-- waves.css -->
      <link rel="stylesheet" href="assets/files/assets/pages/waves/css/waves.min.css" type="text/css" media="all">
      <!-- themify-icons line icon -->
      <link rel="stylesheet" type="text/css" href="assets/files/assets/icon/themify-icons/themify-icons.css">
      <!-- ico font -->
      <link rel="stylesheet" type="text/css" href="assets/files/assets/icon/icofont/css/icofont.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" type="text/css" href="assets/files/assets/icon/font-awesome/css/font-awesome.min.css">
      <!-- Style.css -->
      <link rel="stylesheet" type="text/css" href="assets/files/assets/css/style.css">
  </head>

  <body themebg-pattern="theme0">
  <!-- Pre-loader start -->
  <div class="theme-loader">
      <div class="loader-track">
          <div class="preloader-wrapper">
              <div class="spinner-layer spinner-blue">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>
              <div class="spinner-layer spinner-red">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>
            
              <div class="spinner-layer spinner-yellow">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>
            
              <div class="spinner-layer spinner-green">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- Pre-loader end -->
  <div id="pcoded" class="pcoded">
      
      <div class="pcoded-container">
        <nav class="navbar header-navbar pcoded-header" header-theme="theme1" pcoded-header-position="relative">
                    <div class="navbar-wrapper">
                        <div class="navbar-logo" style="width: auto;">
                           
                            <div class="mobile-search waves-effect waves-light">
                                <div class="header-search">
                                    <div class="main-search morphsearch-search">
                                        <div class="input-group">
                                            <span class="input-group-prepend search-close"><i class="ti-close input-group-text"></i></span>
                                            <input type="text" class="form-control" placeholder="Enter Keyword">
                                            <span class="input-group-append search-btn"><i class="ti-search input-group-text"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="index.html">
                                <img class="img-fluid" src="img/logo.png" alt="Theme-Logo">
                                <label style="font-size: 18px;"><b>Sistem Informasi Manajemen Rumah Sakit</b></label>
                            </a>
                            <a class="mobile-options waves-effect waves-light">
                                <i class="ti-more"></i>
                            </a>
                        </div>
                        
                       
                    </div>
                </nav>
       
          <!-- <nav class="navbar header-navbar pcoded-header" header-theme="theme1">
              <div class="navbar-wrapper">
                  <div class="navbar-logo">
                      <div class="mobile-search waves-effect waves-light">
                          <div class="header-search">
                              <div class="main-search morphsearch-search">
                                  <div class="input-group">
                                      <span class="input-group-addon search-close"><i class="ti-close"></i></span>
                                      <input type="text" class="form-control" placeholder="Enter Keyword">
                                      <span class="input-group-addon search-btn"><i class="ti-search"></i></span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <a href="index.html">
                          <img class="img-fluid" src="assets/files/assets/images/logo.png" alt="Theme-Logo" />
                      </a>
                  </div>
              </div>
          </nav> -->
      </div>
        <!-- Menu header end -->
        <section class="login-block">
            <!-- Container-fluid starts -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Authentication card start -->
                        <form class="md-float-material form-material m-t-40 m-b-40" id="frm" method="post" action="user_level.php">
                          <?php
                            if(isset($_POST['signin'])){
                              require_once("login.php");
                            } 
                          ?>
                            <div class="auth-box card">
                                <div class="card-block">
                                    <div class="row m-b-20">
                                        <div class="col-md-12">
                                            <h3 class="text-center">LOGIN FORM</h3>
                                        </div>
                                    </div>
                                    <div class="form-group form-primary">
                                        <input class="form-control text" id="NIP" type="text" size="25" name="USERNAME" onBlur="javascript: MyAjaxRequest('valid_nip','include/process.php?NIP=','NIP');return false;" />
                                        <span id="valid_nip"></span>
                                        <label class="float-label">Your Email Address</label>
                                    </div>
                                    <div class="form-group form-primary">
                                        <input type="password" id="PWD" size="25" name="PWD" class="form-control" required="">
                                        <span class="form-bar"></span>
                                        <label class="float-label">Password</label>
                                    </div>
                                    <div class="row m-t-30">
                                        <div class="col-md-12">
                                            <input type="button" value=" LOGIN " class=" btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20 "  name="LOGIN" id="LOGIN" onclick="document.getElementById('frm').submit();"/><span id="valid_pwd"></span>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-inverse text-center m-b-0"><?=strtoupper($rstitle)?> <?=strtoupper($singhead1)?> &copy; <?php echo date("Y"); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- Authentication card end -->
                    </div>
                    <!-- end of col-sm-12 -->
                </div>
                <!-- end of row -->
            </div>
            <!-- end of container-fluid -->

        </section>
    </div>


    <div class="footer">
        <p class="text-center m-b-0"><?= ucwords($rstitle)?> &copy; <?php echo date("Y"); ?></p>
    </div>
  <!-- Warning Section Ends -->
  <!-- Required Jquery -->
  <script type="text/javascript" src="assets/files/bower_components/jquery/js/jquery.min.js"></script>
  <script type="text/javascript" src="assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
  <script type="text/javascript" src="assets/files/bower_components/popper.js/js/popper.min.js"></script>
  <script type="text/javascript" src="assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
  <!-- waves js -->
  <script src="assets/files/assets/pages/waves/js/waves.min.js"></script>
  <!-- jquery slimscroll js -->
  <script type="text/javascript" src="assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
  <!-- modernizr js -->
  <script type="text/javascript" src="assets/files/bower_components/modernizr/js/modernizr.js"></script>
  <script type="text/javascript" src="assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
  <!-- Custom js -->
  <script type="text/javascript" src="assets/files/assets/js/common-pages.js"></script>

  <script type="text/javascript" language="javascript" src="include/ajaxrequest.js"></script>
  <script type="text/javascript">
    function jumpTo (link){
      var new_url=link;
      if ((new_url != "")  &&  (new_url != null))
      window.location=new_url;
    }

    // jQuery(document).ready(function(){
      jQuery("#NIP").keyup(function(event){
        if(event.keyCode == 13){
          MyAjaxRequest('valid_nip','include/process.php?NIP=','NIP');
          jQuery("#PWD").focus();
        }
      });
      
      jQuery("#PWD").keyup(function(event){   
        if(event.keyCode == 13){      
          MyAjaxRequest('valid_pwd','include/process.php?PWD=','PWD');
          jQuery('#frm').submit();
        }
      });
    // });
  </script>
</body>

</html>
