<? session_start();
if(!isset($_SESSION['SES_REG'])){
    header("location:login.php");
}
if($_SESSION['ROLES']=="3") { ?>
<link href="../css/dropdown/dropdown.css" media="all" rel="stylesheet" type="text/css" />
<link href="../css/dropdown/themes/default/default.css" media="all" rel="stylesheet" type="text/css" />
    <? }else { ?>
<link href="css/dropdown/dropdown.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/dropdown/themes/default/default.css" media="all" rel="stylesheet" type="text/css" />
    <? } ?>
<? if($_SESSION['ROLES']=="1017") { ?>

<ul class="pcoded-item pcoded-left-item" style="width: 100%">
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">MASTER</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=add_user" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">ADD USER</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=private" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-users"></i></span>
                    <span class="pcoded-mtext">LIST USER</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=191" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">EDIT ICD</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=19" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">LIST ICD</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=jdoc2" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">ADD JADWAL</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=jdoc3" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">LIST JADWAL</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="1") { ?>
<ul class="pcoded-item pcoded-left-item" style="width: 100%">
    <li class="pcoded-hasmenu">
        <a href="index.php?link=2" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">PENDAFTARAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=telepon" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENDAFTARAN MELALUI TELEPON</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=2bayi" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENDAFTARAN BAYI BARU LAHIR</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=21" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-user"></i></span>
            <span class="pcoded-mtext">LIST DATA PASIEN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=22" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">LIST KUNJUNGAN PASIEN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=14_" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">ASURANSI</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-bookmark-alt"></i></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=140" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP PENDAFTARAN PASIEN RAWAT JALAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="2") { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="index.php?link=33" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">BILL RAJAL</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=billaps" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">BILL APS</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=37" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">DEPOSIT RANAP</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=33a" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">BILL RANAP</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=33depo_rajal" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">DEPO RAJAL</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=33gizi_rajal" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">GIZI RAJAL</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-bookmark-alt"></i></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=31" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">RAWAT JALAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=35" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">RAWAT INAP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>
    <? }elseif($_SESSION['ROLES']=="4") { ?>

<ul class="pcoded-item pcoded-left-item" style="width: 100%;">
    <li class="pcoded-hasmenu">
        <a href="index.php?link=5&page=<?=$_SESSION['page']?>&tgl_reg=<?=$_SESSION['tgl_reg']?>&tgl_reg2=<?=$_SESSION['tgl_reg2']?>&nama=<?=$_SESSION['nama']?>&norm=<?=$_SESSION['norm']?>" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">LIST KUNJUNGAN PASIEN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <? if($_SESSION['KDUNIT']=="10") { ?>

    <li class="pcoded-hasmenu">
        <a href="index.php?link=5ranap" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">LIST PASIEN RAWAT INAP VK</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=v01" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">REGISTRASI PARTUS</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=139" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">SENSUS HARIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=jas9" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">JASPEL</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    

    <? }elseif($_SESSION['KDUNIT']=="9") { ?>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=54" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">SENSUS HARIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <? }else { ?>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=54" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">SENSUS HARIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=jas1" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">JASPEL</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <? } ?>

    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">FARMASI & LOGISTIK</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f04" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENGELUARAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f06" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PENGELUARAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f01" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PERMINTAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f02" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PERMINTAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f21" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENGEMBALIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f22" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PENGEMBALIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f07" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PERENCANAAN PENGADAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f08" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PRENCANAAN PENGADAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f09" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LAPORAN BULANAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f11" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LAPORAN HARIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f66" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LAPORAN STOK</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">MASTER</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=19" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">ICD</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="5") { ?>

<ul class="pcoded-item pcoded-left-item" style="width: 100%">
    <li class="pcoded-hasmenu">
        <a href="index.php?link=6" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">LIST ORDER LAB</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=6order" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">LIST PEMERIKSAAN LAB</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=l01" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">DAFTAR APS</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=list_pasien_ranap_lab" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">DAFTAR PASIEN RANAP</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=list_pasien_rajal_lab" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">DAFTAR PASIEN RAJAL</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=sisipan_lab" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">SISIPAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-bookmark-alt"></i></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=l05" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REGISTER PELAYANAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=jas5" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">JASPEL</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="6") { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-list"></i><b>N</b></span>
            <span class="pcoded-mtext">LIST</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=7" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-list"></i></span>
                    <span class="pcoded-mtext">LIST PEMERIKSAAN RADIOLOGI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=7order" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-list"></i></span>
                    <span class="pcoded-mtext">LIST PEMERIKSAAN RADIOLOGI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=71" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">HASIL PEMERIKSAAN RADIOLOGI</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">DAFTAR</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=r01" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
                    <span class="pcoded-mtext">APS</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=list_pasien_ranap_rad" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
                    <span class="pcoded-mtext">PASIEN RANAP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=list_pasien_rajal_rad" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
                    <span class="pcoded-mtext">PASIEN RAJAL</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-bookmark-alt"></i></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=74" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REGISTRASI PELAYANAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=jas6" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">JASPEL</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>


    <? }elseif($_SESSION['ROLES']=="7") { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">PERMINTAAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=8" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PERMINTAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=85" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">HISTORI PERMINTAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">PENERIMAAN BARANG</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=83" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">FORM PENERIMAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=x83" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">HISTORI PENERIMAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    <li class="pcoded-hasmenu">
        <a href="?link=89" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">PERENCANAAN PENGADAAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=82" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">MASTER BARANG</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=84&lap=2" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">BULANAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=84&lap=3" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP BULANAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=84&lap=4" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP TRIWULAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=84&lap=5" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP TAHUNAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=84&lap=6" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">STOK UNIT</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="8") { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">PERMINTAAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=8" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PERMINTAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=85" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">HISTORI PERMINTAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">PENERIMAAN BARANG</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=83" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">FORM PENERIMAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=x83" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">HISTORI PENERIMAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=x83" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">PENGEMBALIAN BARANG</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=89" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">PERENCANAAN PENGADAAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=82" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">MASTER BARANG</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=84&lap=1" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">HARIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=84&lap=2" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">BULANAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=84&lap=3" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP BULANAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=84&lap=4" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP TRIWULAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=84&lap=5" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP TAHUNAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=84&lap=6" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">STOK UNIT</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="9") { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="?link=list_pasien_apotek_rajal" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">LIST PASIEN RAJAL</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>   
    <li class="pcoded-hasmenu">
        <a href="?link=list_pasien_apotek_ranap" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">LIST PASIEN RANAP</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=list_pasien_apotek_aps" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">LIST PASIEN APS</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=list_obat_rajal" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENGELUARAN OBAT RAJAL</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=list_obat_ranap" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENGELUARAN OBAT RANAP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=list_obat_aps" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENGELUARAN OBAT APS</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=114" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
                    <span class="pcoded-mtext">REKAP RESEP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="#" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
                    <span class="pcoded-mtext">LAPORAN PEMANTAUAN RESEP OBAT GENERIK</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="pcoded-hasmenu">
                        <a href="?link=110x"  class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-home"></i></span>
                            <span class="pcoded-mtext">RAWAT JALAN</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=110xt" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-home"></i></span>
                            <span class="pcoded-mtext">RAWAT INAP</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">FARMASI & LOGISTIK</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f04"  class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENGELUARAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f06" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PENGELUARAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f01" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PERMINTAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f02" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
                    <span class="pcoded-mtext">LIST PERMINTAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f21" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENGEMBALIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f22" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PENGEMBALIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f07" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PERENCANAAN PENGADAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f08" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
                    <span class="pcoded-mtext">LIST PERENCANAAN PENGADAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f09" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LAPORAN BULANAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f11" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LAPORAN HARIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f66" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
                    <span class="pcoded-mtext">LAPORAN STOK</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="10") { ?>
        
<ul class="pcoded-item pcoded-left-item" style="width: 100%">
    <li class="pcoded-hasmenu">
        <a href="?link=11" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">LIST KUNJUNGAN PASIEN UGD</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=111" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">PEMERIKSAAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="11") { ?>

<ul class="pcoded-item pcoded-left-item" style="width: 100%">
    <li class="pcoded-hasmenu">
        <a href="?link=12" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">LIST PASIEN RAWAT INAP</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=129x" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">PERM. MAKAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=124" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">DATA KAMAR</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=125" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">PENCARIAN PASIEN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=122harianpasienkeluar" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PASIEN KELUAR</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=122" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">SENSUS HARIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=122x" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">BUKU REGISTER</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=jas4" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">JASPEL</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">FARMASI & LOGISTIK</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f04" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENGELUARAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f06" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PENGELUARAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f01" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PERMINTAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f02" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PERMINTAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f21" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENGEMBALIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f22" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PENGEMBALIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
             <li class="pcoded-hasmenu">
                <a href="index.php?link=f07" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PERENCANAAN PENGADAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f08" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PRENCANAAN PENGADAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f09" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LAPORAN BULANAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f11" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LAPORAN HARIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f66" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LAPORAN STOK</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">MASTER</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=19" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">ICD</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="12") { ?>

<ul class="pcoded-item pcoded-left-item" style="width: 100%">
    <li class="pcoded-hasmenu">
        <a href="?link=13" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">TRACER</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN INTERNAL</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=133" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">SENSUS HARIAN RAWAT JALAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=140" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">SENSUS PENDAFTARAN RAWAT JALAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=134" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">SENSUS HARIAN RAWAT INAP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=135" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">LIST PASIEN RAWAT INAP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=138" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">LAPORAN HARIAN RAWAT INAP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=139" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">LAPORAN HARIAN VK</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=lapok" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">LAPORAN HARIAN KAMAR OPERASI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=1311" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">SENSUS LAB</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=1316" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">SENSUS HARIAN UGD</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=1313" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">SENSUS RADIOLOGI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">REKAPAN INTERNAL</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=140R" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">REKAP PENDAFTARAN RAWAT JALAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=144R" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">REKAP STATUS PULANG RAWAT JALAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=141R" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">REKAP POLIKLINIK RAWAT JALAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=142R" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">REKAP PENDAFTARAN RAWAT INAP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">RIWAYAT PASIEN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=rm4" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">RAWAT JALAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=rm5" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">RAWAT INAP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=private21" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">GRAFIK KUNJUNGAN PASIEN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=jas1" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">JASPEL</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=iso2" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">ISO PENDAFTARAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=pasienrujukan" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">PASIEN RUJUKAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN RL</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">RL 1</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl11" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 1.1 Data Dasar</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl12" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 1.2 Indikator Pelayanan</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl13" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 1.3 Tempat Tidur</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=rl2" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">RL 2 Ketenagaan</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">RL 3</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl31" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.1 Rawat Inap</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl32" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.2 Rawat Darurat</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl33" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.3 Gigi Mulut</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl34" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.4 Kebidanan</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl35" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.5 Perinatologi</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl36" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.6 Pembedahan</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl37" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.7 Radiologi</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl38" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.8 Laboratorium</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl39" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.9 Rehab Medik</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl310" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.10 Pelayanan Khusus</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl311" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.11 Kesehatan Jiwa</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl312" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.12 Keluarga Berencana</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl313" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.13 Obat</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl314" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.14 Rujukan</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=rl315" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 3.15 Cara Bayar</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">RL 4</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="pcoded-hasmenu">
                        <a href="?link=RL2A" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 4A Penyakit Rawat Inap</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu">
                        <a href="?link=RL2B" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 4B Penyakit Rawat Jalan</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">RL 5</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="?link=rl51" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 5.1 Pengunjung</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="">
                        <a href="?link=rl52" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 5.2 Kunj. Rawat Jalan</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="">
                        <a href="?link=rl5310" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 5.3 10 Penyakit Rawat Inap</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="">
                        <a href="?link=rl5410" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-user"></i></span>
                            <span class="pcoded-mtext">RL 5.4 10 Penyakit Rawat Jalan</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="javascript:void(0)" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">MASTER</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=19" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                    <span class="pcoded-mtext">ICD</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="13") { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="?link=14_" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">VERIFIKASI</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=14_askes" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">DATA ASKES</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=33a_asuransi" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">BILLING RANAP</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=33_asuransi" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">BILLING RAJAL</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=144_rekap" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAPITULASI KLAIM ASURANSI RAJAL</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=144_rekap_ranap" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAPITULASI KLAIM ASURANSI RANAP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=140" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP PENDAFTARAN RAWAT JALAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=142R" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP PENDAFTARAN RAWAT INAP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=14h" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">HISTORI PASIEN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">TOOL</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENCARIAN DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="15") { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="?link=16" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">DATA DPMP</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=161" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP DPMP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENCARIAN DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="16") { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">RAWAT JALAN, UGD & VK</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a a href="?link=private21" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP KUNJUNGAN PASIEN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=private22" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP KUNJUNGAN PER KUNJUNGAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=private23" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP KUNJUNGAN PER CARA BAYAR</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=private24" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP 10 PENYAKIT TERBANYAK</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=private25" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP PENDAPATAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=private27" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP PENDAPATAN PER CARABAYAR</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">RAWAT INAP</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=private26" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP PASIEN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=private26_crbyr" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP PASIEN PER CARA BAYAR</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LABORATORIUM</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=privatelab1" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP CARA BAYAR</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">RADIOLOGI</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=privaterad1" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP CARA BAYAR</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">KAMAR OPERASI</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=privatekam1" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">REKAP CARA BAYAR</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=privategizi1" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">GIZI</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=privateapotek1" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">APOTEK</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=private27All" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">TOTAL SEMUA PENDAPATAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="17") { ?>
	<script>
	function popUpWARNING(URL) {
		//day = new Date();
		//id = day.getTime();
		id	= 'popcuy';
		eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1000,height=400,left=50,top=50');");
	}
	jQuery(document).ready(function(){
								
		var auto_refresh = setInterval(function (){
			//popUpWARNING('daftar_pasien_harus_dipulangkan.php');
			jQuery.get('daftar_pasien_harus_dipulangkan.php',function(data){
				if(data){
					popUpWARNING('daftar_pasien_harus_dipulangkan.php');
				}
			});
		}, <?php echo _POPUPTIME_;?>);
	});
	</script>
<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="?link=17a" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">DAFTAR RAWAT INAP</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=171" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">LIST PASIEN RAWAT INAP</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=17f" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">LIST PASIEN RAWAT JALAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=173" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">DATA KAMAR</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=138" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">SENSUS RAWAT INAP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=122x" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">BUKU REGISTER RAWAT INAP</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENCARIAN DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=list_billing_ranap" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-left"></i></span>
            <span class="pcoded-mtext">LIST BILLING RANAP</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=setting_dokter" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">SETTING DOKTER JAGA</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=praktek_dokter" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PRAKTEK DOKTER</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                    <?php #echo date('Y/m/d H:i:s',mktime(date('H')+6,date('i'),date('s'),date('m'),date('d'),date('Y')));?>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="19") { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="?link=20" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">LIST OPERASI</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="?link=205" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">LIST RENCANA OPERASI</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=lapok" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">Pasien OK</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENCARIAN DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">FARMASI & LOGISTIK</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f04" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENGELUARAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f06" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PENGELUARAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f01" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PERMINTAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f02" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PERMINTAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f21" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENGEMBALIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f22" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PENGEMBALIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
             <li class="pcoded-hasmenu">
                <a href="index.php?link=f07" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PERENCANAAN PENGADAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f08" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LIST PRENCANAAN PENGADAAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f09" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LAPORAN BULANAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f11" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LAPORAN HARIAN</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="index.php?link=f66" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">LAPORAN STOK</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="22") { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="?link=As01" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">LIST PASIEN ASKES</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=2f" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">PENCARIAN DATA ASURANSI</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="23") { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">SETUP</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=general_ledger" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">General Ledger</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">LAPORAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=laporan_hutang" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">Laporan Hutang</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=pendapatan_piutang" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">Laporan Piutang</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=general_ledger" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">General Ledger</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">PENDAPATAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=private27All" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">Rekap Pendapatan Per Unit</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=36k2" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">Rekap Pendapatan Per Carabayar</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>

    <? }elseif($_SESSION['ROLES']=="24") { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="index.php?link=jas0" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">SETTING JASPEL</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=jas1" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">RAWAT JALAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=jas2" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">KAMAR OPERASI</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=jas4" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">RAWAT INAP</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=jas5" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">LABORATORIUM</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=jas6" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">RADIOLOGI</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=jas10" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">REKAP JASPEL ALL</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
</ul>

<? } elseif($_SESSION['ROLES'] == '26'){ ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="index.php?link=adminrajal" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">DATA PASIEN RAWAT JALAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=adminrajal_aps" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">DATA PASIEN APS</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="index.php?link=adminrajal_daftar_aps" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">PENDAFTARAN PASIEN APS</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
</ul>

    <? } elseif($_SESSION['ROLES'] == '27'){ ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="index.php?link=list_kep" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">DATA PERAWAT</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">ASUHAN KEPERAWATAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=askep__" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">Pengkajian Keperawatan & Diagnosa Keperawatan</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-notepad"></i><b>N</b></span>
            <span class="pcoded-mtext">MANAJEMEN PELAYANAN KEPERAWATAN</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="pcoded-hasmenu">
                <a href="?link=sdm_kep" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">SDM Keperawatan</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=met_gas" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">Metode Penugasan</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=supvis" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">Supervisi</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="?link=lap_ranap_kep" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i></span>
                    <span class="pcoded-mtext">Laporan Rawat Inap</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
</ul>
    <? } else { ?>

<ul class="pcoded-item pcoded-left-item">
    <li class="pcoded-hasmenu">
        <a href="#" class="waves-effect waves-dark">
            <span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
            <span class="pcoded-mtext">NO MENU FOUND.</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
</ul>
    <? } ?>



